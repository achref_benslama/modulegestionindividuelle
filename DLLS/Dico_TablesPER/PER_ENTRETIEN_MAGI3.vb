﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_MAGI3
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsSiUtilisationDIF As String
        Private WsParcours_AvisEvaluateur As String
        Private WsAnneeEcoulee_Objectifs As String
        Private WsAnneeEcoulee_Evenements As String
        Private WsAnneeEcoulee_BilanRealisations As String
        Private WsAnneeEcoulee_BilanDifficultes As String
        Private WsAnneeEcoulee_ObservationsEvalue As String
        Private WsAnneeEcoulee_AvisMIJA As String
        Private WsAnneeEcoulee_Objectifs_Juridiction As String
        Private WsAnneeEcoulee_Evenements_Juridiction As String
        Private WsAnneeEcoulee_BilanRealisations_Juridiction As String
        Private WsAnneeEcoulee_BilanDifficultes_Juridiction As String
        Private WsAnneeEcoulee_AvisMIJA_Juridiction As String
        Private WsAnneeAVenir_Objectifs As String
        Private WsAnneeAVenir_Demarche As String
        Private WsAnneeAVenir_ObservationsEvalue As String
        Private WsAnneeAVenir_Objectifs_Juridiction As String
        Private WsAnneeAVenir_Demarche_Juridiction As String
        Private WsFormationsSuivies As String
        Private WsParcours_AvisEvalue As String
        Private WsNbJoursDIF As Single
        Private WsFormation_Voeux As String
        Private WsFormation_AvisEvaluateur As String
        Private WsFormationsAssurees As String
        Private WsSiObligationResidence As String
        Private WsObsEtudesPublications As String
        Private WsObsAptitudesGenerales As String
        Private WsObsAptitudesProfessionnelles As String
        Private WsObsManiereServir As String
        Private WsObsEncadrement As String
        Private WsSiBeneficeDerogation As String
        Private WsLibCritere35 As String
        Private WsNoteCritere35 As Single
        Private WsVoeuxMobilite As String
        Private WsSiSouhaitEntretienCMC As String
        '
        Private TsTypeFormulaire As String

        Public ReadOnly Property NomClient() As String
            Get
                Return "Conseil d'Etat"
            End Get
        End Property

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_TER"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 89
            End Get
        End Property

        Public Property SiUtilisationduDIF() As String
            Get
                Return WsSiUtilisationDIF
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiUtilisationDIF = value
                    Case Else
                        WsSiUtilisationDIF = Strings.Left(value, 3)
                End Select
                If WsSiUtilisationDIF = "" Or WsSiUtilisationDIF = "non" Then
                    WsSiUtilisationDIF = "Non"
                End If
            End Set
        End Property

        Public Property Parcours_AvisEvaluateur() As String
            Get
                Return WsParcours_AvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsParcours_AvisEvaluateur = value
                    Case Else
                        WsParcours_AvisEvaluateur = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Objectifs() As String
            Get
                Return WsAnneeEcoulee_Objectifs
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_Objectifs = value
                    Case Else
                        WsAnneeEcoulee_Objectifs = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Evenements() As String
            Get
                Return WsAnneeEcoulee_Evenements
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_Evenements = value
                    Case Else
                        WsAnneeEcoulee_Evenements = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_BilanRealisations() As String
            Get
                Return WsAnneeEcoulee_BilanRealisations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_BilanRealisations = value
                    Case Else
                        WsAnneeEcoulee_BilanRealisations = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_BilanDifficultes() As String
            Get
                Return WsAnneeEcoulee_BilanDifficultes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_BilanDifficultes = value
                    Case Else
                        WsAnneeEcoulee_BilanDifficultes = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Objectifs_ObservationsEvalue() As String
            Get
                Return WsAnneeEcoulee_ObservationsEvalue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_ObservationsEvalue = value
                    Case Else
                        WsAnneeEcoulee_ObservationsEvalue = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Objectifs_AvisMIJA() As String
            Get
                Return WsAnneeEcoulee_AvisMIJA
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_AvisMIJA = value
                    Case Else
                        WsAnneeEcoulee_AvisMIJA = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Juridiction_Objectifs() As String
            Get
                Return WsAnneeEcoulee_Objectifs_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_Objectifs_Juridiction = value
                    Case Else
                        WsAnneeEcoulee_Objectifs_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Juridiction_Evenements() As String
            Get
                Return WsAnneeEcoulee_Evenements_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_Evenements_Juridiction = value
                    Case Else
                        WsAnneeEcoulee_Evenements_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Juridiction_BilanRealisations() As String
            Get
                Return WsAnneeEcoulee_BilanRealisations_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_BilanRealisations_Juridiction = value
                    Case Else
                        WsAnneeEcoulee_BilanRealisations_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Juridiction_BilanDifficultes() As String
            Get
                Return WsAnneeEcoulee_BilanDifficultes_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_BilanDifficultes_Juridiction = value
                    Case Else
                        WsAnneeEcoulee_BilanDifficultes_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeEcoulee_Juridiction_AvisMIJA() As String
            Get
                Return WsAnneeEcoulee_AvisMIJA_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeEcoulee_AvisMIJA_Juridiction = value
                    Case Else
                        WsAnneeEcoulee_AvisMIJA_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeAVenir_Objectifs() As String
            Get
                Return WsAnneeAVenir_Objectifs
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeAVenir_Objectifs = value
                    Case Else
                        WsAnneeAVenir_Objectifs = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeAVenir_Demarche() As String
            Get
                Return WsAnneeAVenir_Demarche
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeAVenir_Demarche = value
                    Case Else
                        WsAnneeAVenir_Demarche = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeAVenir_Objectifs_ObservationsEvalue() As String
            Get
                Return WsAnneeAVenir_ObservationsEvalue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeAVenir_ObservationsEvalue = value
                    Case Else
                        WsAnneeAVenir_ObservationsEvalue = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeAVenir_Juridiction_Objectifs() As String
            Get
                Return WsAnneeAVenir_Objectifs_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeAVenir_Objectifs_Juridiction = value
                    Case Else
                        WsAnneeAVenir_Objectifs_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AnneeAVenir_Juridiction_Demarche() As String
            Get
                Return WsAnneeAVenir_Demarche_Juridiction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAnneeAVenir_Demarche_Juridiction = value
                    Case Else
                        WsAnneeAVenir_Demarche_Juridiction = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property FormationsSuivies() As String
            Get
                Return WsFormationsSuivies
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFormationsSuivies = value
                    Case Else
                        WsFormationsSuivies = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Parcours_AvisEvalue() As String
            Get
                Return WsParcours_AvisEvalue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsParcours_AvisEvalue = value
                    Case Else
                        WsParcours_AvisEvalue = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Nombre_Jours_DIF() As Single
            Get
                Return WsNbJoursDIF
            End Get
            Set(ByVal value As Single)
                WsNbJoursDIF = value
            End Set
        End Property

        Public Property Formations_Voeux() As String
            Get
                Return WsFormation_Voeux
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFormation_Voeux = value
                    Case Else
                        WsFormation_Voeux = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Formations_AvisEvaluateur() As String
            Get
                Return WsFormation_AvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFormation_AvisEvaluateur = value
                    Case Else
                        WsFormation_AvisEvaluateur = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property FormationsAssurees() As String
            Get
                Return WsFormationsAssurees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFormationsAssurees = value
                    Case Else
                        WsFormationsAssurees = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property SiObligationdeResidence() As String
            Get
                Return WsSiObligationResidence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiObligationResidence = value
                    Case Else
                        WsSiObligationResidence = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Observations_Etudes_Publications() As String
            Get
                Return WsObsEtudesPublications
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObsEtudesPublications = value
                    Case Else
                        WsObsEtudesPublications = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Observations_Aptitudes_Generales() As String
            Get
                Return WsObsAptitudesGenerales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObsAptitudesGenerales = value
                    Case Else
                        WsObsAptitudesGenerales = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Observations_Aptitudes_Profession() As String
            Get
                Return WsObsAptitudesProfessionnelles
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObsAptitudesProfessionnelles = value
                    Case Else
                        WsObsAptitudesProfessionnelles = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Observations_Maniere_Servir() As String
            Get
                Return WsObsManiereServir
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObsManiereServir = value
                    Case Else
                        WsObsManiereServir = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Observations_Encadrement() As String
            Get
                Return WsObsEncadrement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObsEncadrement = value
                    Case Else
                        WsObsEncadrement = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property SiBeneficeDerogation() As String
            Get
                Return WsSiBeneficeDerogation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiBeneficeDerogation = value
                    Case Else
                        WsSiBeneficeDerogation = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Libelle_Critere_35() As String
            Get
                Return V_InfoCritere(32)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsLibCritere35 = value
                    Case Else
                        WsLibCritere35 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Note_Critere_35() As Single
            Get
                Return WsNoteCritere35
            End Get
            Set(ByVal value As Single)
                WsNoteCritere35 = value
            End Set
        End Property

        Public Property Voeux_Mobilite() As String
            Get
                Return WsVoeuxMobilite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsVoeuxMobilite = value
                    Case Else
                        WsVoeuxMobilite = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property SiSouhait_EntretienConseiller() As String
            Get
                Return WsSiSouhaitEntretienCMC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiSouhaitEntretienCMC = value
                    Case Else
                        WsSiSouhaitEntretienCMC = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(SiUtilisationduDIF & VI.Tild)
                Chaine.Append(Parcours_AvisEvaluateur & VI.Tild)
                Chaine.Append(AnneeEcoulee_Objectifs & VI.Tild)
                Chaine.Append(AnneeEcoulee_Evenements & VI.Tild)
                Chaine.Append(AnneeEcoulee_BilanRealisations & VI.Tild)
                Chaine.Append(AnneeEcoulee_BilanDifficultes & VI.Tild)
                Chaine.Append(AnneeEcoulee_Objectifs_ObservationsEvalue & VI.Tild)
                Chaine.Append(AnneeEcoulee_Objectifs_AvisMIJA & VI.Tild)
                Chaine.Append(AnneeEcoulee_Juridiction_Objectifs & VI.Tild)
                Chaine.Append(AnneeEcoulee_Juridiction_Evenements & VI.Tild)
                Chaine.Append(AnneeEcoulee_Juridiction_BilanRealisations & VI.Tild)
                Chaine.Append(AnneeEcoulee_Juridiction_BilanDifficultes & VI.Tild)
                Chaine.Append(AnneeEcoulee_Juridiction_AvisMIJA & VI.Tild)
                Chaine.Append(AnneeAVenir_Objectifs & VI.Tild)
                Chaine.Append(AnneeAVenir_Demarche & VI.Tild)
                Chaine.Append(AnneeAVenir_Objectifs_ObservationsEvalue & VI.Tild)
                Chaine.Append(AnneeAVenir_Juridiction_Objectifs & VI.Tild)
                Chaine.Append(AnneeAVenir_Juridiction_Demarche & VI.Tild)
                Chaine.Append(FormationsSuivies & VI.Tild)
                Chaine.Append(Parcours_AvisEvalue & VI.Tild)
                Chaine.Append(Nombre_Jours_DIF.ToString & VI.Tild)
                Chaine.Append(Formations_Voeux & VI.Tild)
                Chaine.Append(Formations_AvisEvaluateur & VI.Tild)
                Chaine.Append(FormationsAssurees & VI.Tild)
                Chaine.Append(SiObligationdeResidence & VI.Tild)
                Chaine.Append(Observations_Etudes_Publications & VI.Tild)
                Chaine.Append(Observations_Aptitudes_Generales & VI.Tild)
                Chaine.Append(Observations_Aptitudes_Profession & VI.Tild)
                Chaine.Append(Observations_Maniere_Servir & VI.Tild)
                Chaine.Append(Observations_Encadrement & VI.Tild)
                Chaine.Append(SiBeneficeDerogation & VI.Tild)
                Chaine.Append(Libelle_Critere_35 & VI.Tild)
                Chaine.Append(Note_Critere_35.ToString & VI.Tild)
                Chaine.Append(Voeux_Mobilite & VI.Tild)
                Chaine.Append(SiSouhait_EntretienConseiller)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 37 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                SiUtilisationduDIF = TableauData(2)
                Parcours_AvisEvaluateur = TableauData(3)
                AnneeEcoulee_Objectifs = TableauData(4)
                AnneeEcoulee_Evenements = TableauData(5)
                AnneeEcoulee_BilanRealisations = TableauData(6)
                AnneeEcoulee_BilanDifficultes = TableauData(7)
                AnneeEcoulee_Objectifs_ObservationsEvalue = TableauData(8)
                AnneeEcoulee_Objectifs_AvisMIJA = TableauData(9)
                AnneeEcoulee_Juridiction_Objectifs = TableauData(10)
                AnneeEcoulee_Juridiction_Evenements = TableauData(11)
                AnneeEcoulee_Juridiction_BilanRealisations = TableauData(12)
                AnneeEcoulee_Juridiction_BilanDifficultes = TableauData(13)
                AnneeEcoulee_Juridiction_AvisMIJA = TableauData(14)
                AnneeAVenir_Objectifs = TableauData(15)
                AnneeAVenir_Demarche = TableauData(16)
                AnneeAVenir_Objectifs_ObservationsEvalue = TableauData(17)
                AnneeAVenir_Juridiction_Objectifs = TableauData(18)
                AnneeAVenir_Juridiction_Demarche = TableauData(19)
                FormationsSuivies = TableauData(20)
                Parcours_AvisEvalue = TableauData(21)
                If TableauData(22) <> "" Then Nombre_Jours_DIF = CSng(VirRhFonction.ConversionDouble(TableauData(22)))
                Formations_Voeux = TableauData(23)
                Formations_AvisEvaluateur = TableauData(24)
                FormationsAssurees = TableauData(25)
                SiObligationdeResidence = TableauData(26)
                Observations_Etudes_Publications = TableauData(27)
                Observations_Aptitudes_Generales = TableauData(28)
                Observations_Aptitudes_Profession = TableauData(29)
                Observations_Maniere_Servir = TableauData(30)
                Observations_Encadrement = TableauData(31)
                SiBeneficeDerogation = TableauData(32)
                Libelle_Critere_35 = TableauData(33)
                If TableauData(34) <> "" Then
                    Note_Critere_35 = CSng(VirRhFonction.ConversionDouble(TableauData(34)))
                End If
                Voeux_Mobilite = TableauData(35)
                SiSouhait_EntretienConseiller = TableauData(36)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property V_Type_Formulaire As String
            Get
                Return TsTypeFormulaire
            End Get
            Set(ByVal value As String)
                TsTypeFormulaire = value
            End Set
        End Property

        Private ReadOnly Property V_InfoCritere(ByVal NumInfo As Integer) As String
            Get
                Dim Chaine As String = ""
                Select Case V_Type_Formulaire
                    Case Is = "C/PC"
                        Select Case NumInfo
                            Case 32
                                Chaine = ""
                        End Select
                    Case Is = "CJ"
                        Select Case NumInfo
                            Case 32
                                Chaine = ""
                        End Select
                    Case Is = "PR"
                        Select Case NumInfo
                            Case 32
                                Chaine = "Maîtrise des connaissances et du raisonnement juridiques"
                        End Select
                End Select
                Return Chaine
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace





