﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_SOCIETE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsSociete As String
        Private WsTypeSociete As String
        Private WsTrancheEffectif As String
        Private WsDate_Anciennete_Conservee As String
        Private WsEtablissement As String
        Private WsDate_de_depart As String
        Private WsMotif_de_depart As String
        Private WsDate_Anciennete_Profession As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_SOCIETE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaSociete 'Spécifique Secteur Privé
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Societe() As String
            Get
                Return WsSociete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSociete = value
                    Case Else
                        WsSociete = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsSociete
            End Set
        End Property

        Public Property TypedeSociete() As String
            Get
                Return WsTypeSociete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeSociete = value
                    Case Else
                        WsTypeSociete = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property TranchedEffectif() As String
            Get
                Return WsTrancheEffectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTrancheEffectif = value
                    Case Else
                        WsTrancheEffectif = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_d_Anciennete_Conservee() As String
            Get
                Return WsDate_Anciennete_Conservee
            End Get
            Set(ByVal value As String)
                WsDate_Anciennete_Conservee = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_de_depart() As String
            Get
                Return WsDate_de_depart
            End Get
            Set(ByVal value As String)
                WsDate_de_depart = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Motif_de_depart() As String
            Get
                Return WsMotif_de_depart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMotif_de_depart = value
                    Case Else
                        WsMotif_de_depart = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Date_d_Anciennete_Profession() As String
            Get
                Return WsDate_Anciennete_Profession
            End Get
            Set(ByVal value As String)
                WsDate_Anciennete_Profession = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Societe & VI.Tild)
                Chaine.Append(TypedeSociete & VI.Tild)
                Chaine.Append(TranchedEffectif & VI.Tild)
                Chaine.Append(Date_d_Anciennete_Conservee & VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(Date_de_depart & VI.Tild)
                Chaine.Append(Motif_de_depart & VI.Tild)
                Chaine.Append(Date_d_Anciennete_Profession & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Societe = TableauData(2)
                TypedeSociete = TableauData(3)
                TranchedEffectif = TableauData(4)
                Date_d_Anciennete_Conservee = TableauData(5)
                Etablissement = TableauData(6)
                Date_de_depart = TableauData(7)
                Motif_de_depart = TableauData(8)
                Date_d_Anciennete_Profession = TableauData(9)
                MyBase.Certification = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_depart
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


