﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_FIA
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsDate_Debut_FI As String
        Private WsDate_Fin_FI As String
        Private WsFiliere_FI_N1 As String
        Private WsNbJours_FI_N1 As Double
        Private WsFiliere_FI_N2 As String
        Private WsNbJours_FI_N2 As Double
        Private WsFiliere_FI_N3 As String
        Private WsNbJours_FI_N3 As Double
        Private WsFiliere_FI_N4 As String
        Private WsNbJours_FI_N4 As Double
        Private WsFiliere_FI_N5 As String
        Private WsNbJours_FI_N5 As Double
        Private WsNbJours_FI_Total As Double
        Private WsDate_Debut_PRO As String
        Private WsDate_Fin_PRO As String
        Private WsFiliere_PRO_N1 As String
        Private WsNbJours_PRO_N1 As Double
        Private WsFiliere_PRO_N2 As String
        Private WsNbJours_PRO_N2 As Double
        Private WsFiliere_PRO_N3 As String
        Private WsNbJours_PRO_N3 As Double
        Private WsFiliere_PRO_N4 As String
        Private WsNbJours_PRO_N4 As Double
        Private WsFiliere_PRO_N5 As String
        Private WsNbJours_PRO_N5 As Double
        Private WsNbJours_PRO_Total As Double
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_FIA"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaFIA
            End Get
        End Property

        Public Property Date_Debut_FI() As String
            Get
                Return WsDate_Debut_FI
            End Get
            Set(ByVal value As String)
                WsDate_Debut_FI = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_FI() As String
            Get
                Return WsDate_Fin_FI
            End Get
            Set(ByVal value As String)
                WsDate_Fin_FI = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Filiere_FI_N1() As String
            Get
                Return WsFiliere_FI_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_FI_N1 = value
                    Case Else
                        WsFiliere_FI_N1 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_FI_N1() As Double
            Get
                Return WsNbJours_FI_N1
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_N1 = value
            End Set
        End Property

        Public Property Filiere_FI_N2() As String
            Get
                Return WsFiliere_FI_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_FI_N2 = value
                    Case Else
                        WsFiliere_FI_N2 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_FI_N2() As Double
            Get
                Return WsNbJours_FI_N2
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_N2 = value
            End Set
        End Property

        Public Property Filiere_FI_N3() As String
            Get
                Return WsFiliere_FI_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_FI_N3 = value
                    Case Else
                        WsFiliere_FI_N3 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_FI_N3() As Double
            Get
                Return WsNbJours_FI_N3
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_N3 = value
            End Set
        End Property

        Public Property Filiere_FI_N4() As String
            Get
                Return WsFiliere_FI_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_FI_N4 = value
                    Case Else
                        WsFiliere_FI_N4 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_FI_N4() As Double
            Get
                Return WsNbJours_FI_N4
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_N4 = value
            End Set
        End Property

        Public Property Filiere_FI_N5() As String
            Get
                Return WsFiliere_FI_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_FI_N5 = value
                    Case Else
                        WsFiliere_FI_N5 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_FI_N5() As Double
            Get
                Return WsNbJours_FI_N5
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_N5 = value
            End Set
        End Property

        Public Property NombreJours_FI_Total() As Double
            Get
                Return WsNbJours_FI_Total
            End Get
            Set(ByVal value As Double)
                WsNbJours_FI_Total = value
            End Set
        End Property

        Public Property Date_Debut_PRO() As String
            Get
                Return WsDate_Debut_PRO
            End Get
            Set(ByVal value As String)
                WsDate_Debut_PRO = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_PRO() As String
            Get
                Return WsDate_Fin_PRO
            End Get
            Set(ByVal value As String)
                WsDate_Fin_PRO = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Filiere_PRO_N1() As String
            Get
                Return WsFiliere_PRO_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_PRO_N1 = value
                    Case Else
                        WsFiliere_PRO_N1 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_PRO_N1() As Double
            Get
                Return WsNbJours_PRO_N1
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_N1 = value
            End Set
        End Property

        Public Property Filiere_PRO_N2() As String
            Get
                Return WsFiliere_PRO_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_PRO_N2 = value
                    Case Else
                        WsFiliere_PRO_N2 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_PRO_N2() As Double
            Get
                Return WsNbJours_PRO_N2
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_N2 = value
            End Set
        End Property

        Public Property Filiere_PRO_N3() As String
            Get
                Return WsFiliere_PRO_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_PRO_N3 = value
                    Case Else
                        WsFiliere_PRO_N3 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_PRO_N3() As Double
            Get
                Return WsNbJours_PRO_N3
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_N3 = value
            End Set
        End Property

        Public Property Filiere_PRO_N4() As String
            Get
                Return WsFiliere_PRO_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_PRO_N4 = value
                    Case Else
                        WsFiliere_PRO_N4 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_PRO_N4() As Double
            Get
                Return WsNbJours_PRO_N4
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_N4 = value
            End Set
        End Property

        Public Property Filiere_PRO_N5() As String
            Get
                Return WsFiliere_PRO_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFiliere_PRO_N5 = value
                    Case Else
                        WsFiliere_PRO_N5 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_PRO_N5() As Double
            Get
                Return WsNbJours_PRO_N5
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_N5 = value
            End Set
        End Property

        Public Property NombreJours_PRO_Total() As Double
            Get
                Return WsNbJours_PRO_Total
            End Get
            Set(ByVal value As Double)
                WsNbJours_PRO_Total = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Date_Debut_FI & VI.Tild)
                Chaine.Append(Date_Fin_FI & VI.Tild)
                Chaine.Append(Filiere_FI_N1 & VI.Tild)
                Chaine.Append(NombreJours_FI_N1.ToString & VI.Tild)
                Chaine.Append(Filiere_FI_N2 & VI.Tild)
                Chaine.Append(NombreJours_FI_N2.ToString & VI.Tild)
                Chaine.Append(Filiere_FI_N3 & VI.Tild)
                Chaine.Append(NombreJours_FI_N3.ToString & VI.Tild)
                Chaine.Append(Filiere_FI_N4 & VI.Tild)
                Chaine.Append(NombreJours_FI_N4.ToString & VI.Tild)
                Chaine.Append(Filiere_FI_N5 & VI.Tild)
                Chaine.Append(NombreJours_FI_N5.ToString & VI.Tild)
                Chaine.Append(NombreJours_FI_Total.ToString & VI.Tild)
                Chaine.Append(Date_Debut_PRO & VI.Tild)
                Chaine.Append(Date_Fin_PRO & VI.Tild)
                Chaine.Append(Filiere_PRO_N1 & VI.Tild)
                Chaine.Append(NombreJours_PRO_N1.ToString & VI.Tild)
                Chaine.Append(Filiere_PRO_N2 & VI.Tild)
                Chaine.Append(NombreJours_PRO_N2.ToString & VI.Tild)
                Chaine.Append(Filiere_PRO_N3 & VI.Tild)
                Chaine.Append(NombreJours_PRO_N3.ToString & VI.Tild)
                Chaine.Append(Filiere_PRO_N4 & VI.Tild)
                Chaine.Append(NombreJours_PRO_N4.ToString & VI.Tild)
                Chaine.Append(Filiere_PRO_N5 & VI.Tild)
                Chaine.Append(NombreJours_PRO_N5.ToString & VI.Tild)
                Chaine.Append(NombreJours_PRO_Total.ToString & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 28 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut_FI = TableauData(1)
                Date_Fin_FI = TableauData(2)
                Filiere_FI_N1 = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                NombreJours_FI_N1 = VirRhFonction.ConversionDouble(TableauData(4))
                Filiere_FI_N2 = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                NombreJours_FI_N2 = VirRhFonction.ConversionDouble(TableauData(6))
                Filiere_FI_N3 = TableauData(7)
                If TableauData(8) = "" Then TableauData(8) = "0"
                NombreJours_FI_N3 = VirRhFonction.ConversionDouble(TableauData(8))
                Filiere_FI_N4 = TableauData(9)
                If TableauData(10) = "" Then TableauData(10) = "0"
                NombreJours_FI_N4 = VirRhFonction.ConversionDouble(TableauData(10))
                Filiere_FI_N5 = TableauData(11)
                If TableauData(12) = "" Then TableauData(12) = "0"
                NombreJours_FI_N5 = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) = "" Then TableauData(13) = "0"
                NombreJours_FI_Total = VirRhFonction.ConversionDouble(TableauData(13))
                Date_Debut_PRO = TableauData(14)
                Date_Fin_PRO = TableauData(15)
                Filiere_PRO_N1 = TableauData(16)
                If TableauData(17) = "" Then TableauData(17) = "0"
                NombreJours_PRO_N1 = VirRhFonction.ConversionDouble(TableauData(17))
                Filiere_PRO_N2 = TableauData(18)
                If TableauData(19) = "" Then TableauData(19) = "0"
                NombreJours_PRO_N2 = VirRhFonction.ConversionDouble(TableauData(19))
                Filiere_PRO_N3 = TableauData(20)
                If TableauData(21) = "" Then TableauData(21) = "0"
                NombreJours_PRO_N3 = VirRhFonction.ConversionDouble(TableauData(21))
                Filiere_PRO_N4 = TableauData(22)
                If TableauData(23) = "" Then TableauData(23) = "0"
                NombreJours_PRO_N4 = VirRhFonction.ConversionDouble(TableauData(23))
                Filiere_PRO_N5 = TableauData(24)
                If TableauData(25) = "" Then TableauData(25) = "0"
                NombreJours_PRO_N5 = VirRhFonction.ConversionDouble(TableauData(25))
                If TableauData(26) = "" Then TableauData(26) = "0"
                NombreJours_PRO_Total = VirRhFonction.ConversionDouble(TableauData(26))
                Observations = TableauData(27)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsDate_Debut_FI
                    Case 2
                        Return WsDate_Fin_FI
                    Case 3
                        Return WsFiliere_FI_N1
                    Case 4
                        Return WsNbJours_FI_N1.ToString
                    Case 5
                        Return WsFiliere_FI_N2
                    Case 6
                        Return WsNbJours_FI_N2.ToString
                    Case 7
                        Return WsFiliere_FI_N3
                    Case 8
                        Return WsNbJours_FI_N3.ToString
                    Case 9
                        Return WsFiliere_FI_N4
                    Case 10
                        Return WsNbJours_FI_N4.ToString
                    Case 11
                        Return WsFiliere_FI_N5
                    Case 12
                        Return WsNbJours_FI_N5.ToString
                    Case 13
                        Return WsNbJours_FI_Total.ToString
                    Case 14
                        Return WsDate_Debut_PRO
                    Case 15
                        Return WsDate_Fin_PRO
                    Case 16
                        Return WsFiliere_PRO_N1
                    Case 17
                        Return WsNbJours_PRO_N1.ToString
                    Case 18
                        Return WsFiliere_PRO_N2
                    Case 19
                        Return WsNbJours_PRO_N2.ToString
                    Case 20
                        Return WsFiliere_PRO_N3
                    Case 21
                        Return WsNbJours_PRO_N3.ToString
                    Case 22
                        Return WsFiliere_PRO_N4
                    Case 23
                        Return WsNbJours_PRO_N4.ToString
                    Case 24
                        Return WsFiliere_PRO_N5
                    Case 25
                        Return WsNbJours_PRO_N5.ToString
                    Case 26
                        Return WsNbJours_PRO_Total.ToString
                    Case 27
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
