﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CET
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsTotal_Epargne As Double = 0
        Private WsTotal_CA As Double = 0
        Private WsTotal_RTT As Double = 0
        Private WsTotal_RC As Double = 0
        Private WsDateOuverture As String
        Private WsTotal_Consomme As Double = 0
        Private WsSolde As Double = 0
        Private WsObservations As String
        Private WsDateValidite As String
        Private WsTotal_Paye As Double = 0
        Private WsDateDebutPaiement As String
        Private WsDateFinPaiement As String
        Private WsTotal_RAFP As Double = 0
        '****
        Private WsTotal_Epargne_Fin2008 As Double = 0
        Private WsTotal_Conges_Fin2008 As Double = 0
        Private WsTotal_Payes_Fin2008 As Double = 0
        Private WsTotal_RAFP_Fin2008 As Double = 0
        Private WsCongesPris_Sur_Fin2008 As Double = 0
        Private WsSoldeConges_Sur_Fin2008 As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CET"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEpargneTemps
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 20
            End Get
        End Property

        Public Property Total_Epargne() As Double
            Get
                Return WsTotal_Epargne
            End Get
            Set(ByVal value As Double)
                WsTotal_Epargne = value
                MyBase.Clef = WsTotal_Epargne.ToString
            End Set
        End Property

        Public Property Total_CA() As Double
            Get
                Return WsTotal_CA
            End Get
            Set(ByVal value As Double)
                WsTotal_CA = value
            End Set
        End Property

        Public Property Total_RTT() As Double
            Get
                Return WsTotal_RTT
            End Get
            Set(ByVal value As Double)
                WsTotal_RTT = value
            End Set
        End Property

        Public Property Total_RC() As Double
            Get
                Return WsTotal_RC
            End Get
            Set(ByVal value As Double)
                WsTotal_RC = value
            End Set
        End Property

        Public Property Date_Ouverture() As String
            Get
                Return WsDateOuverture
            End Get
            Set(ByVal value As String)
                WsDateOuverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Total_Consomme() As Double
            Get
                Return WsTotal_Consomme
            End Get
            Set(ByVal value As Double)
                WsTotal_Consomme = value
            End Set
        End Property

        Public Property Solde() As Double
            Get
                Return WsSolde
            End Get
            Set(ByVal value As Double)
                WsSolde = value
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Date_de_Validite() As String
            Get
                Return WsDateValidite
            End Get
            Set(ByVal value As String)
                WsDateValidite = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Total_Paye() As Double
            Get
                Return WsTotal_Paye
            End Get
            Set(ByVal value As Double)
                WsTotal_Paye = value
            End Set
        End Property

        Public Property Date_Debut_Paiement() As String
            Get
                Return WsDateDebutPaiement
            End Get
            Set(ByVal value As String)
                WsDateDebutPaiement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Paiement() As String
            Get
                Return WsDateFinPaiement
            End Get
            Set(ByVal value As String)
                WsDateFinPaiement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Total_RAFP() As Double
            Get
                Return WsTotal_RAFP
            End Get
            Set(ByVal value As Double)
                WsTotal_RAFP = value
            End Set
        End Property
        '****
        Public Property Total_Epargne_Fin2008() As Double
            Get
                Return WsTotal_Epargne_Fin2008
            End Get
            Set(ByVal value As Double)
                WsTotal_Epargne_Fin2008 = value
            End Set
        End Property

        Public Property Total_Conges_Fin2008() As Double
            Get
                Return WsTotal_Conges_Fin2008
            End Get
            Set(ByVal value As Double)
                WsTotal_Conges_Fin2008 = value
            End Set
        End Property

        Public Property Total_Payes_Fin2008() As Double
            Get
                Return WsTotal_Payes_Fin2008
            End Get
            Set(ByVal value As Double)
                WsTotal_Payes_Fin2008 = value
            End Set
        End Property

        Public Property Total_RAFP_Fin2008() As Double
            Get
                Return WsTotal_RAFP_Fin2008
            End Get
            Set(ByVal value As Double)
                WsTotal_RAFP_Fin2008 = value
            End Set
        End Property

        Public Property CongesPris_Sur_Fin2008() As Double
            Get
                Return WsCongesPris_Sur_Fin2008
            End Get
            Set(ByVal value As Double)
                WsCongesPris_Sur_Fin2008 = value
            End Set
        End Property

        Public Property SoldeConges_Sur_Fin2008() As Double
            Get
                Return WsSoldeConges_Sur_Fin2008
            End Get
            Set(ByVal value As Double)
                WsSoldeConges_Sur_Fin2008 = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Select Case Total_Epargne
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Epargne.ToString) & VI.Tild)
                End Select
                Select Case Total_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_CA.ToString) & VI.Tild)
                End Select
                Select Case Total_RTT
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_RTT.ToString) & VI.Tild)
                End Select
                Select Case Total_RC
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_RC.ToString) & VI.Tild)
                End Select
                Chaine.Append(Date_Ouverture & VI.Tild)
                Select Case Total_Consomme
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Consomme.ToString) & VI.Tild)
                End Select
                Select Case Solde
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Solde.ToString) & VI.Tild)
                End Select
                Chaine.Append(Observation & VI.Tild)
                Chaine.Append(Date_de_Validite & VI.Tild)
                Select Case Total_Paye
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Paye.ToString) & VI.Tild)
                End Select
                Chaine.Append(Date_Debut_Paiement & VI.Tild)
                Chaine.Append(Date_Fin_Paiement & VI.Tild)
                Select Case Total_RAFP
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_RAFP.ToString) & VI.Tild)
                End Select
                '****
                Select Case Total_Epargne_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Epargne_Fin2008.ToString) & VI.Tild)
                End Select
                Select Case Total_Conges_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Conges_Fin2008.ToString) & VI.Tild)
                End Select
                Select Case Total_Payes_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_Payes_Fin2008.ToString) & VI.Tild)
                End Select
                Select Case Total_RAFP_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Total_RAFP_Fin2008.ToString) & VI.Tild)
                End Select
                Select Case CongesPris_Sur_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(CongesPris_Sur_Fin2008.ToString) & VI.Tild)
                End Select
                Select Case SoldeConges_Sur_Fin2008
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(SoldeConges_Sur_Fin2008.ToString) & VI.Tild)
                End Select
                Chaine.Append(MyBase.Certification)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 21 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Total_Epargne = VirRhFonction.ConversionDouble(TableauData(1))
                If TableauData(2) <> "" Then Total_CA = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Total_RTT = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Total_RC = VirRhFonction.ConversionDouble(TableauData(4))
                Date_Ouverture = TableauData(5)
                If TableauData(6) <> "" Then Total_Consomme = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Solde = VirRhFonction.ConversionDouble(TableauData(7))
                Observation = TableauData(8)
                Date_de_Validite = TableauData(9)
                If TableauData(10) <> "" Then Total_Paye = VirRhFonction.ConversionDouble(TableauData(10))
                Date_Debut_Paiement = TableauData(11)
                Date_Fin_Paiement = TableauData(12)
                If TableauData(13) <> "" Then Total_RAFP = VirRhFonction.ConversionDouble(TableauData(13))
                '**
                If TableauData(14) <> "" Then Total_Epargne_Fin2008 = VirRhFonction.ConversionDouble(TableauData(14))
                If TableauData(15) <> "" Then Total_Conges_Fin2008 = VirRhFonction.ConversionDouble(TableauData(15))
                If TableauData(16) <> "" Then Total_Payes_Fin2008 = VirRhFonction.ConversionDouble(TableauData(16))
                If TableauData(17) <> "" Then Total_RAFP_Fin2008 = VirRhFonction.ConversionDouble(TableauData(17))
                If TableauData(18) <> "" Then CongesPris_Sur_Fin2008 = VirRhFonction.ConversionDouble(TableauData(18))
                If TableauData(19) <> "" Then SoldeConges_Sur_Fin2008 = VirRhFonction.ConversionDouble(TableauData(19))
                MyBase.Certification = TableauData(20)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Select Case Total_Epargne
                    Case Is = 0
                        Chaine.Append("<TotalEpargne>" & "0" & "</TotalEpargne>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalEpargne>" & Total_Epargne.ToString & "</TotalEpargne>" & vbCrLf)
                End Select
                Select Case Total_CA
                    Case Is = 0
                        Chaine.Append("<TotalCA>" & "0" & "</TotalCA>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalCA>" & VirRhFonction.VirgulePoint(Total_CA.ToString) & "</TotalCA>" & vbCrLf)
                End Select
                Select Case Total_RTT
                    Case Is = 0
                        Chaine.Append("<TotalRTT>" & "0" & "</TotalRTT>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalRTT>" & VirRhFonction.VirgulePoint(Total_RTT.ToString) & "</TotalRTT>" & vbCrLf)
                End Select
                Select Case Total_RC
                    Case Is = 0
                        Chaine.Append("<TotalRC>" & "0" & "</TotalRC>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalRC>" & VirRhFonction.VirgulePoint(Total_RC.ToString) & "</TotalRC>" & vbCrLf)
                End Select
                Chaine.Append("<DateOuvertureCET>" & Date_Ouverture & "</DateOuvertureCET>" & vbCrLf)
                Select Case Total_Consomme
                    Case Is = 0
                        Chaine.Append("<TotalConsomme>" & "0" & "</TotalConsomme>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalConsomme>" & VirRhFonction.VirgulePoint(Total_Consomme.ToString) & "</TotalConsomme>" & vbCrLf)
                End Select
                Select Case Solde
                    Case Is = 0
                        Chaine.Append("<Solde>" & "0" & "</Solde>" & vbCrLf)
                    Case Else
                        Chaine.Append("<Solde>" & VirRhFonction.MontantStocke(Solde.ToString) & "</Solde>" & vbCrLf)
                End Select
                Chaine.Append("<DateValiditeCET>" & Date_de_Validite & "</DateValiditeCET>" & vbCrLf)
                Select Case Total_Paye
                    Case Is = 0
                        Chaine.Append("<TotalPaye>" & "0" & "</TotalPaye>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalPaye>" & VirRhFonction.VirgulePoint(Total_Paye.ToString) & "</TotalPaye>" & vbCrLf)
                End Select
                Chaine.Append("<DateDebutPaiement>" & Date_Debut_Paiement & "</DateDebutPaiement>" & vbCrLf)
                Chaine.Append("<DateFinPaiement>" & Date_Fin_Paiement & "</DateFinPaiement>" & vbCrLf)
                Select Case Total_RAFP
                    Case Is = 0
                        Chaine.Append("<TotalRAFP>" & "0" & "</TotalRAFP>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalRAFP>" & VirRhFonction.VirgulePoint(Total_RAFP.ToString) & "</TotalRAFP>" & vbCrLf)
                End Select
                Select Case Total_Epargne_Fin2008
                    Case Is = 0
                        Chaine.Append("<TotalEpargneFin2008>" & "0" & "</TotalEpargneFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalEpargneFin2008>" & VirRhFonction.VirgulePoint(Total_Epargne_Fin2008.ToString) & "</TotalEpargneFin2008>" & vbCrLf)
                End Select
                Select Case Total_Conges_Fin2008
                    Case Is = 0
                        Chaine.Append("<TotalCongesFin2008>" & "0" & "</TotalCongesFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalCongesFin2008>" & VirRhFonction.VirgulePoint(Total_Conges_Fin2008.ToString) & "</TotalCongesFin2008>" & vbCrLf)
                End Select
                Select Case Total_Payes_Fin2008
                    Case Is = 0
                        Chaine.Append("<TotalPayesFin2008>" & "0" & "</TotalPayesFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalPayesFin2008>" & VirRhFonction.VirgulePoint(Total_Payes_Fin2008.ToString) & "</TotalPayesFin2008>" & vbCrLf)
                End Select
                Select Case Total_RAFP_Fin2008
                    Case Is = 0
                        Chaine.Append("<TotalRAFPFin2008>" & "0" & "</TotalRAFPFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TotalRAFPFin2008>" & VirRhFonction.VirgulePoint(Total_RAFP_Fin2008.ToString) & "</TotalRAFPFin2008>" & vbCrLf)
                End Select
                Select Case CongesPris_Sur_Fin2008
                    Case Is = 0
                        Chaine.Append("<CongesPrisSurFin2008>" & "0" & "</CongesPrisSurFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<CongesPrisSurFin2008>" & VirRhFonction.VirgulePoint(CongesPris_Sur_Fin2008.ToString) & "</CongesPrisSurFin2008>" & vbCrLf)
                End Select
                Select Case SoldeConges_Sur_Fin2008
                    Case Is = 0
                        Chaine.Append("<SoldeCongesSurFin2008>" & "0" & "</SoldeCongesSurFin2008>" & vbCrLf)
                    Case Else
                        Chaine.Append("<SoldeCongesSurFin2008>" & VirRhFonction.VirgulePoint(SoldeConges_Sur_Fin2008.ToString) & "</SoldeCongesSurFin2008>" & vbCrLf)
                End Select

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



