﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_BASE_COTISATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Valeur As String
        Private WsGradeReference As String
        Private WsCorpsReference As String
        Private WsEchelon As String
        Private WsIndiceBrut As Integer
        Private WsIndiceMajore As Integer
        Private WsRegimeRetraite As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_BASE_COTISATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 85 'Spécifique Secteur Public
            End Get
        End Property

        Public Property Grade_Reference() As String
            Get
                Return WsGradeReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGradeReference = value
                    Case Else
                        WsGradeReference = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Corps_Reference() As String
            Get
                Return WsCorpsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCorpsReference = value
                    Case Else
                        WsCorpsReference = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Echelon() As String
            Get
                Return WsEchelon
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEchelon = value
                    Case Else
                        WsEchelon = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Indice_Brut() As Integer
            Get
                Return WsIndiceBrut
            End Get
            Set(ByVal value As Integer)
                WsIndiceBrut = value
            End Set
        End Property

        Public Property Indice_Majore() As Integer
            Get
                Return WsIndiceMajore
            End Get
            Set(ByVal value As Integer)
                WsIndiceMajore = value
            End Set
        End Property

        Public Property RegimeRetraite() As String
            Get
                Return WsRegimeRetraite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegimeRetraite = value
                    Case Else
                        WsRegimeRetraite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Grade_Reference & VI.Tild)
                Chaine.Append(Corps_Reference & VI.Tild)
                Chaine.Append(Echelon & VI.Tild)
                Chaine.Append(Indice_Brut.ToString & VI.Tild)
                Chaine.Append(Indice_Majore.ToString & VI.Tild)
                Chaine.Append(RegimeRetraite)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Grade_Reference = TableauData(2)
                Corps_Reference = TableauData(3)
                Echelon = TableauData(4)
                If TableauData(5) = "" Then TableauData(5) = "0"
                Indice_Brut = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) = "" Then TableauData(6) = "0"
                Indice_Majore = VirRhFonction.ConversionDouble(TableauData(6))
                RegimeRetraite = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Valeur
                    Case 1
                        Return WsGradeReference
                    Case 2
                        Return WsCorpsReference
                    Case 3
                        Return WsEchelon
                    Case 4
                        Return WsIndiceBrut.ToString
                    Case 5
                        Return WsIndiceMajore.ToString
                    Case 6
                        Return WsRegimeRetraite
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

