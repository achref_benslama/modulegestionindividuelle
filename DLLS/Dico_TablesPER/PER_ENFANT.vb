Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ENFANT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean = False
        '
        Private WsDate_de_naissance As String
        Private WsNom As String
        Private WsPrenom As String
        Private WsSexe As String
        Private WsA_charge As String
        Private WsJumeau As String
        Private WsSexe_du_jumeau As String
        Private WsJumeau_a_charge As String
        Private WsTriple As String
        Private WsSexe_du_triple As String
        Private WsTriple_a_charge As String
        Private WsLieudenaissance As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENFANT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEnfant
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 12
            End Get
        End Property

        Public Property Date_de_naissance() As String
            Get
                Return WsDate_de_naissance
            End Get
            Set(ByVal value As String)
                WsDate_de_naissance = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_de_naissance
            End Set
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                If WsSiMajuscule = True Then
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = value.ToUpper
                        Case Else
                            WsNom = Strings.Left(value.ToUpper, 35)
                    End Select
                Else
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = VirRhFonction.Lettre1Capi(value, 2)
                        Case Else
                            WsNom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 2)
                    End Select
                End If
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
                MyBase.Clef = WsPrenom
            End Set
        End Property

        Public Property Sexe() As String
            Get
                Return WsSexe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsSexe = value
                    Case Else
                        WsSexe = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property A_charge() As String
            Get
                Return WsA_charge
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui", "1"
                        WsA_charge = "Oui"
                    Case Is = "", "Non", "0"
                        WsA_charge = "Non"
                    Case Else
                        WsA_charge = "Autre"
                End Select
            End Set
        End Property

        Public Property Jumeau() As String
            Get
                Return WsJumeau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsJumeau = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsJumeau = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Sexe_du_jumeau() As String
            Get
                Return WsSexe_du_jumeau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsSexe_du_jumeau = value
                    Case Else
                        WsSexe_du_jumeau = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property Jumeau_a_charge() As String
            Get
                Return WsJumeau_a_charge
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui", "1"
                        WsJumeau_a_charge = "Oui"
                    Case Is = "Non", "0"
                        WsJumeau_a_charge = "Non"
                    Case Is = ""
                        WsJumeau_a_charge = ""
                    Case Else
                        WsJumeau_a_charge = "Autre"
                End Select
            End Set
        End Property

        Public Property Triple() As String
            Get
                Return WsTriple
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsTriple = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsTriple = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Sexe_du_Triple() As String
            Get
                Return WsSexe_du_triple
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsSexe_du_triple = value
                    Case Else
                        WsSexe_du_triple = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property Triple_a_charge() As String
            Get
                Return WsTriple_a_charge
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui", "1"
                        WsTriple_a_charge = "Oui"
                    Case Is = "Non", "0"
                        WsTriple_a_charge = "Non"
                    Case Is = ""
                        WsTriple_a_charge = ""
                    Case Else
                        WsTriple_a_charge = "Autre"
                End Select
            End Set
        End Property

        Public Property Lieudenaissance() As String
            Get
                Return WsLieudenaissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLieudenaissance = value
                    Case Else
                        WsLieudenaissance = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_de_naissance & VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Sexe & VI.Tild)
                Chaine.Append(A_charge & VI.Tild)
                Chaine.Append(Jumeau & VI.Tild)
                Chaine.Append(Sexe_du_jumeau & VI.Tild)
                Chaine.Append(Jumeau_a_charge & VI.Tild)
                Chaine.Append(Triple & VI.Tild)
                Chaine.Append(Sexe_du_Triple & VI.Tild)
                Chaine.Append(Triple_a_charge & VI.Tild)
                Chaine.Append(Lieudenaissance & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 14 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_de_naissance = TableauData(1)
                Nom = TableauData(2)
                Prenom = TableauData(3)
                Sexe = TableauData(4)
                A_charge = TableauData(5)
                Jumeau = TableauData(6)
                Sexe_du_jumeau = TableauData(7)
                Jumeau_a_charge = TableauData(8)
                Triple = TableauData(9)
                Sexe_du_Triple = TableauData(10)
                Triple_a_charge = TableauData(11)
                Lieudenaissance = TableauData(12)
                MyBase.Certification = TableauData(13)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_Age As Double
            Get
                Dim Champ As String
                Champ = VirRhDate.CalcAge(Date_de_naissance, VirRhDate.DateduJour, True)
                Return Math.Round(VirRhFonction.ConversionDouble(Champ), 2)
            End Get
        End Property

        Public ReadOnly Property V_XML(Optional ByVal Section As String = "Enfant") As String
            Get
                Dim Chaine As System.Text.StringBuilder
                If A_charge = "Oui" And V_Age >= 20 Then
                    A_charge = "Non"
                End If
                If Jumeau_a_charge = "Oui" And V_Age >= 20 Then
                    Jumeau_a_charge = "Non"
                End If
                If Triple_a_charge = "Oui" And V_Age >= 20 Then
                    Triple_a_charge = "Non"
                End If

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<" & Section & ">" & vbCrLf)
                Chaine.Append("<PrenomEnfant>" & Prenom & "</PrenomEnfant>" & vbCrLf)
                Chaine.Append("<DateNaissanceEnfant>" & Date_de_naissance & "</DateNaissanceEnfant>" & vbCrLf)
                Chaine.Append("<NomEnfant>" & Nom & "</NomEnfant>" & vbCrLf)
                Chaine.Append("<SexeEnfant>" & Sexe & "</SexeEnfant>" & vbCrLf)
                Chaine.Append("<SiACharge>" & A_charge & "</SiACharge>" & vbCrLf)
                Chaine.Append("<LieuNaissanceEnfant>" & VirRhFonction.ChaineXMLValide(Lieudenaissance) & "</LieuNaissanceEnfant>" & vbCrLf)
                Chaine.Append("</" & Section & ">" & vbCrLf)
                If Jumeau <> "" Then
                    Chaine.Append("<" & Section & ">" & vbCrLf)
                    Chaine.Append("<PrenomEnfant>" & Jumeau & "</PrenomEnfant>" & vbCrLf)
                    Chaine.Append("<DateNaissanceEnfant>" & Date_de_naissance & "</DateNaissanceEnfant>" & vbCrLf)
                    Chaine.Append("<NomEnfant>" & Nom & "</NomEnfant>" & vbCrLf)
                    Chaine.Append("<SexeEnfant>" & Sexe_du_jumeau & "</SexeEnfant>" & vbCrLf)
                    Chaine.Append("<SiACharge>" & Jumeau_a_charge & "</SiACharge>" & vbCrLf)
                    Chaine.Append("<LieuNaissanceEnfant>" & VirRhFonction.ChaineXMLValide(Lieudenaissance) & "</LieuNaissanceEnfant>" & vbCrLf)
                    Chaine.Append("</" & Section & ">" & vbCrLf)
                End If
                If Triple <> "" Then
                    Chaine.Append("<" & Section & ">" & vbCrLf)
                    Chaine.Append("<PrenomEnfant>" & Triple & "</PrenomEnfant>" & vbCrLf)
                    Chaine.Append("<DateNaissanceEnfant>" & Date_de_naissance & "</DateNaissanceEnfant>" & vbCrLf)
                    Chaine.Append("<NomEnfant>" & Nom & "</NomEnfant>" & vbCrLf)
                    Chaine.Append("<SexeEnfant>" & Sexe_du_Triple & "</SexeEnfant>" & vbCrLf)
                    Chaine.Append("<SiACharge>" & Triple_a_charge & "</SiACharge>" & vbCrLf)
                    Chaine.Append("<LieuNaissanceEnfant>" & VirRhFonction.ChaineXMLValide(Lieudenaissance) & "</LieuNaissanceEnfant>" & vbCrLf)
                    Chaine.Append("</" & Section & ">" & vbCrLf)
                End If

                Return Chaine.ToString

            End Get
        End Property
        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Sub New()
            Me.New(False)
        End Sub

        Public Sub New(ByVal SiMajuscule As Boolean)
            MyBase.New()
            WsSiMajuscule = SiMajuscule
        End Sub


        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace

