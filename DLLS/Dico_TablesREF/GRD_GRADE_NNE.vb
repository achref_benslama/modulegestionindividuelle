﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRADE_NNE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        Private WsLstGrilles As List(Of GRD_GRILLE_NNE)
        Private WsLstCategories As List(Of GRD_CATEGORIE_NNE)
        '
        Private WsCodeNomenclature As String
        Private WsIntitule As String
        Private WsNbEchelons As Integer
        Private WsNbEchSpecifiques As Integer
        Private WsCodePCS As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_GRADE_NNE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property CodeNomenclature() As String
            Get
                Return WsCodeNomenclature
            End Get
            Set(ByVal value As String)
                WsCodeNomenclature = F_FormatAlpha(value, 10)
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                WsIntitule = F_FormatAlpha(value, 100)
            End Set
        End Property

        Public Property Nombre_Echelons As Integer
            Get
                Return WsNbEchelons
            End Get
            Set(ByVal value As Integer)
                WsNbEchelons = value
            End Set
        End Property

        Public Property Nombre_Echelons_Specifiques As Integer
            Get
                Return WsNbEchSpecifiques
            End Get
            Set(ByVal value As Integer)
                WsNbEchSpecifiques = value
            End Set
        End Property

        Public Property Code_PCS() As String
            Get
                Return WsCodePCS
            End Get
            Set(ByVal value As String)
                WsCodePCS = F_FormatAlpha(value, 4)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(CodeNomenclature & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Nombre_Echelons.ToString & VI.Tild)
                Chaine.Append(Nombre_Echelons_Specifiques.ToString & VI.Tild)
                Chaine.Append(Code_PCS & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                CodeNomenclature = TableauData(2)
                Intitule = TableauData(3)
                Nombre_Echelons = CInt(F_FormatNumerique(TableauData(4)))
                Nombre_Echelons_Specifiques = CInt(F_FormatNumerique(TableauData(5)))
                Code_PCS = TableauData(6)
                MyBase.Date_de_Fin = TableauData(7)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesCategories() As List(Of GRD_CATEGORIE_NNE)
            Get
                If WsLstCategories Is Nothing Then
                    Return Nothing
                End If
                Return (From Instance In WsLstCategories Order By Instance.Ministere Ascending).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Echelon(ByVal Fiche As GRD_GRILLE_NNE) As Integer

            If WsLstGrilles Is Nothing Then
                WsLstGrilles = New List(Of GRD_GRILLE_NNE)
            End If
            WsLstGrilles.Add(Fiche)
            Return WsLstGrilles.Count

        End Function

        Public Function ListedesEchelons(ByVal index As Integer) As List(Of GRD_GRILLE_NNE)

            If WsLstGrilles Is Nothing Then
                Return Nothing
            End If

            Return (From Instance In WsLstGrilles Where Instance.Rang = index Order By Instance.Date_Valeur_ToDate Descending).ToList

        End Function

        Public Function ListedesEchelons(ByVal ArgumentDate As String) As List(Of GRD_GRILLE_NNE)

            If WsLstGrilles Is Nothing Then
                Return Nothing
            End If

            Return (From Instance In WsLstGrilles Where Instance.Date_Valeur_ToDate <= CDate(ArgumentDate) _
                        Order By Instance.Date_Valeur_ToDate Descending, Instance.Rang Ascending).ToList

        End Function

        Public Function FicheEchelon(ByVal Index As Integer, ByVal ArgumentDate As String) As GRD_GRILLE_NNE

            If WsLstGrilles Is Nothing Then
                Return Nothing
            End If

            Dim LstTriee As List(Of GRD_GRILLE_NNE) = ListedesEchelons(ArgumentDate)
            If LstTriee Is Nothing Then
                Return Nothing
            End If

            Return (From instance In LstTriee Where instance.Rang = Index).FirstOrDefault()

        End Function

        Public Function Ajouter_Categorie(ByVal Fiche As GRD_CATEGORIE_NNE) As Integer

            If WsLstCategories Is Nothing Then
                WsLstCategories = New List(Of GRD_CATEGORIE_NNE)
            End If
            WsLstCategories.Add(Fiche)
            Return WsLstCategories.Count

        End Function

        Public Function FicheCategorie(ByVal Index As Integer) As GRD_CATEGORIE_NNE

            If WsLstCategories Is Nothing Then
                Return Nothing
            End If

            If Index < 0 Or Index > ListedesCategories.Count - 1 Then
                Return Nothing
            End If

            Return ListedesCategories.Item(Index)

        End Function

    End Class
End Namespace
