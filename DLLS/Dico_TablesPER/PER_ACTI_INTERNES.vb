﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ACTI_INTERNES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsActiviteInterne As String
        Private WsTypeActivite As String
        Private WsFonction As String
        Private WsDuree As Integer
        Private WsObservations As String
        Private WsRegimeActivite As String
        Private WsNatureActivite As String
        Private WsSecteur As String
        Private WsOrganismeCompetent As String
        '
        Private TsOrdreTri As Integer = 999     'Issu du Point de vue Commission
        Private TsRangFonction As Integer = 999 'Issu du Point de vue Commission

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ACTI_INTERNES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActiInternes
            End Get
        End Property

        Public Property ActiviteInterne() As String
            Get
                Return WsActiviteInterne
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsActiviteInterne = value
                    Case Else
                        WsActiviteInterne = Strings.Left(value, 200)
                End Select
                MyBase.Clef = WsActiviteInterne
            End Set
        End Property

        Public Property TypeActivite() As String
            Get
                Return WsTypeActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeActivite = value
                    Case Else
                        WsTypeActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Fonction() As String
            Get
                Return WsFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsFonction = value
                    Case Else
                        WsFonction = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Duree() As Integer
            Get
                Return WsDuree
            End Get
            Set(ByVal value As Integer)
                WsDuree = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Regime_Activite() As String
            Get
                Return WsRegimeActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegimeActivite = value
                    Case Else
                        WsRegimeActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Nature_Activite() As String
            Get
                Return WsNatureActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNatureActivite = value
                    Case Else
                        WsNatureActivite = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Secteur() As String
            Get
                Return WsSecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteur = value
                    Case Else
                        WsSecteur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property OrganismeCompetent() As String
            Get
                Return WsOrganismeCompetent
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsOrganismeCompetent = value
                    Case Else
                        WsOrganismeCompetent = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(ActiviteInterne & VI.Tild)
                Chaine.Append(TypeActivite & VI.Tild)
                Chaine.Append(Fonction & VI.Tild)
                Chaine.Append(Duree.ToString & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Regime_Activite & VI.Tild)
                Chaine.Append(Nature_Activite & VI.Tild)
                Chaine.Append(Secteur & VI.Tild)
                Chaine.Append(OrganismeCompetent)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                ActiviteInterne = TableauData(2)
                TypeActivite = TableauData(3)
                Fonction = TableauData(4)
                If TableauData(5) <> "" Then Duree = CInt(TableauData(5))
                MyBase.Date_de_Fin = TableauData(6)
                Observations = TableauData(7)
                Regime_Activite = TableauData(8)
                Nature_Activite = TableauData(9)
                Secteur = TableauData(10)
                OrganismeCompetent = TableauData(11)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Property DynaTri_Commission() As Integer
            Get
                Return TsOrdreTri
            End Get
            Set(ByVal value As Integer)
                TsOrdreTri = value
            End Set
        End Property

        Public Property DynaRang_Fonction() As Integer
            Get
                Return TsRangFonction
            End Get
            Set(ByVal value As Integer)
                TsRangFonction = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsActiviteInterne
                    Case 2
                        Return WsTypeActivite
                    Case 3
                        Return WsFonction
                    Case 4
                        Return WsDuree.ToString
                    Case 5
                        Return Date_de_Fin
                    Case 6
                        Return WsObservations
                    Case 7
                        Return WsRegimeActivite
                    Case 8
                        Return WsNatureActivite
                    Case 9
                        Return WsSecteur
                    Case 10
                        Return WsOrganismeCompetent
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

