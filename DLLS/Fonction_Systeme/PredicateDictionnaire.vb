﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.MetaModele.Outils
Namespace MetaModele
    Namespace Predicats
        Public Class PredicateDictionnaire
            Private WsIndex As Integer
            Private WsPvue As Integer
            Private WsNoObjet As Integer
            Private WsNoInfo As Integer
            Private WsIntitule As String
            '
            Public Function InformationParIndex(ByVal Source As FicheInfoDictionnaire) As Boolean
                Return Source.VIndex = WsIndex
            End Function

            Public Function InformationParIdeObjet(ByVal Source As FicheObjetDictionnaire) As Boolean
                Return Source.PointdeVue = WsPvue And Source.Objet = WsNoObjet
            End Function

            Public Function InformationParIdeInfo(ByVal Source As FicheInfoDictionnaire) As Boolean
                Return Source.PointdeVue = WsPvue And Source.Objet = WsNoObjet And Source.Information = WsNoInfo
            End Function

            Public Function InformationParIdeExperte(ByVal Source As FicheInfoExperte) As Boolean
                Return Source.PointdeVue = WsPvue And Source.Objet = WsNoObjet And Source.Information = WsNoInfo
            End Function

            Public Function InformationParIndexExperte(ByVal Source As FicheInfoExperte) As Boolean
                Return Source.Information = WsIndex
            End Function

            Public Function AllerAIntitule(ByVal Source As FicheInfoDictionnaire) As Boolean

                Dim Lg As Integer = WsIntitule.Length

                If Lg = 0 Then
                    Return True
                End If

                If WsIntitule = "*" Then
                    Return True
                End If

                If (Strings.Left(WsIntitule, 1) = "*") Then
                    Return Strings.Right(WsIntitule, Lg - 1).Contains(Source.Intitule)
                End If

                Return Strings.Left(Source.Intitule, Lg) = WsIntitule

            End Function

            Public Function AllerAIntituleExperte(ByVal Source As FicheInfoExperte) As Boolean

                Dim Lg As Integer = WsIntitule.Length

                If Lg = 0 Then
                    Return True
                End If

                If WsIntitule = "*" Then
                    Return True
                End If

                If (Strings.Left(WsIntitule, 1) = "*") Then
                    Return Strings.Right(WsIntitule, Lg - 1).Contains(Source.Intitule)
                End If

                Return Strings.Left(Source.Intitule, Lg) = WsIntitule

            End Function

            Public Sub New(ByVal Index As Integer)
                MyBase.New()
                WsIndex = Index
            End Sub

            Public Sub New(ByVal PointdeVue As Integer, ByVal NoObjet As Integer)
                MyBase.New()
                WsPvue = PointdeVue
                WsNoObjet = NoObjet
            End Sub

            Public Sub New(ByVal PointdeVue As Integer, ByVal NoObjet As Integer, ByVal NoInfo As Integer)
                MyBase.New()
                WsPvue = PointdeVue
                WsNoObjet = NoObjet
                WsNoInfo = NoInfo
            End Sub

            Public Sub New(ByVal Libelle As String)
                MyBase.New()

                If (Libelle Is Nothing) Then
                    WsIntitule = ""
                    Return
                End If

                WsIntitule = Libelle.Trim()
            End Sub
        End Class

    End Namespace
End Namespace

