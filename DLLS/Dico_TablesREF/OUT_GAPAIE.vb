﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class OUT_GAPAIE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsNumObjetApplicatif As Integer
        Private WsDate_Effet As String
        Private WsCode_Modification As String
        Private WsDate_Modification As String
        Private WsHeure_Modification As String
        Private WsUtilisateur As String
        Private WsCode_Traitement As String
        Private WsDate_Traitement As String
        Private WsHeure_Traitement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "OUT_GAPAIE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueInterface
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value
            End Set
        End Property

        Public Property ObjetApplicatif() As Integer
            Get
                Return WsNumObjetApplicatif
            End Get
            Set(ByVal value As Integer)
                WsNumObjetApplicatif = value
            End Set
        End Property

        Public Property Date_Effet() As String
            Get
                Return WsDate_Effet
            End Get
            Set(ByVal value As String)
                WsDate_Effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Effet
            End Set
        End Property

        Public Property Code_Modification() As String
            Get
                Return WsCode_Modification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsCode_Modification = value
                    Case Else
                        WsCode_Modification = Strings.Left(value, 1)
                End Select
            End Set
        End Property

        Public Property Date_Modification() As String
            Get
                Return WsDate_Modification
            End Get
            Set(ByVal value As String)
                WsDate_Modification = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Modification() As String
            Get
                Return WsHeure_Modification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure_Modification = value
                    Case Else
                        WsHeure_Modification = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Code_Traitement() As String
            Get
                Return WsCode_Traitement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsCode_Traitement = value
                    Case Else
                        WsCode_Traitement = Strings.Left(value, 1)
                End Select
            End Set
        End Property

        Public Property Date_Traitement() As String
            Get
                Return WsDate_Traitement
            End Get
            Set(ByVal value As String)
                WsDate_Traitement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Traitement() As String
            Get
                Return WsHeure_Traitement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure_Traitement = value
                    Case Else
                        WsHeure_Traitement = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(ObjetApplicatif.ToString & VI.Tild)
                Chaine.Append(Date_Effet & VI.Tild)
                Chaine.Append(Code_Modification & VI.Tild)
                Chaine.Append(Date_Modification & VI.Tild)
                Chaine.Append(Heure_Modification & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(Code_Traitement & VI.Tild)
                Chaine.Append(Date_Traitement & VI.Tild)
                Chaine.Append(Heure_Traitement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If IsNumeric(TableauData(1)) Then
                    Rang = CInt(TableauData(1))
                End If
                If IsNumeric(TableauData(2)) Then
                    ObjetApplicatif = CInt(TableauData(2))
                End If
                Date_Effet = TableauData(3)
                Code_Traitement = TableauData(4)
                Date_Modification = TableauData(5)
                Heure_Modification = TableauData(6)
                Utilisateur = TableauData(7)
                Code_Traitement = TableauData(8)
                Date_Traitement = TableauData(9)
                Heure_Traitement = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

