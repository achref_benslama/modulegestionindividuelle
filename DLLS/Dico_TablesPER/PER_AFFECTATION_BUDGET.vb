﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_AFFECTATION_BUDGET
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsSection As String
        Private WsSousSection As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_AFFECTATION_BUDGET"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPostebud 'Spécifique Secteur Privé
            End Get
        End Property

        Public Property SectionAnalytique() As String
            Get
                Return WsSection
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSection = value
                    Case Else
                        WsSection = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsSection
            End Set
        End Property

        Public Property SousSectionAnalytique() As String
            Get
                Return WsSousSection
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSousSection = value
                    Case Else
                        WsSousSection = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(SectionAnalytique & VI.Tild)
                Chaine.Append(SousSectionAnalytique & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                SectionAnalytique = TableauData(2)
                SousSectionAnalytique = TableauData(3)
                MyBase.Date_de_Fin = TableauData(4)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return SectionAnalytique
                    Case 2
                        Return SousSectionAnalytique
                    Case 3
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

