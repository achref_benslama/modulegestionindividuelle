﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports System.Text
Imports Virtualia.Ressources.Predicats
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetStageFormation
        Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION)

        Private WsListeStages As List(Of FOR_IDENTIFICATION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property NombredeStages() As Integer Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).NombredItems
            Get
                Return WsListeStages.Count
            End Get
        End Property

        Private Sub EnrichirListe(ByVal Ide As Integer)
            Dim FicheStage As FOR_IDENTIFICATION
            Dim FicheSession As FOR_SESSION

            '** 8  Liste des inscriptions
            Dim lstinsc As List(Of FOR_INSCRIPTIONS) = CreationCollectionVIRFICHE(Of FOR_INSCRIPTIONS).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstinsc.ForEach(Sub(f)
                                FicheStage = Fiche_Stage(f.Ide_Dossier)
                                If (FicheStage Is Nothing) Then
                                    Return
                                End If

                                FicheSession = FicheStage.Fiche_Session_Precise(f.Datedebut_formation)
                                If (FicheSession Is Nothing) Then
                                    Return
                                End If

                                FicheSession.Ajouter_Inscription(f)
                            End Sub)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeStages.Clear()
            Dim FicheStage As FOR_IDENTIFICATION

            '** 1  Liste de tous les logiciels
            WsListeStages.AddRange(CreationCollectionVIRFICHE(Of FOR_IDENTIFICATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListeStages.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les descriptifs
            Dim lstdesc As List(Of FOR_DESCRIPTIF) = CreationCollectionVIRFICHE(Of FOR_DESCRIPTIF).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstdesc.ForEach(Sub(f)
                                FicheStage = Fiche_Stage(f.Ide_Dossier)
                                If (FicheStage Is Nothing) Then
                                    Return
                                End If
                                FicheStage.FicheDescriptif = f
                            End Sub)

            '** 3  Liste de toutes les caractéristiques
            Dim lstcar As List(Of FOR_CARACTERISTIC) = CreationCollectionVIRFICHE(Of FOR_CARACTERISTIC).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstcar.ForEach(Sub(f)
                               FicheStage = Fiche_Stage(f.Ide_Dossier)
                               If (FicheStage Is Nothing) Then
                                   Return
                               End If
                               FicheStage.FicheCaracteristique = f
                           End Sub)

            '** 4  Liste de toutes les évaluations
            Dim lsteval As List(Of FOR_EVALUATION) = CreationCollectionVIRFICHE(Of FOR_EVALUATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lsteval.ForEach(Sub(f)
                                FicheStage = Fiche_Stage(f.Ide_Dossier)
                                If (FicheStage Is Nothing) Then
                                    Return
                                End If
                                FicheStage.FicheEvaluation = f
                            End Sub)

            '** 5  Liste de toutes les sessions
            Dim lstSession As List(Of FOR_SESSION) = CreationCollectionVIRFICHE(Of FOR_SESSION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstSession.ForEach(Sub(f)
                                   FicheStage = Fiche_Stage(f.Ide_Dossier)
                                   If (FicheStage Is Nothing) Then
                                       Return
                                   End If
                                   FicheStage.Ajouter_Session(f)
                               End Sub)

            '** 6  Liste de tous les coûts
            Dim lstcout As List(Of FOR_COUTS) = CreationCollectionVIRFICHE(Of FOR_COUTS).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstcout.ForEach(Sub(f)
                                FicheStage = Fiche_Stage(f.Ide_Dossier)
                                If (FicheStage Is Nothing) Then
                                    Return
                                End If
                                FicheStage.Ajouter_Cout(f)
                            End Sub)

            '** 7  Liste de toutes les factures
            Dim lstfact As List(Of FOR_FACTURE) = CreationCollectionVIRFICHE(Of FOR_FACTURE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstfact.ForEach(Sub(f)
                                FicheStage = Fiche_Stage(f.Ide_Dossier)
                                If (FicheStage Is Nothing) Then
                                    Return
                                End If
                                FicheStage.Ajouter_Facture(f)
                            End Sub)

            '** 9  Liste de tous les intervenants
            Dim lstint As List(Of FOR_INTERVENANT) = CreationCollectionVIRFICHE(Of FOR_INTERVENANT).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstint.ForEach(Sub(f)
                               FicheStage = Fiche_Stage(f.Ide_Dossier)
                               If (FicheStage Is Nothing) Then
                                   Return
                               End If
                               FicheStage.Ajouter_Intervenant(f)
                           End Sub)
        End Sub

        Public Function Fiche_Stage(ByVal IdeDossier As Integer) As FOR_IDENTIFICATION Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).GetFiche
            Return WsListeStages.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Stage(ByVal Intitule As String) As FOR_IDENTIFICATION Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).GetFiche
            Return WsListeStages.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Function ListeDesStages_Alpha_Etablissement(ByVal Lettre As String, ByVal Etablissement As String) As List(Of FOR_IDENTIFICATION)
            If WsListeStages Is Nothing Then
                Return Nothing
            End If

            Dim Predicat As New PredicateUniversel(Lettre, VI.PointdeVue.PVueFormation)
            Dim LstRes As List(Of FOR_IDENTIFICATION)
            Dim LstTri As List(Of FOR_IDENTIFICATION) = Nothing

            LstRes = WsListeStages.FindAll(AddressOf Predicat.RechercherLettre)

            If LstRes Is Nothing Then
                Return Nothing
            End If

            Select Case Etablissement
                Case ""
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Ide_Dossier > 0 _
                           Order By instance.Intitule Ascending).ToList()

                Case Else
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Etablissement = Etablissement _
                           Order By instance.Intitule Ascending).ToList()

            End Select

            Return LstTri
        End Function

        Public Function ListeDesStages_Alpha_Plan(ByVal Lettre As String, ByVal Plan As String) As List(Of FOR_IDENTIFICATION)
            If WsListeStages Is Nothing Then
                Return Nothing
            End If

            Dim Predicat As New PredicateUniversel(Lettre, VI.PointdeVue.PVueFormation)
            Dim LstRes As List(Of FOR_IDENTIFICATION)
            Dim LstTri As List(Of FOR_IDENTIFICATION) = Nothing

            LstRes = WsListeStages.FindAll(AddressOf Predicat.RechercherLettre)

            If LstRes Is Nothing Then
                Return Nothing
            End If

            Select Case Plan
                Case ""
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Ide_Dossier > 0 _
                           Order By instance.Intitule Ascending).ToList()

                Case Else
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.PlandeFormation = Plan _
                           Order By instance.Intitule Ascending).ToList()

            End Select

            Return LstTri
        End Function

        Public Function ListeDesStages(ByVal CritereSel As String, ByVal Valeur As String, Optional ByVal CritereTri As String = "") As List(Of FOR_IDENTIFICATION)
            If WsListeStages Is Nothing Then
                Return Nothing
            End If

            Dim LstRes As List(Of FOR_IDENTIFICATION) = Nothing

            If CritereSel = "" Then
                Select Case CritereTri
                    Case "" 'Idem Plan
                        LstRes = (From instance In WsListeStages Select instance _
                                           Order By instance.PlandeFormation Ascending, _
                                           instance.Domaine Ascending, _
                                           instance.Theme Ascending, instance.Intitule Ascending).ToList()
                    Case "Domaine"
                        LstRes = (From instance In WsListeStages Select instance _
                                            Order By instance.Domaine Ascending, _
                                            instance.Intitule Ascending).ToList()
                    Case "Intitulé"
                        Return WsListeStages.OrderBy(Function(m) m.Intitule).ToList()
                    Case Is = "Organisme"
                        LstRes = (From instance In WsListeStages Select instance _
                                            Order By instance.Organisme Ascending, _
                                            instance.Intitule Ascending).ToList()
                    Case Is = "Plan" 'Standard
                        LstRes = (From instance In WsListeStages Select instance _
                                           Order By instance.PlandeFormation Ascending, _
                                           instance.Domaine Ascending, _
                                           instance.Theme Ascending, instance.Intitule Ascending).ToList()
                    Case Is = "Reference"
                        Return WsListeStages.OrderBy(Function(m) m.ReferenceduStage).ToList()
                    Case Is = "Thème"
                        LstRes = (From instance In WsListeStages Select instance _
                                            Order By instance.Theme Ascending, _
                                            instance.Intitule Ascending).ToList()
                End Select
            End If

            Select Case CritereSel
                Case Is = "Domaine"
                    Select Case CritereTri
                        Case "" 'Intitulé
                            LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Domaine = Valeur _
                                            Order By instance.Intitule Ascending).ToList()
                        Case Else
                            Select Case CritereTri
                                Case "Intitulé"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Domaine = Valeur _
                                            Order By instance.Intitule Ascending).ToList()
                                Case Is = "Plan" 'Standard
                                    LstRes = (From instance In WsListeStages Select instance _
                                                       Where instance.Domaine = Valeur _
                                                       Order By instance.PlandeFormation Ascending, _
                                                       instance.Domaine Ascending, _
                                                       instance.Theme Ascending, instance.Intitule Ascending).ToList()
                                Case Is = "Reference"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Domaine = Valeur _
                                            Order By instance.ReferenceduStage Ascending).ToList()
                            End Select
                    End Select
                Case Is = "Organisme"
                    Select Case CritereTri
                        Case Is = "" 'Intitulé
                            LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Organisme = Valeur _
                                            Order By instance.Intitule Ascending).ToList()
                        Case Else
                            Select Case CritereTri
                                Case "Intitulé"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Organisme = Valeur _
                                            Order By instance.Intitule Ascending).ToList()
                                Case Is = "Plan" 'Standard
                                    LstRes = (From instance In WsListeStages Select instance _
                                                       Where instance.Organisme = Valeur _
                                                       Order By instance.PlandeFormation Ascending, _
                                                       instance.Domaine Ascending, _
                                                       instance.Theme Ascending, instance.Intitule Ascending).ToList()
                                Case Is = "Reference"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.Organisme = Valeur _
                                            Order By instance.ReferenceduStage Ascending).ToList()
                            End Select
                    End Select
                Case Is = "Plan"
                    Select Case CritereTri
                        Case "" 'Idem Plan
                            LstRes = (From instance In WsListeStages Select instance _
                                                       Where instance.PlandeFormation = Valeur _
                                                       Order By instance.Domaine Ascending, _
                                                       instance.Theme Ascending, instance.Intitule Ascending).ToList()
                        Case Else
                            Select Case CritereTri
                                Case "Intitulé"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.PlandeFormation = Valeur _
                                            Order By instance.Intitule Ascending).ToList()
                                Case Is = "Plan" 'Standard
                                    LstRes = (From instance In WsListeStages Select instance _
                                                       Where instance.PlandeFormation = Valeur _
                                                       Order By instance.Domaine Ascending, _
                                                       instance.Theme Ascending, instance.Intitule Ascending).ToList()
                                Case Is = "Reference"
                                    LstRes = (From instance In WsListeStages Select instance _
                                            Where instance.PlandeFormation = Valeur _
                                            Order By instance.ReferenceduStage Ascending).ToList()
                            End Select
                    End Select
            End Select

            If LstRes Is Nothing Then
                Return WsListeStages
            End If

            Return LstRes

        End Function

        Public Sub EnrichirItem(ByVal Ide As Integer)
            EnrichirListe(Ide)
        End Sub

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).SupprimerItem
            Dim FicheStage As FOR_IDENTIFICATION = Fiche_Stage(Ide)
            If FicheStage IsNot Nothing Then
                WsListeStages.Remove(FicheStage)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of FOR_IDENTIFICATION).Actualiser
            WsListeStages.Clear()
            ConstituerListe(0)
        End Sub

        Public Function GetListeAvecPredicat(funcpredicat As Func(Of FOR_IDENTIFICATION, Boolean), Optional A_Actualiser As Boolean = False) As List(Of FOR_IDENTIFICATION)

            If (A_Actualiser) Then
                Actualiser()
            End If

            Dim predicat As Func(Of FOR_IDENTIFICATION, Boolean) = Function(f)
                                                                       If (funcpredicat Is Nothing) Then
                                                                           Return True
                                                                       End If
                                                                       Return funcpredicat(f)
                                                                   End Function

            Return (From it In WsListeStages Where predicat(it) Select it Order By it.PlandeFormation, it.Domaine, it.Theme, it.Intitule).ToList()
        End Function

        Public Sub New(ByVal NomUtilisateur As String)
            Me.New(NomUtilisateur, 0)
        End Sub
        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeStages = New List(Of FOR_IDENTIFICATION)

            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace



