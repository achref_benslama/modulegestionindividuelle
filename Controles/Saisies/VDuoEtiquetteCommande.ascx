﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VDuoEtiquetteCommande" Codebehind="VDuoEtiquetteCommande.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<style type="text/css">
    .form__group {
  white-space: nowrap;
  text-overflow: ellipsis;
  position:relative;

 
}

    </style>


<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0" CssClass="form__group">
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel ID="Etiquette" runat="server" Font-Size="Medium"
                    style="margin-bottom: 5px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="DonneeTable" runat="server" Height="22px" Width="300px" Visible="true" CommandName="Ref" OnCommand="DonneeTable_Commande"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#25355A"
                    Font-Bold="false" Font-Names="Calibri" Font-Size="Medium" Font-Italic="false"
                    style="padding-left: 1px; margin-top: 2px; text-align: left" > 
            </asp:Button>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>

