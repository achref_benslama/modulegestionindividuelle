﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PerAdministratifPublic
    
    '''<summary>
    '''Contrôle CadreGeneralAdm.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneralAdm As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreGeneral As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreInfosAdm.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfosAdm As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiPerStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerStatut As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStatut As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDS00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDS00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoDS08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDS08 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabS01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabS01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabS02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabS02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle CadreArrete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreArrete As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDS03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDS03 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoHS04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHS04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiUploadContrat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadContrat As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxUploadContrat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxUploadContrat As Global.AjaxControlToolkit.AjaxFileUpload
    
    '''<summary>
    '''Contrôle EtiPerPosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerPosition As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadrePosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePosition As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDP00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDP00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoDP07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDP07 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabP01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabP05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP05 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabP11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP11 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoHP02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP02 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP12 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle DontabP08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP08 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle EtiArrete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiArrete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoDP03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDP03 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoHP04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiDecret.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDecret As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreJO.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreJO As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDP09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDP09 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoDP10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDP10 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle EtiUploadPosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadPosition As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxUploadPosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxUploadPosition As Global.AjaxControlToolkit.AjaxFileUpload
    
    '''<summary>
    '''Contrôle EtiPerGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGrade As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDG00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDG00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabG01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG11 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle CadreIndices.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreIndices As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoHG03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHG03 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHG04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHG04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHG23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHG23 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle DontabG05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG05 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG13 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG12 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG06 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabG07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabG07 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle Reliquat08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Reliquat08 As Global.Virtualia.Net.Controles_VReliquatAnciennete
    
    '''<summary>
    '''Contrôle EtiArreteGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiArreteGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoDG09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDG09 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoHG10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHG10 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiDecretGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDecretGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoDG22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDG22 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle EtiUploadGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiUploadGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle AjaxUploadGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents AjaxUploadGrade As Global.AjaxControlToolkit.AjaxFileUpload
    
    '''<summary>
    '''Contrôle CellReference.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellReference As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupReferentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupReferentiel As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefVirtualia As Global.Virtualia.Net.VReferentiel
    
    '''<summary>
    '''Contrôle HPopupRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRef As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle CellRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRefGrade As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupRefGrade As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefGradePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefGradePopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefGradeGrilles.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefGradeGrilles As Global.Virtualia.Net.VReferentielGrades
    
    '''<summary>
    '''Contrôle HPopupRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRefGrade As Global.System.Web.UI.WebControls.HiddenField
End Class
