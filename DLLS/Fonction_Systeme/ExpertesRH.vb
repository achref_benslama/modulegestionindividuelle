Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions

Namespace MetaModele
    Namespace Expertes
        Public Class ExpertesRH
            Inherits List(Of MetaModele.Expertes.PointdevueExpert)
            Private Const SysFicExperte As String = "VirtualiaExpertes"
            Private WsRepertoire As String

            Public ReadOnly Property NomCompletFichierXml() As String
                Get
                    Return WsRepertoire & SysFicExperte & ".Xml"
                End Get
            End Property

            Public ReadOnly Property NombredePointdeVue() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As MetaModele.Expertes.PointdevueExpert
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public Sub New(ByVal LstPointdeVue As List(Of MetaModele.Expertes.PointdevueExpert))
                If LstPointdeVue Is Nothing Then
                    Exit Sub
                End If
                Dim IndiceK As Integer
                Dim IPvue As Integer

                For IndiceK = 0 To LstPointdeVue.Count - 1
                    IPvue = CreerUnPointdeVue()
                    Me(IPvue) = LstPointdeVue(IndiceK)
                    Me(IPvue).VIndex = IPvue
                Next IndiceK
            End Sub

            Public Sub New()
                Dim FicXML As FichierXML
                Dim LireXML As LectureXML
                Dim LireXMLObjet As LectureXML
                Dim LireXMLInfo As LectureXML
                Dim Element As String
                Dim IPvue As Integer
                Dim IObjet As Integer
                Dim IInfo As Integer
                Dim BalisePvue As String = "VirtualiaExpertes~PointdeVue"
                Dim BaliseObjet As String = "VirtualiaExpertes~PointdeVue~Objet"
                Dim BaliseInfo As String = "VirtualiaExpertes~PointdeVue~Objet~Information"
                Dim BaliseInfoRef As String = "VirtualiaExpertes~PointdeVue~Objet~Information~InfoReference"
                Dim BaliseInfoAccess As String = "VirtualiaExpertes~PointdeVue~Objet~Information~Accessibilite"

                Dim ItemPtdeVue As PointdevueExpert = Nothing
                Dim ItemObjet As ObjetExpert = Nothing
                Dim ItemInfo As InformationExperte = Nothing

                WsRepertoire = VI.DossierVirtualiaService("Modele")

                FicXML = New FichierXML(WsRepertoire & SysFicExperte & ".Xsd")
                FicXML.FichierXML() = WsRepertoire & SysFicExperte & ".Xml"

                'Les points de Vue
                LireXML = New LectureXML(FicXML)
                LireXML.SelectData(BalisePvue, "Numero")
                Do Until LireXML.EndOfData

                    Element = LireXML.GetData()
                    IPvue = CreerUnPointdeVue()
                    ItemPtdeVue = Me(IPvue)
                    ItemPtdeVue.Numero = CInt(Val(Element))
                    ItemPtdeVue.Intitule = ""

                    'Objets
                    LireXMLObjet = New LectureXML(FicXML)
                    LireXMLObjet.RattacherAuPere(LireXML.PointeurDebut, LireXML.PointeurFin)
                    LireXMLObjet.SelectData(BaliseObjet, "Numero")

                    Do Until LireXMLObjet.EndOfData
                        Element = LireXMLObjet.GetData()
                        IObjet = ItemPtdeVue.CreerUnObjet()
                        ItemObjet = ItemPtdeVue.Item(IObjet)
                        ItemObjet.Numero = CInt(Val(Element))

                        'Informations
                        LireXMLInfo = New LectureXML(FicXML)
                        LireXMLInfo.RattacherAuPere(LireXMLObjet.PointeurDebut, LireXMLObjet.PointeurFin)
                        LireXMLInfo.SelectData(BaliseInfo, "Numero")

                        Do Until LireXMLInfo.EndOfData
                            Element = LireXMLInfo.GetData()
                            IInfo = ItemObjet.CreerUneInformation()
                            ItemInfo = ItemObjet.Item(IInfo)
                            ItemInfo.Numero = CInt(Val(Element))
                            ItemInfo.Intitule = LireXMLInfo.GetDataVoisin(BaliseInfo, "Intitule", "After")
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Nature", "After")
                            If Element <> "" Then
                                ItemInfo.VNature = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Format", "After")
                            If Element <> "" Then
                                ItemInfo.Format = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Mode", "After")
                            If Element <> "" Then
                                ItemInfo.CategorieDLL = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Algorithme", "After")
                            If Element <> "" Then
                                ItemInfo.Algorithme = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "FormeResultat", "After")
                            If Element <> "" Then
                                ItemInfo.FormeduResultat = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Exploitation", "After")
                            If Element <> "" Then
                                ItemInfo.Exploitation = CInt(Val(Element))
                            End If
                            ItemInfo.Description = LireXMLInfo.GetDataVoisin(BaliseInfo, "Description", "After")
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoRef, "No1", "After")
                            If Element <> "" Then
                                ItemInfo.InfoReference(0) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoRef, "No2", "After")
                            If Element <> "" Then
                                ItemInfo.InfoReference(1) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoRef, "No3", "After")
                            If Element <> "" Then
                                ItemInfo.InfoReference(2) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Individuelle", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(2) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "MultiCriteres", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(3) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Statistiques", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(4) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Impression", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(5) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Export", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(6) = CInt(Val(Element))
                            End If
                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Import", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(7) = CInt(Val(Element))
                            End If
                            LireXMLInfo.FindNextData()
                        Loop
                        LireXMLInfo = Nothing
                        LireXMLObjet.FindNextData()
                    Loop
                    LireXMLObjet = Nothing
                    Element = ""
                    LireXML.FindNextData()
                Loop

            End Sub

            Private Function CreerUnPointdeVue() As Integer
                Add(New MetaModele.Expertes.PointdevueExpert(Me))
                Return Count - 1
            End Function

        End Class
    End Namespace
End Namespace
