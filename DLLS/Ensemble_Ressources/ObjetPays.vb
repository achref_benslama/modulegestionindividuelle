﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPays
        Implements IGestionVirFiche(Of PAYS_DESCRIPTION)

        Private WsListePays As List(Of PAYS_DESCRIPTION) = New List(Of PAYS_DESCRIPTION)()
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesPays(ByVal Lettre As String) As List(Of PAYS_DESCRIPTION)
            Get
                If WsListePays Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVuePays)
                Dim LstRes As List(Of PAYS_DESCRIPTION)
                Dim LstTri As List(Of PAYS_DESCRIPTION) = Nothing

                LstRes = WsListePays.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance _
                       Where instance.Ide_Dossier > 0 _
                       Order By instance.Intitule Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesPays() As List(Of PAYS_DESCRIPTION)
            Get
                If WsListePays Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of PAYS_DESCRIPTION) = Nothing
                LstRes = (From instance In WsListePays Select instance _
                                               Order By instance.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredePays() As Integer Implements IGestionVirFiche(Of PAYS_DESCRIPTION).NombredItems
            Get
                Return WsListePays.Count
            End Get
        End Property

        Public Function Fiche_Pays(ByVal IdeDossier As Integer) As PAYS_DESCRIPTION Implements IGestionVirFiche(Of PAYS_DESCRIPTION).GetFiche
            Return WsListePays.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Pays(ByVal Intitule As String) As PAYS_DESCRIPTION Implements IGestionVirFiche(Of PAYS_DESCRIPTION).GetFiche
            Return WsListePays.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of PAYS_DESCRIPTION).Actualiser
            WsListePays.Clear()
            WsListePays = Nothing
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListePays.Clear()
            Dim FichePays As PAYS_DESCRIPTION

            '** 1  Liste de tous les Pays
            WsListePays.AddRange(CreationCollectionVIRFICHE(Of PAYS_DESCRIPTION).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListePays.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les JF Fixes
            Dim lstfix As List(Of PAYS_JOURFERIE_FIXE) = CreationCollectionVIRFICHE(Of PAYS_JOURFERIE_FIXE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstfix.ForEach(Sub(f)
                               FichePays = Fiche_Pays(f.Ide_Dossier)

                               If (FichePays Is Nothing) Then
                                   Return
                               End If

                               FichePays.Ajouter_JF_Fixe(f)
                           End Sub)

            '** 3  Liste de tous les JF Mobiles
            'Dim lstmobil As List(Of PAYS_JOURFERIE_MOBILE) = CreationCollectionVIRFICHE(Of PAYS_JOURFERIE_MOBILE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            'lstmobil.ForEach(Sub(f)
            '                     FichePays = Fiche_Pays(f.Ide_Dossier)

            '                     If (FichePays Is Nothing) Then
            '                         Return
            '                     End If

            '                     FichePays.Ajouter_JF_Mobile(f)
            '                 End Sub)

            ''** 4  Liste de tous les droits à congé
            'Dim lstdroit As List(Of PAYS_DROITS_CONGE) = CreationCollectionVIRFICHE(Of PAYS_DROITS_CONGE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            'lstdroit.ForEach(Sub(f)
            '                     FichePays = Fiche_Pays(f.Ide_Dossier)

            '                     If (FichePays Is Nothing) Then
            '                         Return
            '                     End If

            '                     FichePays.Ajouter_DroitConge(f)
            '                 End Sub)

            ''** 5  Liste de toutes les prises en charge des voyages
            ''Dim lstvoyag As List(Of PAYS_VOYAGE_CONGE) = CreationCollectionVIRFICHE(Of PAYS_VOYAGE_CONGE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            ''lstvoyag.ForEach(Sub(f)
            ''                     FichePays = Fiche_Pays(f.Ide_Dossier)

            ''                     If (FichePays Is Nothing) Then
            ''                         Return
            ''                     End If

            ''                     FichePays.Ajouter_VoyageConge(f)
            ''                 End Sub)

            ''** 6  Liste de tous les barèmes
            ''Dim lstbarem As List(Of PAYS_MIS_ETRANGER) = CreationCollectionVIRFICHE(Of PAYS_MIS_ETRANGER).GetCollection(WsNomUtilisateur, VirServiceServeur)
            ''lstbarem.ForEach(Sub(f)
            ''                     FichePays = Fiche_Pays(f.Ide_Dossier)

            ''                     If (FichePays Is Nothing) Then
            ''                         Return
            ''                     End If

            ''                     FichePays.Ajouter_Bareme(f)
            ''                 End Sub)

            ''** 7  Liste de tous les taux de change
            ''Dim lstbtaux As List(Of PAYS_TAUX_CHANGE) = CreationCollectionVIRFICHE(Of PAYS_TAUX_CHANGE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            ''lstbtaux.ForEach(Sub(f)
            ''                     FichePays = Fiche_Pays(f.Ide_Dossier)

            ''                     If (FichePays Is Nothing) Then
            ''                         Return
            ''                     End If

            ''                     FichePays.Ajouter_TauxChange(f)
            ''                 End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            Call ConstituerListe()
        End Sub

    End Class
End Namespace
