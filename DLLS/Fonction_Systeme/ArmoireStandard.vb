Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Armoire
        Public Class ArmoireStandard
            Private ReadOnly CollArmoire As Virtualia.Systeme.MetaModele.Armoire.ArmoiresRH
            Private WsIndex As Integer
            Private WsCategorie As String
            Private WsParticularite As String
            Private WsPtdeVue As Integer
            Private WsSelObjet(0) As Integer
            Private WsSelInfo(0) As Integer
            Private WsSelCondition(0) As String
            Private WsExtObjet(0) As Integer
            Private WsExtInfo(0) As Integer
            Private WsExtTri(0) As Integer
            Private WsExtRupture(0) As Integer

            Public ReadOnly Property VParent() As Virtualia.Systeme.MetaModele.Armoire.ArmoiresRH
                Get
                    Return CollArmoire
                End Get
            End Property

            Public ReadOnly Property NombredeRequetes() As Integer
                Get
                    Select Case ObjetSelection(1)
                        Case Is > 0
                            Return 2
                        Case Else
                            Return 1
                    End Select
                End Get
            End Property

            Public ReadOnly Property NombredExtraits() As Integer
                Get
                    Dim IndiceK As Integer
                    For IndiceK = 0 To WsExtObjet.Count - 1
                        Select Case WsExtObjet(IndiceK)
                            Case Is = 0
                                Exit For
                        End Select
                    Next IndiceK
                    Return IndiceK
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property Categorie() As String
                Get
                    Return WsCategorie
                End Get
                Set(ByVal value As String)
                    WsCategorie = value
                End Set
            End Property

            Public Property Particularite() As String
                Get
                    Return WsParticularite
                End Get
                Set(ByVal value As String)
                    WsParticularite = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPtdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPtdeVue = value
                End Set
            End Property

            Public Property ObjetSelection(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsSelObjet.Count - 1
                            Return WsSelObjet(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsSelObjet.Count - 1
                            ReDim Preserve WsSelObjet(Index + 1)
                    End Select
                    WsSelObjet(Index) = value
                End Set
            End Property

            Public Property InfoSelection(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsSelInfo.Count - 1
                            Return WsSelInfo(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsSelInfo.Count - 1
                            ReDim Preserve WsSelInfo(Index + 1)
                    End Select
                    WsSelInfo(Index) = value
                End Set
            End Property

            Public Property ConditionSelection(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 0 To WsSelCondition.Count - 1
                            Return WsSelCondition(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
                Set(ByVal value As String)
                    Select Case Index
                        Case Is > WsSelCondition.Count - 1
                            ReDim Preserve WsSelCondition(Index + 1)
                    End Select
                    WsSelCondition(Index) = value
                End Set
            End Property

            Public Property ObjetExtrait(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsExtObjet.Count - 1
                            Return WsExtObjet(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsExtObjet.Count - 1
                            ReDim Preserve WsExtObjet(Index + 1)
                    End Select
                    WsExtObjet(Index) = value
                End Set
            End Property

            Public Property InfoExtraite(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsExtInfo.Count - 1
                            Return WsExtInfo(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsExtInfo.Count - 1
                            ReDim Preserve WsExtInfo(Index + 1)
                    End Select
                    WsExtInfo(Index) = value
                End Set
            End Property

            Public Property TriExtrait(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsExtTri.Count - 1
                            Return WsExtTri(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsExtTri.Count - 1
                            ReDim Preserve WsExtTri(Index + 1)
                    End Select
                    WsExtTri(Index) = value
                End Set
            End Property

            Public Property NiveauHierarchique(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsExtRupture.Count - 1
                            Return WsExtRupture(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case Is > WsExtRupture.Count - 1
                            ReDim Preserve WsExtRupture(Index + 1)
                    End Select
                    WsExtRupture(Index) = value
                End Set
            End Property

            Public Sub New(ByVal host As Virtualia.Systeme.MetaModele.Armoire.ArmoiresRH)
                CollArmoire = host
            End Sub
        End Class
    End Namespace
End Namespace