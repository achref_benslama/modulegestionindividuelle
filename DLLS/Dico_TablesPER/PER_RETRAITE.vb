﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_RETRAITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsDate_Demande_Depart As String
        Private WsMotif As String
        Private WsDate_Demande_Invalidite As String
        Private WsDate_Decision_ComiteMedical As String
        Private WsDate_Demande_Conjoint As String
        Private WsDate_Decision_PensionReversion As String
        Private WsLimiteAgePersonnelle As Integer
        Private WsReference_Texte_LimiteAge As String
        Private WsDate_Debut_Prolongation As String
        Private WsDate_Fin_Prolongation As String
        Private WsType_Prolongation As String
        Private WsDuree_Liquidation_RachatEtudes As Integer
        Private WsDate_Decision_Liquidation_RachatEtudes As String
        Private WsDate_Debut_Liquidation_RachatEtudes As String
        Private WsDate_Fin_Liquidation_RachatEtudes As String
        Private WsDuree_Assurance_RachatEtudes As Integer
        Private WsDate_Decision_Assurance_RachatEtudes As String
        Private WsDate_Debut_Assurance_RachatEtudes As String
        Private WsDate_Fin_Assurance_RachatEtudes As String
        Private WsDuree_Rachat_RachatEtudes As Integer
        Private WsDate_Decision_Rachat_RachatEtudes As String
        Private WsDate_Debut_Rachat_RachatEtudes As String
        Private WsDate_Fin_Rachat_RachatEtudes As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_RETRAITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaRetraite
            End Get
        End Property

        Public Property Date_Demande_Depart() As String
            Get
                Return WsDate_Demande_Depart
            End Get
            Set(ByVal value As String)
                WsDate_Demande_Depart = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Motif() As String
            Get
                Return WsMotif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotif = value
                    Case Else
                        WsMotif = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsMotif
            End Set
        End Property

        Public Property Date_Demande_Invalidite() As String
            Get
                Return WsDate_Demande_Invalidite
            End Get
            Set(ByVal value As String)
                WsDate_Demande_Invalidite = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Decision_ComiteMedical() As String
            Get
                Return WsDate_Decision_ComiteMedical
            End Get
            Set(ByVal value As String)
                WsDate_Decision_ComiteMedical = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Demande_Conjoint() As String
            Get
                Return WsDate_Demande_Conjoint
            End Get
            Set(ByVal value As String)
                WsDate_Demande_Conjoint = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Decision_PensionReversion() As String
            Get
                Return WsDate_Decision_PensionReversion
            End Get
            Set(ByVal value As String)
                WsDate_Decision_PensionReversion = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property LimiteAgePersonnelle() As Integer
            Get
                Return WsLimiteAgePersonnelle
            End Get
            Set(ByVal value As Integer)
                WsLimiteAgePersonnelle = value
            End Set
        End Property

        Public Property Reference_Texte_LimiteAge() As String
            Get
                Return WsReference_Texte_LimiteAge
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsReference_Texte_LimiteAge = value
                    Case Else
                        WsReference_Texte_LimiteAge = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Debut_Prolongation() As String
            Get
                Return WsDate_Debut_Prolongation
            End Get
            Set(ByVal value As String)
                WsDate_Debut_Prolongation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Prolongation() As String
            Get
                Return WsDate_Fin_Prolongation
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Prolongation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Type_Prolongation() As String
            Get
                Return WsType_Prolongation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsType_Prolongation = value
                    Case Else
                        WsType_Prolongation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Duree_Liquidation_RachatEtudes() As Integer
            Get
                Return WsDuree_Liquidation_RachatEtudes
            End Get
            Set(ByVal value As Integer)
                WsDuree_Liquidation_RachatEtudes = value
            End Set
        End Property

        Public Property Date_Decision_Liquidation_RachatEtudes() As String
            Get
                Return WsDate_Decision_Liquidation_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Decision_Liquidation_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Debut_Liquidation_RachatEtudes() As String
            Get
                Return WsDate_Debut_Liquidation_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Debut_Liquidation_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Liquidation_RachatEtudes() As String
            Get
                Return WsDate_Fin_Liquidation_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Liquidation_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Duree_Assurance_RachatEtudes() As Integer
            Get
                Return WsDuree_Assurance_RachatEtudes
            End Get
            Set(ByVal value As Integer)
                WsDuree_Assurance_RachatEtudes = value
            End Set
        End Property

        Public Property Date_Decision_Assurance_RachatEtudes() As String
            Get
                Return WsDate_Decision_Assurance_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Decision_Assurance_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Debut_Assurance_RachatEtudes() As String
            Get
                Return WsDate_Debut_Assurance_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Debut_Assurance_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Assurance_RachatEtudes() As String
            Get
                Return WsDate_Fin_Assurance_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Assurance_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Duree_Rachat_RachatEtudes() As Integer
            Get
                Return WsDuree_Rachat_RachatEtudes
            End Get
            Set(ByVal value As Integer)
                WsDuree_Rachat_RachatEtudes = value
            End Set
        End Property

        Public Property Date_Decision_Rachat_RachatEtudes() As String
            Get
                Return WsDate_Decision_Rachat_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Decision_Rachat_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Debut_Rachat_RachatEtudes() As String
            Get
                Return WsDate_Debut_Rachat_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Debut_Rachat_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Rachat_RachatEtudes() As String
            Get
                Return WsDate_Fin_Rachat_RachatEtudes
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Rachat_RachatEtudes = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Date_Demande_Depart & VI.Tild)
                Chaine.Append(Motif & VI.Tild)
                Chaine.Append(Date_Demande_Invalidite & VI.Tild)
                Chaine.Append(Date_Decision_ComiteMedical & VI.Tild)
                Chaine.Append(Date_Demande_Conjoint & VI.Tild)
                Chaine.Append(Date_Decision_PensionReversion & VI.Tild)
                Chaine.Append(LimiteAgePersonnelle.ToString & VI.Tild)
                Chaine.Append(Reference_Texte_LimiteAge & VI.Tild)
                Chaine.Append(Date_Debut_Prolongation & VI.Tild)
                Chaine.Append(Date_Fin_Prolongation & VI.Tild)
                Chaine.Append(Type_Prolongation & VI.Tild)
                Chaine.Append(Duree_Liquidation_RachatEtudes.ToString & VI.Tild)
                Chaine.Append(Date_Decision_Liquidation_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Debut_Liquidation_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Fin_Liquidation_RachatEtudes & VI.Tild)
                Chaine.Append(Duree_Assurance_RachatEtudes.ToString & VI.Tild)
                Chaine.Append(Date_Decision_Assurance_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Debut_Assurance_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Fin_Assurance_RachatEtudes & VI.Tild)
                Chaine.Append(Duree_Rachat_RachatEtudes.ToString & VI.Tild)
                Chaine.Append(Date_Decision_Rachat_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Debut_Rachat_RachatEtudes & VI.Tild)
                Chaine.Append(Date_Fin_Rachat_RachatEtudes)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 24 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Demande_Depart = TableauData(1)
                Motif = TableauData(2)
                Date_Demande_Invalidite = TableauData(3)
                Date_Decision_ComiteMedical = TableauData(4)
                Date_Demande_Conjoint = TableauData(5)
                Date_Decision_PensionReversion = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                LimiteAgePersonnelle = CInt(TableauData(7))
                Reference_Texte_LimiteAge = TableauData(8)
                Date_Debut_Prolongation = TableauData(9)
                Date_Fin_Prolongation = TableauData(10)
                Type_Prolongation = TableauData(11)
                If TableauData(12) = "" Then TableauData(12) = "0"
                Duree_Liquidation_RachatEtudes = CInt(TableauData(12))
                Date_Decision_Liquidation_RachatEtudes = TableauData(13)
                Date_Debut_Liquidation_RachatEtudes = TableauData(14)
                Date_Fin_Liquidation_RachatEtudes = TableauData(15)
                If TableauData(16) = "" Then TableauData(16) = "0"
                Duree_Assurance_RachatEtudes = CInt(TableauData(16))
                Date_Decision_Assurance_RachatEtudes = TableauData(17)
                Date_Debut_Assurance_RachatEtudes = TableauData(18)
                Date_Fin_Assurance_RachatEtudes = TableauData(19)
                If TableauData(20) = "" Then TableauData(20) = "0"
                Duree_Rachat_RachatEtudes = CInt(TableauData(20))
                Date_Decision_Rachat_RachatEtudes = TableauData(21)
                Date_Debut_Rachat_RachatEtudes = TableauData(22)
                Date_Fin_Rachat_RachatEtudes = TableauData(23)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsDate_Demande_Depart
                    Case 2
                        Return WsMotif
                    Case 3
                        Return WsDate_Demande_Invalidite
                    Case 4
                        Return WsDate_Decision_ComiteMedical
                    Case 5
                        Return WsDate_Demande_Conjoint
                    Case 6
                        Return WsDate_Decision_PensionReversion
                    Case 7
                        Return WsLimiteAgePersonnelle.ToString
                    Case 8
                        Return WsReference_Texte_LimiteAge
                    Case 9
                        Return WsDate_Debut_Prolongation
                    Case 10
                        Return WsDate_Fin_Prolongation
                    Case 11
                        Return WsType_Prolongation
                    Case 12
                        Return WsDuree_Liquidation_RachatEtudes
                    Case 13
                        Return WsDate_Decision_Liquidation_RachatEtudes
                    Case 14
                        Return WsDate_Debut_Liquidation_RachatEtudes
                    Case 15
                        Return WsDate_Fin_Liquidation_RachatEtudes
                    Case 16
                        Return WsDuree_Assurance_RachatEtudes
                    Case 17
                        Return WsDate_Decision_Assurance_RachatEtudes
                    Case 18
                        Return WsDate_Debut_Assurance_RachatEtudes
                    Case 19
                        Return WsDate_Fin_Assurance_RachatEtudes
                    Case 20
                        Return WsDuree_Rachat_RachatEtudes
                    Case 21
                        Return WsDate_Decision_Rachat_RachatEtudes
                    Case 22
                        Return WsDate_Debut_Rachat_RachatEtudes
                    Case 23
                        Return WsDate_Fin_Rachat_RachatEtudes
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
