﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_RETRAITE_SERVICES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Debut As String
        Private WsTauxActivite As Double
        Private WsTauxCotisation As Double
        Private WsStatut As String
        Private WsDate_Decision As String
        Private WsAdministration As String
        Private WsDureeRetenue As String
        Private WsModaliteTravail As String
        Private WsSiCotisationsPayees As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_RETRAITE_SERVICES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 99
            End Get
        End Property

        Public Property Date_Debut() As String
            Get
                Return WsDate_Debut
            End Get
            Set(ByVal value As String)
                WsDate_Debut = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Debut
            End Set
        End Property

        Public Property Taux_Activite() As Double
            Get
                Return WsTauxActivite
            End Get
            Set(ByVal value As Double)
                WsTauxActivite = value
                MyBase.Clef = WsTauxActivite.ToString
            End Set
        End Property

        Public Property Taux_Cotisation() As Double
            Get
                Return WsTauxCotisation
            End Get
            Set(ByVal value As Double)
                WsTauxCotisation = value
            End Set
        End Property

        Public Property Statut() As String
            Get
                Return WsStatut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsStatut = value
                    Case Else
                        WsStatut = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Decision() As String
            Get
                Return WsDate_Decision
            End Get
            Set(ByVal value As String)
                WsDate_Decision = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Administration() As String
            Get
                Return WsAdministration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAdministration = value
                    Case Else
                        WsAdministration = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property DureeRetenue() As String
            Get
                Return WsDureeRetenue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsDureeRetenue = value
                    Case Else
                        WsDureeRetenue = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property ModaliteTravail() As String
            Get
                Return WsModaliteTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsModaliteTravail = value
                    Case Else
                        WsModaliteTravail = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SiPaiementCotisations() As String
            Get
                Return WsSiCotisationsPayees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiCotisationsPayees = value
                    Case Else
                        WsSiCotisationsPayees = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(Taux_Activite.ToString & VI.Tild)
                Chaine.Append(Taux_Cotisation.ToString & VI.Tild)
                Chaine.Append(Statut & VI.Tild)
                Chaine.Append(Date_Decision & VI.Tild)
                Chaine.Append(Administration & VI.Tild)
                Chaine.Append(DureeRetenue & VI.Tild)
                Chaine.Append(ModaliteTravail & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(SiPaiementCotisations & VI.Tild)

                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Taux_Activite = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) = "" Then TableauData(3) = "0"
                Taux_Cotisation = VirRhFonction.ConversionDouble(TableauData(3))
                Statut = TableauData(4)
                Date_Decision = TableauData(5)
                Administration = TableauData(6)
                DureeRetenue = TableauData(7)
                ModaliteTravail = TableauData(8)
                MyBase.Date_de_Fin = TableauData(9)
                SiPaiementCotisations = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Debut
                    Case 1
                        Return WsTauxActivite.ToString
                    Case 2
                        Return WsTauxCotisation.ToString
                    Case 3
                        Return WsStatut
                    Case 4
                        Return WsDate_Decision
                    Case 5
                        Return WsAdministration
                    Case 6
                        Return WsDureeRetenue
                    Case 7
                        Return WsModaliteTravail
                    Case 8
                        Return Date_de_Fin
                    Case 9
                        Return WsSiCotisationsPayees
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
