﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_SAVOIR_FAIRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDomaine As String
        Private WsNiveau_complexite As String
        Private WsSavoirfairetechniques_no1 As String
        Private WsSavoirfairetechniques_no2 As String
        Private WsSavoirfairetechniques_no3 As String
        Private WsSavoirfairetechniques_no4 As String
        Private WsSavoirfairetechniques_no5 As String
        Private WsSavoirfairetechniques_no6 As String
        Private WsSavoirfairetechniques_no7 As String
        Private WsSavoirfairetechniques_no8 As String
        Private WsSavoirfairetechniques_no9 As String
        Private WsSavoirfairetechniques_no10 As String
        Private WsSavoirfairetechniques_no11 As String
        Private WsSavoirfairetechniques_no12 As String
        Private WsSavoirfairetechniques_no13 As String
        Private WsSavoirfairetechniques_no14 As String
        Private WsSavoirfairetechniques_no15 As String
        Private WsSavoirfairetechniques_no16 As String
        Private WsSavoirfairetechniques_no17 As String
        Private WsSavoirfairetechniques_no18 As String
        Private WsCompetence_cle As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_SAVOIR_FAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Niveau_complexite() As String
            Get
                Return WsNiveau_complexite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsNiveau_complexite = value
                    Case Else
                        WsNiveau_complexite = Strings.Left(value, 2)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no1() As String
            Get
                Return WsSavoirfairetechniques_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no1 = value
                    Case Else
                        WsSavoirfairetechniques_no1 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no2() As String
            Get
                Return WsSavoirfairetechniques_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no2 = value
                    Case Else
                        WsSavoirfairetechniques_no2 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no3() As String
            Get
                Return WsSavoirfairetechniques_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no3 = value
                    Case Else
                        WsSavoirfairetechniques_no3 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no4() As String
            Get
                Return WsSavoirfairetechniques_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no4 = value
                    Case Else
                        WsSavoirfairetechniques_no4 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no5() As String
            Get
                Return WsSavoirfairetechniques_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no5 = value
                    Case Else
                        WsSavoirfairetechniques_no5 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no6() As String
            Get
                Return WsSavoirfairetechniques_no6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no6 = value
                    Case Else
                        WsSavoirfairetechniques_no6 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no7() As String
            Get
                Return WsSavoirfairetechniques_no7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no7 = value
                    Case Else
                        WsSavoirfairetechniques_no7 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no8() As String
            Get
                Return WsSavoirfairetechniques_no8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no8 = value
                    Case Else
                        WsSavoirfairetechniques_no8 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no9() As String
            Get
                Return WsSavoirfairetechniques_no9
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no9 = value
                    Case Else
                        WsSavoirfairetechniques_no9 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no10() As String
            Get
                Return WsSavoirfairetechniques_no10
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no10 = value
                    Case Else
                        WsSavoirfairetechniques_no10 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no11() As String
            Get
                Return WsSavoirfairetechniques_no11
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no11 = value
                    Case Else
                        WsSavoirfairetechniques_no11 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no12() As String
            Get
                Return WsSavoirfairetechniques_no12
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no12 = value
                    Case Else
                        WsSavoirfairetechniques_no12 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no13() As String
            Get
                Return WsSavoirfairetechniques_no13
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no13 = value
                    Case Else
                        WsSavoirfairetechniques_no13 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no14() As String
            Get
                Return WsSavoirfairetechniques_no14
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no14 = value
                    Case Else
                        WsSavoirfairetechniques_no14 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no15() As String
            Get
                Return WsSavoirfairetechniques_no15
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no15 = value
                    Case Else
                        WsSavoirfairetechniques_no15 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no16() As String
            Get
                Return WsSavoirfairetechniques_no16
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no16 = value
                    Case Else
                        WsSavoirfairetechniques_no16 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no17() As String
            Get
                Return WsSavoirfairetechniques_no17
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no17 = value
                    Case Else
                        WsSavoirfairetechniques_no17 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Savoirfairetechniques_no18() As String
            Get
                Return WsSavoirfairetechniques_no18
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsSavoirfairetechniques_no18 = value
                    Case Else
                        WsSavoirfairetechniques_no18 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Competence_cle() As String
            Get
                Select Case WsCompetence_cle
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsCompetence_cle
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsCompetence_cle = value
                    Case Else
                        WsCompetence_cle = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Niveau_complexite & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no1 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no2 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no3 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no4 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no5 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no6 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no7 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no8 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no9 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no10 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no11 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no12 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no13 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no14 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no15 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no16 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no17 & VI.Tild)
                Chaine.Append(Savoirfairetechniques_no18 & VI.Tild)
                Chaine.Append(Competence_cle)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Domaine = TableauData(1)
                Niveau_complexite = TableauData(2)
                Savoirfairetechniques_no1 = TableauData(3)
                Savoirfairetechniques_no2 = TableauData(4)
                Savoirfairetechniques_no3 = TableauData(5)
                Savoirfairetechniques_no4 = TableauData(6)
                Savoirfairetechniques_no5 = TableauData(7)
                Savoirfairetechniques_no6 = TableauData(8)
                Savoirfairetechniques_no7 = TableauData(9)
                Savoirfairetechniques_no8 = TableauData(10)
                Savoirfairetechniques_no9 = TableauData(11)
                Savoirfairetechniques_no10 = TableauData(12)
                Savoirfairetechniques_no11 = TableauData(13)
                Savoirfairetechniques_no12 = TableauData(14)
                Savoirfairetechniques_no13 = TableauData(15)
                Savoirfairetechniques_no14 = TableauData(16)
                Savoirfairetechniques_no15 = TableauData(17)
                Savoirfairetechniques_no16 = TableauData(18)
                Savoirfairetechniques_no17 = TableauData(19)
                Savoirfairetechniques_no18 = TableauData(20)
                Competence_cle = TableauData(21)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


