﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleEtiquetteExperte" Codebehind="VCoupleEtiquetteExperte.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<asp:Table ID="CadreExperte" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel runat="server" ID="Etiquette" Height="20px" Width="170px"
                    BackColor="#8DA8A3"  ForeColor="#E9FDF9" 
                    Font-Bold="False" 
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadLabel ID="EtiDonnee" runat="server" Height="20px" Width="300px"
                    BackColor="#C5E0DA" ForeColor="#586361" Visible="true"
                    Font-Bold="false" Font-Size="Medium"
                    style="margin-top: 2px; margin-left: 1px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>