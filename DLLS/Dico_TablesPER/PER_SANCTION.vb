﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_SANCTION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsNature As String
        Private WsGroupe As String
        Private WsDateDecision As String
        Private WsReference As String
        Private WsNbJoursSuspension As Integer
        Private WsDateConseilDiscipline As String
        Private WsDateClotureConseilDiscipline As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_SANCTION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaSanction
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Nature_Sanction() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNature
            End Set
        End Property

        Public Property Groupe_Sanction() As String
            Get
                Return WsGroupe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGroupe = value
                    Case Else
                        WsGroupe = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Date_Decision() As String
            Get
                Return WsDateDecision
            End Get
            Set(ByVal value As String)
                WsDateDecision = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference_Decision() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property NombreJours_Suspension() As Integer
            Get
                Return WsNbJoursSuspension
            End Get
            Set(ByVal value As Integer)
                WsNbJoursSuspension = value
            End Set
        End Property

        Public Property Date_ConseilDiscipline() As String
            Get
                Return WsDateConseilDiscipline
            End Get
            Set(ByVal value As String)
                WsDateConseilDiscipline = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Cloture_ConseilDiscipline() As String
            Get
                Return WsDateClotureConseilDiscipline
            End Get
            Set(ByVal value As String)
                WsDateClotureConseilDiscipline = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Nature_Sanction & VI.Tild)
                Chaine.Append(Groupe_Sanction & VI.Tild)
                Chaine.Append(Date_Decision & VI.Tild)
                Chaine.Append(Reference_Decision & VI.Tild)
                Chaine.Append(NombreJours_Suspension.ToString & VI.Tild)
                Chaine.Append(Date_ConseilDiscipline & VI.Tild)
                Chaine.Append(Date_Cloture_ConseilDiscipline & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Nature_Sanction = TableauData(2)
                Groupe_Sanction = TableauData(3)
                Date_Decision = TableauData(4)
                Reference_Decision = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = 0
                NombreJours_Suspension = CInt(TableauData(6))
                Date_ConseilDiscipline = TableauData(7)
                Date_Cloture_ConseilDiscipline = TableauData(8)
                MyBase.Date_de_Fin = TableauData(9)
                MyBase.Certification = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property
        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


