﻿Option Strict Off
Option Explicit On
Option Compare Text
Namespace ShemaREF
    Public Class CER_EMPREINTE
        Private CharSep As String = "_"

        Private WsPointeurPrecedent As Integer
        Private WsIde_Dossier As Integer
        Private WsPtdeVue As Integer
        Private WsNumObjet As Integer
        Private WsDateTransaction As String = ""
        Private WsHeureTransaction As String = ""
        Private WsModuleOrigine As String = ""
        Private WsProcedureOrigine As String = ""
        Private WsNomEmetteur As String = ""
        Private WsIPEmetteur As String = ""
        Private WsMachineEmetteur As String = ""
        Private WsIPServeur As String = ""
        Private WsMachineServeur As String = ""

        Public Property PointeurChainage As Integer
            Get
                Return WsPointeurPrecedent
            End Get
            Set(value As Integer)
                WsPointeurPrecedent = value
            End Set
        End Property
        Public Property Ide_Dossier As Integer
            Get
                Return WsIde_Dossier
            End Get
            Set(value As Integer)
                WsIde_Dossier = value
            End Set
        End Property

        Public Property PointdeVue As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property NumObjet As Integer
            Get
                Return WsNumObjet
            End Get
            Set(value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property DateTransaction As String
            Get
                If WsDateTransaction = "" Then
                    WsDateTransaction = VirRhDate.DateduJour
                End If
                Return WsDateTransaction
            End Get
            Set(value As String)
                WsDateTransaction = value
            End Set
        End Property
        Public Property HeureTransaction As String
            Get
                If WsHeureTransaction = "" Then
                    WsHeureTransaction = Strings.Format(System.DateTime.Now, "HH:mm:ss.ff")
                End If
                Return WsHeureTransaction
            End Get
            Set(value As String)
                WsHeureTransaction = value
            End Set
        End Property
        Public Property ModuleOrigine As String
            Get
                Return WsModuleOrigine
            End Get
            Set(value As String)
                WsModuleOrigine = value.Replace(CharSep, Strings.Space(1))
            End Set
        End Property
        Public Property ProcedureOrigine As String
            Get
                Return WsProcedureOrigine
            End Get
            Set(value As String)
                WsProcedureOrigine = value.Replace(CharSep, Strings.Space(1))
            End Set
        End Property
        Public Property NomEmetteur As String
            Get
                Return WsNomEmetteur
            End Get
            Set(value As String)
                WsNomEmetteur = value.Replace(CharSep, Strings.Space(1))
            End Set
        End Property
        Public Property IPEmetteur As String
            Get
                Return WsIPEmetteur
            End Get
            Set(value As String)
                WsIPEmetteur = value
            End Set
        End Property
        Public Property MachineEmetteur As String
            Get
                Return WsMachineEmetteur
            End Get
            Set(value As String)
                WsMachineEmetteur = value.Replace(CharSep, Strings.Space(1))
            End Set
        End Property
        Public Property IPServeur As String
            Get
                Return WsIPServeur
            End Get
            Set(value As String)
                WsIPServeur = value
            End Set
        End Property
        Public Property MachineServeur As String
            Get
                Return WsMachineServeur
            End Get
            Set(value As String)
                WsMachineServeur = value.Replace(CharSep, Strings.Space(1))
            End Set
        End Property
        Public Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(PointeurChainage.ToString & CharSep)
                Chaine.Append(Ide_Dossier.ToString & CharSep)
                Chaine.Append(PointdeVue.ToString & CharSep)
                Chaine.Append(NumObjet.ToString & CharSep)
                Chaine.Append(DateTransaction & CharSep)
                Chaine.Append(HeureTransaction & CharSep)
                Chaine.Append(ModuleOrigine & CharSep)
                Chaine.Append(ProcedureOrigine & CharSep)
                Chaine.Append(NomEmetteur & CharSep)
                Chaine.Append(IPEmetteur & CharSep)
                Chaine.Append(MachineEmetteur & CharSep)
                Chaine.Append(IPServeur & CharSep)
                Chaine.Append(MachineServeur)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String

                TableauData = Strings.Split(value, CharSep, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                If IsNumeric(TableauData(0)) = True Then
                    PointeurChainage = CInt(TableauData(0))
                Else
                    PointeurChainage = 0
                End If
                If IsNumeric(TableauData(1)) = True Then
                    Ide_Dossier = CInt(TableauData(1))
                Else
                    Ide_Dossier = 0
                End If
                If IsNumeric(TableauData(2)) = True Then
                    PointdeVue = CInt(TableauData(2))
                Else
                    PointdeVue = 0
                End If
                If IsNumeric(TableauData(3)) = True Then
                    NumObjet = CInt(TableauData(3))
                Else
                    NumObjet = 0
                End If
                DateTransaction = TableauData(4)
                HeureTransaction = TableauData(5)
                ModuleOrigine = TableauData(6)
                ProcedureOrigine = TableauData(7)
                NomEmetteur = TableauData(8)
                IPEmetteur = TableauData(9)
                MachineEmetteur = TableauData(10)
                IPServeur = TableauData(11)
                MachineServeur = TableauData(12)
            End Set
        End Property

        Public ReadOnly Property SiValide As Boolean
            Get
                If WsPointeurPrecedent = 0 Or WsIde_Dossier = 0 Or WsPtdeVue = 0 Or WsNumObjet = 0 Then
                    Return False
                End If
                If WsModuleOrigine = "" Or WsNomEmetteur = "" Then
                    Return False
                End If
                Return True
            End Get
        End Property
    End Class
End Namespace