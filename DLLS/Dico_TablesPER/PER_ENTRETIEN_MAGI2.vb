﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_MAGI2
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsFonctionPrincipale As String
        Private WsLibCritere(13) As String
        Private WsNoteCritere(13) As Single
        Private WsAutresFonctionsPrincipales As String
        Private WsMatieresTraitees As String
        Private WsFonctionJuridictionnelle(3) As String
        Private WsAutresFonctionsJuridictionnelles As String
        Private WsPeriodicite As String
        Private WsSiEncadrementAssistant As String
        Private WsConditonEncadrement As String
        Private WsActivitesAdministratives As String
        Private WsCommissionsAdministratives As String
        Private WsManifestationsOrganisees As String
        Private WsActivitesExterieures As String
        Private WsAutresFonctionsConfiees As String
        Private WsFonctionsAvisEvaluateur As String
        Private WsEmploisEnDetachement As String
        Private WsDetachementAvisEvaluateur As String
        '
        Private TsTypeFormulaire As String

        Public ReadOnly Property NomClient() As String
            Get
                Return "Conseil d'Etat"
            End Get
        End Property

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_BIS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 88
            End Get
        End Property

        Public Property Fonction_Jurid_Principale() As String
            Get
                Return WsFonctionPrincipale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsFonctionPrincipale = value
                    Case Else
                        WsFonctionPrincipale = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Libelle_Critere(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0
                        Return V_InfoCritere(21)
                    Case 1 To 9
                        Return V_InfoCritere((Index * 2) + 1)
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 9
                        Select Case value.Length
                            Case Is <= 60
                                WsLibCritere(Index) = value
                            Case Else
                                WsLibCritere(Index) = Strings.Left(value, 60)
                        End Select
                End Select
            End Set
        End Property

        Public Property Note_Critere(ByVal Index As Integer) As Single
            Get
                Select Case Index
                    Case 0 To 9
                        Return WsNoteCritere(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Single)
                Select Case Index
                    Case 0 To 9
                        WsNoteCritere(Index) = value
                End Select
            End Set
        End Property

        Public Property AutresFonctions_Jurid_Principales() As String
            Get
                Return WsAutresFonctionsPrincipales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsAutresFonctionsPrincipales = value
                    Case Else
                        WsAutresFonctionsPrincipales = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property MatieresTraitees() As String
            Get
                Return WsMatieresTraitees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsMatieresTraitees = value
                    Case Else
                        WsMatieresTraitees = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property AutreFonction_Jurid(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsFonctionJuridictionnelle(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        Select Case value.Length
                            Case Is <= 50
                                WsFonctionJuridictionnelle(Index) = value
                            Case Else
                                WsFonctionJuridictionnelle(Index) = Strings.Left(value, 50)
                        End Select
                End Select
            End Set
        End Property

        Public Property AutresFonctions_Jurid() As String
            Get
                Return WsAutresFonctionsJuridictionnelles
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsAutresFonctionsJuridictionnelles = value
                    Case Else
                        WsAutresFonctionsJuridictionnelles = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Periodicite() As String
            Get
                Return WsPeriodicite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsPeriodicite = value
                    Case Else
                        WsPeriodicite = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property SiEncadrementAssistant() As String
            Get
                Return WsSiEncadrementAssistant
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEncadrementAssistant = value
                    Case Else
                        WsSiEncadrementAssistant = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Conditions_d_Encadrement() As String
            Get
                Return WsConditonEncadrement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsConditonEncadrement = value
                    Case Else
                        WsConditonEncadrement = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property ActivitesAdministratives() As String
            Get
                Return WsActivitesAdministratives
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsActivitesAdministratives = value
                    Case Else
                        WsActivitesAdministratives = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property CommissionsAdministratives() As String
            Get
                Return WsCommissionsAdministratives
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsCommissionsAdministratives = value
                    Case Else
                        WsCommissionsAdministratives = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property ManifestationsOrganisees() As String
            Get
                Return WsManifestationsOrganisees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsManifestationsOrganisees = value
                    Case Else
                        WsManifestationsOrganisees = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property ActivitesExterieures() As String
            Get
                Return WsActivitesExterieures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsActivitesExterieures = value
                    Case Else
                        WsActivitesExterieures = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property AutresFonctionsConfiees() As String
            Get
                Return WsAutresFonctionsConfiees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAutresFonctionsConfiees = value
                    Case Else
                        WsAutresFonctionsConfiees = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Fonctions_AvisEvaluateur() As String
            Get
                Return WsFonctionsAvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFonctionsAvisEvaluateur = value
                    Case Else
                        WsFonctionsAvisEvaluateur = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Detachement_Emplois() As String
            Get
                Return WsEmploisEnDetachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsEmploisEnDetachement = value
                    Case Else
                        WsEmploisEnDetachement = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Detachement_AvisEvaluateur() As String
            Get
                Return WsDetachementAvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDetachementAvisEvaluateur = value
                    Case Else
                        WsDetachementAvisEvaluateur = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Fonction_Jurid_Principale & VI.Tild)
                Chaine.Append(Note_Critere(0).ToString & VI.Tild)
                For IndiceI = 1 To 9
                    Chaine.Append(Libelle_Critere(IndiceI) & VI.Tild)
                    Chaine.Append(Note_Critere(IndiceI).ToString & VI.Tild)
                Next IndiceI
                Chaine.Append(Libelle_Critere(0) & VI.Tild)
                Chaine.Append(AutresFonctions_Jurid_Principales & VI.Tild)
                Chaine.Append(MatieresTraitees & VI.Tild)
                For IndiceI = 0 To 3
                    Chaine.Append(AutreFonction_Jurid(IndiceI) & VI.Tild)
                Next IndiceI
                Chaine.Append(AutresFonctions_Jurid & VI.Tild)
                Chaine.Append(Periodicite & VI.Tild)
                Chaine.Append(SiEncadrementAssistant & VI.Tild)
                Chaine.Append(Conditions_d_Encadrement & VI.Tild)
                Chaine.Append(ActivitesAdministratives & VI.Tild)
                Chaine.Append(CommissionsAdministratives & VI.Tild)
                Chaine.Append(ManifestationsOrganisees & VI.Tild)
                Chaine.Append(ActivitesExterieures & VI.Tild)
                Chaine.Append(AutresFonctionsConfiees & VI.Tild)
                Chaine.Append(Fonctions_AvisEvaluateur & VI.Tild)
                Chaine.Append(Detachement_Emplois & VI.Tild)
                Chaine.Append(Detachement_AvisEvaluateur)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim IndiceK As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 41 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Fonction_Jurid_Principale = TableauData(2)
                IndiceK = 3
                If TableauData(IndiceK) <> "" Then Note_Critere(0) = CSng(VirRhFonction.ConversionDouble(TableauData(IndiceK)))
                IndiceK += 1
                For IndiceI = 1 To 9
                    Libelle_Critere(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                    If TableauData(IndiceK) <> "" Then Note_Critere(IndiceI) = CSng(VirRhFonction.ConversionDouble(TableauData(IndiceK)))
                    IndiceK += 1
                Next IndiceI
                Libelle_Critere(0) = TableauData(IndiceK)
                AutresFonctions_Jurid_Principales = TableauData(23)
                MatieresTraitees = TableauData(24)
                IndiceK = 25
                For IndiceI = 0 To 3
                    AutreFonction_Jurid(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                Next IndiceI
                AutresFonctions_Jurid = TableauData(29)
                Periodicite = TableauData(30)
                SiEncadrementAssistant = TableauData(31)
                Conditions_d_Encadrement = TableauData(32)
                ActivitesAdministratives = TableauData(33)
                CommissionsAdministratives = TableauData(34)
                ManifestationsOrganisees = TableauData(35)
                ActivitesExterieures = TableauData(36)
                AutresFonctionsConfiees = TableauData(37)
                Fonctions_AvisEvaluateur = TableauData(38)
                Detachement_Emplois = TableauData(39)
                Detachement_AvisEvaluateur = TableauData(40)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property V_Type_Formulaire As String
            Get
                Return TsTypeFormulaire
            End Get
            Set(ByVal value As String)
                TsTypeFormulaire = value
            End Set
        End Property

        Private ReadOnly Property V_InfoCritere(ByVal NumInfo As Integer) As String
            Get
                Dim Chaine As String = ""
                Select Case V_Type_Formulaire
                    Case Is = "C/PC"
                        Select Case NumInfo
                            Case 3
                                Chaine = ""
                            Case 5
                                Chaine = ""
                            Case 7
                                Chaine = ""
                            Case 11
                                Chaine = "Capacités d’écoute et d’animation"
                            Case 13
                                Chaine = "Sens de l’organisation et de la gestion"
                            Case 15
                                Chaine = "Capacités d’anticipation et de proposition"
                            Case 17
                                Chaine = "Exercice de l’autorité"
                            Case 19
                                Chaine = "Relations publiques"
                            Case 21
                                Chaine = ""
                        End Select
                    Case Is = "CJ"
                        Select Case NumInfo
                            Case 3
                                Chaine = "Attention portée aux décisions rendues par la juridiction"
                            Case 5
                                Chaine = "Capacités d’anticipation et de proposition"
                            Case 7
                                Chaine = "Relations publiques"
                            Case 11
                                Chaine = ""
                            Case 13
                                Chaine = ""
                            Case 15
                                Chaine = ""
                            Case 17
                                Chaine = ""
                            Case 19
                                Chaine = ""
                            Case 21
                                Chaine = "Attention portée aux questions de formation"
                        End Select
                    Case Is = "PR"
                        Select Case NumInfo
                            Case 3
                                Chaine = ""
                            Case 5
                                Chaine = ""
                            Case 7
                                Chaine = ""
                            Case 11
                                Chaine = "Capacités d’écoute et d’animation"
                            Case 13
                                Chaine = "Sens de l’organisation et de la gestion"
                            Case 15
                                Chaine = "Capacités d’anticipation et de proposition"
                            Case 17
                                Chaine = "Exercice de l’autorité"
                            Case 19
                                Chaine = "Relations publiques"
                            Case 21
                                Chaine = ""
                        End Select
                End Select
                Return Chaine
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace





