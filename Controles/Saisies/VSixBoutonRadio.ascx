﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VSixBoutonRadio" Codebehind="VSixBoutonRadio.ascx.vb" %>

<asp:table ID="CadreOption" runat="server" height="20px" CellPadding="1" CellSpacing="0">
     <asp:TableRow>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN1" runat="server" Text="Option N1" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN2" runat="server" Text="Option N2" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN3" runat="server" Text="Option N3" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN4" runat="server" Text="Option N4" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN5" runat="server" Text="Option N5" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique; 
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
         <asp:TableCell>
             <Telerik:RadRadioButton ID="VRadioN6" runat="server" Text="Option N6" Visible="true"
                  AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="25px" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" Font-Size="Medium"
                  style="margin-top: 0px; margin-left: 5px; font-style: oblique; 
                  text-indent: 5px; text-align: center; vertical-align: middle" />
         </asp:TableCell>
     </asp:TableRow>
</asp:table>