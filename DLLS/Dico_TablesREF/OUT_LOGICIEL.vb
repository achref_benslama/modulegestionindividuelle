﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class OUT_LOGICIEL
        Inherits VIR_FICHE

        Private WsLstRubriques As List(Of OUT_RUBRIQUES)
        Private WsParamContexte As OUT_CONTEXTE
        Private WsFicheLue As StringBuilder
        '
        Private WsNom As String
        Private WsSociete As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "OUT_LOGICIEL"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueInterface
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Societe() As String
            Get
                Return WsSociete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsSociete = value
                    Case Else
                        WsSociete = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Societe)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                Societe = TableauData(2)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property Fiche_Contexte() As OUT_CONTEXTE
            Get
                Return WsParamContexte
            End Get
            Set(ByVal value As OUT_CONTEXTE)
                WsParamContexte = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Rubrique(ByVal Fiche As OUT_RUBRIQUES) As Integer

            If WsLstRubriques Is Nothing Then
                WsLstRubriques = New List(Of OUT_RUBRIQUES)
            End If
            WsLstRubriques.Add(Fiche)
            Return WsLstRubriques.Count

        End Function

        Public Function ListedesRubriques(ByVal SiCode As Boolean) As List(Of OUT_RUBRIQUES)

            If WsLstRubriques Is Nothing Then
                Return Nothing
            End If
            If SiCode Then
                Return WsLstRubriques.OrderBy(Function(m) m.CodeRubrique).ToList
            Else
                Return WsLstRubriques.OrderBy(Function(m) m.Intitule).ToList
            End If

        End Function

        Public Function Fiche_Rubrique(ByVal ValeurCode As String) As OUT_RUBRIQUES

            If WsLstRubriques Is Nothing Then
                Return Nothing
            End If

            Return (From it As OUT_RUBRIQUES In WsLstRubriques Where it.CodeRubrique = ValeurCode Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Rubrique_I(ByVal Valeur As String) As OUT_RUBRIQUES

            If WsLstRubriques Is Nothing Then
                Return Nothing
            End If

            Return (From it As OUT_RUBRIQUES In WsLstRubriques Where it.Intitule = Valeur Select it).FirstOrDefault()

        End Function

    End Class
End Namespace
