﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ANNUAIRE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNiveau1 As String
        Private WsNiveau2 As String
        Private WsNiveau3 As String
        Private WsNiveau4 As String
        Private WsNiveau5 As String
        Private WsNiveau6 As String
        Private WsFonction As String
        Private WsOrdreApparition As Integer
        Private WsObservations As String
        'Traitements
        Private TsTag As String
        Private TsNom As String
        Private TsPrenom As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ANNUAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAnnuaire
            End Get
        End Property

        Public Property Structure_N1() As String
            Get
                Return WsNiveau1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau1 = value
                    Case Else
                        WsNiveau1 = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsNiveau1
            End Set
        End Property

        Public Property Structure_N2() As String
            Get
                Return WsNiveau2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau2 = value
                    Case Else
                        WsNiveau2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Structure_N3() As String
            Get
                Return WsNiveau3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau3 = value
                    Case Else
                        WsNiveau3 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Structure_N4() As String
            Get
                Return WsNiveau4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau4 = value
                    Case Else
                        WsNiveau4 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Structure_N5() As String
            Get
                Return WsNiveau5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau5 = value
                    Case Else
                        WsNiveau5 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Structure_N6() As String
            Get
                Return WsNiveau6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau6 = value
                    Case Else
                        WsNiveau6 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Fonction() As String
            Get
                Return WsFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonction = value
                    Case Else
                        WsFonction = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Ordre_Apparition() As Integer
            Get
                Return WsOrdreApparition
            End Get
            Set(ByVal value As Integer)
                WsOrdreApparition = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Structure_N1 & VI.Tild)
                Chaine.Append(Structure_N2 & VI.Tild)
                Chaine.Append(Structure_N3 & VI.Tild)
                Chaine.Append(Structure_N4 & VI.Tild)
                Chaine.Append(Structure_N5 & VI.Tild)
                Chaine.Append(Structure_N6 & VI.Tild)
                Chaine.Append(Fonction & VI.Tild)
                Chaine.Append(Ordre_Apparition.ToString & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Structure_N1 = TableauData(2)
                Structure_N2 = TableauData(3)
                Structure_N3 = TableauData(4)
                Structure_N4 = TableauData(5)
                Structure_N5 = TableauData(6)
                Structure_N6 = TableauData(7)
                Fonction = TableauData(8)
                If TableauData(9) <> "" Then Ordre_Apparition = CInt(TableauData(9))
                MyBase.Date_de_Fin = TableauData(10)
                Observations = TableauData(11)
                '*** Si Redondance sur nom et prénom
                If TableauData.Count > 13 Then
                    TsNom = TableauData(12)
                    TsPrenom = TableauData(13)
                End If
                '******************************************
                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To 11
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsNiveau1
                    Case 2
                        Return WsNiveau2
                    Case 3
                        Return WsNiveau3
                    Case 4
                        Return WsNiveau4
                    Case 5
                        Return WsNiveau5
                    Case 6
                        Return WsNiveau6
                    Case 7
                        Return WsFonction
                    Case 8
                        Return WsOrdreApparition.ToString
                    Case 9
                        Return Date_de_Fin
                    Case 10
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Property VTag() As String
            Get
                Return TsTag
            End Get
            Set(ByVal value As String)
                TsTag = value
            End Set
        End Property

        Public ReadOnly Property Nom() As String
            Get
                Return TsNom
            End Get
        End Property

        Public ReadOnly Property Prenom() As String
            Get
                Return TsPrenom
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


