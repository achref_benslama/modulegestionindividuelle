Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_RESULTAT
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsInformatif As String
        Private WsCorrectif As String
        Private WsMessageAlerte As String
        Private WsMessageCptRendu As String

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Informatif() As String
            Get
                Return WsInformatif
            End Get
            Set(ByVal value As String)
                WsInformatif = value
            End Set
        End Property

        Public Property Correctif() As String
            Get
                Return WsCorrectif
            End Get
            Set(ByVal value As String)
                WsCorrectif = value
            End Set
        End Property

        Public Property MessageAlerte() As String
            Get
                Return WsMessageAlerte
            End Get
            Set(ByVal value As String)
                WsMessageAlerte = F_FormatAlpha(value, 200)
            End Set
        End Property

        Public Property MessageCptRendu() As String
            Get
                Return WsMessageCptRendu
            End Get
            Set(ByVal value As String)
                WsMessageCptRendu = F_FormatAlpha(value, 200)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Informatif & VI.Tild)
                Chaine.Append(Correctif & VI.Tild)
                Chaine.Append(MessageAlerte & VI.Tild)
                Chaine.Append(MessageCptRendu)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                Informatif = TableauData(1)
                Correctif = TableauData(2)
                MessageAlerte = TableauData(3)
                MessageCptRendu = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsInformatif
                    Case 2
                        Return WsCorrectif
                    Case 3
                        Return WsMessageAlerte
                    Case 4
                        Return WsMessageCptRendu
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone
            Return New REG_RESULTAT(Ide_Dossier.ToString & ContenuTable)
        End Function


    End Class
End Namespace

