﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerDossierAcceuil.ascx.vb" Inherits="Virtualia.Net.PerDossierAcceuil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Standards/VMessage.ascx" TagName="VMessage" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VReferentiel.ascx" TagName="VReference" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDate.ascx" TagName="VCoupleEtiDate" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" TagName="VTrioHorizontalRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VNumTelephone.ascx" TagName="VNumTelephone" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerEnfants.ascx" TagName="VPerEnfant" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerListeEnfants.ascx" TagName="PerListeEnfants" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/PER/CtlPvuePER.ascx" TagName="VPER" TagPrefix="Virtualia" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        .roundedcorner {
            font-size: 11pt;
            margin-left: auto;
            margin-right: auto;
            margin-top: 1px;
            margin-bottom: 1px;
            padding: 3px;
            border-top: 1px solid;
            border-left: 1px solid;
            border-right: 1px solid;
            border-bottom: 1px solid;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .roundedcorner {
            font-size: 11pt;
            margin-left: auto;
            margin-right: auto;
            margin-top: 1px;
            margin-bottom: 1px;
            padding: 3px;
            border-top: 1px solid;
            border-left: 1px solid;
            border-right: 1px solid;
            border-bottom: 1px solid;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .titre {
            font-size: 25px;
            margin-top: 1px;
            margin-bottom: 1px;
            margin-left: 100px;
            padding: 3px;
            text-align: center;
        }

        .background {
            background-color: black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .marginleft {
            margin-left: 150px;
        }

        .popup {
            background-color: #E2F5F1;
            border-width: 3px;
            border-color: black;
            border-style: solid;
            padding-top: 10px;
            padding-left: 10px;
            width: 500px;
            height: 400px;
            text-align: center;
        }

        .name {
            font-weight: 400;
            font-size: 22px;
            margin-bottom: 0;
            color: #000000;
            font-family: serif;
        }

         .text {
            font-weight: 300;
            font-size: 20px;
            margin-bottom: 0;
            color: #000000;
            font-family: serif;
        }

    </style>
</head>
<body>

        <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
        <asp:Table ID="CadreInfo" runat="server" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" HorizontalAlign="Center" Style="margin-top: 1px; background-color: #E2F5F1" Visible="true" Height="700px" Width="100%">
            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label CssClass="name" ID="Etatcivil123" runat="server" ></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiquetteAge" Text="Age : " runat="server" CssClass="name"></asp:Label>
                        <asp:Label ID="Age" runat="server" CssClass="text"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Etiquette" Text="Ide Virtualia" runat="server" Height="20px" Width="120px"
                                                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                            Style="margin-top: 0px;   text-indent: 5px; text-align: left;">   </asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                       <asp:Label ID="idevirtualia" CssClass="text" Style="margin-right: 100px; font-size:21px" Text="PER_REFEXTERNE 8" runat="server" Width="120px" Height="20px"></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>

                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                              <asp:Label ID="ettiquette_present" Text="Présent : " runat="server" CssClass="name"></asp:Label>
                                               <asp:Label ID="present"  Text="PRESENT" CssClass="text" runat="server" ></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
                <asp:TableCell CssClass="marginleft">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="Button1" runat="server" BackColor="#1C5150" Width="215px" Height="32px"
                                    BorderStyle="None" BorderWidth="1px" BorderColor="WhiteSmoke" Font-Names="Trebuchet MS"
                                    Font-Size="Medium" ForeColor="#B0E0D7" Font-Bold="false" Font-Italic="true"
                                    Text="Situation en gestion" Style="margin-top: 5px; text-align: center" />               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="Btn_visualiser_dossier" runat="server" BackColor="#1C5150" Width="215px" Height="32px"
                                    BorderStyle="None" BorderWidth="1px" BorderColor="WhiteSmoke" Font-Names="Trebuchet MS"
                                    Font-Size="Medium" ForeColor="#B0E0D7" Font-Bold="false" Font-Italic="true"
                                    Text="Visualiser tout le dossier"  Style="margin-top: 5px; text-align: center" />     
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>




            <asp:TableRow>
                <asp:TableCell>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                <asp:Image runat="server" ImageUrl="~/Images/gc.jpg" Width="180px" Height="180px" />
                                            </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="Email" Text="Email : "  runat="server" CssClass="text"></asp:Label>s
                        <asp:Label ID="per_adressepro7"  runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                 <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="per_collectivite1" Text="Entrée PER_Collectivité1 Date recalculée" runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                 <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="per_affectation1_2"  Text="Affecté PER_AFFECTATION 1 et 2" runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                 <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="emploi"  Text="Emploi exercé : " runat="server" CssClass="text"></asp:Label>
                        <asp:Label ID="per_affectation5"  Text="PER_Affectation5" runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                        </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>                       
                </asp:TableCell>
                <asp:TableCell >
                     <asp:Table runat="server" CssClass="marginleft">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="per_adressepro2" runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                 <asp:TableRow>
                                                    <asp:TableCell>
                        <asp:Label ID="per_adressepro9" runat="server" CssClass="text"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                </asp:TableCell>
            </asp:TableRow>





            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                  <asp:Label ID="per_statut1_2" Text="PER_STATUT 1 et 2 " runat="server"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="datestatut" Text="Depuis date recalculée " runat="server" CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="per_statut8" Text="Jusqu'au PER_Statut 8 " runat="server"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            </asp:Table>
                    </asp:TableCell>                    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                  <asp:Label ID="per_position1_11_2" Text="PER_POSITION 1 11 2 " runat="server"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label Id="datecalculposition" Text="Depuis date recalculée" runat="server" CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="per_position7" Text="Jusqu'au PER_POSITION 7" runat="server"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label id="Label1" Text="ETP : " runat="server" CssClass="text"></asp:Label>
                        <asp:Label id="etp" Text="ETP année courante (calcul)" runat="server" CssClass="text"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
            </asp:TableRow>


            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Cat" Text="Catégorie : " runat="server" CssClass="text"></asp:Label>
                        <asp:Label ID="per_grade5" Text="PER_GRADE 5" runat="server" CssClass="text"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
            </asp:TableRow>


            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                 <asp:Label ID="Label2" Text="Grade : " runat="server"  CssClass="text"></asp:Label>
                                  <asp:Label ID="per_grade1" Text="PER_GRADE1" runat="server" Style="font-size:17px"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="depuisgrade1" Text="Depuis date recalculée " runat="server" CssClass="text"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
            </asp:TableRow>


            <asp:TableRow>
                <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                  <asp:Label ID="per_grade2_4" Text="PER_GRADE2 (IM PER_GRADE 4)" runat="server"  CssClass="text"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="depuisgrade2_4" Text="Depuis date recalculée " runat="server" CssClass="text"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                </asp:TableCell>
            </asp:TableRow>




            <asp:TableRow>
                <asp:TableCell>
           
                </asp:TableCell>
                <asp:TableCell CssClass="marginleft">
                </asp:TableCell>
                <asp:TableCell CssClass="marginleft">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="Button3" runat="server" BackColor="#1C5150" Width="215px" Height="32px"
                                    BorderStyle="None" BorderWidth="1px" BorderColor="WhiteSmoke" Font-Names="Trebuchet MS"
                                    Font-Size="Medium" ForeColor="#B0E0D7" Font-Bold="false"  Font-Italic="true"
                                    Text="Supprimer le dossier" Style="margin-top: 5px; text-align: center" />    
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>



            <asp:TableRow>
                <asp:TableCell ID="CellMessage" Visible="false">
                    <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                    <asp:Panel ID="PanelMsgPopup" runat="server">
                        <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                    </asp:Panel>
                    <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ID="CellReference" Visible="false">
                    <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
                    <asp:Panel ID="PanelRefPopup" runat="server">
                        <Virtualia:VReference ID="RefVirtualia" runat="server" />
                    </asp:Panel>
                    <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</body>
</html>

