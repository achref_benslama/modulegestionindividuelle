Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class Armoire
        Private WsPointdeVue As Integer
        Private WsInstanceBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
        Private WsNomUtilisateur As String
        Private WsChaineLue As System.Text.StringBuilder
        Private WsTableauEnr As List(Of String)
        Private WsTableauChamp(0) As String
        Private WsNbDossiers As Integer
        Private WsArmoireCourante(0) As Boolean
        Private WsArmoireCmc(0) As Boolean
        Private WsArmoireStats(0) As Boolean
        Private WsIndexCourant As Integer = -1
        '
        Private WsObjetIcone As Integer
        Private WsInfoIcone As Integer
        Private WsSiDateIcone As Boolean
        '
        Public WriteOnly Property InfoIconeArmoire(ByVal NoObjet As Integer, ByVal SiDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsObjetIcone = NoObjet
                WsSiDateIcone = SiDate
                WsInfoIcone = value
            End Set
        End Property

        Public ReadOnly Property NombredeDossiers() As Integer
            Get
                Return WsNbDossiers
            End Get
        End Property

        Public ReadOnly Property ArmoireDynamique(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer, ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, DateDebut, DateFin, VI.Operateurs.OU) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
                Select Case ConditionValeurs
                    Case Is <> ""
                        Constructeur.ValeuraComparer(0, OpeLiaison, OpeComparaison, False) = ConditionValeurs
                End Select
                Constructeur.InfoExtraite(0, 1, 1) = 1
                Select Case WsPointdeVue
                    Case VI.PointdeVue.PVueApplicatif
                        Constructeur.InfoExtraite(0, 1, 1) = 2
                        Constructeur.InfoExtraite(1, 1, 2) = 3
                        If WsObjetIcone > 0 And WsInfoIcone > 0 Then
                            Constructeur.InfoExtraite(2, WsObjetIcone, 0) = WsInfoIcone
                            Select Case WsSiDateIcone
                                Case True
                                    Constructeur.InfoExtraite(3, WsObjetIcone, 0) = 0
                            End Select
                        End If
                End Select

                WsTableauEnr = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, WsPointdeVue, 1, Constructeur.OrdreSqlDynamique)

                If WsTableauEnr Is Nothing Then
                    WsNbDossiers = 0
                Else
                    WsNbDossiers = WsTableauEnr.Count
                End If
                Return WsNbDossiers
            End Get
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer, ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, DateDebut, DateFin, VI.Operateurs.OU) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
                Select Case ConditionValeurs
                    Case Is <> ""
                        Constructeur.ValeuraComparer(0, OpeLiaison, OpeComparaison, False) = ConditionValeurs
                End Select
                Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo

                WsTableauEnr = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, WsPointdeVue, NoObjet, Constructeur.OrdreSqlDynamique)

                If WsTableauEnr Is Nothing Then
                    WsNbDossiers = 0
                Else
                    WsNbDossiers = WsTableauEnr.Count
                End If
                Return WsNbDossiers
            End Get
        End Property

        Public ReadOnly Property Identifiant(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To WsTableauEnr.Count - 1
                        Call DecodifierTableauData(Index)
                        Return Convert.ToInt32(WsTableauChamp(0))
                    Case Else
                        Return 0
                End Select
            End Get
        End Property

        Public ReadOnly Property ValeurParAccesDirect(ByVal Ide As Integer) As String
            Get
                Dim Pos1 As Integer = 0
                Dim Pos2 As Integer = 0
                Dim Recherche As String = ""

                Pos1 = Strings.InStr(WsChaineLue.ToString, Ide.ToString & VI.Tild)
                Select Case Pos1
                    Case Is = 0
                        Return ""
                End Select
                Pos2 = Strings.InStr(Pos1 + 1, WsChaineLue.ToString, VI.Tild)
                Select Case Pos2
                    Case Is = 0
                        Return ""
                End Select
                Recherche = Strings.Mid(WsChaineLue.ToString, Pos1, Pos2 - Pos1)
                'Confirmation
                If CInt(Recherche) = Ide Then
                    Pos1 = Pos2
                    Pos2 = Strings.InStr(Pos1 + 1, WsChaineLue.ToString, VI.Tild)
                    Select Case Pos2
                        Case Is = 0
                            Return ""
                    End Select
                    Recherche = Strings.Mid(WsChaineLue.ToString, Pos1 + 1, Pos2 - Pos1 - 1)
                Else
                    Recherche = ""
                End If
                Return Recherche
            End Get
        End Property

        Public ReadOnly Property Donnee(ByVal Index As Integer, ByVal Position As Integer) As String
            Get
                Call DecodifierTableauData(Index)
                Select Case Position
                    Case 1 To WsTableauChamp.Count - 1
                        Return WsTableauChamp(Position)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Property Courante(ByVal Index As Integer) As Boolean
            Get
                Select Case Index
                    Case 0 To WsArmoireCourante.Count - 1
                        Return WsArmoireCourante(Index)
                End Select
                Return False
            End Get
            Set(ByVal value As Boolean)
                Select Case Index
                    Case 0 To WsArmoireCourante.Count - 1
                        WsArmoireCourante(Index) = value
                End Select
            End Set
        End Property

        Public Property Recherche(ByVal Index As Integer) As Boolean
            Get
                Select Case Index
                    Case 0 To WsArmoireCmc.Count - 1
                        Return WsArmoireCmc(Index)
                End Select
                Return False
            End Get
            Set(ByVal value As Boolean)
                Select Case Index
                    Case 0 To WsArmoireCmc.Count - 1
                        WsArmoireCmc(Index) = value
                End Select
            End Set
        End Property

        Public Property Statistique(ByVal Index As Integer) As Boolean
            Get
                Select Case Index
                    Case 0 To WsArmoireStats.Count - 1
                        Return WsArmoireStats(Index)
                End Select
                Return False
            End Get
            Set(ByVal value As Boolean)
                Select Case Index
                    Case 0 To WsArmoireStats.Count - 1
                        WsArmoireStats(Index) = value
                End Select
            End Set
        End Property

        Public Sub InitialiserArmoireCourante()
            Erase WsArmoireCourante
            ReDim WsArmoireCourante(WsNbDossiers)
        End Sub

        Public Sub InitialiserArmoireCmc()
            Erase WsArmoireCmc
            ReDim WsArmoireCmc(WsNbDossiers)
        End Sub

        Public Sub InitialiserArmoireStats()
            Erase WsArmoireStats
            ReDim WsArmoireStats(WsNbDossiers)
        End Sub

        Private Sub DecodifierTableauData(ByVal Index As Integer)
            Select Case Index
                Case Is < 0
                    Erase WsTableauChamp
                    ReDim WsTableauChamp(0)
                    Exit Sub
                Case Is > WsTableauEnr.Count - 1
                    Erase WsTableauChamp
                    ReDim WsTableauChamp(0)
                    Exit Sub
                Case Is = WsIndexCourant
                    Exit Sub
            End Select
            WsTableauChamp = Strings.Split(WsTableauEnr(Index), VI.Tild, -1)
            WsIndexCourant = Index
        End Sub

        Public WriteOnly Property NomUtilisateur(ByVal Sgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase) As String
            Set(ByVal value As String)
                WsNomUtilisateur = value
                WsInstanceBd = Sgbd
            End Set
        End Property

        Public Sub New(ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, ByVal PointdeVue As Integer)
            MyBase.New()
            VirModeleRh = Modele
            WsPointdeVue = PointdeVue
        End Sub
    End Class
End Namespace

