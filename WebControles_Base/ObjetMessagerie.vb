﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetMessagerie
        Private WsParent As ObjetGlobalBase
        Private WsUrlWseOutil As String = ""
        Private WsListeIdentifiants As List(Of Integer)
        Private WsOrigine As String = ""
        Private WsListeAdressePro As List(Of Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO) = Nothing
        Private WsListeMessages As List(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE) = Nothing

        Public Function EnvoyerEMail(ByVal EmetteurMsg As String, ByVal SujetMsg As String, ByVal ContenuMsg As String, Optional ByVal SiCopieEtablissement As Boolean = False) As Boolean
            If WsUrlWseOutil = "" Then
                Return False
            End If
            If WsListeAdressePro Is Nothing OrElse WsListeAdressePro.Count = 0 Then
                Return False
            End If
            Dim SiOK As Boolean = False
            Dim ListeDestinataires As List(Of KeyValuePair(Of Integer, String))
            Dim WseOutil As Virtualia.Net.WebService.VirtualiaOutils
            Dim ChaineDest As String = ""
            Dim IndiceI As Integer
            Dim FicheMSG As Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE

            ListeDestinataires = New List(Of KeyValuePair(Of Integer, String))
            For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO In WsListeAdressePro
                If FichePER.Email <> "" Then
                    ListeDestinataires.Add(New KeyValuePair(Of Integer, String)(FichePER.Ide_Dossier, FichePER.Email))
                End If
            Next
            If ListeDestinataires.Count = 0 Then
                Return False
            End If
            For IndiceI = 0 To ListeDestinataires.Count - 1
                ChaineDest &= ListeDestinataires.Item(IndiceI).Value
                If IndiceI < ListeDestinataires.Count - 1 Then
                    ChaineDest &= VI.PointVirgule
                End If
            Next IndiceI
            Try
                WseOutil = New Virtualia.Net.WebService.VirtualiaOutils(WsUrlWseOutil)
                If SujetMsg <> "" Then
                    SiOK = WseOutil.EnvoyerEmail2(SujetMsg, "", ChaineDest, ContenuMsg, False)
                Else
                    SiOK = WseOutil.EnvoyerEmail("", ChaineDest, ContenuMsg, False)
                End If
            Catch ex As Exception
                Return False
            End Try
            If SiOK = False Then
                Return False
            End If
            For Each Destinataire In ListeDestinataires
                FicheMSG = New Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE
                FicheMSG.Ide_Dossier = Destinataire.Key
                FicheMSG.Rang = NouveauRang(Destinataire.Key)
                FicheMSG.Date_de_Valeur = WsParent.VirRhDates.DateduJour
                FicheMSG.Heure_Message = System.DateTime.Now.ToShortTimeString
                FicheMSG.ModuleOrigine = WsOrigine
                FicheMSG.Emetteur = EmetteurMsg
                FicheMSG.Objet_Message = SujetMsg
                FicheMSG.Contenu_Message = ContenuMsg
                SiOK = WsParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaMessageIntranet, Destinataire.Key, "C", "", FicheMSG.ContenuTable)
            Next
            If SiCopieEtablissement = False Then
                Return True
            End If
            ChaineDest = WsParent.VirMailEtablissement(ListeDestinataires.Item(0).Key)
            If ChaineDest = "" Then
                Return True
            End If
            Try
                WseOutil = New Virtualia.Net.WebService.VirtualiaOutils(WsUrlWseOutil)
                If SujetMsg <> "" Then
                    SiOK = WseOutil.EnvoyerEmail2(SujetMsg, "", ChaineDest, ContenuMsg, False)
                Else
                    SiOK = WseOutil.EnvoyerEmail("", ChaineDest, ContenuMsg, False)
                End If
            Catch ex As Exception
                Exit Try
            End Try
            Return True
        End Function

        Private ReadOnly Property NouveauRang(ByVal Ide As Integer) As Integer
            Get
                Dim LstFichesMSG As List(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE) = ListeMessages(Ide)
                If LstFichesMSG Is Nothing OrElse LstFichesMSG.Count = 0 Then
                    Return 1
                End If
                Dim RangMax As Integer = 0
                For Each PerMsg In LstFichesMSG
                    If PerMsg.Rang > RangMax Then
                        RangMax = PerMsg.Rang
                    End If
                Next
                Return RangMax + 1
            End Get
        End Property

        Public ReadOnly Property ListeMessages(ByVal Ide As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE)
            Get
                If WsListeMessages Is Nothing Then
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    If WsListeIdentifiants.Count = 1 Then
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaMessageIntranet, WsListeIdentifiants.Item(0), False)
                    Else
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_Plus_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaMessageIntranet, False, WsListeIdentifiants)
                    End If
                    If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                        Return Nothing
                    End If
                    WsListeMessages = WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE)(LstFiches)
                End If
                If Ide = 0 Then
                    Return (From Msg In WsListeMessages Order By Msg.Rang).ToList
                Else
                    Try
                        Return (From Msg In WsListeMessages Where Msg.Ide_Dossier = Ide Order By Msg.Rang).ToList
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End If
            End Get
        End Property

        Public ReadOnly Property ListeMessages(ByVal Ide As Integer, ByVal OrigineMsg As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE)
            Get
                If WsListeMessages Is Nothing Then
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    If WsListeIdentifiants.Count = 1 Then
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaMessageIntranet, WsListeIdentifiants.Item(0), False)
                    Else
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_Plus_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaMessageIntranet, False, WsListeIdentifiants)
                    End If
                    If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                        Return Nothing
                    End If
                    WsListeMessages = WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_MESSAGERIE)(LstFiches)
                End If
                If Ide = 0 Then
                    Return (From Msg In WsListeMessages Where Msg.ModuleOrigine = OrigineMsg Order By Msg.Rang).ToList
                Else
                    Try
                        Return (From Msg In WsListeMessages Where Msg.Ide_Dossier = Ide And Msg.ModuleOrigine = OrigineMsg Order By Msg.Rang).ToList
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End If
            End Get
        End Property

        Private Sub LireAdressePro()
            If WsListeAdressePro IsNot Nothing Then
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            If WsListeIdentifiants.Count = 1 Then
                LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, WsListeIdentifiants.Item(0), False)
            Else
                LstFiches = WsParent.VirServiceServeur.LectureObjet_Plus_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, False, WsListeIdentifiants)
            End If
            If LstFiches IsNot Nothing Then
                WsListeAdressePro = WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO)(LstFiches)
            End If
        End Sub

        Public Sub New(ByVal Host As ObjetGlobalBase, ByVal UrlWebServiceOutil As String, ByVal Ide As Integer, ByVal ModuleOrigine As String)
            WsParent = Host
            WsUrlWseOutil = UrlWebServiceOutil
            WsOrigine = ModuleOrigine
            WsListeIdentifiants = New List(Of Integer)
            WsListeIdentifiants.Add(Ide)
            If WsUrlWseOutil = "" Then
                WsUrlWseOutil = WsParent.UrlWebOutil
            End If
            If WsOrigine = "" Then
                WsOrigine = "Virtualia.Net"
            End If
            Call LireAdressePro()
        End Sub

        Public Sub New(ByVal Host As ObjetGlobalBase, ByVal UrlWebServiceOutil As String, ByVal LstIde As List(Of Integer), ByVal ModuleOrigine As String)
            WsParent = Host
            WsUrlWseOutil = UrlWebServiceOutil
            WsOrigine = ModuleOrigine
            WsListeIdentifiants = LstIde
            If WsUrlWseOutil = "" Then
                WsUrlWseOutil = WsParent.UrlWebOutil
            End If
            If WsOrigine = "" Then
                WsOrigine = "Virtualia.Net"
            End If
            Call LireAdressePro()
        End Sub

    End Class
End Namespace