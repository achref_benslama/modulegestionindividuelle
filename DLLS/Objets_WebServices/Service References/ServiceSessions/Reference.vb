﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Ce code a été généré par un outil.
'     Version du runtime :4.0.30319.42000
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System
Imports System.Runtime.Serialization

Namespace ServiceSessions
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0"),  _
     System.Runtime.Serialization.DataContractAttribute(Name:="ContexteUtilisateurType", [Namespace]:="http://schemas.datacontract.org/2004/07/Virtualia.TablesObjet.Wcf"),  _
     System.SerializableAttribute()>  _
    Partial Public Class ContexteUtilisateurType
        Inherits Object
        Implements System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
        
        <System.NonSerializedAttribute()>  _
        Private extensionDataField As System.Runtime.Serialization.ExtensionDataObject
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Date_ConnexionField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Filtre_EtablissementField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Filtre_Version3Field As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Heure_ConnexionField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_SessionField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private InstanceBDField As Integer
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private LstContexteField As System.Collections.Generic.List(Of String)
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Numero_URLField As Integer
        
        <Global.System.ComponentModel.BrowsableAttribute(false)>  _
        Public Property ExtensionData() As System.Runtime.Serialization.ExtensionDataObject Implements System.Runtime.Serialization.IExtensibleDataObject.ExtensionData
            Get
                Return Me.extensionDataField
            End Get
            Set
                Me.extensionDataField = value
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Date_Connexion() As String
            Get
                Return Me.Date_ConnexionField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Date_ConnexionField, value) <> true) Then
                    Me.Date_ConnexionField = value
                    Me.RaisePropertyChanged("Date_Connexion")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Filtre_Etablissement() As String
            Get
                Return Me.Filtre_EtablissementField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Filtre_EtablissementField, value) <> true) Then
                    Me.Filtre_EtablissementField = value
                    Me.RaisePropertyChanged("Filtre_Etablissement")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Filtre_Version3() As String
            Get
                Return Me.Filtre_Version3Field
            End Get
            Set
                If (Object.ReferenceEquals(Me.Filtre_Version3Field, value) <> true) Then
                    Me.Filtre_Version3Field = value
                    Me.RaisePropertyChanged("Filtre_Version3")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Heure_Connexion() As String
            Get
                Return Me.Heure_ConnexionField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Heure_ConnexionField, value) <> true) Then
                    Me.Heure_ConnexionField = value
                    Me.RaisePropertyChanged("Heure_Connexion")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_Session() As String
            Get
                Return Me.ID_SessionField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_SessionField, value) <> true) Then
                    Me.ID_SessionField = value
                    Me.RaisePropertyChanged("ID_Session")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property InstanceBD() As Integer
            Get
                Return Me.InstanceBDField
            End Get
            Set
                If (Me.InstanceBDField.Equals(value) <> true) Then
                    Me.InstanceBDField = value
                    Me.RaisePropertyChanged("InstanceBD")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property LstContexte() As System.Collections.Generic.List(Of String)
            Get
                Return Me.LstContexteField
            End Get
            Set
                If (Object.ReferenceEquals(Me.LstContexteField, value) <> true) Then
                    Me.LstContexteField = value
                    Me.RaisePropertyChanged("LstContexte")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Numero_URL() As Integer
            Get
                Return Me.Numero_URLField
            End Get
            Set
                If (Me.Numero_URLField.Equals(value) <> true) Then
                    Me.Numero_URLField = value
                    Me.RaisePropertyChanged("Numero_URL")
                End If
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0"),  _
     System.Runtime.Serialization.DataContractAttribute(Name:="SessionUtilisateurType", [Namespace]:="http://schemas.datacontract.org/2004/07/Virtualia.TablesObjet.Wcf"),  _
     System.SerializableAttribute()>  _
    Partial Public Class SessionUtilisateurType
        Inherits Object
        Implements System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
        
        <System.NonSerializedAttribute()>  _
        Private extensionDataField As System.Runtime.Serialization.ExtensionDataObject
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Email_PersonnelField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Email_ProfessionnelField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_AdresseIPField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_LogonIdentityField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_MachineField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_NomConnexionField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_SessionField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private ID_TypeUtiField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private IdentifiantField As Integer
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Nom_UsuelField As String
        
        <System.Runtime.Serialization.OptionalFieldAttribute()>  _
        Private Prenom_UsuelField As String
        
        <Global.System.ComponentModel.BrowsableAttribute(false)>  _
        Public Property ExtensionData() As System.Runtime.Serialization.ExtensionDataObject Implements System.Runtime.Serialization.IExtensibleDataObject.ExtensionData
            Get
                Return Me.extensionDataField
            End Get
            Set
                Me.extensionDataField = value
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Email_Personnel() As String
            Get
                Return Me.Email_PersonnelField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Email_PersonnelField, value) <> true) Then
                    Me.Email_PersonnelField = value
                    Me.RaisePropertyChanged("Email_Personnel")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Email_Professionnel() As String
            Get
                Return Me.Email_ProfessionnelField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Email_ProfessionnelField, value) <> true) Then
                    Me.Email_ProfessionnelField = value
                    Me.RaisePropertyChanged("Email_Professionnel")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_AdresseIP() As String
            Get
                Return Me.ID_AdresseIPField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_AdresseIPField, value) <> true) Then
                    Me.ID_AdresseIPField = value
                    Me.RaisePropertyChanged("ID_AdresseIP")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_LogonIdentity() As String
            Get
                Return Me.ID_LogonIdentityField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_LogonIdentityField, value) <> true) Then
                    Me.ID_LogonIdentityField = value
                    Me.RaisePropertyChanged("ID_LogonIdentity")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_Machine() As String
            Get
                Return Me.ID_MachineField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_MachineField, value) <> true) Then
                    Me.ID_MachineField = value
                    Me.RaisePropertyChanged("ID_Machine")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_NomConnexion() As String
            Get
                Return Me.ID_NomConnexionField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_NomConnexionField, value) <> true) Then
                    Me.ID_NomConnexionField = value
                    Me.RaisePropertyChanged("ID_NomConnexion")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_Session() As String
            Get
                Return Me.ID_SessionField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_SessionField, value) <> true) Then
                    Me.ID_SessionField = value
                    Me.RaisePropertyChanged("ID_Session")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property ID_TypeUti() As String
            Get
                Return Me.ID_TypeUtiField
            End Get
            Set
                If (Object.ReferenceEquals(Me.ID_TypeUtiField, value) <> true) Then
                    Me.ID_TypeUtiField = value
                    Me.RaisePropertyChanged("ID_TypeUti")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Identifiant() As Integer
            Get
                Return Me.IdentifiantField
            End Get
            Set
                If (Me.IdentifiantField.Equals(value) <> true) Then
                    Me.IdentifiantField = value
                    Me.RaisePropertyChanged("Identifiant")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Nom_Usuel() As String
            Get
                Return Me.Nom_UsuelField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Nom_UsuelField, value) <> true) Then
                    Me.Nom_UsuelField = value
                    Me.RaisePropertyChanged("Nom_Usuel")
                End If
            End Set
        End Property
        
        <System.Runtime.Serialization.DataMemberAttribute()>  _
        Public Property Prenom_Usuel() As String
            Get
                Return Me.Prenom_UsuelField
            End Get
            Set
                If (Object.ReferenceEquals(Me.Prenom_UsuelField, value) <> true) Then
                    Me.Prenom_UsuelField = value
                    Me.RaisePropertyChanged("Prenom_Usuel")
                End If
            End Set
        End Property
        
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        
        Protected Sub RaisePropertyChanged(ByVal propertyName As String)
            Dim propertyChanged As System.ComponentModel.PropertyChangedEventHandler = Me.PropertyChangedEvent
            If (Not (propertyChanged) Is Nothing) Then
                propertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(propertyName))
            End If
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute(ConfigurationName:="ServiceSessions.IVirtualiaSession")>  _
    Public Interface IVirtualiaSession
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ContexteSession", ReplyAction:="http://tempuri.org/IVirtualiaSession/ContexteSessionResponse")>  _
        Function ContexteSession(ByVal NoSession As String) As ServiceSessions.ContexteUtilisateurType
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ContexteSession", ReplyAction:="http://tempuri.org/IVirtualiaSession/ContexteSessionResponse")>  _
        Function ContexteSessionAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of ServiceSessions.ContexteUtilisateurType)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/Deconnexion", ReplyAction:="http://tempuri.org/IVirtualiaSession/DeconnexionResponse")>  _
        Function Deconnexion(ByVal NoSession As String) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/Deconnexion", ReplyAction:="http://tempuri.org/IVirtualiaSession/DeconnexionResponse")>  _
        Function DeconnexionAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ID_SessionVirtualia", ReplyAction:="http://tempuri.org/IVirtualiaSession/ID_SessionVirtualiaResponse")>  _
        Function ID_SessionVirtualia(ByVal Identification As String, ByVal NoSession As String) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ID_SessionVirtualia", ReplyAction:="http://tempuri.org/IVirtualiaSession/ID_SessionVirtualiaResponse")>  _
        Function ID_SessionVirtualiaAsync(ByVal Identification As String, ByVal NoSession As String) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ListeContextes", ReplyAction:="http://tempuri.org/IVirtualiaSession/ListeContextesResponse")>  _
        Function ListeContextes() As System.Collections.Generic.List(Of ServiceSessions.ContexteUtilisateurType)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ListeContextes", ReplyAction:="http://tempuri.org/IVirtualiaSession/ListeContextesResponse")>  _
        Function ListeContextesAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of ServiceSessions.ContexteUtilisateurType))
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ListeSessions", ReplyAction:="http://tempuri.org/IVirtualiaSession/ListeSessionsResponse")>  _
        Function ListeSessions() As System.Collections.Generic.List(Of ServiceSessions.SessionUtilisateurType)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/ListeSessions", ReplyAction:="http://tempuri.org/IVirtualiaSession/ListeSessionsResponse")>  _
        Function ListeSessionsAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of ServiceSessions.SessionUtilisateurType))
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/NouveauPassword", ReplyAction:="http://tempuri.org/IVirtualiaSession/NouveauPasswordResponse")>  _
        Function NouveauPassword(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Email As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/NouveauPassword", ReplyAction:="http://tempuri.org/IVirtualiaSession/NouveauPasswordResponse")>  _
        Function NouveauPasswordAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Email As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/SiConnexionAutorisee", ReplyAction:="http://tempuri.org/IVirtualiaSession/SiConnexionAutoriseeResponse")>  _
        Function SiConnexionAutorisee(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal InfosCnx As String, ByVal Identification As String, ByVal Password As String) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/SiConnexionAutorisee", ReplyAction:="http://tempuri.org/IVirtualiaSession/SiConnexionAutoriseeResponse")>  _
        Function SiConnexionAutoriseeAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal InfosCnx As String, ByVal Identification As String, ByVal Password As String) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/SiModificationPasswordOK", ReplyAction:="http://tempuri.org/IVirtualiaSession/SiModificationPasswordOKResponse")>  _
        Function SiModificationPasswordOK(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Password As String) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/SiModificationPasswordOK", ReplyAction:="http://tempuri.org/IVirtualiaSession/SiModificationPasswordOKResponse")>  _
        Function SiModificationPasswordOKAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Password As String) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UrlWebApplication", ReplyAction:="http://tempuri.org/IVirtualiaSession/UrlWebApplicationResponse")>  _
        Function UrlWebApplication(ByVal NoSession As String, ByVal Index As Integer, ByVal Parametre As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UrlWebApplication", ReplyAction:="http://tempuri.org/IVirtualiaSession/UrlWebApplicationResponse")>  _
        Function UrlWebApplicationAsync(ByVal NoSession As String, ByVal Index As Integer, ByVal Parametre As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UrlWebServiceOutils", ReplyAction:="http://tempuri.org/IVirtualiaSession/UrlWebServiceOutilsResponse")>  _
        Function UrlWebServiceOutils() As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UrlWebServiceOutils", ReplyAction:="http://tempuri.org/IVirtualiaSession/UrlWebServiceOutilsResponse")>  _
        Function UrlWebServiceOutilsAsync() As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UtilisateurVirtualia", ReplyAction:="http://tempuri.org/IVirtualiaSession/UtilisateurVirtualiaResponse")>  _
        Function UtilisateurVirtualia(ByVal NoSession As String) As ServiceSessions.SessionUtilisateurType
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaSession/UtilisateurVirtualia", ReplyAction:="http://tempuri.org/IVirtualiaSession/UtilisateurVirtualiaResponse")>  _
        Function UtilisateurVirtualiaAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of ServiceSessions.SessionUtilisateurType)
    End Interface
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface IVirtualiaSessionChannel
        Inherits ServiceSessions.IVirtualiaSession, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class VirtualiaSessionClient
        Inherits System.ServiceModel.ClientBase(Of ServiceSessions.IVirtualiaSession)
        Implements ServiceSessions.IVirtualiaSession
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        Public Function ContexteSession(ByVal NoSession As String) As ServiceSessions.ContexteUtilisateurType Implements ServiceSessions.IVirtualiaSession.ContexteSession
            Return MyBase.Channel.ContexteSession(NoSession)
        End Function
        
        Public Function ContexteSessionAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of ServiceSessions.ContexteUtilisateurType) Implements ServiceSessions.IVirtualiaSession.ContexteSessionAsync
            Return MyBase.Channel.ContexteSessionAsync(NoSession)
        End Function
        
        Public Function Deconnexion(ByVal NoSession As String) As Boolean Implements ServiceSessions.IVirtualiaSession.Deconnexion
            Return MyBase.Channel.Deconnexion(NoSession)
        End Function
        
        Public Function DeconnexionAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceSessions.IVirtualiaSession.DeconnexionAsync
            Return MyBase.Channel.DeconnexionAsync(NoSession)
        End Function
        
        Public Function ID_SessionVirtualia(ByVal Identification As String, ByVal NoSession As String) As Boolean Implements ServiceSessions.IVirtualiaSession.ID_SessionVirtualia
            Return MyBase.Channel.ID_SessionVirtualia(Identification, NoSession)
        End Function
        
        Public Function ID_SessionVirtualiaAsync(ByVal Identification As String, ByVal NoSession As String) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceSessions.IVirtualiaSession.ID_SessionVirtualiaAsync
            Return MyBase.Channel.ID_SessionVirtualiaAsync(Identification, NoSession)
        End Function
        
        Public Function ListeContextes() As System.Collections.Generic.List(Of ServiceSessions.ContexteUtilisateurType) Implements ServiceSessions.IVirtualiaSession.ListeContextes
            Return MyBase.Channel.ListeContextes
        End Function
        
        Public Function ListeContextesAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of ServiceSessions.ContexteUtilisateurType)) Implements ServiceSessions.IVirtualiaSession.ListeContextesAsync
            Return MyBase.Channel.ListeContextesAsync
        End Function
        
        Public Function ListeSessions() As System.Collections.Generic.List(Of ServiceSessions.SessionUtilisateurType) Implements ServiceSessions.IVirtualiaSession.ListeSessions
            Return MyBase.Channel.ListeSessions
        End Function
        
        Public Function ListeSessionsAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of ServiceSessions.SessionUtilisateurType)) Implements ServiceSessions.IVirtualiaSession.ListeSessionsAsync
            Return MyBase.Channel.ListeSessionsAsync
        End Function
        
        Public Function NouveauPassword(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Email As String) As String Implements ServiceSessions.IVirtualiaSession.NouveauPassword
            Return MyBase.Channel.NouveauPassword(NoSgbd, ModeCnx, Identification, Email)
        End Function
        
        Public Function NouveauPasswordAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Email As String) As System.Threading.Tasks.Task(Of String) Implements ServiceSessions.IVirtualiaSession.NouveauPasswordAsync
            Return MyBase.Channel.NouveauPasswordAsync(NoSgbd, ModeCnx, Identification, Email)
        End Function
        
        Public Function SiConnexionAutorisee(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal InfosCnx As String, ByVal Identification As String, ByVal Password As String) As Boolean Implements ServiceSessions.IVirtualiaSession.SiConnexionAutorisee
            Return MyBase.Channel.SiConnexionAutorisee(NoSgbd, ModeCnx, InfosCnx, Identification, Password)
        End Function
        
        Public Function SiConnexionAutoriseeAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal InfosCnx As String, ByVal Identification As String, ByVal Password As String) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceSessions.IVirtualiaSession.SiConnexionAutoriseeAsync
            Return MyBase.Channel.SiConnexionAutoriseeAsync(NoSgbd, ModeCnx, InfosCnx, Identification, Password)
        End Function
        
        Public Function SiModificationPasswordOK(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Password As String) As Boolean Implements ServiceSessions.IVirtualiaSession.SiModificationPasswordOK
            Return MyBase.Channel.SiModificationPasswordOK(NoSgbd, ModeCnx, Identification, Password)
        End Function
        
        Public Function SiModificationPasswordOKAsync(ByVal NoSgbd As Integer, ByVal ModeCnx As String, ByVal Identification As String, ByVal Password As String) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceSessions.IVirtualiaSession.SiModificationPasswordOKAsync
            Return MyBase.Channel.SiModificationPasswordOKAsync(NoSgbd, ModeCnx, Identification, Password)
        End Function
        
        Public Function UrlWebApplication(ByVal NoSession As String, ByVal Index As Integer, ByVal Parametre As String) As String Implements ServiceSessions.IVirtualiaSession.UrlWebApplication
            Return MyBase.Channel.UrlWebApplication(NoSession, Index, Parametre)
        End Function
        
        Public Function UrlWebApplicationAsync(ByVal NoSession As String, ByVal Index As Integer, ByVal Parametre As String) As System.Threading.Tasks.Task(Of String) Implements ServiceSessions.IVirtualiaSession.UrlWebApplicationAsync
            Return MyBase.Channel.UrlWebApplicationAsync(NoSession, Index, Parametre)
        End Function
        
        Public Function UrlWebServiceOutils() As String Implements ServiceSessions.IVirtualiaSession.UrlWebServiceOutils
            Return MyBase.Channel.UrlWebServiceOutils
        End Function
        
        Public Function UrlWebServiceOutilsAsync() As System.Threading.Tasks.Task(Of String) Implements ServiceSessions.IVirtualiaSession.UrlWebServiceOutilsAsync
            Return MyBase.Channel.UrlWebServiceOutilsAsync
        End Function
        
        Public Function UtilisateurVirtualia(ByVal NoSession As String) As ServiceSessions.SessionUtilisateurType Implements ServiceSessions.IVirtualiaSession.UtilisateurVirtualia
            Return MyBase.Channel.UtilisateurVirtualia(NoSession)
        End Function
        
        Public Function UtilisateurVirtualiaAsync(ByVal NoSession As String) As System.Threading.Tasks.Task(Of ServiceSessions.SessionUtilisateurType) Implements ServiceSessions.IVirtualiaSession.UtilisateurVirtualiaAsync
            Return MyBase.Channel.UtilisateurVirtualiaAsync(NoSession)
        End Function
    End Class
End Namespace
