﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VArmoirePER
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsAppelant As String = ""
    Private WsParametre As String = ""
    Private WsNomStateCache As String = "VPerArmoire"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheArmoire

    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler
    Public Delegate Sub Choix_MenuEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ArmoireSelectionEventArgs)
    Public Event Choix_Click As Choix_MenuEventHandler

    Protected Overridable Sub VChoix_Click(ByVal e As Virtualia.Systeme.Evenements.ArmoireSelectionEventArgs)
        RaiseEvent Choix_Click(Me, e)
    End Sub

    Protected Overridable Sub VDossier_Click(ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheArmoire
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheArmoire)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheArmoire
            NewCache = New Virtualia.Net.VCaches.CacheArmoire
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheArmoire)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_Appelant As String
        Get
            Return WsAppelant
        End Get
        Set(ByVal value As String)
            WsAppelant = value
        End Set
    End Property

    Public Property V_Parametre As String
        Get
            Return WsParametre
        End Get
        Set(value As String)
            WsParametre = value
        End Set
    End Property

    Private Property V_ListeCoches As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.ListeCoches
        End Get
        Set(ByVal value As List(Of String))
            If TreeListeDossier.CheckedNodes.Count = 0 Then
                Exit Property
            End If
            Dim IndiceA As Integer
            Dim VCache As New List(Of String)
            For IndiceA = 0 To TreeListeDossier.CheckedNodes.Count - 1
                VCache.Add(TreeListeDossier.CheckedNodes(IndiceA).ValuePath)
            Next IndiceA
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.ListeCoches = VCache
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Sub V_ListePerClear()
        WsCtl_Cache = CacheVirControle
        VListePer.Items.Clear()
        WsCtl_Cache.Ide_Selection = 0
        EtiSelection.Text = ""
        EtiSelection.ToolTip = ""
        EtiStatus2.Text = ""
        WsCtl_Cache.DataPath_Selection = ""
        WsCtl_Cache.ListeCriteres = Nothing
        WsCtl_Cache.ListeIde = Nothing
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FaireListeOrganisee()
        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim IndiceA As Integer
        Dim RuptureN1 As String = ""
        Dim RuptureN2 As String = ""
        Dim RuptureN3 As String = ""
        Dim RuptureN4 As String = ""
        Dim RuptureN5 As String = ""
        Dim RuptureN6 As String = ""
        Dim RuptureN7 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImageN2 = "~/Images/Armoire/JauneFermer16.bmp"
        Dim UrlImageN3 = "~/Images/Armoire/VertFonceFermer16.bmp"
        Dim UrlImageN4 = "~/Images/Armoire/GrisClairFermer16.bmp"
        Dim UrlImageN5 = "~/Images/Armoire/OrangeFonceFermer16.bmp"
        Dim UrlImageN6 = "~/Images/Armoire/RougeFermer16.bmp"
        Dim UrlImageN7 = "~/Images/Armoire/BleuFonceFermer16.bmp"
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN1 As TreeNode = Nothing
        Dim NewNoeudN2 As TreeNode = Nothing
        Dim NewNoeudN3 As TreeNode = Nothing
        Dim NewNoeudN4 As TreeNode = Nothing
        Dim NewNoeudN5 As TreeNode = Nothing
        Dim NewNoeudN6 As TreeNode = Nothing
        Dim NewNoeudN7 As TreeNode = Nothing
        Dim CptN1 As Integer = 0
        Dim CptN2 As Integer = 0
        Dim CptN3 As Integer = 0
        Dim CptN4 As Integer = 0
        Dim CptN5 As Integer = 0
        Dim CptN6 As Integer = 0
        Dim SiAFaire As Boolean
        Dim Etablissement As String = WebFct.PointeurUtilisateur.Param_Etablissement

        If Etablissement = "(Tous)" Then
            Etablissement = ""
        End If

        TreeListeDossier.Nodes.Clear()

        EtiStatus1.Text = ""
        EtiStatus2.Text = ""
        TableOnglets.Visible = True
        LstVuesDyna = WebFct.PointeurUtilisateur.PointeurListeCourantePER
        If LstVuesDyna Is Nothing Then
            Exit Sub
        End If
        If LstVuesDyna.Count = 0 Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        TreeListeDossier.LevelStyles.Clear()
        TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.All

        For Each VueDyna As Virtualia.Net.ServiceServeur.VirRequeteType In LstVuesDyna
            SiAFaire = True
            If VueDyna.Valeurs(2) = "" Then
                VueDyna.Valeurs(2) = "(non renseigné)"
            End If
            If Etablissement <> "" Then
                If VueDyna.Valeurs(0) <> Etablissement Then
                    SiAFaire = False
                End If
            End If
            If SiAFaire = True Then
                Select Case VueDyna.Valeurs(0)
                    Case Is <> RuptureN1
                        If NewNoeudN7 IsNot Nothing Then
                            If NewNoeudN7.ChildNodes.Count > 1 Then
                                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                            Else
                                NewNoeudN7.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN6 IsNot Nothing Then
                            If CptN6 > 1 Then
                                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                            Else
                                NewNoeudN6.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN5 IsNot Nothing Then
                            If CptN5 > 1 Then
                                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                            Else
                                NewNoeudN5.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN4 IsNot Nothing Then
                            If CptN4 > 1 Then
                                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                            Else
                                NewNoeudN4.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN3 IsNot Nothing Then
                            If CptN3 > 1 Then
                                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                            Else
                                NewNoeudN3.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN2 IsNot Nothing Then
                            If CptN2 > 1 Then
                                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                            Else
                                NewNoeudN2.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN1 IsNot Nothing Then
                            If CptN1 > 1 Then
                                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
                            Else
                                NewNoeudN1.ToolTip = "Un dossier"
                            End If
                        End If
                        RuptureN1 = VueDyna.Valeurs(0)
                        RuptureN2 = ""
                        RuptureN3 = ""
                        RuptureN4 = ""
                        RuptureN5 = ""
                        RuptureN6 = ""
                        RuptureN7 = ""
                        NewNoeudN2 = Nothing
                        NewNoeudN3 = Nothing
                        NewNoeudN4 = Nothing
                        NewNoeudN5 = Nothing
                        NewNoeudN6 = Nothing
                        NewNoeudN7 = Nothing
                        CptN1 = 0
                        CptN2 = 0
                        CptN3 = 0
                        CptN4 = 0
                        CptN5 = 0
                        CptN6 = 0

                        NewNoeudN1 = New TreeNode(RuptureN1, "N1" & VI.Tild & IndiceA.ToString)
                        NewNoeudN1.ImageUrl = UrlImageN1

                        NewNoeudN1.PopulateOnDemand = False
                        NewNoeudN1.ShowCheckBox = True
                        NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                        TreeListeDossier.Nodes.Add(NewNoeudN1)
                End Select

                Select Case VueDyna.Valeurs(1)
                    Case Is <> RuptureN2
                        If NewNoeudN7 IsNot Nothing Then
                            If NewNoeudN7.ChildNodes.Count > 1 Then
                                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                            Else
                                NewNoeudN7.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN6 IsNot Nothing Then
                            If CptN6 > 1 Then
                                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                            Else
                                NewNoeudN6.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN5 IsNot Nothing Then
                            If CptN5 > 1 Then
                                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                            Else
                                NewNoeudN5.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN4 IsNot Nothing Then
                            If CptN4 > 1 Then
                                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                            Else
                                NewNoeudN4.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN3 IsNot Nothing Then
                            If CptN3 > 1 Then
                                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                            Else
                                NewNoeudN3.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN2 IsNot Nothing Then
                            If CptN2 > 1 Then
                                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                            Else
                                NewNoeudN2.ToolTip = "Un dossier"
                            End If
                        End If
                        RuptureN2 = VueDyna.Valeurs(1)
                        RuptureN3 = ""
                        RuptureN4 = ""
                        RuptureN5 = ""
                        RuptureN6 = ""
                        RuptureN7 = ""
                        NewNoeudN3 = Nothing
                        NewNoeudN4 = Nothing
                        NewNoeudN5 = Nothing
                        NewNoeudN6 = Nothing
                        NewNoeudN7 = Nothing
                        CptN2 = 0
                        CptN3 = 0
                        CptN4 = 0
                        CptN5 = 0
                        CptN6 = 0

                        If NewNoeudN1 IsNot Nothing Then
                            NewNoeudN2 = New TreeNode(RuptureN2, "N2" & VI.Tild & RuptureN1 & VI.Tild & IndiceA.ToString)
                            NewNoeudN2.ImageUrl = UrlImageN2
                            NewNoeudN2.PopulateOnDemand = False
                            NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                            NewNoeudN1.ChildNodes.Add(NewNoeudN2)
                        End If
                End Select

                If VueDyna.Valeurs.Count > 2 Then
                    If VueDyna.Valeurs(2) <> "" Then
                        Select Case VueDyna.Valeurs(2)
                            Case Is <> RuptureN3
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN4 IsNot Nothing Then
                                    If CptN4 > 1 Then
                                        NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                    Else
                                        NewNoeudN4.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN3 IsNot Nothing Then
                                    If CptN3 > 1 Then
                                        NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                                    Else
                                        NewNoeudN3.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN3 = VueDyna.Valeurs(2)
                                RuptureN4 = ""
                                RuptureN5 = ""
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN4 = Nothing
                                NewNoeudN5 = Nothing
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN3 = 0
                                CptN4 = 0
                                CptN5 = 0
                                CptN6 = 0

                                If NewNoeudN2 IsNot Nothing Then
                                    NewNoeudN3 = New TreeNode(RuptureN3, "N3" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild & IndiceA.ToString)
                                    NewNoeudN3.ImageUrl = UrlImageN3
                                    NewNoeudN3.PopulateOnDemand = False
                                    NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                                    NewNoeudN2.ChildNodes.Add(NewNoeudN3)
                                End If
                        End Select
                    End If
                End If

                If VueDyna.Valeurs.Count > 3 Then
                    If VueDyna.Valeurs(3) <> "" Then
                        Select Case VueDyna.Valeurs(3)
                            Case Is <> RuptureN4
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN4 IsNot Nothing Then
                                    If CptN4 > 1 Then
                                        NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                    Else
                                        NewNoeudN4.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN4 = VueDyna.Valeurs(3)
                                RuptureN5 = ""
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN5 = Nothing
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN4 = 0
                                CptN5 = 0
                                CptN6 = 0

                                NewNoeudN4 = New TreeNode(RuptureN4, "N4" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & IndiceA.ToString)
                                NewNoeudN4.ImageUrl = UrlImageN4
                                NewNoeudN4.PopulateOnDemand = False
                                NewNoeudN4.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN3 Is Nothing Then
                                    If NewNoeudN2 IsNot Nothing Then
                                        NewNoeudN2.ChildNodes.Add(NewNoeudN4)
                                    End If
                                Else
                                    NewNoeudN3.ChildNodes.Add(NewNoeudN4)
                                End If
                        End Select
                    End If
                End If

                If VueDyna.Valeurs.Count > 4 Then
                    If VueDyna.Valeurs(4) <> "" Then
                        Select Case VueDyna.Valeurs(4)
                            Case Is <> RuptureN5
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN5 = VueDyna.Valeurs(4)
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN5 = 0
                                CptN6 = 0

                                NewNoeudN5 = New TreeNode(RuptureN5, "N5" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & IndiceA.ToString)
                                NewNoeudN5.ImageUrl = UrlImageN5
                                NewNoeudN5.PopulateOnDemand = False
                                NewNoeudN5.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN4 Is Nothing Then
                                    If NewNoeudN3 Is Nothing Then
                                        If NewNoeudN2 IsNot Nothing Then
                                            NewNoeudN2.ChildNodes.Add(NewNoeudN5)
                                        End If
                                    Else
                                        NewNoeudN3.ChildNodes.Add(NewNoeudN5)
                                    End If
                                Else
                                    NewNoeudN4.ChildNodes.Add(NewNoeudN5)
                                End If
                        End Select
                    End If
                End If

                If VueDyna.Valeurs.Count > 5 And WsCtl_Cache.Index_Liste = 2 Then
                    If VueDyna.Valeurs(5) <> "" Then
                        Select Case VueDyna.Valeurs(5)
                            Case Is <> RuptureN6
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN6 = VueDyna.Valeurs(5)
                                RuptureN7 = ""
                                NewNoeudN7 = Nothing
                                CptN6 = 0

                                NewNoeudN6 = New TreeNode(RuptureN6, "N6" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & RuptureN5 & VI.Tild & IndiceA.ToString)
                                NewNoeudN6.ImageUrl = UrlImageN6
                                NewNoeudN6.PopulateOnDemand = False
                                NewNoeudN6.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN5 Is Nothing Then
                                    If NewNoeudN4 Is Nothing Then
                                        If NewNoeudN3 Is Nothing Then
                                            If NewNoeudN2 IsNot Nothing Then
                                                NewNoeudN2.ChildNodes.Add(NewNoeudN6)
                                            End If
                                        Else
                                            NewNoeudN3.ChildNodes.Add(NewNoeudN6)
                                        End If
                                    Else
                                        NewNoeudN4.ChildNodes.Add(NewNoeudN6)
                                    End If
                                Else
                                    NewNoeudN5.ChildNodes.Add(NewNoeudN6)
                                End If
                        End Select
                    End If
                End If

                If VueDyna.Valeurs.Count > 6 And WsCtl_Cache.Index_Liste = 2 Then
                    If VueDyna.Valeurs(6) <> "" Then
                        Select Case VueDyna.Valeurs(6)
                            Case Is <> RuptureN7
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN7 = VueDyna.Valeurs(6)

                                NewNoeudN7 = New TreeNode(RuptureN7, "N7" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & RuptureN5 & VI.Tild _
                                                          & RuptureN6 & VI.Tild & IndiceA.ToString)
                                NewNoeudN7.ImageUrl = UrlImageN7
                                NewNoeudN7.PopulateOnDemand = False
                                NewNoeudN7.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN6 Is Nothing Then
                                    If NewNoeudN5 Is Nothing Then
                                        If NewNoeudN4 Is Nothing Then
                                            If NewNoeudN3 Is Nothing Then
                                                If NewNoeudN2 IsNot Nothing Then
                                                    NewNoeudN2.ChildNodes.Add(NewNoeudN7)
                                                End If
                                            Else
                                                NewNoeudN3.ChildNodes.Add(NewNoeudN7)
                                            End If
                                        Else
                                            NewNoeudN4.ChildNodes.Add(NewNoeudN7)
                                        End If
                                    Else
                                        NewNoeudN5.ChildNodes.Add(NewNoeudN7)
                                    End If
                                Else
                                    NewNoeudN6.ChildNodes.Add(NewNoeudN7)
                                End If
                        End Select
                    End If
                    CptN1 += 1
                    CptN2 += 1
                    CptN3 += 1
                    CptN4 += 1
                    CptN5 += 1
                    CptN6 += 1
                End If
            End If
        Next

        If NewNoeudN7 IsNot Nothing Then
            If NewNoeudN7.ChildNodes.Count > 1 Then
                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
            Else
                NewNoeudN7.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN6 IsNot Nothing Then
            If CptN6 > 1 Then
                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
            Else
                NewNoeudN6.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN5 IsNot Nothing Then
            If CptN5 > 1 Then
                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
            Else
                NewNoeudN5.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN4 IsNot Nothing Then
            If CptN4 > 1 Then
                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
            Else
                NewNoeudN4.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN3 IsNot Nothing Then
            If CptN3 > 1 Then
                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
            Else
                NewNoeudN3.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN2 IsNot Nothing Then
            If CptN2 > 1 Then
                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
            Else
                NewNoeudN2.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN1 IsNot Nothing Then
            If CptN1 > 1 Then
                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
            Else
                NewNoeudN1.ToolTip = "Un dossier"
            End If
        End If

        EtiStatus1.Text = LstVuesDyna.Count.ToString
        EtiStatus2.Text = VListePer.Items.Count.ToString

        Dim SelCoches As List(Of String) = V_ListeCoches
        If SelCoches IsNot Nothing Then
            For IndiceA = 0 To SelCoches.Count - 1
                Try
                    NewNoeudN1 = TreeListeDossier.FindNode(SelCoches(IndiceA))
                    If NewNoeudN1 IsNot Nothing Then
                        NewNoeudN1.Checked = True
                    End If
                Catch ex As Exception
                    NewNoeudN1 = Nothing
                End Try
            Next IndiceA
        End If

        RuptureN1 = WsCtl_Cache.DataPath_Selection
        If RuptureN1 <> "" Then
            Try
                NewNoeudN1 = TreeListeDossier.FindNode(RuptureN1)
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
            If NewNoeudN1 IsNot Nothing Then
                NewNoeudN1.Checked = True
                Select Case NewNoeudN1.Depth
                    Case Is = 0
                        NewNoeudN1.ExpandAll()
                    Case Is = 1
                        NewNoeudN1.Parent.ExpandAll()
                    Case Is = 2
                        NewNoeudN1.Parent.Parent.ExpandAll()
                    Case Is = 3
                        NewNoeudN1.Parent.Parent.Parent.ExpandAll()
                    Case Is = 4
                        NewNoeudN1.Parent.Parent.Parent.Parent.ExpandAll()
                    Case Is = 5
                        NewNoeudN1.Parent.Parent.Parent.Parent.Parent.ExpandAll()
                End Select
            Else
                TreeListeDossier.CollapseAll()
            End If
        Else
            TreeListeDossier.CollapseAll()
        End If
    End Sub

    Private Sub FaireListeAlphabetique()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim IndiceA As Integer = 0
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim Etablissement As String = WebFct.PointeurUtilisateur.Param_Etablissement

        TreeListeDossier.Nodes.Clear()
        TableOnglets.Visible = False

        EtiStatus1.Text = ""
        EtiStatus2.Text = ""
        LstVuesDyna = WebFct.PointeurUtilisateur.PointeurListeCourantePER
        If LstVuesDyna Is Nothing Then
            Exit Sub
        End If
        If LstVuesDyna.Count = 0 Then
            Exit Sub
        End If

        Call StylerlArmoire(1)

        For Each VueDyna As Virtualia.Net.ServiceServeur.VirRequeteType In LstVuesDyna
            If IndiceA < 250 Then
                Dim NewNoeudPER As TreeNode = New TreeNode(VueDyna.Valeurs(0) & Strings.Space(1) & VueDyna.Valeurs(1), VueDyna.Ide_Dossier.ToString)
                NewNoeudPER.ImageUrl = UrlImagePer
                NewNoeudPER.PopulateOnDemand = False
                NewNoeudPER.SelectAction = TreeNodeSelectAction.Select
                TreeListeDossier.Nodes.Add(NewNoeudPER)
            End If
            IndiceA += 1
        Next

        EtiStatus1.Text = IndiceA.ToString
        EtiStatus2.Text = ""
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Call FaireListeChoix()
    End Sub

    Private Sub StylerlArmoire(ByVal Profondeur As Integer)
        Dim Vstyle As System.Web.UI.WebControls.TreeNodeStyle
        TreeListeDossier.LevelStyles.Clear()

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If

        If Profondeur = 1 Then
            TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.None

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Italic = True
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(1)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#142425")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 2 Then
            TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.Root

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.All

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#1C5151")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 3 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#137A76")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 4 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            TreeListeDossier.LevelStyles.Add(Vstyle)
        End If

    End Sub

    Private Sub TreeListeDossier_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeDossier.SelectedNodeChanged
        WsCtl_Cache = CacheVirControle
        Dim ObjetReparti As String = WsCtl_Cache.Objet_Selection
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If

        If ObjetReparti = "" Then 'Liste Alphabétique
            Dim SelIde As Integer = 0
            Dim Tableaudata(0) As String

            If IsNumeric(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value) Then
                SelIde = CInt(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value)
            Else
                If Strings.InStr(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, "-") = 0 Then
                    Exit Sub
                End If
                Tableaudata = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, "-", -1)
                If IsNumeric(Tableaudata(Tableaudata.Count - 1)) Then
                    SelIde = CInt(Tableaudata(Tableaudata.Count - 1))
                End If
            End If
            WsCtl_Cache.Ide_Selection = SelIde
            CacheVirControle = WsCtl_Cache
            If SelIde = 0 Then
                Exit Sub
            End If
            Dim Evenement As Virtualia.Systeme.Evenements.DossierClickEventArgs = Nothing
            Evenement = New Virtualia.Systeme.Evenements.DossierClickEventArgs(WsCtl_Cache.Ide_Selection, "")
            VDossier_Click(Evenement)

        Else
            Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
            Dim IndiceI As Integer
            Dim TableauData(0) As String
            Dim ChaineSel As String = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
            Dim Etablissement As String = ""
            Dim Sel1 As String = ""
            Dim Sel2 As String = ""
            Dim Sel3 As String = ""
            Dim Sel4 As String = ""
            Dim Sel5 As String = ""
            Dim Sel6 As String = ""
            Dim LstCriteres As List(Of String)
            TableauData = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, VI.Tild, -1)
            LstCriteres = New List(Of String)
            LstCriteres.Add(ObjetReparti)
            Select Case TableauData(0)
                Case "N1"
                    Etablissement = ChaineSel
                    LstCriteres.Add(ChaineSel)
                Case "N2"
                    Etablissement = TableauData(1)
                    Sel1 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    LstCriteres.Add(ChaineSel)
                Case "N3"
                    Etablissement = TableauData(1)
                    Sel1 = TableauData(2)
                    Sel2 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    LstCriteres.Add(TableauData(2))
                    LstCriteres.Add(ChaineSel)
                Case "N4"
                    Etablissement = TableauData(1)
                    Sel1 = TableauData(2)
                    Sel2 = TableauData(3)
                    Sel3 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    For IndiceI = 2 To 3
                        LstCriteres.Add(TableauData(IndiceI))
                    Next IndiceI
                    LstCriteres.Add(ChaineSel)
                Case "N5"
                    Etablissement = TableauData(1)
                    Sel1 = TableauData(2)
                    Sel2 = TableauData(3)
                    Sel3 = TableauData(4)
                    Sel4 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    For IndiceI = 2 To 4
                        LstCriteres.Add(TableauData(IndiceI))
                    Next IndiceI
                    LstCriteres.Add(ChaineSel)
                Case "N6"
                    Etablissement = TableauData(1)
                    Sel1 = TableauData(2)
                    Sel2 = TableauData(3)
                    Sel3 = TableauData(4)
                    Sel4 = TableauData(5)
                    Sel5 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    For IndiceI = 2 To 5
                        LstCriteres.Add(TableauData(IndiceI))
                    Next IndiceI
                    LstCriteres.Add(ChaineSel)
                Case "N7"
                    Etablissement = TableauData(1)
                    Sel1 = TableauData(2)
                    Sel2 = TableauData(3)
                    Sel3 = TableauData(4)
                    Sel4 = TableauData(5)
                    Sel5 = TableauData(6)
                    Sel6 = ChaineSel
                    LstCriteres.Add(Etablissement)
                    For IndiceI = 2 To 6
                        LstCriteres.Add(TableauData(IndiceI))
                    Next IndiceI
                    LstCriteres.Add(ChaineSel)
            End Select
            Select Case ObjetReparti
                Case "Organigramme"
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.AffectationsFonctionnelles(Etablissement, Sel1, Sel2, Sel3, Sel4, Sel5, Sel6)
                Case "Carrieres"
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.SituationsAdministratives(Etablissement, Sel1, Sel2, Sel3, Sel4)
            End Select
            If LstVuesDyna Is Nothing Then
                Exit Sub
            End If

            Call V_ListePerClear()

            EtiSelection.Text = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
            EtiSelection.ToolTip = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value
            EtiStatus2.Text = ""
            Dim ItemPer As System.Web.UI.WebControls.ListItem
            Dim LstPERBrut As New List(Of System.Web.UI.WebControls.ListItem)
            Dim LstPERTrie As List(Of System.Web.UI.WebControls.ListItem)
            Dim LstIde As List(Of Integer)
            For IndiceI = 0 To LstVuesDyna.Count - 1
                Try
                    If VListePer.Items.FindByValue(CStr(LstVuesDyna.Item(IndiceI).Ide_Dossier)) Is Nothing Then
                        ItemPer = New System.Web.UI.WebControls.ListItem
                        ItemPer.Text = LstVuesDyna.Item(IndiceI).Valeurs(LstVuesDyna.Item(IndiceI).Valeurs.Count - 2) & Strings.Space(2) & LstVuesDyna.Item(IndiceI).Valeurs(LstVuesDyna.Item(IndiceI).Valeurs.Count - 1) 'Nom et Prénom
                        ItemPer.Value = CStr(LstVuesDyna.Item(IndiceI).Ide_Dossier)
                        LstPERBrut.Add(ItemPer)
                    End If
                Catch ex As Exception
                    Exit Try
                End Try
            Next IndiceI
            If LstPERBrut Is Nothing Then
                Exit Sub
            End If
            LstPERTrie = (From it In LstPERBrut Order By it.Text).ToList
            LstIde = New List(Of Integer)
            For IndiceI = 0 To LstPERTrie.Count - 1
                VListePer.Items.Add(LstPERTrie(IndiceI))
                LstIde.Add(CInt(LstPERTrie(IndiceI).Value))
            Next IndiceI
            EtiStatus2.Text = VListePer.Items.Count.ToString
            WsCtl_Cache.DataPath_Selection = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath
            WsCtl_Cache.ListeCriteres = LstCriteres
            WsCtl_Cache.ListeIde = LstIde

        End If
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VListePer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VListePer.SelectedIndexChanged
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Dim SelIde As Integer = 0

        Try
            SelIde = CInt(CType(sender, System.Web.UI.WebControls.ListBox).SelectedItem.Value)
        Catch ex As Exception
            Exit Try
        End Try

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Selection = SelIde
        CacheVirControle = WsCtl_Cache
        Dim Evenement As Virtualia.Systeme.Evenements.DossierClickEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DossierClickEventArgs(SelIde, "")
        VDossier_Click(Evenement)
    End Sub

    Private Sub ExecuterListe()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        Dim ObjetdelaListe As String = ""
        Dim IndiceA As Integer

        'DonRecherche.Text = HSelLettre.Value
        Select Case WsCtl_Cache.Index_Liste
            Case 0, 1
                ObjetdelaListe = ""
            Case 2
                ObjetdelaListe = "Organigramme"
            Case 3
                ObjetdelaListe = "Carrieres"
        End Select

        WsCtl_Cache.Objet_Selection = ObjetdelaListe
        CacheVirControle = WsCtl_Cache

        If ObjetdelaListe <> "" Then
            CadrePER.Visible = True
            CelluleLettre.Visible = False
            PanelArmoire.Width = New Unit(1010)
            Select Case WsCtl_Cache.Index_Liste
                Case 2 'Organigramme
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.AffectationsFonctionnelles("", "", "", "", "", "", "")
                Case 3 'Carrieres
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.SituationsAdministratives("", "", "", "", "")
            End Select
        Else
            CadrePER.Visible = False
            CelluleLettre.Visible = True
            PanelArmoire.Width = New Unit(700)
            Select Case WsCtl_Cache.Index_Liste
                Case 0 'Les dossiers en activité
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.ListeAlphabetique(HSelLettre.Value, True)
                Case 1 'Tous les dossiers
                    LstVuesDyna = WebFct.PointeurUtilisateur.PointeurArmoirePER.ListeAlphabetique(HSelLettre.Value, False)
            End Select
        End If
        WebFct.PointeurUtilisateur.PointeurListeCourantePER = LstVuesDyna

        Select Case WsCtl_Cache.Index_Liste
            Case 0, 1
                Call FaireListeAlphabetique()
            Case Else
                Call FaireListeOrganisee()

                TreeListeDossier.CheckedNodes.Clear()
                For IndiceA = 0 To TreeListeDossier.Nodes.Count - 1
                    If TreeListeDossier.Nodes.Item(IndiceA).Text = WebFct.PointeurUtilisateur.Param_Etablissement Then
                        TreeListeDossier.Nodes.Item(IndiceA).Checked = True
                        Select Case WsCtl_Cache.Index_Liste
                            Case 2 'Organigramme
                                If WebFct.PointeurUtilisateur.Param_Niveau1 = "" Then
                                    TreeListeDossier.Nodes.Item(IndiceA).Expanded = True
                                    Exit For
                                End If
                                Dim IndiceB As Integer
                                For IndiceB = 0 To TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Count - 1
                                    If TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Item(IndiceB).Text = _
                                        WebFct.PointeurUtilisateur.Param_Niveau1 Then
                                        TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Item(IndiceB).Checked = True
                                        If TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Item(IndiceB).Depth > 0 Then
                                            TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Item(IndiceB).Parent.Checked = False
                                            TreeListeDossier.Nodes.Item(IndiceA).ChildNodes.Item(IndiceB).Parent.Expanded = True
                                            Exit For
                                        End If
                                    End If
                                Next IndiceB
                                Exit For
                            Case Else
                                TreeListeDossier.Nodes.Item(IndiceA).Expanded = True
                                Exit For
                        End Select
                    End If
                Next IndiceA
        End Select

    End Sub

    Private Sub DropDownArmoire_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownArmoire.SelectedIndexChanged
        Dim SelIndex As Integer
        Call V_ListePerClear()

        SelIndex = CInt(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedIndex)
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_Liste = SelIndex
        CacheVirControle = WsCtl_Cache
        If SelIndex = 0 Or SelIndex = 1 Then
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            If WebFct.PointeurUtilisateur Is Nothing Then
                Exit Sub
            End If
            If WebFct.PointeurUtilisateur.Etablissement <> "" And WebFct.PointeurUtilisateur.Etablissement <> "(Tous)" Then
                HSelLettre.Value = ""
            ElseIf HSelLettre.Value = "" Then
                HSelLettre.Value = "A"
            End If
        End If
        Call ExecuterListe()
    End Sub

    Private Sub FaireListeChoix()
        If WsAppelant = "" Then
            Exit Sub
        End If
        If DropDownArmoire.Items.Count > 0 Then
            Exit Sub
        End If
        DropDownArmoire.Items.Add("Liste alphabétique des dossiers en activité")
        DropDownArmoire.Items.Add("Liste alphabétique de tous les dossiers")
        DropDownArmoire.Items.Add("Par affectations fonctionnelles")
        DropDownArmoire.Items.Add("Par situations administratives")
        WsCtl_Cache = CacheVirControle
        DropDownArmoire.SelectedIndex = WsCtl_Cache.Index_Liste

        Call ExecuterListe()
    End Sub

    Private Sub ButtonA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonA.Click, _
    ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, _
    ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, _
    ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, _
    ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonAll.Click

        Call InitialiserBoutonsLettre()
        If CType(sender, ImageButton).ID = "ButtonAll" Then
            HSelLettre.Value = ""
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & "Arobase_Sel.bmp"
        Else
            HSelLettre.Value = Strings.Right(CType(sender, ImageButton).ID, 1)
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(sender, ImageButton).ID, 1) & "_Sel.bmp"
        End If
        Call ExecuterListe()
    End Sub

    Private Sub InitialiserBoutonsLettre()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Do
            Ctl = WebFct.VirWebControle(CadreLettre, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            If CType(Ctl, ImageButton).ID = "ButtonAll" Then
                CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & "Arobase.bmp"
            Else
                CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(Ctl, ImageButton).ID, 1) & ".bmp"
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub CommandeCsv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeCsv.Click

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim CarSep As String = Strings.Chr(9)
        Dim Suffixe As String = "." & Strings.Right(CType(sender, System.Web.UI.WebControls.Button).ID, 3)
        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        LstVuesDyna = WebFct.PointeurUtilisateur.PointeurListeCourantePER
        If LstVuesDyna Is Nothing Then
            Exit Sub
        End If
        If LstVuesDyna.Count = 0 Then
            Exit Sub
        End If
        Dim CodeIso As System.Text.Encoding = System.Text.Encoding.UTF8
        Dim CodeUnicode As System.Text.Encoding = System.Text.Encoding.Unicode
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim ChemindeBase As String
        Dim NomRepertoire As String
        Dim NomFichier As String
        Dim Chaine As String
        Dim FluxTeleChargement As Byte()
        Dim IndiceI As Integer
        Dim IndiceK As Integer

        ChemindeBase = Request.PhysicalApplicationPath
        If Strings.Right(ChemindeBase, 1) <> "\" Then
            ChemindeBase &= "\"
        End If
        NomRepertoire = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID
        If My.Computer.FileSystem.DirectoryExists(NomRepertoire) = False Then
            My.Computer.FileSystem.CreateDirectory(NomRepertoire)
        End If
        NomFichier = "Armoire_" & WebFct.PointeurUtilisateur.Param_Annee & "_" & WebFct.ViRhFonction.ChaineSansAccent(WebFct.PointeurUtilisateur.Param_Etablissement, False) & Suffixe

        FicStream = New System.IO.FileStream(NomRepertoire & "\" & NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, CodeUnicode)
        For IndiceI = 0 To LstVuesDyna.Count - 1
            Chaine = ""
            For IndiceK = 0 To LstVuesDyna.Item(IndiceI).Valeurs.Count - 1
                Chaine &= LstVuesDyna.Item(IndiceI).Valeurs(IndiceK) & CarSep
            Next IndiceK
            FicWriter.WriteLine(Chaine)
        Next IndiceI
        FicWriter.Flush()
        FicWriter.Close()
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomRepertoire & "\" & NomFichier)

        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub MenuChoix_MenuItemClick(sender As Object, e As MenuEventArgs) Handles MenuChoix.MenuItemClick
        Dim Evenement As Virtualia.Systeme.Evenements.ArmoireSelectionEventArgs
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.ListeIde Is Nothing Then
            Evenement = New Virtualia.Systeme.Evenements.ArmoireSelectionEventArgs(e.Item.Value, Nothing, Nothing)
        Else
            Evenement = New Virtualia.Systeme.Evenements.ArmoireSelectionEventArgs(e.Item.Value, WsCtl_Cache.ListeCriteres, WsCtl_Cache.ListeIde)
        End If
        VChoix_Click(Evenement)
    End Sub

    Private Sub DonRecherche_TextChanged(sender As Object, e As System.EventArgs) Handles DonRecherche.TextChanged
        HSelLettre.Value = DonRecherche.Text.ToUpper
        Call ExecuterListe()
    End Sub

End Class