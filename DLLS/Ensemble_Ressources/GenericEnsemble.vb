﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Ressources.Datas
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class GenericEnsemble
        Inherits List(Of VIR_FICHE)
        Private WsTypeModele As Integer
        Private WsNomUtilisateur As String
        Private WsPointdeVue As Integer = VI.PointdeVue.PVueApplicatif
        '
        Private WsListeIde As List(Of Integer)
        Private WsListeFiltre As List(Of String)

        Public ReadOnly Property ListeDesFiches(ByVal Ide As Integer, ByVal NumObjet As Integer) As List(Of VIR_FICHE)
            Get
                Return (From Instance In Me Where Instance.Ide_Dossier = Ide And Instance.NumeroObjet = NumObjet).ToList
            End Get
        End Property

        Public ReadOnly Property ListeDesFiches(ByVal NumObjet As Integer) As List(Of VIR_FICHE)
            Get
                Return (From Instance In Me Where Instance.NumeroObjet = NumObjet Order By Instance.Ide_Dossier).ToList
            End Get
        End Property

        Public Sub AjouterUnObjet(ByVal NumObjet As Integer)
            Call LireObjet(NumObjet)
        End Sub

        Private Sub LireObjet(ByVal NumObjet As Integer)
            Dim LstResultat As List(Of VIR_FICHE)
            If WsListeIde Is Nothing Then
                If WsListeFiltre Is Nothing Then
                    LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, WsPointdeVue, NumObjet, 0, False)
                Else
                    LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, WsPointdeVue, NumObjet, 0, False, WsListeFiltre)
                End If
            Else
                LstResultat = VirServiceServeur.LectureObjet_Plus_ToFiches(WsNomUtilisateur, WsPointdeVue, NumObjet, False, WsListeIde)
            End If
            Me.Clear()
            If LstResultat Is Nothing Then
                Exit Sub
            End If
            Me.AddRange(LstResultat)

        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer, ByVal LstFiltre As List(Of String))
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsPointdeVue = PtdeVue
            WsListeIde = Nothing
            WsListeFiltre = LstFiltre
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer, ByVal LstIde As List(Of Integer))
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsPointdeVue = PtdeVue
            WsListeIde = LstIde
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer, ByVal TabIde() As Integer)
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsPointdeVue = PtdeVue
            WsListeIde = TabIde.ToList()
            WsListeFiltre = Nothing
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer)
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsPointdeVue = PtdeVue
            WsListeIde = Nothing
            WsListeFiltre = Nothing
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal LstFiltre As List(Of String))
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsListeIde = Nothing
            WsListeFiltre = LstFiltre
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal LstIde As List(Of Integer))
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsListeIde = LstIde
            WsListeFiltre = Nothing
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String, ByVal TabIde() As Integer)
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsListeIde = TabIde.ToList()
            WsListeFiltre = Nothing
        End Sub

        Public Sub New(ByVal Modele As Integer, ByVal NomUtilisateur As String)
            MyBase.New()
            WsTypeModele = Modele
            WsNomUtilisateur = NomUtilisateur
            WsListeIde = Nothing
            WsListeFiltre = Nothing
        End Sub

    End Class
End Namespace