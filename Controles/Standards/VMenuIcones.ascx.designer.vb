﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VMenuIcones

    '''<summary>
    '''Contrôle CadreMenu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreMenu As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreChoix.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreChoix As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreTuile01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile01 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd01 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd02 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd03 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile02 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd04 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd05 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd06 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile03 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd07 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd08 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd09 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile04 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd10 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd11 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd12 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile05 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd13 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd14 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd15 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile06 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd16 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd17 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd18 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile07 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd19 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd20 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd21 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle CadreTuile08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTuile08 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Cmd22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd22 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd23 As Global.Virtualia.Net.Controles_VCoupleIconeEti

    '''<summary>
    '''Contrôle Cmd24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cmd24 As Global.Virtualia.Net.Controles_VCoupleIconeEti
End Class
