﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PerAffectationPublic
    
    '''<summary>
    '''Contrôle CadreGeneralAff.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneralAff As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitreGeneral As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreInfosAff.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfosAff As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiPerEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerEtablissement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEtablissement As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDT00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDT00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabT01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabT01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabT10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabT10 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoDT12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDT12 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabT11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabT11 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoVT13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoVT13 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
    
    '''<summary>
    '''Contrôle EtiPerFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerFonction As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreFonction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFonction As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoDF00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDF00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoDF07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoDF07 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DontabF01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabF02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabF03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF03 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabF04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF04 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabF14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF14 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle DontabF15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF15 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle EtiEmploi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEmploi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DontabF05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF05 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP17 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP18 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle DontabF16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabF16 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle CocheB12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CocheB12 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle EtiPerAdrPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPerAdrPro As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreAdrPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAdrPro As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoHP07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP07 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP11 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP02 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP08 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle Bureau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Bureau As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiAdresseBureau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAdresseBureau As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DontabP09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP09 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoHP01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP01 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP10 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP12 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoHP13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoHP13 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle DontabP14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DontabP14 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle CellReference.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellReference As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupReferentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupReferentiel As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefVirtualia As Global.Virtualia.Net.VReferentiel
    
    '''<summary>
    '''Contrôle HPopupRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRef As Global.System.Web.UI.WebControls.HiddenField
End Class
