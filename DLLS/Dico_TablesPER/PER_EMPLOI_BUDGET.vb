﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_EMPLOI_BUDGET
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsEmploiBudgetaire As String
        Private WsBrancheActivite As String
        Private WsNatureEmploi As String
        Private WsNumeroEmploi As String
        Private WsRattachement As String
        Private WsSpecialite As String
        Private WsCategorie As String
        Private WsFiliere As String
        Private WsSecteur As String
        Private WsChapitreBudgetaire As String
        Private WsGradedeReference As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_EMPLOI_BUDGET"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPostebud 'Spécifique Secteur Public
            End Get
        End Property

        Public Property EmploiBudgetaire() As String
            Get
                Return WsEmploiBudgetaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEmploiBudgetaire = value
                    Case Else
                        WsEmploiBudgetaire = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsEmploiBudgetaire
            End Set
        End Property

        Public Property BrancheActivite() As String
            Get
                Return WsBrancheActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsBrancheActivite = value
                    Case Else
                        WsBrancheActivite = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property NatureEmploi() As String
            Get
                Return WsNatureEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsNatureEmploi = value
                    Case Else
                        WsNatureEmploi = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Numero() As String
            Get
                Return WsNumeroEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumeroEmploi = value
                    Case Else
                        WsNumeroEmploi = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Rattachement() As String
            Get
                Return WsRattachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRattachement = value
                    Case Else
                        WsRattachement = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Specialite() As String
            Get
                Return WsSpecialite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsSpecialite = value
                    Case Else
                        WsSpecialite = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Secteur() As String
            Get
                Return WsSecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteur = value
                    Case Else
                        WsSecteur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property ChapitreBudgetaire() As String
            Get
                Return WsChapitreBudgetaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsChapitreBudgetaire = value
                    Case Else
                        WsChapitreBudgetaire = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Grade_de_Reference() As String
            Get
                Return WsGradedeReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGradedeReference = value
                    Case Else
                        WsGradedeReference = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(EmploiBudgetaire & VI.Tild)
                Chaine.Append(BrancheActivite & VI.Tild)
                Chaine.Append(NatureEmploi & VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Rattachement & VI.Tild)
                Chaine.Append(Specialite & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Secteur & VI.Tild)
                Chaine.Append(ChapitreBudgetaire & VI.Tild)
                Chaine.Append(Grade_de_Reference)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 14 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                EmploiBudgetaire = TableauData(2)
                BrancheActivite = TableauData(3)
                NatureEmploi = TableauData(4)
                Numero = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)
                Rattachement = TableauData(7)
                Specialite = TableauData(8)
                Categorie = TableauData(9)
                Filiere = TableauData(10)
                Secteur = TableauData(11)
                ChapitreBudgetaire = TableauData(12)
                Grade_de_Reference = TableauData(13)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return EmploiBudgetaire
                    Case 2
                        Return BrancheActivite
                    Case 3
                        Return NatureEmploi
                    Case 4
                        Return Numero
                    Case 5
                        Return Date_de_Fin
                    Case 6
                        Return Rattachement
                    Case 7
                        Return Specialite
                    Case 8
                        Return Categorie
                    Case 9
                        Return Filiere
                    Case 10
                        Return Secteur
                    Case 11
                        Return ChapitreBudgetaire
                    Case 12
                        Return Grade_de_Reference
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateValeurEmploiBudgetaire>" & MyBase.Date_de_Valeur & "</DateValeurEmploiBudgetaire>" & vbCrLf)
                Chaine.Append("<DateFinEmploiBudgetaire>" & MyBase.Date_de_Fin & "</DateFinEmploiBudgetaire>" & vbCrLf)
                Chaine.Append("<NumeroEmploiBudgetaire>" & Numero & "</NumeroEmploiBudgetaire>" & vbCrLf)
                Chaine.Append("<EmploiBudgetaire>" & VirRhFonction.ChaineXMLValide(EmploiBudgetaire) & "</EmploiBudgetaire>" & vbCrLf)
                Chaine.Append("<Categorie>" & Categorie & "</Categorie>" & vbCrLf)
                Chaine.Append("<Filiere>" & VirRhFonction.ChaineXMLValide(Filiere) & "</Filiere>" & vbCrLf)
                Chaine.Append("<Secteur>" & VirRhFonction.ChaineXMLValide(Secteur) & "</Secteur>" & vbCrLf)
                Chaine.Append("<GradeReference>" & VirRhFonction.ChaineXMLValide(Grade_de_Reference) & "</GradeReference>" & vbCrLf)
                Chaine.Append("<NatureEmploiBudgetaire>" & VirRhFonction.ChaineXMLValide(NatureEmploi) & "</NatureEmploiBudgetaire>" & vbCrLf)
                Chaine.Append("<Rattachement>" & VirRhFonction.ChaineXMLValide(Rattachement) & "</Rattachement>" & vbCrLf)
                Chaine.Append("<BrancheActivite>" & VirRhFonction.ChaineXMLValide(BrancheActivite) & "</BrancheActivite>" & vbCrLf)
                Chaine.Append("<Specialite>" & VirRhFonction.ChaineXMLValide(Specialite) & "</Specialite>" & vbCrLf)
                Chaine.Append("<ChapitreBudgetaire>" & VirRhFonction.ChaineXMLValide(ChapitreBudgetaire) & "</ChapitreBudgetaire>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

