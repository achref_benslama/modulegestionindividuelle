﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class VIL_COMMUNES
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsCodePostal As String
        Private WsCodeInsee As String
        Private WsVille As String
        Private WsAdressePostale As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "VIL_COMMUNES"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueCommunesFr
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Code_Postal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Code_Insee() As String
            Get
                Return WsCodeInsee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeInsee = value
                    Case Else
                        WsCodeInsee = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Nom_de_la_Commune() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Adresse_de_la_Commune() As String
            Get
                Return WsAdressePostale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAdressePostale = value
                    Case Else
                        WsAdressePostale = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Code_Postal & VI.Tild)
                Chaine.Append(Code_Insee & VI.Tild)
                Chaine.Append(Nom_de_la_Commune & VI.Tild)
                Chaine.Append(Adresse_de_la_Commune)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Code_Postal = TableauData(1)
                Code_Insee = TableauData(2)
                Nom_de_la_Commune = TableauData(3)
                Adresse_de_la_Commune = TableauData(4)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
