﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VLibelJourPlanning
    
    '''<summary>
    '''Contrôle TableGlobale.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableGlobale As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle TableEntete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableEntete As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LstLibelMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstLibelMois As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle LstNbMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstNbMois As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CellPagination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPagination As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle VPagination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VPagination As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PagePrecedente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PagePrecedente As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle PageEtiText.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PageEtiText As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PageSuivante.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PageSuivante As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle EtiSelection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSelection As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LibellesPlanning.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LibellesPlanning As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LibelGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LibelGauche As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiGauche As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine00 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mercredi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mercredi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Jeudi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jeudi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Vendredi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vendredi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Samedi00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Samedi00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dimanche00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dimanche00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mercredi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mercredi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Jeudi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jeudi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Vendredi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vendredi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Samedi01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Samedi01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dimanche01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dimanche01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine02 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mercredi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mercredi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Jeudi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jeudi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Vendredi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vendredi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Samedi02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Samedi02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dimanche02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dimanche02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine03 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mercredi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mercredi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Jeudi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jeudi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Vendredi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vendredi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Samedi03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Samedi03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dimanche03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dimanche03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine04 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mercredi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mercredi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Jeudi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Jeudi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Vendredi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vendredi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Samedi04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Samedi04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dimanche04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dimanche04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Semaine05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Semaine05 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle NumSemaine05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents NumSemaine05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Lundi05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lundi05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Mardi05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Mardi05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle HSelMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelMois As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HSelNombre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelNombre As Global.System.Web.UI.WebControls.HiddenField
End Class
