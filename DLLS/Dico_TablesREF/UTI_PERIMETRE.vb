﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class UTI_PERIMETRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsAccesDatabase As String
        Private WsSiSuppression As Integer
        Private WsSiTransfert As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "UTI_PERIMETRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueUtilisateur
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Acces_Database() As String
            Get
                Return WsAccesDatabase
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAccesDatabase = value
                    Case Else
                        WsAccesDatabase = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property SiSuppression() As Integer
            Get
                Return WsSiSuppression
            End Get
            Set(ByVal value As Integer)
                WsSiSuppression = value
            End Set
        End Property

        Public Property SiTransfert() As Integer
            Get
                Return WsSiTransfert
            End Get
            Set(ByVal value As Integer)
                WsSiTransfert = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Acces_Database & VI.Tild)
                Chaine.Append(SiSuppression.ToString & VI.Tild)
                Chaine.Append(SiTransfert.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Acces_Database = TableauData(1)
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                SiSuppression = CInt(TableauData(2))
                If TableauData(3) = "" Then
                    TableauData(3) = "0"
                End If
                SiTransfert = CInt(TableauData(3))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
