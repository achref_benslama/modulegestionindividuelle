Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_REFEXTERNE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsMatricule As String
        Private WsPhoto As String
        Private WsArme As String
        Private WsBadge As String
        Private WsUnite_de_gestion As String
        Private WsGestionnaire As String
        Private WsNumen As String
        Private WsCantine As String
        Private WsLibre As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_REFEXTERNE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaExterne
            End Get
        End Property

        Public Property Matricule() As String
            Get
                Return WsMatricule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMatricule = value
                    Case Else
                        WsMatricule = Strings.Left(value, 30)
                End Select
                MyBase.Clef = WsMatricule
            End Set
        End Property

        Public Property Photo() As String
            Get
                Return WsPhoto
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsPhoto = value
                    Case Else
                        WsPhoto = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Badge() As String
            Get
                Return WsBadge
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsBadge = value
                    Case Else
                        WsBadge = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Unite_de_gestion() As String
            Get
                Return WsUnite_de_gestion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUnite_de_gestion = value
                    Case Else
                        WsUnite_de_gestion = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Gestionnaire() As String
            Get
                Return WsGestionnaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsGestionnaire = value
                    Case Else
                        WsGestionnaire = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Numen() As String
            Get
                Return WsNumen
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 13
                        WsNumen = value
                    Case Else
                        WsNumen = Strings.Left(value, 13)
                End Select
            End Set
        End Property

        Public Property Cantine() As String
            Get
                Return WsCantine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCantine = value
                    Case Else
                        WsCantine = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property IdeVirtualia() As String
            Get
                Return MyBase.Ide_Dossier.ToString
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLibre = value
                    Case Else
                        WsLibre = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Matricule & VI.Tild)
                Chaine.Append(Photo & VI.Tild)
                Chaine.Append(Badge & VI.Tild)
                Chaine.Append(Unite_de_gestion & VI.Tild)
                Chaine.Append(Gestionnaire & VI.Tild)
                Chaine.Append(Numen & VI.Tild)
                Chaine.Append(Cantine & VI.Tild)
                Chaine.Append(IdeVirtualia)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Matricule = TableauData(1)
                Photo = TableauData(2)
                Badge = TableauData(3)
                Unite_de_gestion = TableauData(4)
                Gestionnaire = TableauData(5)
                Numen = TableauData(6)
                Cantine = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<Matricule>" & Matricule & "</Matricule>" & vbCrLf)
                Chaine.Append("<Photo>" & VirRhFonction.ChaineXMLValide(Photo) & "</Photo>" & vbCrLf)
                Chaine.Append("<NumeroBadge>" & Badge & "</NumeroBadge>" & vbCrLf)
                Chaine.Append("<UniteGestion>" & VirRhFonction.ChaineXMLValide(Unite_de_gestion) & "</UniteGestion>" & vbCrLf)
                Chaine.Append("<Gestionnaire>" & VirRhFonction.ChaineXMLValide(Unite_de_gestion) & "</Gestionnaire>" & vbCrLf)
                Chaine.Append("<Numen>" & Numen & "</Numen>" & vbCrLf)
                Chaine.Append("<NumeroCarteCantine>" & VirRhFonction.ChaineXMLValide(Cantine) & "</NumeroCarteCantine>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsMatricule
                    Case 2
                        Return WsPhoto
                    Case 3
                        Return WsArme
                    Case 4
                        Return WsBadge
                    Case 5
                        Return WsUnite_de_gestion
                    Case 6
                        Return WsGestionnaire
                    Case 7
                        Return WsNumen
                    Case 8
                        Return WsCantine
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


