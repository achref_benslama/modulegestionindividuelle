﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VMenuCommun.ascx.vb" Inherits="Virtualia.Net.VMenuCommun" %>

<asp:Table ID="CadreMenu" runat ="server" BorderColor="#B0E0D7" BorderWidth="2px" BorderStyle="Ridge" BackColor="White" style="margin-top: 5px; width: 350px; height:800px; background-attachment: inherit; display: table-cell;" >
    <asp:TableRow>
        <asp:TableCell>
            <div style=" margin-top: 0px; width: 350px; height:900px; vertical-align:top; overflow: auto; background-color: #fff; text-align: left ; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;background: #fff;">   
                <telerik:RadTreeView ID="TreeListeMenu"  runat="server" MaxDataBindDepth="2">
                </telerik:RadTreeView>
            </div>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
