﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Ressources.Datas
Imports Virtualia.TablesObjet.ShemaREF

Namespace Predicats
    Public Class PredicateSysRef
        Private WsIdentifiant As Integer
        Private WsPvue As Integer
        Private WsNomTable As String
        Private WsLettre As String

        Public Function RechercherDossier(ByVal Source As ObjetDossierREF) As Boolean
            Return Source.NomTable_Reference = WsNomTable And Source.V_Identifiant = WsIdentifiant
        End Function

        Public Function RechercherIdentifiant(ByVal Source As ObjetDossierREF) As Boolean
            Return Source.V_Identifiant = WsIdentifiant
        End Function

        Public Function RechercherTable(ByVal Source As ObjetDossierREF) As Boolean
            Return Source.NomTable_Reference = WsNomTable
        End Function

        Public Function RechercherLettre(ByVal Source As ObjetDossierREF) As Boolean

            If (WsLettre = "") Then
                Return Source.NomTable_Reference = WsNomTable
            End If

            If Source.NomTable_Reference = WsNomTable Then

                If (Strings.Left(WsLettre, 1) = "*") Then
                    Return Source.Valeur.Contains(Strings.Right(WsLettre, WsLettre.Length - 1))
                End If

                Return Strings.Left(Source.Valeur, WsLettre.Length) = WsLettre
            End If

            Return False

        End Function

        Public Function RechercherPointdeVue(ByVal Source As ObjetDossierREF) As Boolean
            Return Source.V_PointdeVue = WsPvue
        End Function

        Public Sub New(ByVal Nomdelatable As String, ByVal Ide As Integer)
            MyBase.New()
            WsIdentifiant = Ide
            WsNomTable = Nomdelatable
        End Sub

        Public Sub New(ByVal Ide As Integer)
            MyBase.New()
            WsLettre = ""
            WsIdentifiant = Ide
        End Sub

        Public Sub New(ByVal Nomdelatable As String, ByVal LettreAlpha As String)
            MyBase.New()
            WsIdentifiant = 0
            WsNomTable = Nomdelatable
            WsLettre = LettreAlpha
        End Sub

        Public Sub New(ByVal PtdeVue As Integer, Optional ByVal Ide As Integer = 0)
            MyBase.New()
            WsPvue = PtdeVue
            WsIdentifiant = Ide
        End Sub

    End Class

    Public Class PredicateUniversel
        Private WsLettre As String
        Private WsPVue As Integer
        Private WsSiPrive As Boolean = False

        Public Function RechercherLettre(ByVal Source As Object) As Boolean
            Dim Intitule As String = ""

            Select Case WsPVue
                Case VI.PointdeVue.PVueGrades
                    Intitule = CType(Source, GRD_GRADE).Intitule
                Case VI.PointdeVue.PVuePosteBud
                    Intitule = CType(Source, ORG_BUDGETAIRE).Intitule__Definition("")
                Case VI.PointdeVue.PVueFormation
                    Intitule = CType(Source, FOR_IDENTIFICATION).Intitule
                Case VI.PointdeVue.PVueEtablissement
                    Select Case WsSiPrive
                        Case False
                            Intitule = CType(Source, ETA_IDENTITE).Denomination
                        Case True
                            Intitule = CType(Source, ETA_IDENTITE_PRIVE).Denomination
                    End Select
                Case VI.PointdeVue.PVueBaseHebdo
                    Intitule = CType(Source, CYC_IDENTIFICATION).Intitule
                Case VI.PointdeVue.PVueCycle
                    Intitule = CType(Source, TRA_IDENTIFICATION).Intitule
                Case VI.PointdeVue.PVueItineraire
                    Intitule = CType(Source, ITI_DISTANCE).Villedepart
                Case VI.PointdeVue.PVueCommission
                    Intitule = CType(Source, ACT_COMMISSION).Intitule
                Case VI.PointdeVue.PVueMission
                    Intitule = CType(Source, MIS_CARACTERISTIC).Intitule
                Case VI.PointdeVue.PVuePosteFct
                    Intitule = CType(Source, PST_IDENTIFICATION).Intitule
                Case VI.PointdeVue.PVueFonctionSupport
                    Intitule = CType(Source, FCT_SUPPORT).Intitule
                Case VI.PointdeVue.PVuePays
                    Intitule = CType(Source, PAYS_DESCRIPTION).Intitule
                Case VI.PointdeVue.PVueActivite
                    Intitule = CType(Source, ONIC_MISSION).Intitule
            End Select

            Return ResultatRechercheLettre(Intitule)

        End Function

        Private Function ResultatRechercheLettre(ByVal Libelle As String) As Boolean

            If (WsLettre = "") Then
                Return True
            End If

            If (Strings.Left(WsLettre, 1) = "*") Then
                Return Libelle.Contains(Strings.Right(WsLettre, WsLettre.Length - 1))
            End If

            Return Strings.Left(Libelle, WsLettre.Length) = WsLettre

        End Function

        Public Sub New(ByVal LettreAlpha As String, ByVal PtdeVue As Integer, ByVal SiPrive As Boolean)

            WsLettre = LettreAlpha
            WsPVue = PtdeVue
            WsSiPrive = SiPrive
        End Sub

        Public Sub New(ByVal LettreAlpha As String, ByVal PtdeVue As Integer)
            Me.New(LettreAlpha, PtdeVue, False)
        End Sub
    End Class

End Namespace