﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPosteBudgetaire
        Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE)

        Private WsListePostes As List(Of ORG_BUDGETAIRE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesPostes_Alpha(ByVal Lettre As String, ByVal Filiere As String) As List(Of ORG_BUDGETAIRE)
            Get
                If WsListePostes Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVuePosteBud)
                Dim LstRes As List(Of ORG_BUDGETAIRE)
                Dim LstTri As List(Of ORG_BUDGETAIRE) = Nothing

                LstRes = WsListePostes.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                If Filiere = "" Then
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Ide_Dossier > 0 _
                           Order By instance.Intitule__Definition Ascending).ToList
                Else
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Filiere_Definition = Filiere _
                           Order By instance.Intitule__Definition Ascending).ToList
                End If
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesPostes(ByVal Filiere As String, ByVal DateValeur As String) As List(Of ORG_BUDGETAIRE)
            Get
                If WsListePostes Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of ORG_BUDGETAIRE) = Nothing

                If DateValeur = "" Then
                    DateValeur = Strings.Format(Now, "dd/MM/yyyy")
                End If
                If Filiere = "" Then
                    LstRes = (From instance In WsListePostes Select instance _
                           Where instance.SiEmploiEnGestion(DateValeur) = True _
                            Order By instance.Filiere_Definition(DateValeur) Ascending, instance.Secteur_Definition Ascending, _
                                     instance.Intitule__Definition Ascending).ToList
                Else
                    LstRes = (From instance In WsListePostes Select instance _
                           Where instance.Filiere_Definition(DateValeur) = Filiere _
                            Order By instance.Secteur_Definition Ascending, _
                                     instance.Intitule__Definition Ascending).ToList
                End If

                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesPostes_Numero(ByVal Filiere As String) As List(Of ORG_BUDGETAIRE)
            Get
                Dim LstRes As List(Of ORG_BUDGETAIRE)
                If Filiere = " then" Then
                    LstRes = (From instance In WsListePostes Select instance _
                                    Where instance.Ide_Dossier > 0 _
                                    Order By instance.Numero_EB Ascending).ToList
                Else
                    LstRes = (From instance In WsListePostes Select instance _
                                    Where instance.Filiere_Definition = Filiere _
                                    Order By instance.Numero_EB Ascending).ToList
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredePostes() As Integer Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).NombredItems
            Get
                Return WsListePostes.Count
            End Get
        End Property

        Public Function Fiche_Poste(ByVal Ide_Dossier As Integer) As ORG_BUDGETAIRE Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).GetFiche
            Return WsListePostes.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Mission(ByVal Numero As String) As ORG_BUDGETAIRE Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).GetFiche
            Return WsListePostes.Find(Function(m) m.Numero_EB = Numero)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).SupprimerItem
            Dim FichePoste As ORG_BUDGETAIRE
            FichePoste = Fiche_Poste(Ide)
            If FichePoste IsNot Nothing Then
                WsListePostes.Remove(FichePoste)
            End If
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).Actualiser
            WsListePostes.Clear()
            ConstituerListe()
        End Sub

        Public Sub AjouterItem(Ide As Integer) Implements IGestionVirFicheModif(Of ORG_BUDGETAIRE).AjouterItem
            Return
        End Sub

        Private Sub ConstituerListe()
            WsListePostes.Clear()
            Dim FichePoste As ORG_BUDGETAIRE

            '** 1  Liste de tous les Postes
            WsListePostes.AddRange(CreationCollectionVIRFICHE(Of ORG_BUDGETAIRE).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListePostes.Count <= 0) Then
                Return
            End If

            '** 2  Liste de toutes les affectations
            Dim lstaff As List(Of ORG_DEFINITION) = CreationCollectionVIRFICHE(Of ORG_DEFINITION).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstaff.ForEach(Sub(f)
                               FichePoste = Fiche_Poste(f.Ide_Dossier)

                               If (FichePoste Is Nothing) Then
                                   Return
                               End If

                               FichePoste.Ajouter_Definition(f)
                           End Sub)

            '** 3  Liste de tous les bons de transport
            Dim lstbon As List(Of ORG_RATTACHEMENT) = CreationCollectionVIRFICHE(Of ORG_RATTACHEMENT).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstbon.ForEach(Sub(f)
                               FichePoste = Fiche_Poste(f.Ide_Dossier)

                               If (FichePoste Is Nothing) Then
                                   Return
                               End If

                               FichePoste.Ajouter_Rattachement(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListePostes = New List(Of ORG_BUDGETAIRE)
            ConstituerListe()
        End Sub

    End Class
End Namespace
