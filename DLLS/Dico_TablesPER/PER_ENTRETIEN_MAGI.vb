﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_MAGI
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNotateur As String
        Private WsNoteGlobale As Single
        Private WsAppreciation As String
        Private WsLibCritere(13) As String
        Private WsNoteCritere(13) As Single
        Private WsObservations As String
        Private WsTypeFormulaire As String
        Private WsEntiteEvaluation As String
        Private WsReferenceFormulaire As String
        Private WsDate_de_Signature_Evaluateur As String
        Private WsDate_de_Signature_Evalue As String
        Private WsVoeux As String
        Private WsAvisEvaluateur As String

        Public ReadOnly Property NomClient() As String
            Get
                Return "Conseil d'Etat"
            End Get
        End Property

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretien
            End Get
        End Property

        Public Property Notateur() As String
            Get
                Return WsNotateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNotateur = value
                    Case Else
                        WsNotateur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Note_Globale() As Single
            Get
                Return WsNoteGlobale
            End Get
            Set(ByVal value As Single)
                WsNoteGlobale = value
            End Set
        End Property

        Public Property Appreciation_Evaluation() As String
            Get
                Return WsAppreciation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3000
                        WsAppreciation = value
                    Case Else
                        WsAppreciation = Strings.Left(value, 3000)
                End Select
            End Set
        End Property

        Public Property Libelle_Critere(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 13
                        Return V_InfoCritere((Index * 2) + 4)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 13
                        Select Case value.Length
                            Case Is <= 80
                                WsLibCritere(Index) = value
                            Case Else
                                WsLibCritere(Index) = Strings.Left(value, 80)
                        End Select
                End Select
            End Set
        End Property

        Public Property Note_Critere(ByVal Index As Integer) As Single
            Get
                Select Case Index
                    Case 0 To 13
                        Return WsNoteCritere(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Single)
                Select Case Index
                    Case 0 To 13
                        If value > 5 Then
                            WsNoteCritere(Index) = 0
                        Else
                            WsNoteCritere(Index) = value
                        End If
                End Select
            End Set
        End Property

        Public Property Observations_Evaluation() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3000
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 3000)
                End Select
            End Set
        End Property

        Public Property Type_Formulaire() As String
            Get
                Return WsTypeFormulaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsTypeFormulaire = value
                    Case Else
                        WsTypeFormulaire = Strings.Left(value, 4)
                End Select
            End Set
        End Property

        Public Property Entite_Evaluation() As String
            Get
                Return WsEntiteEvaluation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsEntiteEvaluation = value
                    Case Else
                        WsEntiteEvaluation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Reference_Formulaire() As String
            Get
                Return WsReferenceFormulaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsReferenceFormulaire = value
                    Case Else
                        WsReferenceFormulaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Date_de_Signature_Evaluateur() As String
            Get
                Return WsDate_de_Signature_Evaluateur
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Evaluateur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Evalue() As String
            Get
                Return WsDate_de_Signature_Evalue
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Evalue = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Voeux() As String
            Get
                Return WsVoeux
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3000
                        WsVoeux = value
                    Case Else
                        WsVoeux = Strings.Left(value, 3000)
                End Select
            End Set
        End Property

        Public Property Avis_Evaluateur() As String
            Get
                Return WsAvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3000
                        WsAvisEvaluateur = value
                    Case Else
                        WsAvisEvaluateur = Strings.Left(value, 3000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Notateur & VI.Tild)
                Chaine.Append(Note_Globale.ToString & VI.Tild)
                Chaine.Append(Appreciation_Evaluation & VI.Tild)
                For IndiceI = 0 To 13
                    Chaine.Append(Libelle_Critere(IndiceI) & VI.Tild)
                    Chaine.Append(Note_Critere(IndiceI).ToString & VI.Tild)
                Next IndiceI
                Chaine.Append(Observations_Evaluation & VI.Tild)
                Chaine.Append(Type_Formulaire & VI.Tild)
                Chaine.Append(Entite_Evaluation & VI.Tild)
                Chaine.Append(Reference_Formulaire & VI.Tild)
                Chaine.Append(Date_de_Signature_Evaluateur & VI.Tild)
                Chaine.Append(Date_de_Signature_Evalue & VI.Tild)
                Chaine.Append(Voeux & VI.Tild)
                Chaine.Append(Avis_Evaluateur)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim IndiceK As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 41 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Notateur = TableauData(2)
                If TableauData(3) <> "" Then Note_Globale = CSng(VirRhFonction.ConversionDouble(TableauData(3)))
                Appreciation_Evaluation = TableauData(4)
                IndiceK = 5
                For IndiceI = 0 To 13
                    Libelle_Critere(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                    If TableauData(IndiceK) <> "" Then Note_Critere(IndiceI) = CSng(VirRhFonction.ConversionDouble(TableauData(IndiceK)))
                    IndiceK += 1
                Next IndiceI
                Observations_Evaluation = TableauData(33)
                Type_Formulaire = TableauData(34)
                Entite_Evaluation = TableauData(35)
                Reference_Formulaire = TableauData(36)
                Date_de_Signature_Evaluateur = TableauData(37)
                Date_de_Signature_Evalue = TableauData(38)
                Voeux = TableauData(39)
                Avis_Evaluateur = TableauData(40)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Private ReadOnly Property V_InfoCritere(ByVal NumInfo As Integer) As String
            Get
                Dim Chaine As String = ""
                Select Case Type_Formulaire
                    Case Is = "C/PC"
                        Select Case NumInfo
                            Case 4
                                Chaine = "Etendue des connaissances"
                            Case 6
                                Chaine = "Précision des connaissances"
                            Case 8
                                Chaine = "Sens de l’application du droit"
                            Case 10
                                Chaine = "Qualité de l’expression écrite et orale"
                            Case 12
                                Chaine = "Sens de la collégialité et de la participation au débat et au délibéré"
                            Case 14
                                Chaine = "Capacité à décider"
                            Case 16
                                Chaine = "Compréhension du contexte de l’activité contentieuse"
                            Case 18
                                Chaine = "Aptitude au changement"
                            Case 20
                                Chaine = "Respect de l’organisation collective du travail"
                            Case 22
                                Chaine = "Autonomie et sens de l’organisation"
                            Case 24
                                Chaine = "Efficacité et puissance de travail"
                            Case 26
                                Chaine = "Sens du service public"
                            Case 28
                                Chaine = "Implication dans le fonctionnement général et la vie de la juridiction"
                            Case 30
                                Chaine = "Qualités relationnelles au sein et à l’extérieur de la juridiction"
                        End Select
                    Case Is = "CJ"
                        Select Case NumInfo
                            Case 4
                                Chaine = "Aptitudes à la constitution et à l’animation d’une équipe"
                            Case 6
                                Chaine = "Capacités d’écoute et de dialogue"
                            Case 8
                                Chaine = "Sens de l’organisation et de la gestion"
                            Case 10
                                Chaine = "Attention portée à la cohésion magistrats / greffe"
                            Case 12
                                Chaine = "Exercice de l’autorité"
                            Case 14
                                Chaine = "Qualités d’évaluateur"
                        End Select
                    Case Is = "PR"
                        Select Case NumInfo
                            Case 4
                                Chaine = "Sens de la direction de la collégialité"
                            Case 6
                                Chaine = "Capacité à décider"
                            Case 8
                                Chaine = "Qualité de la révision"
                            Case 10
                                Chaine = "Encadrement du greffe"
                            Case 12
                                Chaine = "Sens de l’évaluation des magistrats"
                            Case 14
                                Chaine = ""
                            Case 16
                                Chaine = "Aptitude au changement"
                            Case 18
                                Chaine = "Respect de l’organisation collective du travail"
                            Case 20
                                Chaine = ""
                            Case 22
                                Chaine = ""
                            Case 24
                                Chaine = "Efficacité et puissance de travail"
                            Case 26
                                Chaine = "Sens du service public"
                            Case 28
                                Chaine = "Implication dans le fonctionnement général et la vie de la juridiction"
                            Case 30
                                Chaine = "Qualités relationnelles au sein et à l’extérieur de la juridiction"
                        End Select
                End Select
                Return Chaine
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace





