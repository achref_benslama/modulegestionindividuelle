﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class EnsembleReferences
        Private WsNomUtilisateur As String
        '** Tables Générales
        Private WsObjetTAB_GEN As ObjetTableGenerale
        '** Temps de travail
        Private WsCollAbsences As ObjetAbsence
        Private WsCollPresences As ObjetPresence
        Private WsCollUnitesCycle As ObjetUniteCycle
        Private WsCollCyclesTravail As ObjetCycleTravail
        Private WsCollReglesValo As ObjetRegleValorisation
        Private WsCollActiviteMesures As ObjetMesureActivite
        '** Positions
        Private WsCollPositions As ObjetPosition
        '** Formations
        Private WsCollStages As ObjetStageFormation
        Private WsCollDirections As ObjetDirections
        '** SECTEUR PUBLIC : Grades et grilles indiciaires, Postes Budgétaires, LOLF, Frais de Mission ******
        Private WsCollGradeGrilles As ObjetGradeGrille
        Private WsCollPostesBud As ObjetPosteBudgetaire
        Private WsCollLolf As ObjetLOLF
        Private WsCollFraisMission As ObjetMission
        '** Etablissements, Postes Fonctionnels, Commissions
        Private WsCollEtablissments As ObjetEtablissement
        Private WsCollEtablissementsPrives As ObjetEtablissementPrive
        Private WsCollPostesFct As ObjetPosteFonctionnel
        Private WsCollCommissions As ObjetCommission
        '** Divers
        Private WsCollPays As ObjetPays
        Private WsCollItineraires As ObjetItineraire
        Private WsCollInterface As ObjetLogicielExterne
        '**** Execution
        Private WsCalcJour As Virtualia.Ressources.Calculs.ObjetCalculJour

        Public ReadOnly Property PointeurTablesGenerales As ObjetTableGenerale
            Get
                If WsObjetTAB_GEN Is Nothing Then
                    Try
                        WsObjetTAB_GEN = New ObjetTableGenerale(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsObjetTAB_GEN = Nothing
                    End Try
                End If
                Return WsObjetTAB_GEN
            End Get
        End Property

        Public ReadOnly Property PointeurAbsences As ObjetAbsence
            Get
                If WsCollAbsences Is Nothing Then
                    Try
                        WsCollAbsences = New ObjetAbsence(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollAbsences = Nothing
                    End Try
                End If
                Return WsCollAbsences
            End Get
        End Property

        Public ReadOnly Property PointeurPresences As ObjetPresence
            Get
                If WsCollPresences Is Nothing Then
                    Try
                        WsCollPresences = New ObjetPresence(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollPresences = Nothing
                    End Try
                End If
                Return WsCollPresences
            End Get
        End Property

        Public ReadOnly Property PointeurUniteCycles As ObjetUniteCycle
            Get
                If WsCollUnitesCycle Is Nothing Then
                    Try
                        WsCollUnitesCycle = New ObjetUniteCycle(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollUnitesCycle = Nothing
                    End Try
                End If
                Return WsCollUnitesCycle
            End Get
        End Property

        Public ReadOnly Property PointeurCyclesTravail As ObjetCycleTravail
            Get
                If WsCollCyclesTravail Is Nothing Then
                    Try
                        WsCollCyclesTravail = New ObjetCycleTravail(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollCyclesTravail = Nothing
                    End Try
                End If
                Return WsCollCyclesTravail
            End Get
        End Property

        Public ReadOnly Property PointeurReglesValo As ObjetRegleValorisation
            Get
                If WsCollReglesValo Is Nothing Then
                    Try
                        WsCollReglesValo = New ObjetRegleValorisation(WsNomUtilisateur)
                    Catch ex As Exception
                        WsCollReglesValo = Nothing
                    End Try
                End If
                Return WsCollReglesValo
            End Get
        End Property

        Public ReadOnly Property PointeurActiviteMesures As ObjetMesureActivite
            Get
                If WsCollActiviteMesures Is Nothing Then
                    Try
                        WsCollActiviteMesures = New ObjetMesureActivite(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollActiviteMesures = Nothing
                    End Try
                End If
                Return WsCollActiviteMesures
            End Get
        End Property

        Public ReadOnly Property PointeurPosition As ObjetPosition
            Get
                If WsCollPositions Is Nothing Then
                    Try
                        WsCollPositions = New ObjetPosition(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollPositions = Nothing
                    End Try
                End If
                Return WsCollPositions
            End Get
        End Property

        Public ReadOnly Property PointeurStagesFormation As ObjetStageFormation
            Get
                If WsCollStages Is Nothing Then
                    Try
                        WsCollStages = New ObjetStageFormation(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollStages = Nothing
                    End Try
                End If
                Return WsCollStages
            End Get
        End Property

        Public ReadOnly Property PointeurGradeGrilles As ObjetGradeGrille
            Get
                If WsCollGradeGrilles Is Nothing Then
                    Try
                        WsCollGradeGrilles = New ObjetGradeGrille(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollGradeGrilles = Nothing
                    End Try
                End If
                Return WsCollGradeGrilles
            End Get
        End Property

        Public ReadOnly Property PointeurPostesBudgetaires As ObjetPosteBudgetaire
            Get
                If WsCollPostesBud Is Nothing Then
                    Try
                        WsCollPostesBud = New ObjetPosteBudgetaire(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollPostesBud = Nothing
                    End Try
                End If
                Return WsCollPostesBud
            End Get
        End Property

        Public ReadOnly Property PointeurLOLF As ObjetLOLF
            Get
                If WsCollLolf Is Nothing Then
                    Try
                        WsCollLolf = New ObjetLOLF(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollLolf = Nothing
                    End Try
                End If
                Return WsCollLolf
            End Get
        End Property

        Public ReadOnly Property PointeurFraisMission As ObjetMission
            Get
                If WsCollFraisMission Is Nothing Then
                    Try
                        WsCollFraisMission = New ObjetMission(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollFraisMission = Nothing
                    End Try
                End If
                Return WsCollFraisMission
            End Get
        End Property

        Public ReadOnly Property PointeurEtablissements As ObjetEtablissement
            Get
                If WsCollEtablissments Is Nothing OrElse WsCollEtablissments.ListeDesEtablissements Is Nothing OrElse WsCollEtablissments.ListeDesEtablissements.Count = 0 Then
                    Try
                        WsCollEtablissments = New ObjetEtablissement(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollEtablissments = Nothing
                    End Try
                End If
                Return WsCollEtablissments
            End Get
        End Property

        Public ReadOnly Property PointeurDirection As ObjetDirections
            Get
                If WsCollDirections Is Nothing OrElse WsCollDirections.ListeDesDirections Is Nothing OrElse WsCollDirections.ListeDesDirections.Count = 0 Then
                    Try
                        WsCollDirections = New ObjetDirections(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollDirections = Nothing
                    End Try
                End If
                Return WsCollDirections
            End Get
        End Property

        Public ReadOnly Property PointeurEtablissements_Prives As ObjetEtablissementPrive
            Get
                If WsCollEtablissementsPrives Is Nothing OrElse WsCollEtablissementsPrives.ListeDesEtablissements Is Nothing OrElse WsCollEtablissementsPrives.ListeDesEtablissements.Count = 0 Then
                    Try
                        WsCollEtablissementsPrives = New ObjetEtablissementPrive(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollEtablissementsPrives = Nothing
                    End Try
                End If
                Return WsCollEtablissementsPrives
            End Get
        End Property

        Public ReadOnly Property PointeurPostesFonctionnels As ObjetPosteFonctionnel
            Get
                If WsCollPostesFct Is Nothing Then
                    Try
                        WsCollPostesFct = New ObjetPosteFonctionnel(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollPostesFct = Nothing
                    End Try
                End If
                Return WsCollPostesFct
            End Get
        End Property

        Public ReadOnly Property PointeurCommissions As ObjetCommission
            Get
                If WsCollCommissions Is Nothing Then
                    Try
                        WsCollCommissions = New ObjetCommission(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollCommissions = Nothing
                    End Try
                End If
                Return WsCollCommissions
            End Get
        End Property

        Public ReadOnly Property PointeurPays As ObjetPays
            Get
                If WsCollPays Is Nothing Then
                    Try
                        WsCollPays = New ObjetPays(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollPays = Nothing
                    End Try
                End If
                Return WsCollPays
            End Get
        End Property

        Public ReadOnly Property PointeurItineraire As ObjetItineraire
            Get
                If WsCollItineraires Is Nothing Then
                    Try
                        WsCollItineraires = New ObjetItineraire(WsNomUtilisateur)
                    Catch Ex As Exception
                        WsCollItineraires = Nothing
                    End Try
                End If
                Return WsCollItineraires
            End Get
        End Property

        Public ReadOnly Property PointeurLogicielExterne As ObjetLogicielExterne
            Get
                If WsCollInterface Is Nothing Then
                    Try
                        WsCollInterface = New ObjetLogicielExterne(WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsCollInterface = Nothing
                    End Try
                End If
                Return WsCollInterface
            End Get
        End Property

        Public ReadOnly Property PointeurCalcul() As Virtualia.Ressources.Calculs.ObjetCalculJour
            Get
                If WsCalcJour Is Nothing Then
                    WsCalcJour = New Virtualia.Ressources.Calculs.ObjetCalculJour(WsNomUtilisateur, PointeurPays)
                End If
                Return WsCalcJour
            End Get
        End Property

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
        End Sub
    End Class
End Namespace