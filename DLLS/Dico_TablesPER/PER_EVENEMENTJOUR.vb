﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_EVENEMENTJOUR
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsHeureDebut As String
        Private WsNature As String
        Private WsDuree As String
        Private WsDate_Origine As String
        Private WsObservations As String
        Private WsHeureFin As String
        '
        Private WsCoefficient_HS As Double
        Private WsForfait_HS As Double

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_EVENEMENTJOUR"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaJournee
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return False
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_d_effet = ""
                End Select
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Heure_Debut() As String
            Get
                Return WsHeureDebut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut = value
                    Case Else
                        WsHeureDebut = Strings.Left(value, 5)
                End Select
                MyBase.Clef = WsHeureDebut
            End Set
        End Property

        Public Property Nature_Evt() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Duree() As String
            Get
                Return WsDuree
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsDuree = value
                    Case Else
                        WsDuree = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Date_d_Origine() As String
            Get
                Return WsDate_Origine
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_Origine = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_Origine = ""
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Heure_Fin() As String
            Get
                Return WsHeureFin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin = value
                    Case Else
                        WsHeureFin = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Heure_Debut & VI.Tild)
                Chaine.Append(Nature_Evt & VI.Tild)
                Chaine.Append(Duree & VI.Tild)
                Chaine.Append(Date_d_Origine & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Heure_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Heure_Debut = TableauData(2)
                Nature_Evt = TableauData(3)
                Duree = TableauData(4)
                Date_d_Origine = TableauData(5)
                Observations = TableauData(6)
                Heure_Fin = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property V_Coefficient_HS As Double
            Get
                Return WsCoefficient_HS
            End Get
            Set(value As Double)
                WsCoefficient_HS = value
            End Set
        End Property

        Public Property V_Forfait_HS As Double
            Get
                Return WsForfait_HS
            End Get
            Set(value As Double)
                WsForfait_HS = value
            End Set
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
