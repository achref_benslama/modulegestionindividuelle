﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleVerticalEtiquetteExperte" Codebehind="VCoupleVerticalEtiquetteExperte.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<asp:Table ID="CadreExperte" runat="server" CellPadding="1" CellSpacing="0" >
   <asp:TableRow>
     <asp:TableCell>
         <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#8DA8A3" BorderColor="#C0BAE5" BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#E9FDF9" Font-Italic="true"
                    Font-Bold="False" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
         <Telerik:RadLabel ID="EtiDonnee" runat="server" Height="170px" Width="170px"
                    BackColor="#C5E0DA" BorderColor="#C0BAE5"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="#586361" Visible="true"
                    Font-Bold="false" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
     </asp:TableCell>
  </asp:TableRow>
</asp:Table>