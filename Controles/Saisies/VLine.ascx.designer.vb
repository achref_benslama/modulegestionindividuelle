﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VLine
    
    '''<summary>
    '''Contrôle Line.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Line As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Date_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Date_L6 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Lieu_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Lieu_L6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Prenom_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Prenom_L6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Nom_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Nom_L6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Radio_L603.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L603 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle Radio_L604.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Radio_L604 As Global.Virtualia.Net.Controles_VTrioHorizontalRadio
    
    '''<summary>
    '''Contrôle CadreCmdClear_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdClear_L6 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle btnClear_L6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents btnClear_L6 As Global.System.Web.UI.WebControls.Button
End Class
