﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_INDEMNITE_KM
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDateJO As String
        Private WsDateArrete As String
        Private WsTaux_Moins_5cv_2000Km As Double = 0
        Private WsTaux_Moins_5cv_10000Km As Double = 0
        Private WsTaux_Moins_5cv_Plus10000Km As Double = 0
        Private WsTaux_6_7cv_2000Km As Double = 0
        Private WsTaux_6_7cv_10000Km As Double = 0
        Private WsTaux_6_7cv_Plus10000Km As Double = 0
        Private WsTaux_Plus_8cv_2000Km As Double = 0
        Private WsTaux_Plus_8cv_10000Km As Double = 0
        Private WsTaux_Plus_8cv_Plus10000Km As Double = 0
        Private WsTaux_Moto As Double = 0
        Private WsTaux_Velo As Double = 0
        Private WsTaux_Petite_Cylindree As Double = 0
        Private WsMontant_Mensuel_Minimum As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_INDEMNITE_KM"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 12
            End Get
        End Property

        Public Property Date_du_JO() As String
            Get
                Return WsDateJO
            End Get
            Set(ByVal value As String)
                WsDateJO = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Arrete() As String
            Get
                Return WsDateArrete
            End Get
            Set(ByVal value As String)
                WsDateArrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Taux_Moins_5cv_2000Km() As Double
            Get
                Return WsTaux_Moins_5cv_2000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Moins_5cv_2000Km = value
            End Set
        End Property

        Public Property Taux_Moins_5cv_10000Km() As Double
            Get
                Return WsTaux_Moins_5cv_10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Moins_5cv_10000Km = value
            End Set
        End Property

        Public Property Taux_Moins_5cv_Plus10000Km() As Double
            Get
                Return WsTaux_Moins_5cv_Plus10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Moins_5cv_Plus10000Km = value
            End Set
        End Property

        Public Property Taux_6_7cv_2000Km() As Double
            Get
                Return WsTaux_6_7cv_2000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_6_7cv_2000Km = value
            End Set
        End Property

        Public Property Taux_6_7cv_10000Km() As Double
            Get
                Return WsTaux_6_7cv_10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_6_7cv_10000Km = value
            End Set
        End Property

        Public Property Taux_6_7cv_Plus10000Km() As Double
            Get
                Return WsTaux_6_7cv_Plus10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_6_7cv_Plus10000Km = value
            End Set
        End Property

        Public Property Taux_Plus_8cv_2000Km() As Double
            Get
                Return WsTaux_Plus_8cv_2000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Plus_8cv_2000Km = value
            End Set
        End Property

        Public Property Taux_Plus_8cv_10000Km() As Double
            Get
                Return WsTaux_Plus_8cv_10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Plus_8cv_10000Km = value
            End Set
        End Property

        Public Property Taux_Plus_8cv_Plus10000Km() As Double
            Get
                Return WsTaux_Plus_8cv_Plus10000Km
            End Get
            Set(ByVal value As Double)
                WsTaux_Plus_8cv_Plus10000Km = value
            End Set
        End Property

        Public Property Taux_Motocyclette() As Double
            Get
                Return WsTaux_Moto
            End Get
            Set(ByVal value As Double)
                WsTaux_Moto = value
            End Set
        End Property

        Public Property Taux_Velomoteur() As Double
            Get
                Return WsTaux_Velo
            End Get
            Set(ByVal value As Double)
                WsTaux_Velo = value
            End Set
        End Property

        Public Property Taux_Petite_Cylindree() As Double
            Get
                Return WsTaux_Petite_Cylindree
            End Get
            Set(ByVal value As Double)
                WsTaux_Petite_Cylindree = value
            End Set
        End Property

        Public Property Montant_Mensuel_Minimum() As Double
            Get
                Return WsMontant_Mensuel_Minimum
            End Get
            Set(ByVal value As Double)
                WsMontant_Mensuel_Minimum = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Date_du_JO & VI.Tild)
                Chaine.Append(Date_Arrete & VI.Tild)
                Chaine.Append(Taux_Moins_5cv_2000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Moins_5cv_10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Moins_5cv_Plus10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_6_7cv_2000Km.ToString & VI.Tild)
                Chaine.Append(Taux_6_7cv_10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_6_7cv_Plus10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Plus_8cv_2000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Plus_8cv_10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Plus_8cv_Plus10000Km.ToString & VI.Tild)
                Chaine.Append(Taux_Motocyclette.ToString & VI.Tild)
                Chaine.Append(Taux_Velomoteur.ToString & VI.Tild)
                Chaine.Append(Taux_Petite_Cylindree.ToString & VI.Tild)
                Chaine.Append(Montant_Mensuel_Minimum.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 17 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Date_du_JO = TableauData(2)
                Date_Arrete = TableauData(3)
                If TableauData(4) <> "" Then Taux_Moins_5cv_2000Km = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Taux_Moins_5cv_10000Km = CInt(TableauData(5))
                If TableauData(6) <> "" Then Taux_Moins_5cv_Plus10000Km = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Taux_6_7cv_2000Km = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then Taux_6_7cv_10000Km = CInt(TableauData(8))
                If TableauData(9) <> "" Then Taux_6_7cv_Plus10000Km = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Taux_Plus_8cv_2000Km = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then Taux_Plus_8cv_10000Km = CInt(TableauData(11))
                If TableauData(12) <> "" Then Taux_Plus_8cv_Plus10000Km = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) <> "" Then Taux_Motocyclette = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) <> "" Then Taux_Velomoteur = CInt(TableauData(14))
                If TableauData(15) <> "" Then Taux_Petite_Cylindree = VirRhFonction.ConversionDouble(TableauData(15))
                If TableauData(16) <> "" Then Taux_Petite_Cylindree = VirRhFonction.ConversionDouble(TableauData(16))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
