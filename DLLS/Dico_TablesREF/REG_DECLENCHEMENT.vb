Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_DECLENCHEMENT
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsFrequence As String
        Private WsEtat As String
        Private WsProfil As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Frequence() As String
            Get
                Return WsFrequence
            End Get
            Set(ByVal value As String)
                WsFrequence = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Etat() As String
            Get
                Return WsEtat
            End Get
            Set(ByVal value As String)
                WsEtat = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Profil() As String
            Get
                Return WsProfil
            End Get
            Set(ByVal value As String)
                WsProfil = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Date_de_Valeur & VI.Tild)
                Chaine.Append(Date_de_Fin & VI.Tild)
                Chaine.Append(Frequence & VI.Tild)
                Chaine.Append(Etat & VI.Tild)
                Chaine.Append(Profil)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                Date_de_Valeur = TableauData(1)
                If Date_de_Valeur = "" Then
                    Date_de_Valeur = "01/01/1950"
                End If
                Date_de_Fin = TableauData(2)
                Frequence = TableauData(3)
                Etat = TableauData(4)
                Profil = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return Date_de_Fin
                    Case 2
                        Return WsFrequence
                    Case 3
                        Return WsEtat
                    Case 4
                        Return WsProfil
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone
            Return New REG_DECLENCHEMENT(MyBase.Ide_Dossier.ToString & ContenuTable)
        End Function
    End Class
End Namespace

