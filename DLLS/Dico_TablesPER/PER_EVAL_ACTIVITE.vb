﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_EVAL_ACTIVITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Appreciation As String
        Private WsRang As String
        Private WsVecteur As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_EVAL_ACTIVITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 111
            End Get
        End Property

        Public Property Date_Appreciation() As String
            Get
                Return WsDate_Appreciation
            End Get
            Set(ByVal value As String)
                WsDate_Appreciation = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Appreciation
            End Set
        End Property

        Public Property Rang() As String
            Get
                Return WsRang
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsRang = value
                    Case Else
                        WsRang = Strings.Left(value, 20)
                End Select
                MyBase.Clef = WsRang
            End Set
        End Property

        Public Property Vecteur() As String
            Get
                Return WsVecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsVecteur = value
                    Case Else
                        WsVecteur = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Appreciation & VI.Tild)
                Chaine.Append(Rang & VI.Tild)
                Chaine.Append(Vecteur)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Appreciation = TableauData(1)
                Rang = TableauData(2)
                Vecteur = TableauData(3)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Appreciation
                    Case 1
                        Return WsRang
                    Case 2
                        Return WsVecteur
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
