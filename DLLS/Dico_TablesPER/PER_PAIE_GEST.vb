﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PAIE_GEST
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsNumObjet As Integer = VI.ObjetPer.ObaPaieGEST
        Private WsRang As Integer 'Numéro d'ordre Virtualia
        '
        '** Total 204 Caractères
        Private WsCodeCorrespondant As String
        Private WsDatePaye As String
        Private WsChainePaye As String = "PP" 'PP pour Rému principale, AA pour chaine d'acomptes
        Private WsNumeroRemise As String = "01" 'En principe toujours égal à 01. Indique que tous les articles sont inclus dans 
        '*************************************** un même fichier remis pour une même chaine au cours du même mois
        Private WsCodeMinistere As String 'Numérique - 3 caractères
        Private WsAdministrationGestionnaire As String
        Private WsDepartementGestionnaire As String
        '
        Private WsNIR As String
        Private WsClefNIR As String
        Private WsNumeroDossier As String = "00" '2 caractères , celui de droite étant toujours à 0
        '** 00 pour tous les articles et 10 pour laes articles 82 à 87.

        Private WsCodeMouvement As String 'Le type de la carte
        '** 00=Nom, prénom , 01= grade et poste, 02=Eléments de rému, 03=Temps partiel et éléments précalculés, 04=Références bancaires,
        '** 05=Indemnités ou retenues permanenetes etc... 92=Adresse(fin), AA=Avances automatiques, R8=Rému hors paye
        '
        Private WsContenuCarte As String '67 Caractères selon le contenu des cartes
        Private WsLibelleCarte As String '52 caractères
        '
        Private WsNomPrenom As String = "" '30 caractères
        Private WsZoneRetour As String = "" 'Zone libre de 3 caractères utilisée pour retour LR
        Private WsMatricule As String '9 caractères
        Private WsNumeroArticle As String = "01" '01 pour les articles obligatoires et 02 à nn pour les articles facultatifs

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PAIE_GEST"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet 'Spécifique Secteur Public - Paie DGFIP
            End Get
        End Property

        Public Property Rang As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Code_Correspondant As String
            Get
                Return WsCodeCorrespondant
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(3)
                If value.Length < 3 Then
                    value &= Chaine
                End If
                WsCodeCorrespondant = Strings.Left(value.ToUpper, 3)
            End Set
        End Property

        Public Property Date_Paye As String
            Get
                Return WsDatePaye
            End Get
            Set(ByVal value As String)
                If value.Length = 10 Then
                    WsDatePaye = Strings.Right(value, 4) & Strings.Mid(value, 4, 2)
                Else
                    WsDatePaye = value
                End If
            End Set
        End Property

        Public Property Chaine_Paye As String
            Get
                Return WsChainePaye
            End Get
            Set(ByVal value As String)
                WsChainePaye = value
            End Set
        End Property

        Public Property Numero_Remise As String
            Get
                Return WsNumeroRemise
            End Get
            Set(ByVal value As String)
                If value.Length < 2 Then
                    WsNumeroRemise = Strings.StrDup(2 - value.Length, "0") & value
                Else
                    WsNumeroRemise = Strings.Left(value, 2)
                End If
            End Set
        End Property

        Public Property Code_Ministere As String
            Get
                Return WsCodeMinistere
            End Get
            Set(ByVal value As String)
                If value.Length < 3 Then
                    WsCodeMinistere = Strings.StrDup(3 - value.Length, "0") & value
                Else
                    WsCodeMinistere = Strings.Left(value, 3)
                End If
            End Set
        End Property

        Public Property Administration_Gestionnaire As String
            Get
                Return WsAdministrationGestionnaire
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(3)
                If value.Length < 3 Then
                    value &= Chaine
                End If
                WsAdministrationGestionnaire = Strings.Left(value.ToUpper, 3)
            End Set
        End Property

        Public Property Departement_Gestionnaire As String
            Get
                Return WsDepartementGestionnaire
            End Get
            Set(ByVal value As String)
                If value.Length < 3 Then
                    WsDepartementGestionnaire = Strings.StrDup(3 - value.Length, "0") & value
                Else
                    WsDepartementGestionnaire = Strings.Left(value, 3)
                End If
            End Set
        End Property

        Public Property NIR As String
            Get
                Return WsNIR
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(13)
                If value.Length < 13 Then
                    value &= Chaine
                End If
                WsNIR = Strings.Left(value.ToUpper, 13)
            End Set
        End Property

        Public Property Clef_NIR As String
            Get
                If WsClefNIR = "00" Then
                    WsClefNIR = Strings.Space(2)
                End If
                Return WsClefNIR
            End Get
            Set(ByVal value As String)
                If value.Length < 2 Then
                    WsClefNIR = Strings.StrDup(2 - value.Length, "0") & value
                Else
                    WsClefNIR = Strings.Left(value, 2)
                End If
                If WsClefNIR = "00" Then
                    WsClefNIR = Strings.Space(2)
                End If
            End Set
        End Property

        Public Property Numero_Dossier As String
            Get
                Return WsNumeroDossier
            End Get
            Set(ByVal value As String)
                If value.Length < 2 Then
                    WsNumeroDossier = "0" & value
                Else
                    WsNumeroDossier = Strings.Left(value, 2)
                End If
            End Set
        End Property

        Public Property Code_Mouvement As String
            Get
                Return WsCodeMouvement
            End Get
            Set(ByVal value As String)
                If value.Length < 2 Then
                    WsCodeMouvement = Strings.StrDup(2 - value.Length, "0") & value
                Else
                    WsCodeMouvement = Strings.Left(value, 2)
                End If
            End Set
        End Property

        Public Property NomEtPrenom As String
            Get
                Return WsNomPrenom
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(30)
                If value.Length < 30 Then
                    value &= Chaine
                End If
                WsNomPrenom = Strings.Left(value.ToUpper, 30)
            End Set
        End Property

        Public Property ZoneRetour As String
            Get
                Return WsZoneRetour
            End Get
            Set(ByVal value As String)
                WsZoneRetour = value
            End Set
        End Property

        Public Property Matricule As String
            Get
                WsMatricule = Strings.Format(Ide_Dossier, "000000000")
                Return WsMatricule
            End Get
            Set(ByVal value As String)
                WsMatricule = value
            End Set
        End Property

        Public Property Numero_Article As String
            Get
                Return WsNumeroArticle
            End Get
            Set(ByVal value As String)
                WsNumeroArticle = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Code_Correspondant & VI.Tild)
                Chaine.Append(Date_Paye & VI.Tild)
                Chaine.Append(Chaine_Paye & VI.Tild)
                Chaine.Append(Numero_Remise & VI.Tild)
                Chaine.Append(Code_Ministere & VI.Tild)
                Chaine.Append(Administration_Gestionnaire & VI.Tild)
                Chaine.Append(Departement_Gestionnaire & VI.Tild)
                Chaine.Append(NIR & VI.Tild)
                Chaine.Append(Clef_NIR & VI.Tild)
                Chaine.Append(Numero_Dossier & VI.Tild)
                Chaine.Append(Code_Mouvement & VI.Tild)
                Chaine.Append(V_Contenu & VI.Tild)
                Chaine.Append(V_Libelle & VI.Tild)
                Chaine.Append(NomEtPrenom & VI.Tild)
                Chaine.Append(ZoneRetour & VI.Tild)
                Chaine.Append(Matricule & VI.Tild)
                Chaine.Append(Numero_Article)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 19 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Rang = CInt(TableauData(2))
                Code_Correspondant = TableauData(3)
                Date_Paye = TableauData(4)
                Chaine_Paye = TableauData(5)
                Numero_Remise = TableauData(6)
                Code_Ministere = TableauData(7)
                Administration_Gestionnaire = TableauData(8)
                Departement_Gestionnaire = TableauData(9)
                NIR = TableauData(10)
                Clef_NIR = TableauData(11)
                Numero_Dossier = TableauData(12)
                Code_Mouvement = TableauData(13)
                V_Contenu = TableauData(14)
                V_Libelle = TableauData(15)
                NomEtPrenom = TableauData(16)
                ZoneRetour = TableauData(17)
                Matricule = TableauData(18)
                Numero_Article = TableauData(19)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public ReadOnly Property V_Enregistrement As String
            Get
                Dim Chaine As New System.Text.StringBuilder
                Chaine.Append(V_Entete)
                Chaine.Append(V_Contenu)
                Chaine.Append(V_Libelle)
                Chaine.Append(V_Identite)
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property V_Entete As String '41 Caractères - entete complet
            Get
                Dim Chaine As New System.Text.StringBuilder
                Chaine.Append(Code_Correspondant)
                Chaine.Append(Date_Paye)
                Chaine.Append(Chaine_Paye)
                Chaine.Append(Numero_Remise)
                Chaine.Append(Code_Ministere)
                Chaine.Append(Administration_Gestionnaire)
                Chaine.Append(Departement_Gestionnaire)
                Chaine.Append(NIR)
                Chaine.Append(Clef_NIR)
                Chaine.Append(Numero_Dossier)
                Chaine.Append(Code_Mouvement)
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property V_Identite As String '44 Caractères - Fin du fichier
            Get
                Dim Chaine As New System.Text.StringBuilder
                Chaine.Append(NomEtPrenom)
                Chaine.Append(Strings.Space(3))
                Chaine.Append(Matricule)
                Chaine.Append(Numero_Article)
                Return Chaine.ToString
            End Get
        End Property

        Public Overridable Property V_Contenu As String
            Get
                If Strings.Left(WsContenuCarte, 1) = Strings.Space(1) Then
                    WsContenuCarte = "@" & Strings.Right(WsContenuCarte, WsContenuCarte.Length - 1)
                End If
                Return WsContenuCarte
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(67)
                If value.Length < 67 Then
                    value &= Chaine
                End If
                WsContenuCarte = Strings.Left(value, 67)
            End Set
        End Property

        Public Overridable Property V_Libelle As String
            Get
                Return WsLibelleCarte
            End Get
            Set(ByVal value As String)
                Dim Chaine As String = Strings.Space(52)
                If value.Length < 52 Then
                    value &= Chaine
                End If
                WsLibelleCarte = Strings.Left(value, 52)
            End Set
        End Property

        Public ReadOnly Property V_DatePaye As String
            Get
                Return "01/" & Strings.Right(Date_Paye, 2) & "/" & Strings.Left(Date_Paye, 4)
            End Get
        End Property

        Public WriteOnly Property NIR_Provisoire As String
            Set(ByVal value As String)
                If value.Length = 10 Then
                    WsNIR = value & Strings.Left(NomEtPrenom, 3).ToUpper
                End If
            End Set
        End Property

        '**** Fonctions de Mise en forme communes à tous les mouvements Gest

        Public ReadOnly Property F_FormatAlpha_GEST(ByVal Valeur As String, ByVal Longueur As Integer, Optional ByVal SiBlancZero As Boolean = False) As String
            Get
                If SiBlancZero Then
                    If Valeur = Strings.StrDup(Valeur.Length, "0") Then
                        Valeur = ""
                    End If
                End If
                Dim Chaine As String = Valeur & Strings.Space(Longueur)
                Return Strings.Left(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_GEST(ByVal Valeur As String, ByVal Longueur As Integer) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                Dim Chaine As String = Strings.StrDup(Longueur, "0") & Valeur
                Return Strings.Right(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_GEST(ByVal Valeur As String, ByVal Longueur As Integer, ByVal SiBlanc As Boolean) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                If SiBlanc = True Then
                    If Valeur.Trim = "" Or Val(Strings.Replace(Valeur, ",", ".")) = 0 Then
                        Return Strings.Space(Longueur)
                    End If
                End If
                Dim Chaine As String
                Chaine = Strings.StrDup(Longueur, "0") & Valeur
                Return Strings.Right(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_GEST(ByVal Valeur As String, ByVal Longueur As Integer, ByVal NbDecimales As Integer, Optional ByVal SiBlanc As Boolean = False) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                If SiBlanc = True Then
                    If Valeur.Trim = "" Or Val(Strings.Replace(Valeur, ",", ".")) = 0 Then
                        Return Strings.Space(Longueur)
                    End If
                End If

                Dim PartieEntiere As String
                Dim PartieDecimale As String
                Dim Chaine As String
                Dim PositionSymbole As Integer

                If NbDecimales = 0 Then
                    PartieEntiere = Strings.StrDup(Longueur, "0") & Valeur
                    Return Strings.Right(PartieEntiere, Longueur)
                End If

                PositionSymbole = Strings.InStr(Valeur, VI.PointFinal)
                If PositionSymbole = 0 Then
                    PositionSymbole = Strings.InStr(Valeur, VI.Virgule)
                End If

                If PositionSymbole = 0 Then
                    Chaine = Strings.StrDup(Longueur, "0") & Valeur
                    PartieEntiere = Strings.Right(Chaine, Longueur - NbDecimales)
                    Return PartieEntiere & Strings.StrDup(NbDecimales, "0")
                End If

                Chaine = Strings.StrDup(Longueur, "0") & Strings.Left(Valeur, PositionSymbole - 1)
                PartieEntiere = Strings.Right(Chaine, Longueur - NbDecimales)
                Chaine = Strings.Right(Valeur, Valeur.Length - PositionSymbole) & Strings.StrDup(NbDecimales, "0")
                PartieDecimale = Strings.Left(Chaine, NbDecimales)

                Return PartieEntiere & PartieDecimale
            End Get
        End Property

        Public ReadOnly Property F_FormatDate_GEST(ByVal ArgumentDate As String, ByVal FormatSortie As String, Optional ByVal CaractereRemplissage As String = " ") As String
            Get
                'ArgumentDate au format Virtualia - JJ/MM/SSAAA
                Dim ChaineRes As String = Strings.StrDup(FormatSortie.Length, CaractereRemplissage)
                Dim JJ As String
                Dim MM As String
                Dim AAAA As String

                If ArgumentDate.Length = 4 And FormatSortie = "AAAA" Then
                    If IsNumeric(ArgumentDate) Then
                        Return ArgumentDate
                    End If
                End If
                If ArgumentDate.Length <> 10 Then
                    Return ChaineRes
                End If
                If VirRhDate.DateSaisieVerifiee(ArgumentDate) = "" Then
                    Return ""
                End If
                JJ = Strings.Left(ArgumentDate, 2)
                MM = Strings.Mid(ArgumentDate, 4, 2)
                AAAA = Strings.Right(ArgumentDate, 4)
                Select Case FormatSortie
                    Case "AAAA"
                        ChaineRes = AAAA
                    Case "JJMMAAAA"
                        ChaineRes = JJ & MM & AAAA
                    Case "AAAAMM"
                        ChaineRes = AAAA & MM
                    Case "AAAAMMJJ"
                        ChaineRes = AAAA & MM & JJ
                    Case "MMAAAA"
                        ChaineRes = MM & AAAA
                    Case "01MMAAAA"
                        ChaineRes = "01" & MM & AAAA
                End Select
                Return ChaineRes
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal NoObjet As Integer)
            MyBase.New()
            WsNumObjet = NoObjet
        End Sub
    End Class

    Public Class PER_PAIE_GEST2
        Inherits PER_PAIE_GEST

        Public Sub New()
            MyBase.New(165)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_PAIE_GEST2"
            End Get
        End Property
    End Class

    Public Class PER_PAIE_GEST3
        Inherits PER_PAIE_GEST

        Public Sub New()
            MyBase.New(167)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_PAIE_GEST3"
            End Get
        End Property
    End Class
End Namespace
