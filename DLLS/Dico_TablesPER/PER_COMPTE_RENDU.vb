﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_COMPTE_RENDU
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_de_creation As String
        Private WsDate_de_maj As String
        Private WsHeure_de_maj As String
        Private WsDernierUtilisateur As String
        Private WsDate_de_validation As String
        Private WsHeure_de_validation As String
        Private WsDernierValideur As String
        Private WsParametres As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_COMPTE_RENDU"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaVirtualia
            End Get
        End Property

        Public Property Date_de_Creation() As String
            Get
                Return WsDate_de_creation
            End Get
            Set(ByVal value As String)
                WsDate_de_creation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_MiseaJour() As String
            Get
                Return WsDate_de_maj
            End Get
            Set(ByVal value As String)
                WsDate_de_maj = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_de_MiseaJour() As String
            Get
                Return WsHeure_de_maj
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure_de_maj = value
                    Case Else
                        WsHeure_de_maj = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property DernierUtilisateur() As String
            Get
                Return WsDernierUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsDernierUtilisateur = value
                    Case Else
                        WsDernierUtilisateur = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Date_de_Validation() As String
            Get
                Return WsDate_de_validation
            End Get
            Set(ByVal value As String)
                WsDate_de_validation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_de_Validation() As String
            Get
                Return WsHeure_de_validation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure_de_validation = value
                    Case Else
                        WsHeure_de_validation = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property DernierValideur() As String
            Get
                Return WsDernierValideur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsDernierValideur = value
                    Case Else
                        WsDernierValideur = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Parametres() As String
            Get
                Return WsParametres
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsParametres = value
                    Case Else
                        WsParametres = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Date_de_Creation & VI.Tild)
                Chaine.Append(Date_de_MiseaJour & VI.Tild)
                Chaine.Append(Heure_de_MiseaJour & VI.Tild)
                Chaine.Append(DernierUtilisateur & VI.Tild)
                Chaine.Append(Date_de_Validation & VI.Tild)
                Chaine.Append(Heure_de_Validation & VI.Tild)
                Chaine.Append(DernierValideur & VI.Tild)
                Chaine.Append(Parametres)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_de_Creation = TableauData(1)
                Date_de_MiseaJour = TableauData(2)
                Heure_de_MiseaJour = TableauData(3)
                DernierUtilisateur = TableauData(4)
                Date_de_Validation = TableauData(5)
                Heure_de_Validation = TableauData(6)
                DernierValideur = TableauData(7)
                Parametres = TableauData(8)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 1
                        Return WsDate_de_creation
                    Case 2
                        Return WsDate_de_maj
                    Case 3
                        Return WsHeure_de_maj
                    Case 4
                        Return WsDernierUtilisateur
                    Case 5
                        Return WsDate_de_validation
                    Case 6
                        Return WsHeure_de_validation
                    Case 7
                        Return WsDernierValideur
                    Case 8
                        Return WsParametres
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


