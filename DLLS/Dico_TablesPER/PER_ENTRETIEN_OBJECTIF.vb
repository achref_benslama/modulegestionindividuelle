﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_OBJECTIF
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumero As Integer
        Private WsIntitule As String
        Private WsDelai As String
        Private WsDemarche As String
        Private WsResultatAttendu As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_OBJECTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienObjectif
            End Get
        End Property

        Public Property Numero() As Integer
            Get
                Return WsNumero
            End Get
            Set(ByVal value As Integer)
                WsNumero = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Delai() As String
            Get
                Return WsDelai
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDelai = value
                    Case Else
                        WsDelai = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Demarche() As String
            Get
                Return WsDemarche
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDemarche = value
                    Case Else
                        WsDemarche = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Resultats_Attendus() As String
            Get
                Return WsResultatAttendu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsResultatAttendu = value
                    Case Else
                        WsResultatAttendu = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Numero.ToString & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Delai & VI.Tild)
                Chaine.Append(Demarche & VI.Tild)
                Chaine.Append(Resultats_Attendus)
               
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Numero = CInt(VirRhFonction.ConversionDouble(TableauData(2)))
                Intitule = TableauData(3)
                Delai = TableauData(4)
                Demarche = TableauData(5)
                Resultats_Attendus = TableauData(6)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
