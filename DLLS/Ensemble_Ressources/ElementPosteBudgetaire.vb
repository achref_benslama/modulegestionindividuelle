Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes

Namespace Datas
    Namespace PosteBudgetaire
        Public Class ElementDetaille
            Private WsHost As Virtualia.Ressources.Datas.PosteBudgetaire.EmploisBudgetaires
            Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
            'Propriétés
            Private WsNumEmploi As String
            Private WsIdeDossier As Integer
            Private WsDateEmploi As String
            Private WsDateFinEmploi As String
            Private WsGradeEmploi As String
            Private WsQualite As String
            Private WsNom As String
            Private WsPrenom As String
            Private WsPatronyme As String
            Private WsDateStatut As String
            Private WsStatut As String
            Private WsModeRemuneration As String
            Private WsDatePosition As String
            Private WsPosition As String
            Private WsTauxActivite As Double
            Private WsDateFinPosition As String
            Private WsDatePositionDetachement As String
            Private WsPositionDetachement As String
            Private WsTauxActiviteDetachement As Double
            Private WsDateFinPositionDetachement As String
            Private WsDateGrade As String
            Private WsGrade As String
            Private WsEchelon As String
            Private WsIndiceBrut As Double
            Private WsIndiceMajore As Double
            Private WsCategorie As String
            Private WsCorps As String
            Private WsForfaitMensuel As Double
            Private WsTauxHoraire As Double
            Private WsDateGradeDetachement As String
            Private WsGradeDetachement As String
            Private WsEchelonDetachement As String
            Private WsIndiceBrutDetachement As Double
            Private WsIndiceMajoreDetachement As Double
            Private WsCategorieDetachement As String
            Private WsCorpsDetachement As String
            Private WsForfaitMensuelDetachement As Double
            Private WsTauxHoraireDetachement As Double
            Private WsDateAffectation As String
            Private WsDirection As String
            Private WsService As String
            Private WsNiveau3 As String
            Private WsNiveau4 As String
            Private WsFonction As String
            Private WsNatureAbsence As String
            '
            Private Const SiDetachement As String = "DETACHEMENT INTERNE"
            '
            Public ReadOnly Property VParent() As Virtualia.Ressources.Datas.PosteBudgetaire.EmploisBudgetaires
                Get
                    Return WsHost
                End Get
            End Property

            Public Property IdentifiantDossier() As Integer
                Get
                    Return WsIdeDossier
                End Get
                Set(ByVal value As Integer)
                    WsIdeDossier = value
                End Set
            End Property

            Public Property Qualite() As String
                Get
                    Return WsQualite
                End Get
                Set(ByVal value As String)
                    WsQualite = value
                End Set
            End Property

            Public Property Nom() As String
                Get
                    Return WsNom
                End Get
                Set(ByVal value As String)
                    WsNom = value
                End Set
            End Property

            Public Property Prenom() As String
                Get
                    Return WsPrenom
                End Get
                Set(ByVal value As String)
                    WsPrenom = value
                End Set
            End Property

            Public Property Patronyme() As String
                Get
                    Select Case WsPatronyme
                        Case Is <> WsNom
                            Return WsPatronyme
                        Case Else
                            Return ""
                    End Select
                End Get
                Set(ByVal value As String)
                    WsPatronyme = value
                End Set
            End Property

            Public Property NumeroEmploi() As String
                Get
                    Select Case WsDateFinEmploi
                        Case Is <> ""
                            Select Case VirRhDates.ComparerDates(VParent.DatedEffet, WsDateFinEmploi)
                                Case VI.ComparaisonDates.PlusGrand
                                    Return "KO"
                            End Select
                    End Select
                    Return WsNumEmploi
                End Get
                Set(ByVal value As String)
                    WsNumEmploi = value
                End Set
            End Property

            Public Property DateEmploi() As String
                Get
                    Return WsDateEmploi
                End Get
                Set(ByVal value As String)
                    WsDateEmploi = value
                End Set
            End Property

            Public Property DateFinEmploi() As String
                Get
                    Return WsDateFinEmploi
                End Get
                Set(ByVal value As String)
                    WsDateFinEmploi = value
                End Set
            End Property

            Public Property GradedelEmploi() As String
                Get
                    Return WsGradeEmploi
                End Get
                Set(ByVal value As String)
                    WsGradeEmploi = value
                End Set
            End Property

            Public Property Statut() As String
                Get
                    Return WsStatut
                End Get
                Set(ByVal value As String)
                    WsStatut = value
                End Set
            End Property

            Public Property DateduStatut() As String
                Get
                    Return WsDateStatut
                End Get
                Set(ByVal value As String)
                    WsDateStatut = value
                End Set
            End Property

            Public Property ModedeRemuneration() As String
                Get
                    Return WsModeRemuneration
                End Get
                Set(ByVal value As String)
                    WsModeRemuneration = value
                End Set
            End Property

            Public Property Position() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsPositionDetachement
                    End Select
                    Return WsPosition
                End Get
                Set(ByVal value As String)
                    WsPosition = value
                End Set
            End Property

            Public Property DatedelaPosition() As String
                Get
                    Return WsDatePosition
                End Get
                Set(ByVal value As String)
                    WsDatePosition = value
                End Set
            End Property

            Public Property TauxdActivite() As Double
                Get
                    Dim Taux As Double
                    Taux = WsTauxActivite
                    Select Case Vacance
                        Case False
                            Select Case WsTauxActivite
                                Case Is = 0
                                    Taux = 100
                            End Select
                        Case True
                            Taux = 0
                    End Select
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Taux = WsTauxActiviteDetachement
                            'Select Case VirServiceFct.VerifierVacanceBudgetaire(WsPositionDetachement)
                            'Case Is = "OUI"
                            '   Taux = 0
                            'Case Else
                            Select Case WsTauxActiviteDetachement
                                Case Is = 0
                                    Taux = 100
                            End Select
                            'End Select
                    End Select
                    Return Taux
                End Get
                Set(ByVal value As Double)
                    WsTauxActivite = value
                End Set
            End Property

            Public Property DatedeFinPosition() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsDateFinPositionDetachement
                    End Select
                    Return WsDateFinPosition
                End Get
                Set(ByVal value As String)
                    WsDateFinPosition = value
                End Set
            End Property

            Public Property PositiondeDetachement() As String
                Get
                    Return WsPositionDetachement
                End Get
                Set(ByVal value As String)
                    WsPositionDetachement = value
                End Set
            End Property

            Public Property DatePositiondeDetachement() As String
                Get
                    Return WsDatePositionDetachement
                End Get
                Set(ByVal value As String)
                    WsDatePositionDetachement = value
                End Set
            End Property

            Public Property TauxdActivitedeDetachement() As Double
                Get
                    Return WsTauxActiviteDetachement
                End Get
                Set(ByVal value As Double)
                    WsTauxActiviteDetachement = value
                End Set
            End Property

            Public Property DateFinPositiondeDetachement() As String
                Get
                    Return WsDateFinPositionDetachement
                End Get
                Set(ByVal value As String)
                    WsDateFinPositionDetachement = value
                End Set
            End Property

            Public Property DateduGrade() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsDateGradeDetachement
                    End Select
                    Return WsDateGrade
                End Get
                Set(ByVal value As String)
                    WsDateGrade = value
                End Set
            End Property

            Public Property Grade() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsGradeDetachement
                    End Select
                    Return WsGrade
                End Get
                Set(ByVal value As String)
                    WsGrade = value
                End Set
            End Property

            Public Property Echelon() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsEchelonDetachement
                    End Select
                    Return WsEchelon
                End Get
                Set(ByVal value As String)
                    WsEchelon = value
                End Set
            End Property

            Public Property Corps() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsCorpsDetachement
                    End Select
                    Return WsCorps
                End Get
                Set(ByVal value As String)
                    WsCorps = value
                End Set
            End Property

            Public Property IndiceBrut() As Double
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsIndiceBrutDetachement
                    End Select
                    Return WsIndiceBrut
                End Get
                Set(ByVal value As Double)
                    WsIndiceBrut = value
                End Set
            End Property

            Public Property IndiceMajore() As Double
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsIndiceMajoreDetachement
                    End Select
                    Return WsIndiceMajore
                End Get
                Set(ByVal value As Double)
                    WsIndiceMajore = value
                End Set
            End Property

            Public Property Categorie() As String
                Get
                    Select Case UCase(WsPosition)
                        Case Is = SiDetachement
                            Return WsCategorieDetachement
                    End Select
                    Return WsCategorie
                End Get
                Set(ByVal value As String)
                    WsCategorie = value
                End Set
            End Property

            Public Property ForfaitMensuel() As Double
                Get
                    Return WsForfaitMensuel
                End Get
                Set(ByVal value As Double)
                    WsForfaitMensuel = value
                End Set
            End Property

            Public Property TauxHoraire() As Double
                Get
                    Return WsTauxHoraire
                End Get
                Set(ByVal value As Double)
                    WsTauxHoraire = value
                End Set
            End Property

            Public Property GradedeDetachement() As String
                Get
                    Return WsGradeDetachement
                End Get
                Set(ByVal value As String)
                    WsGradeDetachement = value
                End Set
            End Property

            Public Property DateGradedeDetachement() As String
                Get
                    Return WsDateGradeDetachement
                End Get
                Set(ByVal value As String)
                    WsDateGradeDetachement = value
                End Set
            End Property

            Public Property EchelondeDetachement() As String
                Get
                    Return WsEchelonDetachement
                End Get
                Set(ByVal value As String)
                    WsEchelonDetachement = value
                End Set
            End Property

            Public Property CorpsdeDetachement() As String
                Get
                    Return WsCorpsDetachement
                End Get
                Set(ByVal value As String)
                    WsCorpsDetachement = value
                End Set
            End Property

            Public Property CategoriedeDetachement() As String
                Get
                    Return WsCategorieDetachement
                End Get
                Set(ByVal value As String)
                    WsCategorieDetachement = value
                End Set
            End Property

            Public Property IndiceBrutdeDetachement() As Double
                Get
                    Return WsIndiceBrutDetachement
                End Get
                Set(ByVal value As Double)
                    WsIndiceBrutDetachement = value
                End Set
            End Property

            Public Property IndiceMajoredeDetachement() As Double
                Get
                    Return WsIndiceMajoreDetachement
                End Get
                Set(ByVal value As Double)
                    WsIndiceMajoreDetachement = value
                End Set
            End Property

            Public Property ForfaitMensueldeDetachement() As Double
                Get
                    Return WsForfaitMensuelDetachement
                End Get
                Set(ByVal value As Double)
                    WsForfaitMensuelDetachement = value
                End Set
            End Property

            Public Property TauxHorairedeDetachement() As Double
                Get
                    Return WsTauxHoraireDetachement
                End Get
                Set(ByVal value As Double)
                    WsTauxHoraireDetachement = value
                End Set
            End Property

            Public Property DatedelAffectationFonctionnelle() As String
                Get
                    Return WsDateAffectation
                End Get
                Set(ByVal value As String)
                    WsDateAffectation = value
                End Set
            End Property

            Public Property Direction() As String
                Get
                    Return WsDirection
                End Get
                Set(ByVal value As String)
                    WsDirection = value
                End Set
            End Property

            Public Property Service() As String
                Get
                    Return WsService
                End Get
                Set(ByVal value As String)
                    WsService = value
                End Set
            End Property

            Public Property Niveau3() As String
                Get
                    Return WsNiveau3
                End Get
                Set(ByVal value As String)
                    WsNiveau3 = value
                End Set
            End Property

            Public Property Niveau4() As String
                Get
                    Return WsNiveau4
                End Get
                Set(ByVal value As String)
                    WsNiveau4 = value
                End Set
            End Property

            Public Property FonctionExercee() As String
                Get
                    Return WsFonction
                End Get
                Set(ByVal value As String)
                    WsFonction = value
                End Set
            End Property

            Public Property NaturedelAbsence() As String
                Get
                    Return WsNatureAbsence
                End Get
                Set(ByVal value As String)
                    WsNatureAbsence = value
                End Set
            End Property

            Public ReadOnly Property Vacance() As Boolean
                Get
                    'Select Case VirServiceFct.VerifierVacanceBudgetaire(WsPosition)
                    '   Case Is = "OUI"
                    'Vacance = True
                    '    Case Else
                    Return False
                    'End Select
                    'Select Case VirServiceFct.VerifierAbsence(WsNatureAbsence)
                    '   Case Is = "OUI"
                    'Vacance = True
                    'End Select
                End Get
            End Property

            Public ReadOnly Property Gage() As Boolean
                Get
                    Select Case Grade
                        Case Is <> ""
                            Select Case WsGradeEmploi
                                Case Is <> ""
                                    Select Case WsGradeEmploi
                                        Case Is <> Grade 'Situation de gage
                                            Return True
                                    End Select
                            End Select
                    End Select
                    Return False
                End Get
            End Property

            Public Sub New(ByVal Host As Virtualia.Ressources.Datas.PosteBudgetaire.EmploisBudgetaires)
                WsHost = Host
            End Sub

        End Class
    End Namespace
End Namespace