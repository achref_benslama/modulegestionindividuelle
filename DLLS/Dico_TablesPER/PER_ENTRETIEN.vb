﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ENTRETIEN
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsResponsable As String
        Private WsEmploiEvolution As String
        Private WsEmploiDemande As String
        Private WsEmploiDateEcheance As String
        Private WsEmploiAvis As String
        Private WsEmploiDateReponse As String
        Private WsMobiliteEvolution As String
        Private WsMobiliteDemande As String
        Private WsMobiliteDateEcheance As String
        Private WsMobiliteAvis As String
        Private WsMobiliteDateReponse As String
        Private WsFormationEvolution As String
        Private WsFormationDemande As String
        Private WsFormationDateEcheance As String
        Private WsFormationAvis As String
        Private WsFormationDateReponse As String
        Private WsValidation As String
        Private WsAtoutsPointsForts As String
        Private WsAdaptationEmploi As String
        Private WsPointsDeveloppement As String
        Private WsAutreMobiliteActivite As String
        Private WsRemarques As String
        Private WsCommentaireSalarie As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretien
            End Get
        End Property

        Public Property ResponsableEntretien() As String
            Get
                Return WsResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsResponsable = value
                    Case Else
                        WsResponsable = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Emploi_SiEvolution() As String
            Get
                Return WsEmploiEvolution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsEmploiEvolution = value
                    Case Else
                        WsEmploiEvolution = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Emploi_Demande() As String
            Get
                Return WsEmploiDemande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEmploiDemande = value
                    Case Else
                        WsEmploiDemande = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Emploi_DateEcheance() As String
            Get
                Return WsEmploiDateEcheance
            End Get
            Set(ByVal value As String)
                WsEmploiDateEcheance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Emploi_Avis() As String
            Get
                Return WsEmploiAvis
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEmploiAvis = value
                    Case Else
                        WsEmploiAvis = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Emploi_DateReponse() As String
            Get
                Return WsEmploiDateReponse
            End Get
            Set(ByVal value As String)
                WsEmploiDateReponse = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property MobiliteGeo_SiEvolution() As String
            Get
                Return WsMobiliteEvolution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsMobiliteEvolution = value
                    Case Else
                        WsMobiliteEvolution = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property MobiliteGeo_Demande() As String
            Get
                Return WsMobiliteDemande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsMobiliteDemande = value
                    Case Else
                        WsMobiliteDemande = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property MobiliteGeo_DateEcheance() As String
            Get
                Return WsMobiliteDateEcheance
            End Get
            Set(ByVal value As String)
                WsMobiliteDateEcheance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property MobiliteGeo_Avis() As String
            Get
                Return WsMobiliteAvis
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMobiliteAvis = value
                    Case Else
                        WsMobiliteAvis = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property MobiliteGeo_DateReponse() As String
            Get
                Return WsMobiliteDateReponse
            End Get
            Set(ByVal value As String)
                WsMobiliteDateReponse = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Formation_SiEvolution() As String
            Get
                Return WsFormationEvolution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsFormationEvolution = value
                    Case Else
                        WsFormationEvolution = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Formation_Demande() As String
            Get
                Return WsFormationDemande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFormationDemande = value
                    Case Else
                        WsFormationDemande = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Formation_DateEcheance() As String
            Get
                Return WsFormationDateEcheance
            End Get
            Set(ByVal value As String)
                WsFormationDateEcheance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Formation_Avis() As String
            Get
                Return WsFormationAvis
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsFormationAvis = value
                    Case Else
                        WsFormationAvis = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Formation_DateReponse() As String
            Get
                Return WsFormationDateReponse
            End Get
            Set(ByVal value As String)
                WsFormationDateReponse = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Validation() As String
            Get
                Return WsValidation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsValidation = value
                    Case Else
                        WsValidation = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Atouts_Et_PointsForts() As String
            Get
                Return WsAtoutsPointsForts
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAtoutsPointsForts = value
                    Case Else
                        WsAtoutsPointsForts = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Adaptation_Emploi() As String
            Get
                Return WsAdaptationEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAdaptationEmploi = value
                    Case Else
                        WsAdaptationEmploi = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Points_a_Developper() As String
            Get
                Return WsPointsDeveloppement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsPointsDeveloppement = value
                    Case Else
                        WsPointsDeveloppement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Mobilite_vers_Activite() As String
            Get
                Return WsAutreMobiliteActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAutreMobiliteActivite = value
                    Case Else
                        WsAutreMobiliteActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Remarques_et_Suggestions() As String
            Get
                Return WsRemarques
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsRemarques = value
                    Case Else
                        WsRemarques = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Property Commentaires_Salarie() As String
            Get
                Return WsCommentaireSalarie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsCommentaireSalarie = value
                    Case Else
                        WsCommentaireSalarie = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(ResponsableEntretien & VI.Tild)
                Chaine.Append(Emploi_SiEvolution & VI.Tild)
                Chaine.Append(Emploi_Demande & VI.Tild)
                Chaine.Append(Emploi_DateEcheance & VI.Tild)
                Chaine.Append(Emploi_Avis & VI.Tild)
                Chaine.Append(Emploi_DateReponse & VI.Tild)
                Chaine.Append(MobiliteGeo_SiEvolution & VI.Tild)
                Chaine.Append(MobiliteGeo_Demande & VI.Tild)
                Chaine.Append(MobiliteGeo_DateEcheance & VI.Tild)
                Chaine.Append(MobiliteGeo_Avis & VI.Tild)
                Chaine.Append(MobiliteGeo_DateReponse & VI.Tild)
                Chaine.Append(MobiliteGeo_Demande & VI.Tild)
                Chaine.Append(Formation_SiEvolution & VI.Tild)
                Chaine.Append(Formation_Demande & VI.Tild)
                Chaine.Append(Formation_DateEcheance & VI.Tild)
                Chaine.Append(Formation_Avis & VI.Tild)
                Chaine.Append(Formation_DateReponse & VI.Tild)
                Chaine.Append(Formation_Demande & VI.Tild)
                Chaine.Append(Validation & VI.Tild)
                Chaine.Append(Atouts_Et_PointsForts & VI.Tild)
                Chaine.Append(Adaptation_Emploi & VI.Tild)
                Chaine.Append(Points_a_Developper & VI.Tild)
                Chaine.Append(Mobilite_vers_Activite & VI.Tild)
                Chaine.Append(Remarques_et_Suggestions & VI.Tild)
                Chaine.Append(Commentaires_Salarie)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 25 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                ResponsableEntretien = TableauData(2)
                Emploi_SiEvolution = TableauData(3)
                Emploi_Demande = TableauData(4)
                Emploi_DateEcheance = TableauData(5)
                Emploi_Avis = TableauData(6)
                Emploi_DateReponse = TableauData(7)
                MobiliteGeo_SiEvolution = TableauData(8)
                MobiliteGeo_Demande = TableauData(9)
                MobiliteGeo_DateEcheance = TableauData(10)
                MobiliteGeo_Avis = TableauData(11)
                MobiliteGeo_DateReponse = TableauData(12)
                Formation_SiEvolution = TableauData(13)
                Formation_Demande = TableauData(14)
                Formation_DateEcheance = TableauData(15)
                Formation_Avis = TableauData(16)
                Formation_DateReponse = TableauData(17)
                Validation = TableauData(18)
                Atouts_Et_PointsForts = TableauData(19)
                Adaptation_Emploi = TableauData(20)
                Points_a_Developper = TableauData(21)
                Mobilite_vers_Activite = TableauData(22)
                Remarques_et_Suggestions = TableauData(23)
                Commentaires_Salarie = TableauData(24)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
