﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VPlanningAnnuel
    
    '''<summary>
    '''Contrôle UpdatePanelCol.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelCol As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle TimerPlanning.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TimerPlanning As Global.System.Web.UI.Timer
    
    '''<summary>
    '''Contrôle CadrePlanning.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePlanning As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle UpdateAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateAttente As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle LibelJours.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LibelJours As Global.Virtualia.Net.Controles_VLibelJourPlanning
    
    '''<summary>
    '''Contrôle PlanningM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM00 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM01 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM02 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM03 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM04 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM05 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM06 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM07 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM08 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM09 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM10 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM11 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM12 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM13 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM14 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM15 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM16 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle PlanningM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PlanningM17 As Global.Virtualia.Net.VPlanningMensuel
    
    '''<summary>
    '''Contrôle Legende.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Legende As Global.Virtualia.Net.Controles_VLegendeCouleurs
    
    '''<summary>
    '''Contrôle HIndex.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HIndex As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HPage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPage As Global.System.Web.UI.WebControls.HiddenField
End Class
