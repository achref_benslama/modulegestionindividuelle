﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DEMANDE_FORMATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Demande As String
        Private WsObjetDemande As String
        Private WsDate_Session As String
        Private WsOrigine As String
        Private WsPriorite As String
        Private WsSuiteDonnee As String
        Private WsDate_Echeance As String
        Private WsObservations As String
        Private WsSiHorsPlan As String
        Private WsOrganisme As String
        Private WsSiDIF As String
        Private WsSiHorsTempsW As String
        Private WsIntranet_Valideur As String
        Private WsIntranet_DateValidation As String
        Private WsIntranet_Decision As String
        Private WsIntranet_Valideur_NPlus1 As String
        Private WsIntranet_DateValidation_NPlus1 As String
        Private WsIntranet_Decision_NPlus1 As String
        Private WsIntranet_Valideur_NPlus2 As String
        Private WsIntranet_DateValidation_NPlus2 As String
        Private WsIntranet_Decision_NPlus2 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DEMANDE_FORMATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDemandeFormation
            End Get
        End Property

        Public Property Date_Demande() As String
            Get
                Return WsDate_Demande
            End Get
            Set(ByVal value As String)
                WsDate_Demande = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Objet_Demande() As String
            Get
                Return WsObjetDemande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsObjetDemande = value
                    Case Else
                        WsObjetDemande = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsObjetDemande
            End Set
        End Property

        Public Property Date_Session() As String
            Get
                Return WsDate_Session
            End Get
            Set(ByVal value As String)
                WsDate_Session = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Origine() As String
            Get
                Return WsOrigine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsOrigine = value
                    Case Else
                        WsOrigine = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Priorite() As String
            Get
                Return WsPriorite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPriorite = value
                    Case Else
                        WsPriorite = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property SuiteDonnee() As String
            Get
                Return WsSuiteDonnee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsSuiteDonnee = value
                    Case Else
                        WsSuiteDonnee = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Date_Echeance() As String
            Get
                Return WsDate_Echeance
            End Get
            Set(ByVal value As String)
                WsDate_Echeance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property SiHorsPlan() As String
            Get
                Return WsSiHorsPlan
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiHorsPlan = value
                    Case Is = "0", "Non"
                        WsSiHorsPlan = "Non"
                    Case Else
                        WsSiHorsPlan = "Oui"
                End Select
            End Set
        End Property

        Public Property Organisme() As String
            Get
                Return WsOrganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsOrganisme = value
                    Case Else
                        WsOrganisme = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property SiDIF() As String
            Get
                Return WsSiDIF
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiDIF = value
                    Case Is = "0", "Non"
                        WsSiDIF = "Non"
                    Case Else
                        WsSiDIF = "Oui"
                End Select
            End Set
        End Property

        Public Property SiHorsTempsTravail() As String
            Get
                Return WsSiHorsTempsW
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiHorsTempsW = value
                    Case Is = "0", "Non"
                        WsSiHorsTempsW = "Non"
                    Case Else
                        WsSiHorsTempsW = "Oui"
                End Select
            End Set
        End Property

        Public Property Intranet_Valideur() As String
            Get
                Return WsIntranet_Valideur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntranet_Valideur = value
                    Case Else
                        WsIntranet_Valideur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intranet_DateValidation() As String
            Get
                Return WsIntranet_DateValidation
            End Get
            Set(ByVal value As String)
                WsIntranet_DateValidation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Intranet_Decision() As String
            Get
                Return WsIntranet_Decision
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsIntranet_Decision = value
                    Case Else
                        WsIntranet_Decision = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Intranet_Valideur_NPlus1() As String
            Get
                Return WsIntranet_Valideur_NPlus1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntranet_Valideur_NPlus1 = value
                    Case Else
                        WsIntranet_Valideur_NPlus1 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intranet_DateValidation_NPlus1() As String
            Get
                Return WsIntranet_DateValidation_NPlus1
            End Get
            Set(ByVal value As String)
                WsIntranet_DateValidation_NPlus1 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Intranet_Decision_NPlus1() As String
            Get
                Return WsIntranet_Decision_NPlus1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsIntranet_Decision_NPlus1 = value
                    Case Else
                        WsIntranet_Decision_NPlus1 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Intranet_Valideur_NPlus2() As String
            Get
                Return WsIntranet_Valideur_NPlus2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntranet_Valideur_NPlus2 = value
                    Case Else
                        WsIntranet_Valideur_NPlus2 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intranet_DateValidation_NPlus2() As String
            Get
                Return WsIntranet_DateValidation_NPlus2
            End Get
            Set(ByVal value As String)
                WsIntranet_DateValidation_NPlus2 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Intranet_Decision_NPlus2() As String
            Get
                Return WsIntranet_Decision_NPlus2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsIntranet_Decision_NPlus2 = value
                    Case Else
                        WsIntranet_Decision_NPlus2 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Demande & VI.Tild)
                Chaine.Append(Objet_Demande & VI.Tild)
                Chaine.Append(Date_Session & VI.Tild)
                Chaine.Append(Origine & VI.Tild)
                Chaine.Append(Priorite & VI.Tild)
                Chaine.Append(SuiteDonnee & VI.Tild)
                Chaine.Append(Date_Echeance & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(SiHorsPlan & VI.Tild)
                Chaine.Append(Organisme & VI.Tild)
                Chaine.Append(SiDIF & VI.Tild)
                Chaine.Append(SiHorsTempsTravail & VI.Tild)
                Chaine.Append(Intranet_Valideur & VI.Tild)
                Chaine.Append(Intranet_DateValidation & VI.Tild)
                Chaine.Append(Intranet_Decision & VI.Tild)
                Chaine.Append(Intranet_Valideur_NPlus1 & VI.Tild)
                Chaine.Append(Intranet_DateValidation_NPlus1 & VI.Tild)
                Chaine.Append(Intranet_Decision_NPlus1 & VI.Tild)
                Chaine.Append(Intranet_Valideur_NPlus2 & VI.Tild)
                Chaine.Append(Intranet_DateValidation_NPlus2 & VI.Tild)
                Chaine.Append(Intranet_Decision_NPlus2)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Demande = TableauData(1)
                Objet_Demande = TableauData(2)
                Date_Session = TableauData(3)
                Origine = TableauData(4)
                Priorite = TableauData(5)
                SuiteDonnee = TableauData(6)
                Date_Echeance = TableauData(7)
                Observations = TableauData(8)
                SiHorsPlan = TableauData(9)
                Organisme = TableauData(10)
                SiDIF = TableauData(11)
                SiHorsTempsTravail = TableauData(12)
                Intranet_Valideur = TableauData(13)
                Intranet_DateValidation = TableauData(14)
                Intranet_Decision = TableauData(15)
                Intranet_Valideur_NPlus1 = TableauData(16)
                Intranet_DateValidation_NPlus1 = TableauData(17)
                Intranet_Decision_NPlus1 = TableauData(18)
                Intranet_Valideur_NPlus2 = TableauData(19)
                Intranet_DateValidation_NPlus2 = TableauData(20)
                Intranet_Decision_NPlus2 = TableauData(21)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_Demande
                    Case 1
                        Return Objet_Demande
                    Case 2
                        Return Date_Session
                    Case 3
                        Return Origine
                    Case 4
                        Return Priorite
                    Case 5
                        Return SuiteDonnee
                    Case 6
                        Return Date_Echeance
                    Case 7
                        Return Observations
                    Case 8
                        Return SiHorsPlan
                    Case 9
                        Return Organisme
                    Case 10
                        Return SiDIF
                    Case 11
                        Return SiHorsTempsTravail
                    Case 12
                        Return Intranet_Valideur
                    Case 13
                        Return Intranet_DateValidation
                    Case 14
                        Return Intranet_Decision
                    Case 15
                        Return Intranet_Valideur_NPlus1
                    Case 16
                        Return Intranet_DateValidation_NPlus1
                    Case 17
                        Return Intranet_Decision_NPlus1
                    Case 18
                        Return Intranet_Valideur_NPlus2
                    Case 19
                        Return Intranet_DateValidation_NPlus2
                    Case 20
                        Return Intranet_Decision_NPlus2
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



