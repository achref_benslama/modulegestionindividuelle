Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRILLE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        Private WsLstEchelons As List(Of GRD_ECHELON)
        '
        Private WsIntitule As String

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrilles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Echelon(ByVal Index As Integer) As String
            Get
                If WsLstEchelons Is Nothing OrElse WsLstEchelons.Count = 0 Then
                    Return 0
                End If
                For Each Element As GRD_ECHELON In WsLstEchelons
                    If Element.IndexListe = Index Then
                        Return Element.ContenuTable
                    End If
                Next
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 1 To 20
                        Select Case value.Length
                            Case Is <= 80
                                Call AjouterUnItem(value)
                            Case Else
                                Call AjouterUnItem(Strings.Left(value, 80))
                        End Select
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder
                Dim IndiceI As Integer

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                For IndiceI = 1 To 20
                    Chaine.Append(Echelon(IndiceI) & VI.Tild)
                Next IndiceI

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 23 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule = TableauData(2)
                For IndiceI = 3 To 22
                    Echelon(IndiceI - 2) = TableauData(IndiceI)
                Next IndiceI
                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set

        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub ReinitialiserMyListe()
            WsLstEchelons.Clear()
        End Sub

        Private Sub AjouterUnItem(ByVal Valeurs As String)
            Dim ItemEchelon As GRD_ECHELON = New GRD_ECHELON(Valeurs)
            ItemEchelon.IndexListe = Ajouter_Echelon(ItemEchelon)
        End Sub

        Public Function FicheEchelon(ByVal Index As Integer) As GRD_ECHELON
            If WsLstEchelons Is Nothing OrElse WsLstEchelons.Count = 0 Then
                Return Nothing
            End If
            If (Index < 0 Or Index >= WsLstEchelons.Count) Then
                Return Nothing
            End If
            Return WsLstEchelons(Index)
        End Function

        Public Function Ajouter_Echelon(ByVal Fiche As GRD_ECHELON) As Integer
            If WsLstEchelons Is Nothing Then
                WsLstEchelons = New List(Of GRD_ECHELON)
            End If
            WsLstEchelons.Add(Fiche)
            Return WsLstEchelons.Count
        End Function

        Public Function ListedesEchelons(ByVal CritereTri As String) As List(Of GRD_ECHELON)
            If WsLstEchelons Is Nothing OrElse WsLstEchelons.Count = 0 Then
                Return Nothing
            End If
            Dim LstRes As List(Of GRD_ECHELON)
            Select Case CritereTri
                Case "Alias"
                    LstRes = (From Element In WsLstEchelons Where Element.AliasEchelon <> "").ToList
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstRes.OrderBy(Function(m) m.AliasEchelon).ToList
                Case "Brut"
                    LstRes = (From Element In WsLstEchelons Where Element.IndiceBrut <> "").ToList
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstRes.OrderBy(Function(m) CInt(m.IndiceBrut)).ToList
                Case "Chevron"
                    LstRes = (From Element In WsLstEchelons Where Element.Chevron <> "").ToList
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstRes.OrderBy(Function(m) m.Chevron).ToList
                Case "Major�"
                    LstRes = (From Element In WsLstEchelons Where Element.IndiceMajore <> "").ToList
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstRes.OrderBy(Function(m) CInt(m.IndiceMajore)).ToList
                Case Else
                    Return WsLstEchelons.OrderBy(Function(m) m.IndexListe).ToList
            End Select
        End Function

        Public ReadOnly Property SiGrilleHorsEchelle As Boolean
            Get
                Dim LstRes As List(Of GRD_ECHELON) = ListedesEchelons("Chevron")
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public ReadOnly Property Majore_Moyen As Integer
            Get
                Dim LstRes As List(Of GRD_ECHELON) = ListedesEchelons("Major�")
                Dim Total As Integer = 0
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return 0
                End If
                For Each Fiche In LstRes
                    Total += CInt(Fiche.IndiceMajore)
                Next
                Return Math.Round(Total / LstRes.Count, 0)
            End Get
        End Property
        Public ReadOnly Property Brut_Moyen As Integer
            Get
                Dim LstRes As List(Of GRD_ECHELON) = ListedesEchelons("Brut")
                Dim Total As Integer = 0
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return 0
                End If
                For Each Fiche In LstRes
                    Total += CInt(Fiche.IndiceBrut)
                Next
                Return Math.Round(Total / LstRes.Count, 0)
            End Get
        End Property
        Public ReadOnly Property Total_Duree(ByVal Nature As String) As String
            Get
                Dim LstRes As List(Of GRD_ECHELON) = ListedesEchelons("Major�")
                Dim CptDuree As String = StrDup(8, "0")
                Dim Valeur As String
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return 0
                End If
                For Each Fiche In LstRes
                    Valeur = ""
                    Select Case Nature
                        Case "Mini"
                            Valeur = Fiche.DureeMini
                        Case "Maxi"
                            Valeur = Fiche.DureeMaxi
                        Case "Moyenne"
                            Valeur = Fiche.DureeMoyenne
                    End Select
                    If Valeur <> "" AndAlso Valeur <> "000000" Then
                        CptDuree = VirRhDate.DureePlus(CptDuree, "00" & Valeur)
                    End If
                Next
                If Val(CptDuree) = 0 Then
                    Return ""
                Else
                    Return Strings.Right(CptDuree, 6)
                End If
            End Get
        End Property
    End Class
End Namespace