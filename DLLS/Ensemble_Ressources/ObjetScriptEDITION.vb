﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.Parametrage.BasesdeDonnees
Imports Virtualia.Systeme.MetaModele.Donnees

Namespace Datas
    Public Class ObjetScriptEDITION
        Implements IGestionVirFiche(Of EDITION_DESCRIPTIF)

        Private WsInstanceSgBd As ParamSgbdDatabase
        Private WsListeScript As List(Of EDITION_DESCRIPTIF)
        Private WsNomUtilisateur As String
        Private WsPtDevue As Integer

        Public ReadOnly Property ListeDesScripts(Optional ByVal CritereSel As String = "", Optional ByVal CritereTri As String = "") As List(Of EDITION_DESCRIPTIF)
            Get
                If WsListeScript Is Nothing Then
                    Return Nothing
                End If

                If CritereSel = "" Then
                    Select Case CritereTri
                        Case Is = ""
                            Return WsListeScript
                        Case "Catégorie"
                            Return WsListeScript.OrderBy(Function(m) m.CategorieRH).ToList
                        Case "Intitulé"
                            Return WsListeScript.OrderBy(Function(m) m.Intitule).ToList
                    End Select
                End If
                Dim LstRes As List(Of EDITION_DESCRIPTIF) = Nothing
                Select Case CritereSel
                    Case Is = "Catégorie"
                        Select Case CritereTri
                            Case Is = ""
                                Return WsListeScript.FindAll(Function(m) m.CategorieRH = CInt(CritereSel)).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.CategorieRH = CInt(CritereSel) _
                                                Order By instance.CategorieRH Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.CategorieRH = CInt(CritereSel) _
                                                Order By instance.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Intitulé"
                        Select Case CritereTri
                            Case Is = ""
                                Return WsListeScript.FindAll(Function(m) m.Intitule = CritereSel).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.Intitule = CritereSel _
                                                Order By instance.CategorieRH Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.Intitule = CritereSel _
                                                Order By instance.Intitule Ascending).ToList
                                End Select
                        End Select
                End Select
                If LstRes Is Nothing Then
                    Return WsListeScript
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeScripts() As Integer Implements IGestionVirFiche(Of EDITION_DESCRIPTIF).NombredItems
            Get
                Return WsListeScript.Count
            End Get
        End Property

        Public Sub Actualiser() Implements IGestionVirFiche(Of EDITION_DESCRIPTIF).Actualiser
            WsListeScript.Clear()
            WsListeScript = Nothing
            ConstituerListe()
        End Sub

        Public Function GetFiche(ByVal Ide_Dossier As Integer) As EDITION_DESCRIPTIF Implements IGestionVirFiche(Of EDITION_DESCRIPTIF).GetFiche
            If (WsListeScript Is Nothing) Then
                Return Nothing
            End If
            Return (From it As EDITION_DESCRIPTIF In WsListeScript Where it.Ide_Dossier = Ide_Dossier Select it).FirstOrDefault()
        End Function

        Public Function GetFiche(ByVal Intitule As String) As EDITION_DESCRIPTIF Implements IGestionVirFiche(Of EDITION_DESCRIPTIF).GetFiche
            If (WsListeScript Is Nothing) Then
                Return Nothing
            End If
            Return (From it As EDITION_DESCRIPTIF In WsListeScript Where it.Intitule = Intitule Select it).FirstOrDefault()
        End Function

        Private Sub LireDossier(ByVal Ide As Integer)
            Dim IndiceO As Integer
            Dim IndiceNb As Integer
            Dim NumObjet As Integer = 0
            Dim TableauData(0) As String
            Dim TableauObjet As List(Of String)
            Dim FicheScript As EDITION_DESCRIPTIF = Nothing
            Dim FicheSelection As EDITION_SELECTION = Nothing
            Dim FicheValeur As EDITION_FILTRE = Nothing
            Dim LstNoObjets As New List(Of Integer)

            For IndiceO = 1 To 3
                LstNoObjets.Add(CInt(IndiceO))
            Next IndiceO
            TableauObjet = VirServiceServeur.LectureDossier_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueScriptCmc, Ide, False, LstNoObjets)

            For IndiceO = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceO) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceO), VI.Tild, -1)
                Select Case Strings.Left(TableauObjet(IndiceO), 6)
                    Case Is = "Objet="
                        NumObjet = CInt(Strings.Right(TableauData(0), TableauData(0).Length - 6))
                    Case Else
                        Select Case NumObjet
                            Case 1
                                FicheScript = New EDITION_DESCRIPTIF
                                FicheScript.ContenuTable = TableauObjet(IndiceO)
                                WsListeScript.Add(FicheScript)
                            Case 2
                                FicheSelection = New EDITION_SELECTION
                                FicheSelection.ContenuTable = TableauObjet(IndiceO)
                                IndiceNb = FicheScript.Ajouter_Valeur(FicheSelection)
                            Case 3
                                FicheValeur = New EDITION_FILTRE
                                FicheValeur.ContenuTable = TableauObjet(IndiceO)
                                FicheSelection = FicheScript.Fiche_Selection(FicheValeur.Numero_LigneIndice)
                                If FicheSelection IsNot Nothing Then
                                    IndiceNb = FicheSelection.Ajouter_Filtre(FicheValeur)
                                End If
                        End Select
                End Select
            Next IndiceO
        End Sub

        Private Sub ConstituerListe()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LstResultat As List(Of String)
            Dim IndiceA As Integer
            Dim TableauData(0) As String
            Dim Ide As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceSgBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueScriptCmc, "", "", VI.Operateurs.ET) = 2
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = WsPtDevue.ToString
            Constructeur.NoInfoSelection(1, 1) = 3
            Select Case WsNomUtilisateur
                Case Is = "Virtualia"
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Virtualia" & VI.PointVirgule & "VStandard"
                Case Else
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "VStandard" & VI.PointVirgule & WsNomUtilisateur
            End Select
            Constructeur.InfoExtraite(0, 1, 0) = 1
            Constructeur.InfoExtraite(1, 1, 2) = 2
            Constructeur.InfoExtraite(2, 1, 0) = 3
            Constructeur.InfoExtraite(3, 1, 1) = 24

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueConfiguration, 1, Constructeur.OrdreSqlDynamique)
            Constructeur = Nothing

            WsListeScript = New List(Of EDITION_DESCRIPTIF)

            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If Not (IsNumeric(TableauData(0))) Then
                    Exit For
                End If
                Ide = CInt(TableauData(0))
                Call LireDossier(Ide)
            Next IndiceA
        End Sub

        Public Sub New(ByVal RhDico As ModeleRH, ByVal Sgbd As ParamSgbdDatabase, ByVal NomUtilisateur As String)
            Me.New(RhDico, Sgbd, NomUtilisateur, 1)
        End Sub

        Public Sub New(ByVal RhDico As ModeleRH, ByVal Sgbd As ParamSgbdDatabase, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer)
            VirModeleRh = RhDico
            WsNomUtilisateur = NomUtilisateur
            WsInstanceSgBd = Sgbd
            WsPtDevue = PtdeVue
            ConstituerListe()
        End Sub

    End Class
End Namespace



