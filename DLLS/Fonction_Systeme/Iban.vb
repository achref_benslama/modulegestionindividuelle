Option Strict Off
Option Explicit On
Option Compare Text
Namespace Fonctions
    Public Class IBAN
        Private TabLg(20) As Integer
        Private TabSigle(20) As String
        Private WsIBAN As String
        Private WsErreur As Integer
        'Longueurs
        Private Const LgAllemagne As Integer = 22
        Private Const LgAutriche As Integer = 20
        Private Const LgBelgique As Integer = 16
        Private Const LgDanemark As Integer = 18
        Private Const LgEspagne As Integer = 24
        Private Const LgFinlande As Integer = 18
        Private Const LgFrance As Integer = 27
        Private Const LgGr�ce As Integer = 27
        Private Const LgIrlande As Integer = 22
        Private Const LgIslande As Integer = 26
        Private Const LgItalie As Integer = 27
        Private Const LgLuxembourg As Integer = 20
        Private Const LgNorv�ge As Integer = 15
        Private Const LgPaysBas As Integer = 18
        Private Const LgPologne As Integer = 28
        Private Const LgPortugal As Integer = 25
        Private Const LgGB As Integer = 22
        Private Const LgSu�de As Integer = 24
        Private Const LgSuisse As Integer = 21
        'Sigles
        Private Const SigleAllemagne As String = "DE"
        Private Const SigleAutriche As String = "AT"
        Private Const SigleBelgique As String = "BE"
        Private Const SigleDanemark As String = "DK"
        Private Const SigleEspagne As String = "ES"
        Private Const SigleFinlande As String = "FI"
        Private Const SigleFrance As String = "FR"
        Private Const SigleGr�ce As String = "GR"
        Private Const SigleIrlande As String = "IE"
        Private Const SigleIslande As String = "IS"
        Private Const SigleItalie As String = "IT"
        Private Const SigleLuxembourg As String = "LU"
        Private Const SigleNorv�ge As String = "NO"
        Private Const SiglePaysBas As String = "NL"
        Private Const SiglePologne As String = "PL"
        Private Const SiglePortugal As String = "PT"
        Private Const SigleGB As String = "GB"
        Private Const SigleSu�de As String = "SE"
        Private Const SigleSuisse As String = "CH"
        '
        Public WriteOnly Property IBANSaisi() As String
            Set(ByVal Value As String)
                Dim I As Integer
                Dim Chaine As String

                Chaine = ""
                WsIBAN = Value.Trim
                WsErreur = 0
                Select Case WsIBAN
                    Case Is = ""
                        WsErreur = 1
                        Exit Property
                End Select
                'Epure des blancs
                For I = 1 To WsIBAN.Length
                    Select Case Strings.Mid(WsIBAN, I, 1)
                        Case Is = Strings.Space(1), "_"
                        Case Else
                            Chaine = Chaine & Strings.Mid(WsIBAN, I, 1).ToUpper
                    End Select
                Next I
                Select Case Chaine.Length
                    Case Is < 6
                        WsIBAN = ""
                        WsErreur = 1
                        Exit Property
                End Select
                Select Case Strings.Left(Chaine, 4)
                    Case Is = "IBAN"
                        WsIBAN = Strings.Right(Chaine, Chaine.Length - 4)
                    Case Else
                        WsIBAN = Chaine
                End Select
                'Contr�les
                For I = 0 To TabSigle.Count - 1
                    Select Case TabSigle(I)
                        Case Is = Strings.Left(WsIBAN, 2)
                            Select Case WsIBAN.Length
                                Case Is = TabLg(I)
                                    Exit Property
                                Case Is <> TabLg(I)
                                    WsErreur = 2
                                    Exit Property
                            End Select
                    End Select
                Next I
                WsErreur = 9
            End Set
        End Property

        Public ReadOnly Property ErreurFormelle() As Integer
            Get
                ErreurFormelle = WsErreur
            End Get
        End Property

        Public ReadOnly Property IBANElectronique() As String
            Get
                Select Case WsErreur
                    Case Is > 0
                        Return ""
                End Select
                IBANElectronique = WsIBAN
            End Get
        End Property

        Public ReadOnly Property IBANEdite() As String
            Get
                Dim I As Integer
                Dim Chaine As String

                Select Case WsErreur
                    Case Is > 0
                        Return ""
                End Select
                Chaine = "IBAN"
                For I = 1 To WsIBAN.Length Step 4
                    Chaine = Chaine & Strings.Space(1) & Strings.Mid(WsIBAN, I, 4)
                Next I
                IBANEdite = Chaine
            End Get
        End Property

        Public ReadOnly Property LongueurIban(ByVal Sigle As String) As Integer
            Get
                Dim I As Integer
                Select Case Sigle
                    Case Is = ""
                        LongueurIban = 0
                        Exit Property
                End Select

                For I = 0 To TabSigle.Count - 1
                    Select Case TabSigle(I)
                        Case Is = Sigle
                            LongueurIban = TabLg(I)
                            Exit Property
                    End Select
                Next I
                LongueurIban = 30
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            TabLg(0) = LgAllemagne
            TabSigle(0) = SigleAllemagne

            TabLg(1) = LgAutriche
            TabSigle(1) = SigleAutriche

            TabLg(2) = LgBelgique
            TabSigle(2) = SigleBelgique

            TabLg(3) = LgDanemark
            TabSigle(3) = SigleDanemark

            TabLg(4) = LgEspagne
            TabSigle(4) = SigleEspagne

            TabLg(5) = LgFinlande
            TabSigle(5) = SigleFinlande

            TabLg(6) = LgFrance
            TabSigle(6) = SigleFrance

            TabLg(7) = LgGr�ce
            TabSigle(7) = SigleGr�ce

            TabLg(8) = LgIrlande
            TabSigle(8) = SigleIrlande

            TabLg(9) = LgIslande
            TabSigle(9) = SigleIslande

            TabLg(10) = LgItalie
            TabSigle(10) = SigleItalie

            TabLg(11) = LgLuxembourg
            TabSigle(11) = SigleLuxembourg

            TabLg(12) = LgNorv�ge
            TabSigle(12) = SigleNorv�ge

            TabLg(13) = LgPaysBas
            TabSigle(13) = SiglePaysBas

            TabLg(14) = LgPologne
            TabSigle(14) = SiglePologne

            TabLg(15) = LgPortugal
            TabSigle(15) = SiglePortugal

            TabLg(16) = LgGB
            TabSigle(16) = SigleGB

            TabLg(17) = LgSu�de
            TabSigle(17) = SigleSu�de

            TabLg(18) = LgSuisse
            TabSigle(18) = SigleSuisse
        End Sub
    End Class
End Namespace
