﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetCalendrier
        Private WsParent As ObjetGlobalBase
        Private WsDossier_Calendrier As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Private WsIdeDossier As Integer = 0
        Private WsDatevaleur As String = ""

        Private WsLstCyclesTravail As List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL) = Nothing
        Private WsLstAjustements As List(Of Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING) = Nothing

        Public ReadOnly Property Ide_Dossier As Integer
            Get
                Return WsIdeDossier
            End Get
        End Property

        Public ReadOnly Property Dossier_Calendrier(ByVal DateValeur As String) As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
            Get
                If DateValeur = "" Then
                    DateValeur = WsParent.VirRhDates.DateduJour
                    WsDatevaleur = DateValeur
                    WsDossier_Calendrier = Nothing
                End If
                If DateValeur <> WsDatevaleur Then
                    If WsParent.VirRhDates.DateTypee(WsDatevaleur).AddMonths(18) < WsParent.VirRhDates.DateTypee(DateValeur) Then
                        WsDossier_Calendrier = Nothing
                    End If
                End If
                If WsDossier_Calendrier IsNot Nothing Then
                    Return WsDossier_Calendrier
                End If
                WsDatevaleur = DateValeur
                Dim IndiceI As Integer
                Dim LstIde As List(Of Integer)
                Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim TabRes As List(Of String)
                Dim DateDebut As String = WsParent.VirRhDates.CalcDateMoinsJour(WsDatevaleur, "0", "800")
                Dim DateFin As String = WsParent.VirRhDates.CalcDateMoinsJour(WsDatevaleur, "800", "0")
                Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier
                Dim DateW As String
                Dim LstAjuste As List(Of Virtualia.Net.Controles.ItemJourCalendrier)

                WsDossier_Calendrier = New List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                LstIde = New List(Of Integer)
                LstIde.Add(WsIdeDossier)
                LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaAbsence)
                If LstRes IsNot Nothing Then
                    For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE In LstRes
                        DateW = FichePER.Date_de_Valeur
                        Do
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Nature, "", FichePER.VTypeJour(DateW), VI.ObjetPer.ObaAbsence)
                            WsDossier_Calendrier.Add(FicheCal)
                            DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                            If CDate(DateW) > FichePER.Date_Fin_ToDate Then
                                Exit Do
                            End If
                        Loop
                    Next
                End If

                LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaFormation)
                If LstRes IsNot Nothing Then
                    For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_SESSION In LstRes
                        DateW = FichePER.Date_de_Valeur
                        Do
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Intitule, FichePER.Intitule_de_la_session, VI.NumeroPlage.Jour_Formation, VI.ObjetPer.ObaFormation)
                            WsDossier_Calendrier.Add(FicheCal)
                            DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                            If CDate(DateW) > FichePER.Date_Fin_ToDate Then
                                Exit Do
                            End If
                        Loop
                    Next
                End If

                TabRes = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsParent.VirNomUtilisateur,
                                                        VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSaisieIntranet, RequeteSaisiesIntranet)
                If TabRes IsNot Nothing Then
                    Dim FicheINTRA As Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
                    For IndiceI = 0 To TabRes.Count - 1
                        FicheINTRA = New Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
                        FicheINTRA.ContenuTable = TabRes.Item(IndiceI).ToString
                        FichePER = FicheINTRA.PointeurAbsence
                        If FichePER IsNot Nothing Then
                            DateW = FichePER.Date_de_Valeur
                            Do
                                FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Nature, "", FichePER.VTypeJour(DateW), VI.ObjetPer.ObaSaisieIntranet)
                                WsDossier_Calendrier.Add(FicheCal)
                                DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                                If CDate(DateW) > FichePER.Date_Fin_ToDate Then
                                    Exit Do
                                End If
                            Loop
                        End If
                    Next IndiceI
                End If

                LstAjuste = Dossier_TempsPartiel_Ajuste(DateDebut, DateFin)
                If LstAjuste IsNot Nothing AndAlso LstAjuste.Count > 0 Then
                    WsDossier_Calendrier.AddRange(LstAjuste)
                End If
                Return (From ItemCal In WsDossier_Calendrier Order By ItemCal.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Private ReadOnly Property Dossier_TempsPartiel_Ajuste(ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
            Get
                If DateDebut = "" Then
                    Return Nothing
                End If
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                If WsLstCyclesTravail Is Nothing Then
                    LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaPresence, WsIdeDossier, True)
                    WsLstCyclesTravail = New List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)
                    If LstFiches IsNot Nothing Then
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstCyclesTravail.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL))
                        Next
                    End If
                End If
                If WsLstAjustements Is Nothing Then
                    LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaModifPlanning, WsIdeDossier, True)
                    WsLstAjustements = New List(Of Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING)
                    If LstFiches IsNot Nothing Then
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstAjustements.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING))
                        Next
                    End If
                End If
                If WsLstCyclesTravail.Count = 0 Then
                    Return Nothing
                End If
                Dim LstRes As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                Dim FicheTravail As Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL
                Dim FicheAjustement As Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING
                Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier
                Dim DateW As String = DateDebut
                Dim SiAfaire As Boolean

                LstRes = New List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                Do
                    FicheCal = Nothing
                    SiAfaire = False
                    For Each FicheTravail In WsLstCyclesTravail
                        Select Case WsParent.VirRhDates.ComparerDates(FicheTravail.Date_de_Valeur, DateW)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                If FicheTravail.Date_de_Fin = "" Then
                                    SiAfaire = True
                                Else
                                    Select Case WsParent.VirRhDates.ComparerDates(FicheTravail.Date_de_Fin, DateW)
                                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                            SiAfaire = True
                                    End Select
                                End If
                                If SiAfaire = True Then
                                    FicheCal = CyclePrevisionnel(DateW, FicheTravail.CycledeTravail, FicheTravail.Date_de_Valeur)
                                End If
                                Exit For
                        End Select
                    Next
                    If FicheCal IsNot Nothing Then
                        FicheCal.ObjetOrigine = VI.ObjetPer.ObaPresence
                        LstRes.Add(FicheCal)
                    End If
                    If WsLstAjustements.Count > 0 Then
                        For Each FicheAjustement In WsLstAjustements
                            Select Case WsParent.VirRhDates.ComparerDates(FicheAjustement.Date_de_Valeur, DateW)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    Select Case WsParent.VirRhDates.ComparerDates(FicheAjustement.Date_de_Fin, DateW)
                                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                            FicheCal = CyclePrevisionnel(DateW, FicheAjustement.CycledeTravail, FicheAjustement.Date_de_Valeur)
                                    End Select
                                    Exit For
                            End Select
                        Next
                        If FicheCal IsNot Nothing Then
                            FicheCal.ObjetOrigine = VI.ObjetPer.ObaModifPlanning
                            LstRes.Add(FicheCal)
                        End If
                    End If
                    DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                    If CDate(DateW) > CDate(DateFin) Then
                        Exit Do
                    End If
                Loop
                If LstRes.Count = 0 Then
                    LstRes = Nothing
                End If
                Return LstRes
            End Get
        End Property

        Private Function CyclePrevisionnel(ByVal ArgumentDate As String, ByVal NomCycleTravail As String, ByVal DateValeurCycle As String) As Virtualia.Net.Controles.ItemJourCalendrier
            Dim CycleTravail As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
            Dim CycleBase As Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
            Dim UniteCycle As Virtualia.TablesObjet.ShemaREF.CYC_UNITE
            Dim QuantDebutCycle As Integer
            Dim QuantJour As Integer
            Dim IndiceA As Integer
            Dim NbTotalRotation As Integer
            Dim IRotation As Integer
            Dim IRotationRelative As Integer
            Dim IndexCycleBase As Integer
            Dim NumObjetUnite As Integer
            Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
            Dim IndicateurJournee As Boolean

            CycleTravail = WsParent.VirInstanceReferentiel.PointeurCyclesTravail.Fiche_Cycle(NomCycleTravail)
            If CycleTravail Is Nothing Then
                Return Nothing
            End If
            NbTotalRotation = WsParent.VirInstanceReferentiel.PointeurCyclesTravail.DureeRotation(CycleTravail, 8)
            QuantDebutCycle = WsParent.VirRhDates.CalcQuantieme(DateValeurCycle)
            QuantJour = WsParent.VirRhDates.CalcQuantieme(ArgumentDate)
            IRotation = 1
            For IndiceA = 1 To (QuantJour - QuantDebutCycle)
                IRotation += 1
                If IRotation > NbTotalRotation Then
                    IRotation = 1
                End If
            Next IndiceA
            IndexCycleBase = WsParent.VirInstanceReferentiel.PointeurCyclesTravail.IndexCycledebase(CycleTravail, IRotation)
            CycleBase = WsParent.VirInstanceReferentiel.PointeurCyclesTravail.CycledeBase(CycleTravail, IndexCycleBase)
            If CycleBase Is Nothing Then
                Return Nothing
            End If
            IRotationRelative = WsParent.VirInstanceReferentiel.PointeurCyclesTravail.DureeRotation(CycleTravail, IndexCycleBase - 1)
            NumObjetUnite = (IRotation - IRotationRelative) + 1
            UniteCycle = CycleBase.Fiche_Cycle(NumObjetUnite)
            If UniteCycle Is Nothing Then
                Return Nothing
            End If
            IndicateurJournee = False
            If UniteCycle.Denomination_Plage1 <> UniteCycle.Denomination_Plage2 Then
                If UniteCycle.Denomination_Plage1 = "Journée" Or UniteCycle.Denomination_Plage2 = "Journée" Then
                    IndicateurJournee = True
                End If
            End If
            If WsParent.VirInstanceReferentiel.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage1) IsNot Nothing Then
                If WsParent.VirInstanceReferentiel.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage2) IsNot Nothing Then
                    FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage1, "CYCLE", VI.NumeroPlage.Jour_Absence, VI.ObjetPer.ObaPresence)
                Else
                    Select Case IndicateurJournee
                        Case True
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, "", "CYCLE", VI.NumeroPlage.Plage1_Absence, VI.ObjetPer.ObaPresence)
                        Case False
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage1, "CYCLE", VI.NumeroPlage.Plage1_Absence, VI.ObjetPer.ObaPresence)
                    End Select
                End If
            ElseIf WsParent.VirInstanceReferentiel.PointeurAbsences.Fiche_Absence(UniteCycle.Denomination_Plage2) IsNot Nothing Then
                Select Case IndicateurJournee
                    Case True
                        FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, "", "CYCLE", VI.NumeroPlage.Plage2_Absence, VI.ObjetPer.ObaPresence)
                    Case False
                        FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage2, "CYCLE", VI.NumeroPlage.Plage2_Absence, VI.ObjetPer.ObaPresence)
                End Select
            End If
            Return FicheCal
        End Function

        Private Function RequeteSaisiesIntranet() As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "01/01/2001", WsParent.VirRhDates.DateduJour, VI.Operateurs.ET) = 2
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = True
            Constructeur.IdentifiantDossier = WsIdeDossier
            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSaisieIntranet) = 15 'Objet
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(VI.ObjetPer.ObaAbsence)
            Constructeur.NoInfoSelection(1, VI.ObjetPer.ObaSaisieIntranet) = 10 'Statut
            Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(VI.DemandeIntranet.EnAttente)
            For IndiceI = 0 To 15
                Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaSaisieIntranet, 0) = IndiceI + 1
            Next IndiceI
            ChaineSql = Constructeur.OrdreSqlDynamique
            Return ChaineSql
        End Function

        Public Sub New(ByVal Host As ObjetGlobalBase, ByVal Ide As Integer)
            WsParent = Host
            WsIdeDossier = Ide
        End Sub
    End Class
End Namespace