﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_RESIDENCE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIndicePlancher As Integer = 0
        Private WsTaux_Zone1 As Double = 0
        Private WsTaux_Zone2 As Double = 0
        Private WsTaux_Zone3 As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_RESIDENCE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property IndicePlancher() As Integer
            Get
                Return WsIndicePlancher
            End Get
            Set(ByVal value As Integer)
                WsIndicePlancher = value
            End Set
        End Property

        Public Property Taux_Zone1() As Double
            Get
                Return WsTaux_Zone1
            End Get
            Set(ByVal value As Double)
                WsTaux_Zone1 = value
            End Set
        End Property

        Public Property Taux_Zone2() As Double
            Get
                Return WsTaux_Zone2
            End Get
            Set(ByVal value As Double)
                WsTaux_Zone2 = value
            End Set
        End Property

        Public Property Taux_Zone3() As Double
            Get
                Return WsTaux_Zone3
            End Get
            Set(ByVal value As Double)
                WsTaux_Zone3 = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(IndicePlancher.ToString & VI.Tild)
                Chaine.Append(Taux_Zone1.ToString & VI.Tild)
                Chaine.Append(Taux_Zone2.ToString & VI.Tild)
                Chaine.Append(Taux_Zone3.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then IndicePlancher = CInt(TableauData(2))
                If TableauData(3) <> "" Then Taux_Zone1 = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Taux_Zone2 = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Taux_Zone3 = VirRhFonction.ConversionDouble(TableauData(5))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
