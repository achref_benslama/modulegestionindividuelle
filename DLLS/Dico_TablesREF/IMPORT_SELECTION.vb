﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class IMPORT_SELECTION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroLigne As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsTableTranscodification As Integer
        Private WsLgInfo As Integer
        Private WsFormatInfo As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "IMPORT_SELECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptImport
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_Ligne() As Integer
            Get
                Return WsNumeroLigne
            End Get
            Set(ByVal value As Integer)
                WsNumeroLigne = value
            End Set
        End Property

        Public Property Numero_ObjetImporte() As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Numero_InfoImporte() As Integer
            Get
                Return WsNumInfo
            End Get
            Set(ByVal value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property TabledeTranscodification() As Integer
            Get
                Return WsTableTranscodification
            End Get
            Set(ByVal value As Integer)
                WsTableTranscodification = value
            End Set
        End Property

        Public Property LongueurDonnee() As Integer
            Get
                Return WsLgInfo
            End Get
            Set(ByVal value As Integer)
                WsLgInfo = value
            End Set
        End Property

        Public Property FormatDonnee() As Integer
            Get
                Return WsFormatInfo
            End Get
            Set(ByVal value As Integer)
                WsFormatInfo = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_Ligne) & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetImporte) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoImporte) & VI.Tild)
                Chaine.Append(CStr(TabledeTranscodification) & VI.Tild)
                Chaine.Append(CStr(LongueurDonnee) & VI.Tild)
                Chaine.Append(CStr(FormatDonnee))

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                For IndiceI = 0 To 3
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI

                Ide_Dossier = CInt(TableauData(0))
                Numero_Ligne = CInt(TableauData(1))
                Numero_ObjetImporte = CInt(TableauData(2))
                Numero_InfoImporte = CInt(TableauData(3))
                For IndiceI = 4 To 6
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI
                TabledeTranscodification = CInt(TableauData(4))
                LongueurDonnee = CInt(TableauData(5))
                FormatDonnee = CInt(TableauData(6))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
