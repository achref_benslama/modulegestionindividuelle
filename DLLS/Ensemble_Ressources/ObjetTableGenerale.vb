﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetTableGenerale
        Implements IGestionVirFicheModif(Of TAB_DESCRIPTION)

        Private WsListeTables As List(Of TAB_DESCRIPTION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesTables() As List(Of TAB_DESCRIPTION)
            Get
                Dim LstRes As List(Of TAB_DESCRIPTION)
                LstRes = (From instance In WsListeTables Select instance Where instance.Ide_Dossier > 0 Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeTables() As Integer Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).NombredItems
            Get
                Return WsListeTables.Count
            End Get
        End Property

        Public Function Fiche_Table(ByVal Ide_Dossier As Integer) As TAB_DESCRIPTION Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).GetFiche
            Return WsListeTables.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Table(ByVal Intitule As String) As TAB_DESCRIPTION Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).GetFiche
            Return WsListeTables.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).SupprimerItem
            Dim FicheTable As TAB_DESCRIPTION
            FicheTable = Fiche_Table(Ide)
            If FicheTable IsNot Nothing Then
                WsListeTables.Remove(FicheTable)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of TAB_DESCRIPTION).Actualiser
            WsListeTables.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeTables.Clear()
            Dim FicheTable As TAB_DESCRIPTION

            '** 1  Liste de toutes les tables
            WsListeTables.AddRange(CreationCollectionVIRFICHE(Of TAB_DESCRIPTION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If WsListeTables.Count <= 0 Then
                Exit Sub
            End If

            '** 2  Liste de toutes les valeurs
            Dim lstval As List(Of TAB_LISTE) = CreationCollectionVIRFICHE(Of TAB_LISTE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstval.ForEach(Sub(fiche)
                               FicheTable = Fiche_Table(fiche.Ide_Dossier)
                               If FicheTable Is Nothing Then
                                   Exit Sub
                               End If
                               FicheTable.Ajouter_Valeur(fiche)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeTables = New List(Of TAB_DESCRIPTION)
            ConstituerListe(Ide)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            Me.New(NomUtilisateur, 0)
        End Sub

    End Class
End Namespace
