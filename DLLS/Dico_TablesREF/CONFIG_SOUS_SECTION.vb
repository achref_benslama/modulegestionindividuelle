﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CONFIG_SOUS_SECTION
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsDescriptif As String
        '
        Private WsListe_Clef As List(Of CONFIG_CLEF) = Nothing

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CONFIG_SOUS_SECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueConfiguration
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Descriptif = TableauData(2)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property ListedesClefs() As List(Of CONFIG_CLEF)
            Get
                If WsListe_Clef Is Nothing Then
                    Return Nothing
                End If
                Return WsListe_Clef.OrderBy(Function(m) m.Intitule).ToList
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Clef(ByVal Fiche As CONFIG_CLEF) As Integer
            If WsListe_Clef Is Nothing Then
                WsListe_Clef = New List(Of CONFIG_CLEF)
            End If
            WsListe_Clef.Add(Fiche)
            Return WsListe_Clef.Count
        End Function

        Public Function ClefDirect(ByVal Intitule As String) As CONFIG_CLEF

            If (WsListe_Clef Is Nothing) Then
                Return Nothing
            End If

            Return (From c As CONFIG_CLEF In WsListe_Clef Where c.Intitule = Intitule Select c).FirstOrDefault()
        End Function

    End Class
End Namespace
