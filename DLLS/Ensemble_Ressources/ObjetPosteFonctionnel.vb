﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPosteFonctionnel
        Implements IGestionVirFicheModif(Of PST_IDENTIFICATION)

        Private WsListePostes As List(Of PST_IDENTIFICATION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesPostes_Alpha(ByVal Lettre As String, ByVal Niveau1 As String) As List(Of PST_IDENTIFICATION)
            Get
                If WsListePostes Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVuePosteFct)
                Dim LstRes As List(Of PST_IDENTIFICATION)
                Dim LstTri As List(Of PST_IDENTIFICATION) = Nothing

                LstRes = WsListePostes.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                If Niveau1 = "" Then
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Ide_Dossier > 0 _
                           Order By instance.Intitule Ascending).ToList
                Else
                    LstTri = (From instance In LstRes Select instance _
                           Where instance.Affectation_Organigramme("", 1) = Niveau1 _
                           Order By instance.Affectation_Organigramme("", 2) Ascending, _
                           instance.Intitule Ascending).ToList
                End If
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesPostes(ByVal DateValeur As String, ByVal Niveau1 As String) As List(Of PST_IDENTIFICATION)
            Get
                If WsListePostes Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of PST_IDENTIFICATION) = Nothing

                Select Case Niveau1
                    Case Is = ""
                        LstRes = (From instance In WsListePostes Select instance _
                              Where instance.SiEmploiEnGestion(DateValeur) = True _
                              Order By instance.Affectation_Organigramme(DateValeur, 1) Ascending, _
                                     instance.Affectation_Organigramme(DateValeur, 2) Ascending, _
                                     instance.Affectation_Organigramme(DateValeur, 3) Ascending, _
                                     instance.Affectation_Organigramme(DateValeur, 4) Ascending, _
                                     instance.Affectation_Organigramme(DateValeur, 5) Ascending, _
                                     instance.Affectation_Organigramme(DateValeur, 6) Ascending, _
                                     instance.Intitule Ascending).ToList

                    Case Else
                        LstRes = (From instance In WsListePostes Select instance _
                              Where instance.Affectation_Organigramme(DateValeur, 1) = Niveau1 _
                              Order By instance.Affectation_Organigramme(DateValeur, 2) Ascending, _
                                       instance.Affectation_Organigramme(DateValeur, 3) Ascending, _
                                       instance.Affectation_Organigramme(DateValeur, 4) Ascending, _
                                       instance.Affectation_Organigramme(DateValeur, 5) Ascending, _
                                       instance.Affectation_Organigramme(DateValeur, 6) Ascending, _
                                       instance.Intitule Ascending).ToList

                End Select
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesPostes(ByVal CritereSel As String, ByVal Valeur As String, ByVal CritereTri As String) As List(Of PST_IDENTIFICATION)
            Get
                If WsListePostes Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of PST_IDENTIFICATION) = Nothing

                If CritereSel = "" Then
                    Select Case CritereTri
                        Case Is = ""
                            LstRes = (From instance In WsListePostes Select instance _
                                              Order By instance.Affectation_Organigramme("", 1) Ascending, _
                                                       instance.Affectation_Organigramme("", 2) Ascending, _
                                                       instance.Intitule Ascending).ToList

                        Case Is = "Numéro"
                            LstRes = (From instance In WsListePostes Select instance _
                                               Order By instance.Numero_du_poste Ascending, _
                                               instance.Intitule Ascending).ToList

                        Case "Intitulé"
                            Return WsListePostes.OrderBy(Function(m) m.Intitule).ToList
                    End Select
                End If

                Select Case CritereSel
                    Case Is = "Niveau1"
                        Select Case CritereTri
                            Case Is = "" 'Intitulé
                                LstRes = (From instance In WsListePostes Select instance _
                                                Where instance.Affectation_Organigramme("", 1) = Valeur _
                                                Order By instance.Affectation_Organigramme("", 2) Ascending, _
                                                       instance.Affectation_Organigramme("", 3) Ascending, _
                                                       instance.Intitule Ascending).ToList

                            Case Else
                                Select Case CritereTri
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListePostes Select instance _
                                                Where instance.Affectation_Organigramme("", 1) = Valeur _
                                                Order By instance.Intitule Ascending).ToList

                                End Select
                        End Select
                    Case Is = "Numéro"
                        Select Case CritereTri
                            Case Is = "" 'Intitulé
                                LstRes = (From instance In WsListePostes Select instance _
                                                Where instance.Numero_du_poste = Valeur _
                                                Order By instance.Intitule Ascending).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListePostes Select instance _
                                                Where instance.Numero_du_poste = Valeur _
                                                Order By instance.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Intitulé"
                        Select Case CritereTri
                            Case Is = ""
                                LstRes = (From instance In WsListePostes Select instance _
                                                           Where instance.Intitule = Valeur _
                                                           Order By instance.NumeroObjet Ascending).ToList
                        End Select
                End Select

                If LstRes Is Nothing Then
                    Return WsListePostes
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredePostes() As Integer Implements IGestionVirFicheModif(Of PST_IDENTIFICATION).NombredItems
            Get
                Return WsListePostes.Count
            End Get
        End Property

        Public Function Fiche_Poste(ByVal Ide_Dossier As Integer) As PST_IDENTIFICATION Implements IGestionVirFiche(Of PST_IDENTIFICATION).GetFiche
            Return WsListePostes.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Poste(ByVal Numero As String) As PST_IDENTIFICATION Implements IGestionVirFiche(Of PST_IDENTIFICATION).GetFiche
            Return WsListePostes.Find(Function(m) m.Numero_du_poste = Numero)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of PST_IDENTIFICATION).SupprimerItem
            Dim FichePoste As PST_IDENTIFICATION
            FichePoste = Fiche_Poste(Ide)
            If FichePoste IsNot Nothing Then
                WsListePostes.Remove(FichePoste)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of PST_IDENTIFICATION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of PST_IDENTIFICATION).Actualiser
            WsListePostes.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListePostes.Clear()
            Dim FichePoste As PST_IDENTIFICATION

            '** 1  Liste de tous les Postes
            WsListePostes.AddRange(CreationCollectionVIRFICHE(Of PST_IDENTIFICATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListePostes.Count <= 0) Then
                Return
            End If

            '** 3  Liste de toutes les affectations
            Dim lstaff As List(Of PST_ORGANIGRAMME) = CreationCollectionVIRFICHE(Of PST_ORGANIGRAMME).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstaff.ForEach(Sub(f)
                               FichePoste = Fiche_Poste(f.Ide_Dossier)

                               If (FichePoste Is Nothing) Then
                                   Return
                               End If

                               FichePoste.Ajouter_Affectation(f)
                           End Sub)

            '** 4  Liste de toutes les affectations budgétaires
            Dim lstbud As List(Of PST_BUDGETAIRE) = CreationCollectionVIRFICHE(Of PST_BUDGETAIRE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstbud.ForEach(Sub(f)
                               FichePoste = Fiche_Poste(f.Ide_Dossier)

                               If (FichePoste Is Nothing) Then
                                   Return
                               End If

                               FichePoste.Ajouter_Budget(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListePostes = New List(Of PST_IDENTIFICATION)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace

