﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TAB_LISTE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsValeur As String
        Private WsReference As String
        Private WsInfoAssociee As String
        Private WsCouleurAssociee As Integer
        Private WsDateOuverture As String
        Private WsDateFermeture As String
        Private WsFiltre As String

        Private WsLstDico As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_LISTE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsValeur = value
                    Case Else
                        WsValeur = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property InformationAssociee() As String
            Get
                Return WsInfoAssociee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsInfoAssociee = value
                    Case Else
                        WsInfoAssociee = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property CouleurAssociee() As Integer
            Get
                Return WsCouleurAssociee
            End Get
            Set(ByVal value As Integer)
                WsCouleurAssociee = value
            End Set
        End Property

        Public Property Date_Ouverture() As String
            Get
                Return WsDateOuverture
            End Get
            Set(ByVal value As String)
                WsDateOuverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fermeture() As String
            Get
                Return WsDateFermeture
            End Get
            Set(ByVal value As String)
                WsDateFermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property FiltreEtablisement() As String
            Get
                Return WsFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiltre = value
                    Case Else
                        WsFiltre = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Valeur & VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(InformationAssociee & VI.Tild)
                Chaine.Append(CouleurAssociee.ToString & VI.Tild)
                Chaine.Append(Date_Ouverture & VI.Tild)
                Chaine.Append(Date_Fermeture & VI.Tild)
                Chaine.Append(FiltreEtablisement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Valeur = TableauData(1)
                Reference = TableauData(2)
                InformationAssociee = TableauData(3)
                If TableauData(4) = "" Then
                    TableauData(4) = "0"
                End If
                CouleurAssociee = TableauData(4)
                Date_Ouverture = TableauData(5)
                Date_Fermeture = TableauData(6)
                FiltreEtablisement = TableauData(7)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListeDico As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)
            Get
                If WsLstDico Is Nothing Then
                    Call FaireDictionnaire()
                End If
                Return WsLstDico
            End Get
        End Property

        Public ReadOnly Property V_DonneeGrid As String
            Get
                If WsLstDico Is Nothing Then
                    Call FaireDictionnaire()
                End If
                Dim Chaine As New StringBuilder
                Chaine.Append(Valeur & VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Date_Ouverture & VI.Tild)
                Chaine.Append(Date_Fermeture & VI.Tild)
                Chaine.Append(FiltreEtablisement & VI.Tild)
                Chaine.Append(Valeur & VI.Tild)
                Return Chaine.ToString
            End Get
        End Property

        Private Sub FaireDictionnaire()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            WsLstDico = New List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Intitulé"
            InfoBase.Longueur = 120
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = "TelleQue"
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Code ou référence"
            InfoBase.Longueur = 30
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = "TelleQue"
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date d'ouverture"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de fermeture"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Filtre Etablissement"
            InfoBase.Longueur = 80
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.PointdeVue = VI.PointdeVue.PVueEtablissement
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
