﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VLine

    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "ZoneElement"
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private Sub LireLaFiche()
        'Dim NoLigne As Integer
        'Dim NoInfo As Integer
        'Dim Ctl As Control
        'Dim IndiceI As Integer = 0
        'Dim SiLectureOnly As Boolean = False

        'Dim VirControle As System.Web.UI.WebControls.TextBox
        'Do
        '    'Ctl = V_WebFonction.VirWebControle(Me., "Nom_", IndiceI)
        '    If Ctl Is Nothing Then
        '        Exit Do
        '    End If
        '    NoLigne = CInt(Strings.Right(Ctl.ID, 1))
        '    VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
        '    VirControle.Text = ValeurLue(1, NoLigne - 1)
        '    VirControle.ReadOnly = SiLectureOnly
        '    VirControle.AutoPostBack = False
        '    IndiceI += 1
        'Loop
        'IndiceI = 0
        'Do
        '    'Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Prenom_", IndiceI)
        '    If Ctl Is Nothing Then
        '        Exit Do
        '    End If
        '    NoLigne = CInt(Strings.Right(Ctl.ID, 1))
        '    VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
        '    VirControle.Text = ValeurLue(2, NoLigne - 1)
        '    VirControle.ReadOnly = SiLectureOnly
        '    VirControle.AutoPostBack = False
        '    IndiceI += 1
        'Loop
        'IndiceI = 0
        'Do
        '    'Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Lieu_", IndiceI)
        '    If Ctl Is Nothing Then
        '        Exit Do
        '    End If
        '    NoLigne = CInt(Strings.Right(Ctl.ID, 1))
        '    VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
        '    VirControle.Text = ValeurLue(11, NoLigne - 1)
        '    VirControle.ReadOnly = SiLectureOnly
        '    VirControle.AutoPostBack = False
        '    IndiceI += 1
        'Loop

        'Dim VirDonneeDate As VCoupleEtiDate
        'IndiceI = 0
        'Do
        '    'Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Date_", IndiceI)
        '    If Ctl Is Nothing Then
        '        Exit Do
        '    End If
        '    NoLigne = CInt(Strings.Right(Ctl.ID, 1))
        '    VirDonneeDate = CType(Ctl, VCoupleEtiDate)
        '    VirDonneeDate.DonText = ValeurLue(0, NoLigne - 1)
        '    VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
        '    VirDonneeDate.V_SiAutoPostBack = False
        '    IndiceI += 1
        'Loop

        'Dim VirRadioH As Controles_VTrioHorizontalRadio
        'IndiceI = 0
        'Do
        '    'Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Radio_", IndiceI)
        '    If Ctl Is Nothing Then
        '        Exit Do
        '    End If
        '    NoInfo = CInt(Strings.Right(Ctl.ID, 2))
        '    NoLigne = CInt(Strings.Mid(Ctl.ID, 8, 1))
        '    VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
        '    VirRadioH.V_SiEnLectureSeule = SiLectureOnly
        '    VirRadioH.V_SiAutoPostBack = True
        '    Select Case NoInfo
        '        Case 3
        '            Select Case ValeurLue(NoInfo, NoLigne - 1)
        '                Case Is = "Masculin"
        '                    VirRadioH.RadioGaucheCheck = True
        '                Case Is = "Féminin"
        '                    VirRadioH.RadioCentreCheck = True
        '                Case Else
        '                    VirRadioH.RadioGaucheCheck = False
        '                    VirRadioH.RadioCentreCheck = False

        '            End Select
        '            IndiceI += 1
        '        Case 4
        '            Select Case ValeurLue(NoInfo, NoLigne - 1)
        '                Case Is = "Oui"
        '                    VirRadioH.RadioGaucheCheck = True
        '                Case Is = "Non"
        '                    VirRadioH.RadioCentreCheck = True
        '                Case Else
        '                    VirRadioH.RadioGaucheCheck = False
        '                    VirRadioH.RadioCentreCheck = False
        '            End Select
        '            IndiceI += 1
        '    End Select

        'Loop
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer, ByVal NoRang As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return ""
            End If
            If NoRang > LstFiches.Count - 1 Then
                Return ""
            End If
            Return LstFiches.Item(NoRang).V_TableauData(NoInfo).ToString
        End Get
    End Property

    Private Sub VLine_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call LireLaFiche()
    End Sub

    Public Sub New()
        Me.Visible = True
    End Sub

End Class