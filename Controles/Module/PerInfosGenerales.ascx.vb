﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerInfosGenerales
    Inherits Virtualia.Net.Controles.ObjetWebControle
    'Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsNomTable As String = "PER_ETATCIVIL"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCivil
    Private WsBoutonclique As Boolean = False

    'Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
    '    Get
    '        If WebFct Is Nothing Then
    '            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    '        End If
    '        Return WebFct
    '    End Get
    'End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerInfosGenerales_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If (Not Page.ClientScript.IsStartupScriptRegistered("AjaxFileUpload_change_text")) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "AjaxFileTraduction", "AjaxFileUpload_change_text();", True)
        End If
        'V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call LireLaFiche()
            Call ControlDonnee()
        End If


        'If WsDossierPer IsNot Nothing Then
        '    CadreListeEnfants.Identifiant = WsDossierPer.getIdNouveau
        'End If
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumObjet As Integer
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))

            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            If VirControle.V_SiDonneeDico = True Then
                VirControle.DonText = ValeurLue(NumObjet, NumInfo)
            Else
                VirControle.DonText = ""
            End If
            VirControle.V_SiEnLectureSeule = SiLectureOnly
            'VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop


        Dim ControleTel As VNumTelephone
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "InfoT", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))

            ControleTel = CType(Ctl, VNumTelephone)
            NumObjet = ControleTel.V_Objet
            If ControleTel.V_SiDonneeDico = True Then
                ControleTel.DonText = ValeurLue(NumObjet, NumInfo)
            Else
                ControleTel.DonText = ""
            End If
            ControleTel.V_SiEnLectureSeule = SiLectureOnly
            'VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumObjet = VirDonneeTable.V_Objet
            VirDonneeTable.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeTable.V_SiEnLectureSeule = SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            NumObjet = VirCoche.V_Objet
            If ValeurLue(NumObjet, NumInfo) = "Oui" Then
                VirCoche.V_Check = True
            Else
                VirCoche.V_Check = False
            End If
            VirCoche.V_SiEnLectureSeule = SiLectureOnly
            VirCoche.V_SiAutoPostBack = Not SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            NumObjet = VirDonneeDate.V_Objet
            VirDonneeDate.DonText = ValeurLue(NumObjet, NumInfo)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            NumObjet = VirVertical.V_Objet
            VirVertical.DonText = ValeurLue(NumObjet, NumInfo)
            VirVertical.V_SiEnLectureSeule = SiLectureOnly
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            NumObjet = VirRadioH.V_Objet
            VirRadioH.V_SiEnLectureSeule = False 'WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadioH.V_SiAutoPostBack = True 'Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumObjet
                Case 1
                    Select Case ValeurLue(NumObjet, NumInfo)
                        Case Is = "Monsieur"
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = "Madame"
                            VirRadioH.RadioCentreCheck = True
                        Case Else
                            VirRadioH.RadioGaucheCheck = False
                            VirRadioH.RadioCentreCheck = False
                    End Select
            End Select
            IndiceI += 1
        Loop

        Dim VirRadioSix As Controles_VSixBoutonRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "RadioX", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioSix = CType(Ctl, Controles_VSixBoutonRadio)
            NumObjet = VirRadioSix.V_Objet
            VirRadioSix.V_SiEnLectureSeule = False 'WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadioSix.V_SiAutoPostBack = True 'Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case NumObjet
                Case 1
                    Select Case ValeurLue(NumObjet, NumInfo)
                        Case VirRadioSix.VRadioN1Text
                            VirRadioSix.VRadioN1Check = True
                        Case VirRadioSix.VRadioN2Text
                            VirRadioSix.VRadioN2Check = True
                        Case VirRadioSix.VRadioN3Text
                            VirRadioSix.VRadioN3Check = True
                        Case VirRadioSix.VRadioN4Text
                            VirRadioSix.VRadioN4Check = True
                        Case VirRadioSix.VRadioN5Text
                            VirRadioSix.VRadioN5Check = True
                        Case Else
                    End Select
            End Select
            IndiceI += 1
        Loop
    End Sub

    Private Sub InfoH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoHA02.ValeurChange, InfoHA03.ValeurChange,
            InfoHA05.ValeurChange, InfoHA11.ValeurChange, InfoHA12.ValeurChange, InfoHA13.ValeurChange, InfoHA14.ValeurChange,
            InfoHB01.ValeurChange, InfoHB02.ValeurChange, InfoHB04.ValeurChange, InfoHD01.ValeurChange,
            InfoHD02.ValeurChange, InfoHD03.ValeurChange, InfoHD04.ValeurChange, InfoHD05.ValeurChange, InfoHD10.ValeurChange, InfoHE05.ValeurChange, InfoHE07.ValeurChange, InfoHE08.ValeurChange,
            InfoHP01.ValeurChange, InfoHP02.ValeurChange, InfoHP03.ValeurChange, InfoHP04.ValeurChange, InfoHP05.ValeurChange, InfoHP10.ValeurChange, InfoHB05.ValeurChange, InfoHD09.ValeurChange, InfoHF18.ValeurChange,
            InfoHF19.ValeurChange, InfoHF20.ValeurChange, InfoHF21.ValeurChange, InfoHF22.ValeurChange, InfoHP09.ValeurChange,
            InfoHP04.ValeurChange, InfoHP05.ValeurChange, InfoH23.ValeurChange


        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleEtiDonnee).V_Objet
        V_Objet(0) = NumObjet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
            Call V_ValeurChange(NumInfo, e.Valeur, CType(sender, Controles_VCoupleEtiDonnee).DonTabIndex)
        End If


        If (NumInfo = 7 And NumObjet = 5) Or (NumInfo = 8 And NumObjet = 5) Then
            If NumInfo = 7 Then
                If InfoHE07.DonText <> "" And InfoHE08.DonText = "" Then
                    InfoHE08.SetValidator("required")
                End If
            ElseIf NumInfo = 8 Then
                If InfoHE08.DonText <> "" And InfoHE07.DonText = "" Then
                    InfoHE07.SetValidator("required")

                End If
            End If
        End If

    End Sub


    Private Sub InfoT_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoTD11.ValeurChange, InfoTD06.ValeurChange, InfoT06.ValeurChange


        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VNumTelephone).ID, 2))
        Dim NumObjet As Integer = CType(sender, VNumTelephone).V_Objet
        V_Objet(0) = NumObjet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, VNumTelephone).DonBackColor = V_WebFonction.CouleurMaj
            If (NumInfo = 11) Then
                WsDossierPer.TableauMaj(NumObjet, NumInfo) = InfoTD11.CodePays + e.Valeur
            End If
            If (NumInfo = 6) And (NumObjet = 4) Then
                WsDossierPer.TableauMaj(NumObjet, NumInfo) = InfoTD06.CodePays + e.Valeur
            End If
            If (NumInfo = 6) And (NumObjet = 30) Then
                WsDossierPer.TableauMaj(NumObjet, NumInfo) = InfoT06.CodePays + e.Valeur
            End If
            Call V_ValeurChange(NumInfo, e.Valeur, CType(sender, VNumTelephone).DonTabIndex)
        End If

        'If NumInfo = 1 And NumObjet = 4 Then
        '    If InfoHD04.DonText = "" Then
        'InfoHD04.SetValidator = True
        '    End If
        '    If InfoHD05.DonText = "" Then
        '        InfoHD05.SetValidator = True

        '    End If
        'End If
    End Sub

    'Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As DonneeChangeEventArgs) Handles InfoHA02.ValeurChange, InfoHA03.ValeurChange,
    '        InfoHA05.ValeurChange, InfoHA11.ValeurChange, InfoHA12.ValeurChange, InfoHA13.ValeurChange, InfoHA14.ValeurChange,
    '        InfoHB01.ValeurChange, InfoHB02.ValeurChange, InfoHB04.ValeurChange, InfoHC05.ValeurChange, InfoHD01.ValeurChange,
    '        InfoHD02.ValeurChange, InfoHD03.ValeurChange, InfoHD04.ValeurChange, InfoHD05.ValeurChange, InfoHD06.ValeurChange,
    '        InfoHD10.ValeurChange, InfoHD11.ValeurChange, InfoHE05.ValeurChange, InfoHE07.ValeurChange, InfoHE08.ValeurChange,
    '        InfoHP01.ValeurChange, InfoHP02.ValeurChange, InfoHP03.ValeurChange, InfoHP04.ValeurChange, InfoHP05.ValeurChange,
    '        InfoHP06.ValeurChange, InfoHP10.ValeurChange

    '    Dim CacheDonnee As List(Of String) = V_CacheMaj
    '    Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
    '    If CacheDonnee IsNot Nothing Then
    '        If CacheDonnee.Item(NumInfo) Is Nothing Then
    '            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
    '            CadreCmdOK.Visible = True
    '        Else
    '            If CacheDonnee.Item(NumInfo) <> e.Valeur Then
    '                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
    '                CadreCmdOK.Visible = True
    '            End If
    '        End If
    '        Call V_ValeurChange(NumInfo, e.Valeur, CType(sender, Controles_VCoupleEtiDonnee).DonTabIndex)
    '    End If
    'End Sub

    Private Sub RadioH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles RadioHA01.ValeurChange
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VTrioHorizontalRadio).V_Objet

        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            '** Contrôle de cohérence
            If e.Valeur = "Monsieur" Then
                WsDossierPer.TableauMaj(NumObjet, 8) = "Masculin"
            Else
                WsDossierPer.TableauMaj(NumObjet, 8) = "Féminin"
            End If
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub RadioX_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles RadioXA09.ValeurChange
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VSixBoutonRadio).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VSixBoutonRadio).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub Coche_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles CocheB07.ValeurChange
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCocheSimple).V_Objet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
        End If
    End Sub

    Private Sub Dontab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles DontabA06.AppelTable, DontabB03.AppelTable, DontabA12.AppelTable
        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                If e.ControleAppelant = "DontabA06" Then
                    RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
                End If
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
            Case VI.ObjetPer.ObaGrade
                V_WebFonction.PointeurContexte.TsTampon_StringList = Nothing
                RefVirtualia.V_Appelant(e.ObjetAppelant, e.DatedeValeur) = e.ControleAppelant
            Case Else
                RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = ""
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer Is Nothing Then
                Exit Property
            End If
            If WsDossierPer.EnCours_Creation = True Then
                Exit Property
            End If

            Dim Ctl As Control
            Dim NumInfo As Integer
            Dim NumObjet As Integer
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Ctl = V_WebFonction.VirWebControle(Me.CadreEtatCivil, "Dontab" & Strings.Right(IDAppelant, 3), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = CInt(Strings.Right(VirDonneeTable.ID, 2))
            NumObjet = VirDonneeTable.V_Objet
            VirDonneeTable.DonText = value
            If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> value Then
                VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                WsDossierPer.TableauMaj(NumObjet, NumInfo) = value
            End If
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Return WsDossierPer.DonneeLue(NoObjet, NoInfo)
        End Get
    End Property


    ''**** AKR 2
    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Private Function SiErreurSpecifique() As List(Of String)
        Dim Ide_Dossier As Integer = V_Identifiant
        Dim TabErreurs As New List(Of String)
        Dim CacheData As List(Of String)
        Dim CacheDoublon As List(Of Integer)

        CacheData = V_CacheMaj

        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        If Ide_Dossier > 0 Then
            Dossier = V_WebFonction.PointeurDossier(Ide_Dossier)
        Else
            Dossier = Nothing
        End If
        If CacheData.Item(2) = "" Or CacheData.Item(3) = "" Or CacheData.Item(4) = "" Then
            TabErreurs.Add("Le Nom, le prénom et la date de naissance sont obligatoires.")
            Return TabErreurs
        End If
        '** Mise en Forme Nom et Prénom
        CacheData.Item(2) = V_WebFonction.ViRhFonction.Lettre1Capi(CacheData.Item(2), 2)
        'If V_WebFonction.PointeurConfiguration IsNot Nothing Then
        '    If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Nom_En_Majuscule = True Then
        '        CacheData.Item(2) = CacheData.Item(2).ToUpper
        '    End If
        'End If
        CacheData.Item(3) = V_WebFonction.ViRhFonction.Lettre1Capi(CacheData.Item(3), 1)
        '** Contrôle Unicité Nom, Prénom, Date de naissance
        CacheDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier,
                                                              2, CacheData.Item(2), CacheData.Item(3), CacheData.Item(4))
        If CacheDoublon IsNot Nothing Then
            TabErreurs.Add("Il existe déjà un dossier ayant la même identification sous le N° " & CacheDoublon(0).ToString)
            Return TabErreurs
        End If
        '** Contrôle Unicité N° NIR
        If CacheData.Item(11) <> "" Then
            CacheDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier, 11, CacheData.Item(11))
            If CacheDoublon IsNot Nothing Then
                TabErreurs.Add("Il existe déjà un dossier ayant le même NIR sous le N° " & CacheDoublon(0).ToString)
                Return TabErreurs
            End If
        End If
        '** Contrôle de cohérence
        If CacheData.Item(1) = "Monsieur" Then
            CacheData.Item(8) = "Masculin"
        ElseIf CacheData.Item(1) <> "" Then
            CacheData.Item(8) = "Féminin"
        End If
        If CacheData.Item(8) = "Masculin" Then
            CacheData.Item(1) = "Monsieur"
        ElseIf CacheData.Item(8) = "Féminin" Then
            Select Case CacheData.Item(1)
                Case Is = "Madame", "Mademoiselle"
                    Exit Select
                Case Else
                    CacheData.Item(1) = "Madame"
            End Select
        End If
        If V_IndexFiche <> -1 Then
            If Ide_Dossier > 0 Then
                WsFiche = CType(Dossier.Item(V_IndexFiche), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
                If WsFiche Is Nothing Then
                    Return Nothing
                End If
                If WsFiche.SiOK_DateNaissance(CacheData.Item(4)) = False Then
                    TabErreurs.Add("Date de naissance erronée.")
                End If
                If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                    If CacheData.Item(11) <> "" And CacheData.Item(12) <> "" Then
                        If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Controle_NIR = True Then
                            If WsFiche.SiOK_NIR(CacheData.Item(11), CacheData.Item(12)) = False Then
                                TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                            End If
                        End If
                    End If
                Else
                    If WsFiche.SiOK_NIR(CacheData.Item(11), CacheData.Item(12)) = False Then
                        TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                    End If
                End If
            End If
            If TabErreurs.Count > 0 Then
                Return TabErreurs
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Call InitialiserControles()
                V_Identifiant = value
                'Spécifique Fiche Etat-Civil
                If V_Fiche IsNot Nothing Then
                    WsFiche = CType(V_Fiche, Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
                    If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                        If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Nom_En_Majuscule = True Then
                            WsFiche.SiNom_En_Majuscule = True
                        End If
                    Else
                        WsFiche.SiNom_En_Majuscule = True
                    End If
                End If
            End If
        End Set
    End Property

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoDA04.ValeurChange, InfoD17.ValeurChange, InfoD10.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        Dim NumObjet As Integer = CType(sender, VCoupleEtiDate).V_Objet
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If


        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(0) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_SiEnLectureSeule = True
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()
        InfoHA02.V_SiEnLectureSeule = True
        InfoHA03.V_SiEnLectureSeule = True
        InfoDA04.V_SiEnLectureSeule = True
        If V_Identifiant = 0 Then
                Call V_CommandeNewDossier(VI.PointdeVue.PVueApplicatif)
            End If


        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")


    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim TableauErreur As List(Of String) = SiErreurSpecifique()
        If TableauErreur Is Nothing Then
            Call V_MajFiche()
            CadreCmdOK.Visible = False

            Dim urlRelative As String
            urlRelative = Request.RawUrl

            If InStr(urlRelative, "NouveauDossier") > 0 Then
                Dim CacheData As List(Of String) = V_CacheMaj
                Dim TabOK As New List(Of String)
                TabOK.Add("Le dossier de " & CacheData.Item(2) & Strings.Space(1) & CacheData.Item(3))
                TabOK.Add(" a été créé.")
                Dim TitreMsg As String = "Nouveau dossier"
                Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
            End If
        Else
            Dim CacheData As List(Of String) = V_CacheMaj
            Dim TitreMsg As String = "Contrôle préalable à l'enregistrement du dossier de " & CacheData.Item(2) & Strings.Space(1) & CacheData.Item(3)
            Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, TitreMsg, TableauErreur))
        End If
    End Sub

    Private Sub InitialiserControles()
        'RadioH01.Visible = False
        'RadioH08.Visible = False

        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CadreExpertStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
            Next IndiceI
        End Set
    End Property

    Protected Sub CommandeEnfant_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim row As TableRow = New TableRow()
        Dim row1 As TableRow = New TableRow()

        Dim cell As TableCell = New TableCell()
        Dim cell1 As TableCell = New TableCell()
        Dim cell2 As TableCell = New TableCell()
        Dim cell3 As TableCell = New TableCell()
        Dim cell4 As TableCell = New TableCell()
        Dim vperenfant As Virtualia.Net.PerEnfants = New Virtualia.Net.PerEnfants

        Dim ligne As Table = New Table
        cell1.Controls.Add(New TextBox())
        cell1.Width = Unit.Pixel(60)
        cell2.Controls.Add(New TextBox())
        cell2.CssClass = "Std_Donnee"
        cell2.Width = Unit.Pixel(120)

        cell3.Controls.Add(New TextBox())
        cell3.CssClass = "Std_Donnee"
        cell3.Width = Unit.Pixel(120)

        cell4.Controls.Add(New TextBox())
        cell4.CssClass = "Std_Donnee"
        cell4.Width = Unit.Pixel(120)


        row1.Cells.Add(cell1)
        row1.Cells.Add(cell2)
        row1.Cells.Add(cell3)
        row1.Cells.Add(cell4)


        ligne.Rows.Add(row1)

        cell.Controls.Add(ligne)
        row.Cells.Add(cell)
        'CadrePerEnfant.Rows.Add(row)
        'SiBoutonClique = False




    End Sub


    Private Sub ControlDonnee()
        InfoHA13.ControlDonnee("char")
        InfoHA14.ControlDonnee("char")
        InfoHA05.ControlDonnee("char")
        InfoD17.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        InfoD17.Attributes.Add("placeholder", "JJ/MM/AAAA")
        InfoHB01.ControlDonnee("char")
        InfoHB02.ControlDonnee("char")
        InfoHB05.ControlDonnee("char")
        InfoD10.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        InfoD10.Attributes.Add("placeholder", "JJ/MM/AAAA")
        InfoHB04.ControlDonnee("char")
        InfoHD05.ControlDonnee("char")
        InfoHE08.ControlDonnee("char")
        InfoHE05.ControlDonnee("char")
        InfoHD10.SetValidator("email")
        InfoHP01.ControlDonnee("char")
        InfoHP10.SetValidator("email")
        InfoHP09.ControlDonnee("entier")
        InfoHP05.ControlDonnee("char")

        InfoTD06.ControlDonnee("entier")
        InfoTD11.ControlDonnee("entier")
        InfoT06.ControlDonnee("entier")
        InfoHD09.ControlDonnee("entier")
        InfoHA11.ControlDonnee("entier")
        InfoHA12.ControlDonnee("entier")
    End Sub

    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property
    Public Property IdDossier() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            Dim Chaine As String = ""
            HSelIde.Value = value.ToString
            If V_Contexte IsNot Nothing Then
                V_ListeDossiers.Identifiant(False) = value
                V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(value)
                V_Contexte.Identifiant_Courant = value
                Chaine = V_Contexte.DossierPER.FicheEtatCivil.Qualite
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Nom
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Prenom
                Chaine &= " (Ide V : " & value & ")"
            End If
        End Set
    End Property

End Class