﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheSysRef
        Private WsSysRefPvueInverse As Integer
        Private WsSysRefNomTable As String = ""
        Private WsSysRefDuoPvueInverse(1) As Integer
        Private WsSysRefDuoNomTable(1) As String
        Private WsSysRefIDAppelant As String = "" 'ID du contrôle appelant le SysRef
        Private WsSysRefObjetAppelant As Integer 'Objet origine appelant le SysRef
        Private WsSysRefLettre As String = ""

        Private WsSiTriOrganise As Boolean = False
        Private WsValeurSelection As String = ""
        Private WsDateSelection As String = ""
        Private WsCheminVirtuel As String = ""

        Private WsRechercheCourante As String = ""
        Private WsIndexCourant As Integer = -1

        Public Property PointdeVueInverse() As Integer
            Get
                Return WsSysRefPvueInverse
            End Get
            Set(ByVal value As Integer)
                WsSysRefPvueInverse = value
            End Set
        End Property

        Public Property NomTable() As String
            Get
                Return WsSysRefNomTable
            End Get
            Set(ByVal value As String)
                WsSysRefNomTable = value
            End Set
        End Property

        Public Property Duo_PointdeVueInverse(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoPvueInverse(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Integer)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoPvueInverse(Index) = value
                End Select
            End Set
        End Property

        Public Property Duo_NomTable(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0, 1
                        Return WsSysRefDuoNomTable(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0, 1
                        WsSysRefDuoNomTable(Index) = value
                End Select
            End Set
        End Property

        Public Property IDAppelant() As String
            Get
                Return WsSysRefIDAppelant
            End Get
            Set(ByVal value As String)
                WsSysRefIDAppelant = value
            End Set
        End Property

        Public Property ObjetAppelant() As Integer
            Get
                Return WsSysRefObjetAppelant
            End Get
            Set(ByVal value As Integer)
                WsSysRefObjetAppelant = value
            End Set
        End Property

        Public Property LettreSelectionnee() As String
            Get
                Return WsSysRefLettre
            End Get
            Set(ByVal value As String)
                WsSysRefLettre = value
            End Set
        End Property

        Public Property SiTriOrganise As Boolean
            Get
                Return WsSiTriOrganise
            End Get
            Set(value As Boolean)
                WsSiTriOrganise = value
            End Set
        End Property

        Public Property ValeurSelection As String
            Get
                Return WsValeurSelection
            End Get
            Set(value As String)
                WsValeurSelection = value
            End Set
        End Property

        Public Property Date_Selection As String
            Get
                Return WsDateSelection
            End Get
            Set(value As String)
                WsDateSelection = value
            End Set
        End Property

        Public Property CheminVirtuel As String
            Get
                Return WsCheminVirtuel
            End Get
            Set(value As String)
                WsCheminVirtuel = value
            End Set
        End Property

        Public Property RechercheCourante As String
            Get
                Return WsRechercheCourante
            End Get
            Set(value As String)
                WsRechercheCourante = value
            End Set
        End Property

        Public Property IndexCourant As Integer
            Get
                Return WsIndexCourant
            End Get
            Set(value As Integer)
                WsIndexCourant = value
            End Set
        End Property
    End Class
End Namespace