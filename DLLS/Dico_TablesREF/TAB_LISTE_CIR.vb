﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TAB_LISTE_CIR
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsValeur As String
        Private WsReference As String
        Private WsIndicateurPension As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_LISTE_CIR"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 230
                        WsValeur = value
                    Case Else
                        WsValeur = Strings.Left(value, 230)
                End Select
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property IndicateurPension() As String
            Get
                Return WsIndicateurPension
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsIndicateurPension = value
                    Case Else
                        WsIndicateurPension = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Valeur & VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(IndicateurPension)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Valeur = TableauData(1)
                Reference = TableauData(2)
                IndicateurPension = TableauData(3)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

