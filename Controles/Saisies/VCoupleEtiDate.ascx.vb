﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class VCoupleEtiDate
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsSiDateFin As Boolean = False

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    'Public Property TypeCalendrier() As String
    '    Get
    '        Return CalendrierMensuel.TypeCalendrier
    '    End Get
    '    Set(ByVal value As String)
    '        CalendrierMensuel.TypeCalendrier = value
    '    End Set
    'End Property

    'Public Property SiEnableWEJF As Boolean
    '    Get
    '        Return CalendrierMensuel.SiEnableWEJF
    '    End Get
    '    Set(value As Boolean)
    '        CalendrierMensuel.SiEnableWEJF = value
    '    End Set
    'End Property

    Public Property SiDateFin As Boolean
        Get
            Return WsSiDateFin
        End Get
        Set(value As Boolean)
            WsSiDateFin = value
        End Set
    End Property


    Public Property Si_DateDebutFin(datedebut As VCoupleEtiDate, datefin As VCoupleEtiDate) As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
            If datedebut.DonText = "" Then
                datefin.V_SiEnable = value
            Else
                datefin.DateDebut = datedebut.DonText
                datefin.V_SiEnable = True
            End If


        End Set
    End Property


    Public WriteOnly Property DateDebut As String
        Set(value As String)
            'Date_Picker.MinDate = value
        End Set
    End Property
    Public Property SelectedDate As String
        Get
            Return Date_Picker.SelectedDate
        End Get
        Set(value As String)
            Date_Picker.SelectedDate = value
        End Set
    End Property

    'Public WriteOnly Property SiDureeVisible As Boolean
    '    Set(value As Boolean)
    '        CalendrierMensuel.CocheButoir = value
    '    End Set
    'End Property

    'Public Property Identifiant As Integer
    '    Get
    '        Return CalendrierMensuel.Identifiant
    '    End Get
    '    Set(value As Integer)
    '        CalendrierMensuel.Identifiant = value
    '    End Set
    'End Property

    Private Sub VCoupleEtiDate_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True And Etiquette.Text = "" Then
            Etiquette.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
        End If
        If V_SiDonneeDico = True And V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).VNature = VI.NatureDonnee.DonneeDateTime Then
            If Date_Picker.DateInput.Text <> "" Then
                Dim Chaine As String = Strings.Left(Date_Picker.DateInput.Text, 10)
                Date_Picker.DateInput.Text = Chaine
            End If
        End If
        If V_SiDonneeDico = True Then
            'Donnee.BorderColor = V_DonBordercolor
            'Etiquette.BackColor = V_EtiBackcolor
            'Etiquette.ForeColor = V_EtiForecolor
            'Etiquette.BorderColor = V_EtiBordercolor

            'Donnee.Font.Name = V_FontName
            'Donnee.Font.Size = V_FontTaille
            'Etiquette.Font.Name = V_FontName
            'Etiquette.Font.Size = V_FontTaille
            'Etiquette.Font.Bold = V_FontBold
            'Etiquette.Font.Italic = V_FontItalic
        End If
        'If V_SiEnLectureSeule = True Then
        '    CellCommande.Visible = False
        '    Exit Sub
        'End If
        'If HPopupCal.Value = "1" Then
        '    CellCalPopup.Visible = True
        '    PopupCal.Show()
        'Else
        '    CellCalPopup.Visible = False
        'End If
    End Sub

    Private Sub Donnee_TextChanged(sender As Object, e As EventArgs) Handles Date_Picker.SelectedDateChanged
        Dim Chaine As String = ""
        Dim date_converti As String
        If Date_Picker.DateInput.Text <> "" Then
            If Date_Picker.DateInput.Text.Length > 10 Then
                date_converti = Date_Picker.DateInput.Text.Substring(8, 2) + "/" + Date_Picker.DateInput.Text.Substring(5, 2) + "/" + Date_Picker.DateInput.Text.Substring(0, 4)
                Chaine = V_WebFonction.ViRhDates.DateSaisieVerifiee(date_converti)
            Else
                Chaine = V_WebFonction.ViRhDates.DateSaisieVerifiee(Date_Picker.DateInput.Text)
            End If
        End If
        Date_Picker.SelectedDate = Chaine
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Chaine)
        Saisie_Change(Evenement)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return Date_Picker.DateInput.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                Date_Picker.DateInput.AutoPostBack = False
                Exit Property
            End If
            Date_Picker.DateInput.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return Date_Picker.DateInput.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Date_Picker.DateInput.ReadOnly = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property V_SiEnable() As Boolean
        Get
            Return CadreDonnee.Enabled
        End Get
        Set(ByVal value As Boolean)
            CadreDonnee.Enabled = value
            If value = False Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Date_Picker.DateInput.Style.Remove(Strings.Trim(TableauW(0)))
                Date_Picker.DateInput.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return Date_Picker.DateInput.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                If value.Length > 10 Then
                    Dim date_converti = value.Substring(8, 2) + "/" + value.Substring(5, 2) + "/" + value.Substring(0, 4)
                    Date_Picker.SelectedDate = date_converti
                Else
                    Date_Picker.SelectedDate = value
                End If

            End If
        End Set
    End Property
    Public Property DonTabIndex() As Short
        Get
            Return Date_Picker.DateInput.TabIndex
        End Get
        Set(ByVal value As Short)
            Date_Picker.DateInput.TabIndex = value
            MyBase.V_TabIndex = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property
    Public Property Skin() As String
        Get
            Date_Picker.EnableEmbeddedSkins = True
            Return Date_Picker.Skin
        End Get
        Set(ByVal value As String)
            Date_Picker.Skin = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property
    Public Property inputWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Date_Picker.DateInput.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Date_Picker.DateInput.Width = value
        End Set
    End Property
    Public Property DonWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Date_Picker.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Date_Picker.Width = value
        End Set
    End Property
    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Date_Picker.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Date_Picker.Height = value
        End Set
    End Property
    Public Property inputHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Date_Picker.DateInput.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Date_Picker.DateInput.Height = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property
    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return Date_Picker.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Date_Picker.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property
    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return Date_Picker.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Date_Picker.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property
    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return Date_Picker.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Date_Picker.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property
    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Date_Picker.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Date_Picker.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property
    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Date_Picker.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Date_Picker.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property
    Public Property DonTooltip() As String
        Get
            Return Date_Picker.ToolTip
        End Get
        Set(ByVal value As String)
            Date_Picker.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return CellEtiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            CellEtiquette.Visible = value
        End Set
    End Property
    Public Property DonVisible() As Boolean
        Get
            Return Date_Picker.Visible
        End Get
        Set(ByVal value As Boolean)
            Date_Picker.Visible = value
        End Set
    End Property

    'Private Sub CmdCalendrier_Click(sender As Object, e As ImageClickEventArgs) Handles CmdCalendrier.Click
    '    If V_SiEnLectureSeule = True Then
    '        Exit Sub
    '    End If
    '    CalendrierMensuel.CalTitre = Etiquette.Text
    '    CalendrierMensuel.DateSelectionnee = Donnee.Text
    '    HPopupCal.Value = "1"
    '    CellCalPopup.Visible = True
    '    PopupCal.Show()
    'End Sub

    'Private Sub CalendrierMensuel_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles CalendrierMensuel.ValeurChange
    '    HPopupCal.Value = "0"
    '    CellCalPopup.Visible = False
    '    If e.Valeur = "" Then
    '        Exit Sub
    '    End If
    '    Donnee.Text = e.Valeur

    '    Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
    '    Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(e.Valeur)
    '    Saisie_Change(Evenement)
    'End Sub


    Public Sub ControlDonnee(ByVal param As String)
        If param = "date" Then
            Date_Picker.DateInput.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
            Date_Picker.DateInput.Attributes.Add("placeholder", "DD/MM/AAAA")
            Date_Picker.DateInput.Attributes.Add("oncopy", "return false;")
            Date_Picker.DateInput.Attributes.Add("onpaste", "return false;")
        End If
    End Sub



End Class