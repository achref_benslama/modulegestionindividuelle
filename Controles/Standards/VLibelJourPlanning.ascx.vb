﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VLibelJourPlanning
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Public Event PaginationChange As Valeur_ChangeEventHandler
    '
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '
    Protected Overridable Sub Valeur_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Protected Overridable Sub Pagination_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent PaginationChange(Me, e)
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public WriteOnly Property SiEtiSemaineVisible() As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Dim IndiceI As Integer = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me, "NumSemaine", IndiceI)
                If Ctl Is Nothing Then
                    Exit Property
                End If
                VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
                VirControle.Visible = value
                IndiceI += 1
            Loop
        End Set
    End Property

    Public WriteOnly Property SiLstLibelMoisVisible() As Boolean
        Set(ByVal value As Boolean)
            LstLibelMois.Visible = value
        End Set
    End Property

    Public WriteOnly Property SiLstNbMoisVisible() As Boolean
        Set(ByVal value As Boolean)
            LstNbMois.Visible = value
        End Set
    End Property

    Public Property SelectionLibelMois() As String
        Get
            Return HSelMois.Value
        End Get
        Set(ByVal value As String)
            HSelMois.Value = value
            LstLibelMois.LstText = value
        End Set
    End Property

    Public Property SelectionNbMois() As String
        Get
            Return HSelNombre.Value
        End Get
        Set(ByVal value As String)
            HSelNombre.Value = value
            LstNbMois.LstText = value
        End Set
    End Property

    Public WriteOnly Property SiPaginationVisible() As Boolean
        Set(ByVal value As Boolean)
            VPagination.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiPagination() As String
        Set(ByVal value As String)
            PageEtiText.Text = value
        End Set
    End Property

    Public WriteOnly Property LibelSelection() As String
        Set(ByVal value As String)
            EtiSelection.Text = value
        End Set
    End Property

    Public WriteOnly Property VNumSemaine(ByVal NoSemaine As Integer) As Integer
        Set(ByVal value As Integer)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = V_WebFonction.VirWebControle(Me, "NumSemaine", NoSemaine)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            If value = 0 Then
                VirControle.Text = ""
            Else
                VirControle.Text = value.ToString
            End If
        End Set
    End Property

    Private Sub FaireListeMois()
        Dim Libel As List(Of String)
        Dim LstValeurs As List(Of String)
        Dim Tableaudata(0) As String
        Dim Annee As Integer
        Dim IndiceI As Integer
        Dim IndiceA As Integer

        Libel = New List(Of String)
        Libel.Add("Aucun mois")
        Libel.Add("Un mois")
        Libel.Add("mois")

        Annee = CInt(Strings.Right(V_WebFonction.ViRhDates.DateduJour, 4))

        LstValeurs = New List(Of String)
        IndiceA = Annee + 1
        Do
            For IndiceI = 12 To 1 Step -1
                LstValeurs.Add(V_WebFonction.ViRhDates.MoisEnClair(IndiceI) & Strings.Space(1) & IndiceA.ToString)
            Next IndiceI
            IndiceA -= 1
            If IndiceA < Annee - 3 Then
                Exit Do
            End If
        Loop
        LstLibelMois.V_Libel = Libel
        LstLibelMois.V_Liste = LstValeurs

        LstValeurs = New List(Of String)
        For IndiceI = 1 To 18
            LstValeurs.Add(IndiceI.ToString & " mois")
        Next IndiceI

        LstNbMois.V_Libel = Libel
        LstNbMois.V_Liste = LstValeurs

        If HSelMois.Value = "" Then
            If VPagination.Visible = False Then
                HSelMois.Value = V_WebFonction.ViRhDates.MoisEnClair(1) & Strings.Space(1) & Annee.ToString
            Else
                HSelMois.Value = V_WebFonction.ViRhDates.MoisEnClair(Month(Now)) & Strings.Space(1) & Annee.ToString
                LibelSelection = V_WebFonction.ViRhDates.MoisEnClair(Month(Now)) & Strings.Space(1) & Annee.ToString
            End If
        End If
        LstLibelMois.LstText = HSelMois.Value
        If HSelNombre.Value = "" Then
            HSelNombre.Value = "12 mois"
        End If
        LstNbMois.LstText = HSelNombre.Value

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If LstLibelMois.VCount = 0 Then
            Call FaireListeMois()
        End If
    End Sub

    Protected Sub LstLibelMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstLibelMois.ValeurChange
        HSelMois.Value = e.Valeur
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Mois", e.Valeur)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub LstNbMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstNbMois.ValeurChange
        HSelNombre.Value = e.Valeur
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Nombre", e.Valeur)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub PagePrecedente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PagePrecedente.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Precedente", "")
        Pagination_Change(Evenement)
    End Sub

    Protected Sub PageSuivante_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PageSuivante.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Suivante", "")
        Pagination_Change(Evenement)
    End Sub

End Class
