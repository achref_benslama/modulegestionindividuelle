﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ONIC_MESURE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumero As Integer = 0
        Private WsActivite As String
        Private WsMesure As String
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ONIC_MESURE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueActivite
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Rang_Numero() As Integer
            Get
                Return WsNumero
            End Get
            Set(ByVal value As Integer)
                WsNumero = value
            End Set
        End Property

        Public Property Activite() As String
            Get
                Return WsActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsActivite = value
                    Case Else
                        WsActivite = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Mesure() As String
            Get
                Return WsMesure
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsMesure = value
                    Case Else
                        WsMesure = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Property Date_d_effet() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang_Numero.ToString & VI.Tild)
                Chaine.Append(Activite & VI.Tild)
                Chaine.Append(Mesure & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If IsNumeric(TableauData(1)) Then
                    Rang_Numero = CInt(TableauData(1))
                End If
                Activite = TableauData(2)
                Mesure = TableauData(3)
                Descriptif = TableauData(4)
                Date_d_effet = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
