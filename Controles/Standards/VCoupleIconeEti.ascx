﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCoupleIconeEti.ascx.vb" ClassName="VCoupleIconeEti" EnableViewState="true" Inherits="Virtualia.Net.Controles_VCoupleIconeEti" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<asp:Table ID="VIcone" runat="server" CellPadding="1" CellSpacing="0" Width="310px" Height="150px"  style="margin-top:2px" BackColor="Window">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Width ="310px" BorderStyle ="None">
            <telerik:RadButton ID="CmdChoix" Skin="Silk" EnableEmbeddedSkins="true" Width="96px" Height="96px" BackColor="Transparent" BorderStyle="None" style="margin-top: 10px"  RenderMode="Lightweight" runat="server" IsBackgroundImage="true">
            </telerik:RadButton>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Bottom" Width ="310px" BorderStyle ="None">
            <telerik:RadLabel ID="Etiquette" runat="server" Height="28px" Width="300px" BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None" ForeColor="White" Font-Italic="True" Skin="Silk" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-left: 10px;margin-top: 10px;text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
