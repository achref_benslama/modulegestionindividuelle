﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FOR_DESCRIPTIF
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsDescriptif As String
        Private WsContenu As String
        Private WsObservations As String
        Private WsFinancement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Contenu() As String
            Get
                Return WsContenu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsContenu = value
                    Case Else
                        WsContenu = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Financement() As String
            Get
                Return WsFinancement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsFinancement = value
                    Case Else
                        WsFinancement = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Contenu & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Financement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Descriptif = TableauData(1)
                Contenu = TableauData(2)
                Observations = TableauData(3)
                Financement = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


