Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ADRESSEPRO
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAdresse_des_bureaux As String
        Private WsTelephone_professionnel As String
        Private WsNumero_de_bureau As String
        Private WsFax As String
        Private WsResidence_administrative As String
        Private WsParticularite As String
        Private WsEmail As String
        Private WsPortable As String
        Private WsSitegeo As String
        Private WsComplementadresse As String
        Private WsLoginldap As String
        Private WsCodepostal As String
        Private WsVille As String
        Private WsPays As String
        Private WsInitiales As String
        Private WsCourrier As String
        Private WsService_associe As String
        Private WsDnldap As String
        Private WsAttributions As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ADRESSEPRO"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAdrPro
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 20
            End Get
        End Property

        Public Property Adresse_des_bureaux() As String
            Get
                Return WsAdresse_des_bureaux
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsAdresse_des_bureaux = value
                    Case Else
                        WsAdresse_des_bureaux = Strings.Left(value, 150)
                End Select
                MyBase.Clef = WsAdresse_des_bureaux
            End Set
        End Property

        Public Property Telephone_professionnel() As String
            Get
                Return WsTelephone_professionnel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone_professionnel = value
                    Case Else
                        WsTelephone_professionnel = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Numero_de_bureau() As String
            Get
                Return WsNumero_de_bureau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsNumero_de_bureau = value
                    Case Else
                        WsNumero_de_bureau = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Residence_administrative() As String
            Get
                Return WsResidence_administrative
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsResidence_administrative = value
                    Case Else
                        WsResidence_administrative = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Particularite() As String
            Get
                Return WsParticularite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsParticularite = value
                    Case Else
                        WsParticularite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Email() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Portable() As String
            Get
                Return WsPortable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsPortable = value
                    Case Else
                        WsPortable = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Sitegeo() As String
            Get
                Return WsSitegeo
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsSitegeo = value
                    Case Else
                        WsSitegeo = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Complementadresse() As String
            Get
                Return WsComplementadresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsComplementadresse = value
                    Case Else
                        WsComplementadresse = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Loginldap() As String
            Get
                Return WsLoginldap
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLoginldap = value
                    Case Else
                        WsLoginldap = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Codepostal() As String
            Get
                Return WsCodepostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodepostal = value
                    Case Else
                        WsCodepostal = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Pays() As String
            Get
                Return WsPays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPays = value
                    Case Else
                        WsPays = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Initiales() As String
            Get
                Return WsInitiales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsInitiales = value
                    Case Else
                        WsInitiales = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Courrier() As String
            Get
                Return WsCourrier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCourrier = value
                    Case Else
                        WsCourrier = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Service_associe() As String
            Get
                Return WsService_associe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsService_associe = value
                    Case Else
                        WsService_associe = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Dnldap() As String
            Get
                Return WsDnldap
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsDnldap = value
                    Case Else
                        WsDnldap = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Attributions() As String
            Get
                Return WsAttributions
            End Get
            Set(ByVal value As String)
                WsAttributions = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Adresse_des_bureaux & VI.Tild)
                Chaine.Append(Telephone_professionnel & VI.Tild)
                Chaine.Append(Numero_de_bureau & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Residence_administrative & VI.Tild)
                Chaine.Append(Particularite & VI.Tild)
                Chaine.Append(Email & VI.Tild)
                Chaine.Append(Portable & VI.Tild)
                Chaine.Append(Sitegeo & VI.Tild)
                Chaine.Append(Complementadresse & VI.Tild)
                Chaine.Append(Loginldap & VI.Tild)
                Chaine.Append(Codepostal & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Pays & VI.Tild)
                Chaine.Append(Initiales & VI.Tild)
                Chaine.Append(Courrier & VI.Tild)
                Chaine.Append(Service_associe & VI.Tild)
                Chaine.Append(Dnldap & VI.Tild)
                Chaine.Append(Attributions & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Adresse_des_bureaux = TableauData(1)
                Telephone_professionnel = TableauData(2)
                Numero_de_bureau = TableauData(3)
                Fax = TableauData(4)
                Residence_administrative = TableauData(5)
                Particularite = TableauData(6)
                Email = TableauData(7)
                Portable = TableauData(8)
                Sitegeo = TableauData(9)
                Complementadresse = TableauData(10)
                Loginldap = TableauData(11)
                Codepostal = TableauData(12)
                Ville = TableauData(13)
                Pays = TableauData(14)
                Initiales = TableauData(15)
                Courrier = TableauData(16)
                Service_associe = TableauData(17)
                Dnldap = TableauData(18)
                Attributions = TableauData(19)
                MyBase.Certification = TableauData(20)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<AdresseBureau>" & VirRhFonction.ChaineXMLValide(Adresse_des_bureaux) & "</AdresseBureau>" & vbCrLf)
                Chaine.Append("<ComplementAdresseBureau>" & VirRhFonction.ChaineXMLValide(Complementadresse) & "</ComplementAdresseBureau>" & vbCrLf)
                Chaine.Append("<CodePostalBureau>" & Codepostal & "</CodePostalBureau>" & vbCrLf)
                Chaine.Append("<VilleBureau>" & VirRhFonction.ChaineXMLValide(Ville) & "</VilleBureau>" & vbCrLf)
                Chaine.Append("<PaysBureau>" & VirRhFonction.ChaineXMLValide(Pays) & "</PaysBureau>" & vbCrLf)
                Chaine.Append("<NumeroBureau>" & VirRhFonction.ChaineXMLValide(Numero_de_bureau) & "</NumeroBureau>" & vbCrLf)
                Chaine.Append("<ResidenceAdministrative>" & VirRhFonction.ChaineXMLValide(Residence_administrative) & "</ResidenceAdministrative>" & vbCrLf)
                Chaine.Append("<Site>" & VirRhFonction.ChaineXMLValide(Sitegeo) & "</Site>" & vbCrLf)
                Chaine.Append("<NumeroTelephonePro>" & Telephone_professionnel & "</NumeroTelephonePro>" & vbCrLf)
                Chaine.Append("<NumeroPortablePro>" & Portable & "</NumeroPortablePro>" & vbCrLf)
                Chaine.Append("<Particularite>" & Portable & "</Particularite>" & vbCrLf)
                Chaine.Append("<FaxBureau>" & Fax & "</FaxBureau>" & vbCrLf)
                Chaine.Append("<EmailPro>" & Email & "</EmailPro>" & vbCrLf)
                Chaine.Append("<Login>" & Loginldap & "</Login>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


