﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_COULEURS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer = 0
        Private WsEtiquette As String
        Private WsCouleur As Long = 0
        Private WsCouleurPolice As Long = 0
        Private WsValeurN1 As String
        Private WsValeurN2 As String
        Private WsValeurN3 As String
        Private WsValeurN4 As String
        Private WsValeurN5 As String
        Private WsValeurN6 As String
        Private WsValeurN7 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_COULEURS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 24
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Etiquette() As String
            Get
                Return WsEtiquette
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtiquette = value
                    Case Else
                        WsEtiquette = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Couleur() As Long
            Get
                Return WsCouleur
            End Get
            Set(ByVal value As Long)
                WsCouleur = value
            End Set
        End Property

        Public Property CouleurPolice() As Long
            Get
                Return WsCouleurPolice
            End Get
            Set(ByVal value As Long)
                WsCouleurPolice = value
            End Set
        End Property

        Public Property Valeur_N1() As String
            Get
                Return WsValeurN1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN1 = value
                    Case Else
                        WsValeurN1 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N2() As String
            Get
                Return WsValeurN2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN2 = value
                    Case Else
                        WsValeurN2 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N3() As String
            Get
                Return WsValeurN3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN3 = value
                    Case Else
                        WsValeurN3 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N4() As String
            Get
                Return WsValeurN4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN4 = value
                    Case Else
                        WsValeurN4 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N5() As String
            Get
                Return WsValeurN5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN5 = value
                    Case Else
                        WsValeurN5 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N6() As String
            Get
                Return WsValeurN6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN6 = value
                    Case Else
                        WsValeurN6 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur_N7() As String
            Get
                Return WsValeurN7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeurN7 = value
                    Case Else
                        WsValeurN7 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Etiquette & VI.Tild)
                Chaine.Append(Couleur & VI.Tild)
                Chaine.Append(CouleurPolice & VI.Tild)
                Chaine.Append(Valeur_N1 & VI.Tild)
                Chaine.Append(Valeur_N2 & VI.Tild)
                Chaine.Append(Valeur_N3 & VI.Tild)
                Chaine.Append(Valeur_N4 & VI.Tild)
                Chaine.Append(Valeur_N5 & VI.Tild)
                Chaine.Append(Valeur_N6 & VI.Tild)
                Chaine.Append(Valeur_N7)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Rang = CInt(TableauData(1))
                Etiquette = TableauData(2)
                If TableauData(3) <> "" Then Couleur = System.Convert.ToInt64(TableauData(3))
                If TableauData(4) <> "" Then CouleurPolice = System.Convert.ToInt64(TableauData(4))
                Valeur_N1 = TableauData(5)
                Valeur_N2 = TableauData(6)
                Valeur_N3 = TableauData(7)
                Valeur_N4 = TableauData(8)
                Valeur_N5 = TableauData(9)
                Valeur_N6 = TableauData(10)
                Valeur_N7 = TableauData(11)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
