﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class UTI_FILTRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsEtablissement As String
        Private WsObjetPER As Integer
        Private WsInfoPER As Integer
        Private WsSensFiltre As String
        Private WsTypeFiltre As String
        Private WsValeursFiltre As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "UTI_FILTRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueUtilisateur
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Numero_ObjetPER() As Integer
            Get
                Return WsObjetPER
            End Get
            Set(ByVal value As Integer)
                WsObjetPER = value
            End Set
        End Property

        Public Property Numero_InfoPER() As Integer
            Get
                Return WsInfoPER
            End Get
            Set(ByVal value As Integer)
                WsInfoPER = value
            End Set
        End Property

        Public Property SensDuFiltre() As String
            Get
                Return WsSensFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsSensFiltre = value
                    Case Else
                        WsSensFiltre = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property TypeDuFiltre() As String
            Get
                Return WsTypeFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsTypeFiltre = value
                    Case Else
                        WsTypeFiltre = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property ValeursFiltre() As String
            Get
                Return WsValeursFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsValeursFiltre = value
                    Case Else
                        WsValeursFiltre = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(Numero_ObjetPER.ToString & VI.Tild)
                Chaine.Append(Numero_InfoPER.ToString & VI.Tild)
                Chaine.Append(SensDuFiltre & VI.Tild)
                Chaine.Append(TypeDuFiltre & VI.Tild)
                Chaine.Append(ValeursFiltre)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Etablissement = TableauData(1)
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                Numero_ObjetPER = CInt(TableauData(2))
                If TableauData(3) = "" Then
                    TableauData(3) = "0"
                End If
                Numero_InfoPER = CInt(TableauData(3))
                SensDuFiltre = TableauData(4)
                TypeDuFiltre = TableauData(5)
                ValeursFiltre = TableauData(6)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
