﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetCycleTravail
        Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION)

        Private WsListeCycles As List(Of TRA_IDENTIFICATION)
        Private WsNomUtilisateur As String
        'Exécution Temps de travail Pointeur sur Cycles de base
        Private WsLstUnitesCycle As ObjetUniteCycle

        Public ReadOnly Property ListeDesCycles(ByVal Etablissement As String) As List(Of TRA_IDENTIFICATION)
            Get
                If WsListeCycles Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of TRA_IDENTIFICATION)
                Select Case Etablissement
                    Case Is = ""
                        LstRes = (From instance In WsListeCycles Select instance _
                                        Where instance.Ide_Dossier > 0 _
                                        Order By instance.Etablissement, instance.Intitule Ascending).ToList
                    Case Else
                        LstRes = (From instance In WsListeCycles Select instance _
                                        Where instance.Etablissement = Etablissement _
                                        Order By instance.Intitule Ascending).ToList
                End Select
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesCycles(ByVal Lettre As String, ByVal Etablissement As String) As List(Of TRA_IDENTIFICATION)
            Get
                If WsListeCycles Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueCycle)
                Dim LstRes As List(Of TRA_IDENTIFICATION)
                Dim LstTri As List(Of TRA_IDENTIFICATION) = Nothing

                LstRes = WsListeCycles.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case Etablissement
                    Case Is = ""
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Ide_Dossier > 0 _
                               Order By instance.Intitule Ascending).ToList

                    Case Else
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Etablissement = Etablissement _
                               Order By instance.Intitule Ascending).ToList

                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property NombredeCycles() As Integer Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).NombredItems
            Get
                Return WsListeCycles.Count
            End Get
        End Property

        '** Exécution Pointeur sur les unités de cycle
        Public Property PointeurUniteCycles() As ObjetUniteCycle
            Get
                If WsLstUnitesCycle Is Nothing Then
                    Try
                        WsLstUnitesCycle = New ObjetUniteCycle( _
                               WsNomUtilisateur, 0)
                    Catch Ex As Exception
                        WsLstUnitesCycle = Nothing
                    End Try
                End If
                Return WsLstUnitesCycle
            End Get
            Set(ByVal value As ObjetUniteCycle)
                WsLstUnitesCycle = value
            End Set
        End Property

        Public ReadOnly Property CycledeBase(ByVal FicheCycle As TRA_IDENTIFICATION, _
                                    ByVal Index As Integer) As CYC_IDENTIFICATION
            Get
                If FicheCycle Is Nothing Then
                    Return Nothing
                End If
                Dim Chaine As String = ""
                Select Case Index
                    Case Is = 1
                        Chaine = FicheCycle.Unite_de_Cycle_N1
                    Case Is = 2
                        Chaine = FicheCycle.Unite_de_Cycle_N2
                    Case Is = 3
                        Chaine = FicheCycle.Unite_de_Cycle_N3
                    Case Is = 4
                        Chaine = FicheCycle.Unite_de_Cycle_N4
                    Case Is = 5
                        Chaine = FicheCycle.Unite_de_Cycle_N5
                    Case Is = 6
                        Chaine = FicheCycle.Unite_de_Cycle_N6
                    Case Is = 7
                        Chaine = FicheCycle.Unite_de_Cycle_N7
                    Case Is = 8
                        Chaine = FicheCycle.Unite_de_Cycle_N8
                End Select
                If Chaine = "" Then
                    Return Nothing
                End If
                If PointeurUniteCycles IsNot Nothing Then
                    Return WsLstUnitesCycle.Fiche_Unite(Chaine)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property IndexCycledebase(ByVal FicheCycle As TRA_IDENTIFICATION, _
                                                  ByVal IndexRotation As Integer) As Integer
            Get
                Dim FicheBase As CYC_IDENTIFICATION
                Dim IndiceI As Integer
                Dim Total As Integer = 0
                For IndiceI = 1 To 8
                    FicheBase = CycledeBase(FicheCycle, IndiceI)
                    If FicheBase Is Nothing Then
                        Exit For
                    End If
                    Total += FicheBase.BasedelUnite
                    If Total >= IndexRotation Then
                        Return IndiceI
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property DureeRotation(ByVal FicheCycle As TRA_IDENTIFICATION, _
                                               ByVal IndexFin As Integer) As Integer
            Get
                Dim FicheBase As CYC_IDENTIFICATION
                Dim IndiceI As Integer
                Dim Total As Integer = 0
                If IndexFin = 0 Then
                    Return 0
                End If
                For IndiceI = 1 To IndexFin
                    FicheBase = CycledeBase(FicheCycle, IndiceI)
                    If FicheBase Is Nothing Then
                        Exit For
                    End If
                    Total += FicheBase.BasedelUnite
                Next IndiceI
                Return Total
            End Get
        End Property

        Public Function Fiche_Cycle(ByVal Ide_Dossier As Integer) As TRA_IDENTIFICATION Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).GetFiche
            Return WsListeCycles.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Cycle(ByVal Intitule As String) As TRA_IDENTIFICATION Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).GetFiche
            Return WsListeCycles.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).SupprimerItem
            Dim FicheCycle As TRA_IDENTIFICATION
            FicheCycle = Fiche_Cycle(Ide)
            If FicheCycle IsNot Nothing Then
                WsListeCycles.Remove(FicheCycle)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of TRA_IDENTIFICATION).Actualiser
            WsListeCycles.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeCycles.Clear()
            Dim FicheCycle As TRA_IDENTIFICATION

            '** 1  Liste de tous les cycles de travail
            WsListeCycles.AddRange(CreationCollectionVIRFICHE(Of TRA_IDENTIFICATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListeCycles.Count <= 0) Then
                Exit Sub
            End If

            '** 2  Liste de toutes les règles de valo
            Dim lstvalo As List(Of TRA_VALORISATION) = CreationCollectionVIRFICHE(Of TRA_VALORISATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstvalo.ForEach(Sub(f)
                                FicheCycle = Fiche_Cycle(f.Ide_Dossier)

                                If (FicheCycle Is Nothing) Then
                                    Return
                                End If

                                FicheCycle.Ajouter_Regle(f)
                            End Sub)

            '** 3  Liste de tous les accords RTT
            Dim lstacc As List(Of TRA_ACCORD_RTT) = CreationCollectionVIRFICHE(Of TRA_ACCORD_RTT).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstacc.ForEach(Sub(f)
                               FicheCycle = Fiche_Cycle(f.Ide_Dossier)

                               If (FicheCycle Is Nothing) Then
                                   Return
                               End If

                               FicheCycle.Ajouter_Accord(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal ListeUnites As ObjetUniteCycle)
            WsNomUtilisateur = NomUtilisateur
            WsLstUnitesCycle = ListeUnites
            WsListeCycles = New List(Of TRA_IDENTIFICATION)
            Call ConstituerListe(0)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeCycles = New List(Of TRA_IDENTIFICATION)
            Call ConstituerListe(Ide)
        End Sub

    End Class
End Namespace
