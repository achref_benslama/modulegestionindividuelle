﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCoupleEtiDate.ascx.vb" Inherits="Virtualia.Net.VCoupleEtiDate" ClassName="VCoupleEtiDate" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Saisies/VSaisieCalendrier.ascx" tagname="VCalend" tagprefix="Virtualia" %>

<link href="../../VirtualiaSkin/Textbox.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Calendar.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<style>
        .form__group {
  white-space: nowrap;
  text-overflow: ellipsis;
}

    </style>

<asp:Table ID="VSaisieDate" runat="server" CellPadding="1" CellSpacing="0" CssClass="form__group">
    <asp:TableRow>
        <asp:TableCell Height="22px">
            <asp:Table ID="VStandardDate" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell ID="CellEtiquette">
                        <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                             style="margin-bottom: 20px; margin-left: 4px; text-indent: 5px; text-align: left" >
                          </Telerik:RadLabel>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="CadreDonnee" runat="server" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px">
                            <asp:TableRow>
                                <asp:TableCell>
                                        <telerik:RadDatePicker ID="Date_Picker" runat="server" ZIndex="500000" Width="210px" MinDate="#01/01/1900 0:00:00 AM#">
                                            <DateInput ID="Donnee" runat="server" Font-Size="Medium" Font-Names="Calibri"></DateInput>
                                            <Calendar ID="Calendrier" runat="server" ></Calendar>
                                        </telerik:RadDatePicker> 
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>     
</asp:Table> 

