﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_RESULTAT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumero As Integer
        Private WsIntitule As String
        Private WsDifficultes As String
        Private WsCritereNumerique As Integer
        Private WsCritereLitteral As String
        Private WsEcheance As String
        Private WsEvenement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_RESULTAT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienResultat
            End Get
        End Property

        Public Property Numero() As Integer
            Get
                Return WsNumero
            End Get
            Set(ByVal value As Integer)
                WsNumero = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Difficultes() As String
            Get
                Return WsDifficultes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDifficultes = value
                    Case Else
                        WsDifficultes = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Critere_Numerique() As Integer
            Get
                Return WsCritereNumerique
            End Get
            Set(ByVal value As Integer)
                WsCritereNumerique = value
            End Set
        End Property

        Public Property Critere_Litteral() As String
            Get
                Return WsCritereLitteral
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCritereLitteral = value
                    Case Else
                        WsCritereLitteral = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Echeance_Realisation() As String
            Get
                Return WsEcheance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsEcheance = value
                    Case Else
                        WsEcheance = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Evenement_Survenu() As String
            Get
                Return WsEvenement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsEvenement = value
                    Case Else
                        WsEvenement = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Numero.ToString & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Difficultes & VI.Tild)
                Chaine.Append(Critere_Numerique.ToString & VI.Tild)
                Chaine.Append(Critere_Litteral & VI.Tild)
                Chaine.Append(Echeance_Realisation & VI.Tild)
                Chaine.Append(Evenement_Survenu)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Numero = CInt(VirRhFonction.ConversionDouble(TableauData(2)))
                Intitule = TableauData(3)
                Difficultes = TableauData(4)
                If TableauData(5) <> "" Then Critere_Numerique = CInt(VirRhFonction.ConversionDouble(TableauData(5)))
                Critere_Litteral = TableauData(6)
                Echeance_Realisation = TableauData(7)
                Evenement_Survenu = TableauData(8)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
