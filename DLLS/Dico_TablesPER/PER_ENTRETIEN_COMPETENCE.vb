﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_COMPETENCE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsCompetence As String
        Private WsNumeroFamille As String
        Private WsIntituleFamille As String
        Private WsNumeroSousFamille As String
        Private WsIntituleSousFamille As String
        Private WsNumeroTri As Integer
        Private WsCritereNumerique As Integer
        Private WsCritereLitteral As String
        Private WsCritereNumeriqueRequis As Integer
        Private WsCritereLitteralRequis As String
        Private WsAppreciation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_COMPETENCE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienServir
            End Get
        End Property

        Public Property Competence() As String
            Get
                Return WsCompetence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCompetence = value
                    Case Else
                        WsCompetence = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Numero_Famille() As String
            Get
                Return WsNumeroFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsNumeroFamille = value
                    Case Else
                        WsNumeroFamille = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Intitule_Famille() As String
            Get
                Return WsIntituleFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleFamille = value
                    Case Else
                        WsIntituleFamille = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Numero_SousFamille() As String
            Get
                Return WsNumeroSousFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsNumeroSousFamille = value
                    Case Else
                        WsNumeroSousFamille = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Intitule_SousFamille() As String
            Get
                Return WsIntituleSousFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleSousFamille = value
                    Case Else
                        WsIntituleSousFamille = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property NumeroTri() As Integer
            Get
                Return WsNumeroTri
            End Get
            Set(ByVal value As Integer)
                WsNumeroTri = value
            End Set
        End Property

        Public Property Critere_Numerique() As Integer
            Get
                Return WsCritereNumerique
            End Get
            Set(ByVal value As Integer)
                WsCritereNumerique = value
            End Set
        End Property

        Public Property Critere_Litteral() As String
            Get
                Return WsCritereLitteral
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCritereLitteral = value
                    Case Else
                        WsCritereLitteral = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Critere_Numerique_Requis() As Integer
            Get
                Return WsCritereNumeriqueRequis
            End Get
            Set(ByVal value As Integer)
                WsCritereNumeriqueRequis = value
            End Set
        End Property

        Public Property Critere_Litteral_Requis() As String
            Get
                Return WsCritereLitteralRequis
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCritereLitteralRequis = value
                    Case Else
                        WsCritereLitteralRequis = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Appreciation_Competence() As String
            Get
                Return WsAppreciation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsAppreciation = value
                    Case Else
                        WsAppreciation = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Competence & VI.Tild)
                Chaine.Append(Numero_Famille & VI.Tild)
                Chaine.Append(Intitule_Famille & VI.Tild)
                Chaine.Append(Numero_SousFamille & VI.Tild)
                Chaine.Append(Intitule_SousFamille & VI.Tild)
                Chaine.Append(NumeroTri.ToString & VI.Tild)
                Chaine.Append(Critere_Numerique.ToString & VI.Tild)
                Chaine.Append(Critere_Litteral & VI.Tild)
                Chaine.Append(Critere_Numerique_Requis.ToString & VI.Tild)
                Chaine.Append(Critere_Litteral_Requis & VI.Tild)
                Chaine.Append(Appreciation_Competence)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Competence = TableauData(2)
                Numero_Famille = TableauData(3)
                Intitule_Famille = TableauData(4)
                Numero_SousFamille = TableauData(5)
                Intitule_SousFamille = TableauData(6)
                If TableauData(7) <> "" Then NumeroTri = CInt(VirRhFonction.ConversionDouble(TableauData(7)))
                If TableauData(8) <> "" Then Critere_Numerique = CInt(VirRhFonction.ConversionDouble(TableauData(8)))
                Critere_Litteral = TableauData(9)
                If TableauData(10) <> "" Then Critere_Numerique_Requis = CInt(VirRhFonction.ConversionDouble(TableauData(10)))
                Critere_Litteral_Requis = TableauData(11)
                Appreciation_Competence = TableauData(12)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
