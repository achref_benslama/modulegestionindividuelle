﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace ShemaREF
    Public Class FOR_INTERVENANT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntervenant As String
        Private WsOrganisme_rattachement As String
        Private WsNumeroetRue As String
        Private WsComplementAdresse As String
        Private WsCodepostal As Integer = 0
        Private WsBureaudistributeur As String
        Private WsTelephone As String
        Private WsTelephoneportable As String
        Private WsAdressemail As String
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_INTERVENANT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Intervenant() As String
            Get
                Return WsIntervenant
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsIntervenant = value
                    Case Else
                        WsIntervenant = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Organisme_rattachement() As String
            Get
                Return WsOrganisme_rattachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme_rattachement = value
                    Case Else
                        WsOrganisme_rattachement = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property NumeroetRue() As String
            Get
                Return WsNumeroetRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumeroetRue = value
                    Case Else
                        WsNumeroetRue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property ComplementAdresse() As String
            Get
                Return WsComplementAdresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsComplementAdresse = value
                    Case Else
                        WsComplementAdresse = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Codepostal() As Integer
            Get
                Return WsCodepostal
            End Get
            Set(ByVal value As Integer)
                WsCodepostal = value
            End Set
        End Property

        Public Property Bureaudistributeur() As String
            Get
                Return WsBureaudistributeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsBureaudistributeur = value
                    Case Else
                        WsBureaudistributeur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Telephoneportable() As String
            Get
                Return WsTelephoneportable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephoneportable = value
                    Case Else
                        WsTelephoneportable = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Adressemail() As String
            Get
                Return WsAdressemail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAdressemail = value
                    Case Else
                        WsAdressemail = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intervenant & VI.Tild)
                Chaine.Append(Organisme_rattachement & VI.Tild)
                Chaine.Append(NumeroetRue & VI.Tild)
                Chaine.Append(ComplementAdresse & VI.Tild)
                Chaine.Append(Codepostal.ToString & VI.Tild)
                Chaine.Append(Bureaudistributeur & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(Telephoneportable & VI.Tild)
                Chaine.Append(Adressemail & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intervenant = TableauData(1)
                Organisme_rattachement = TableauData(2)
                NumeroetRue = TableauData(3)
                ComplementAdresse = TableauData(4)
                If TableauData(5) <> "" Then Codepostal = CInt(TableauData(5))
                Bureaudistributeur = TableauData(6)
                Telephone = TableauData(7)
                Telephoneportable = TableauData(8)
                Adressemail = TableauData(9)
                Observations = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


