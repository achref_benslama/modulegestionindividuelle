﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_HEURESUP" Codebehind="PER_HEURESUP.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="585px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="510px" Width="585px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="585px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Heures supplémentaires" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="61" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD16" runat="server" TypeCalendrier="Standard" EtiWidth="120px" SiDateFin="true"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="61" V_Information="16" DonTabIndex="2"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
             <asp:Table ID="TableauTitreHeuresup" runat="server" CellPadding="0" CellSpacing="0" Width="450px">
               <asp:TableRow>
                 <asp:TableCell>
                    <asp:Label ID="EtiHSUP" runat="server" Height="20px" Width="200px" Text="Tranche"
                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 21px; text-indent: 5px; text-align: center">
                    </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                    <asp:Label ID="EtiHSUPNombre" runat="server" Height="20px" Width="60px" Text="Nombre"
                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center">
                    </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                    <asp:Label ID="EtiHSUPTaux" runat="server" Height="20px" Width="60px" Text="Taux"
                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center">
                    </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                    <asp:Label ID="EtiHSUPMontant" runat="server" Height="20px" Width="60px" Text="Montant"
                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 1px; margin-left: 1px; text-indent: 1px; text-align: center">
                    </asp:Label>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell Height="3px" Columnspan="4"></asp:TableCell>
               </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="TableauDonHeures" runat="server" CellPadding="0" CellSpacing="0" Width="450px">
               <asp:TableRow>
                 <asp:TableCell>
                       <asp:Label ID="LabelTranche1" runat="server" Height="20px" Width="200px" Text="1ère tranche"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left">
                       </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_SiAutoPostBack="true"
                            V_PointdeVue="1" V_Objet="61" V_Information="1" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="3" Donstyle="margin-left: 3px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="2" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="4" Donstyle="margin-left: 1px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="3" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="5" Donstyle="margin-left: 0px;"/>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell>
                       <asp:Label ID="LabelTranche2" runat="server" Height="20px" Width="200px" Text="2ème tranche"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left">
                       </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_SiAutoPostBack="true"
                            V_PointdeVue="1" V_Objet="61" V_Information="4" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="6" Donstyle="margin-left: 3px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="5" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="7" Donstyle="margin-left: 1px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="6" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="8" Donstyle="margin-left: 0px;"/>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell>
                       <asp:Label ID="LabelTranche3" runat="server" Height="20px" Width="200px" Text="3ème tranche"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left">
                       </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_SiAutoPostBack="true"
                            V_PointdeVue="1" V_Objet="61" V_Information="7" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="9" Donstyle="margin-left: 3px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="8" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="10" Donstyle="margin-left: 1px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="9" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="11" Donstyle="margin-left: 0px;"/>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell>
                       <asp:Label ID="LabelTranche4" runat="server" Height="20px" Width="200px" Text="4ème tranche"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 0px; margin-left: 21px; text-indent: 5px; text-align: left">
                       </asp:Label>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_SiAutoPostBack="true"
                            V_PointdeVue="1" V_Objet="61" V_Information="10" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="12" Donstyle="margin-left: 3px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="11" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="13" Donstyle="margin-left: 1px;"/>
                 </asp:TableCell>
                 <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="12" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="55px" DonTabIndex="14" Donstyle="margin-left: 0px;"/>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell Height="2px" ColumnSpan="4"></asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell HorizontalAlign="Left" ColumnSpan="4">
                       <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                            V_PointdeVue="1" V_Objet="61" V_Information="13" V_SiDonneeDico="true"
                            Etistyle="margin-left: 20px;" Donstyle="margin-left: 4px;"
                            EtiWidth="354px" DonWidth="55px" DonTabIndex="15"/>
                 </asp:TableCell>
               </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="585px">
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD14" runat="server" TypeCalendrier="Standard" EtiWidth="100px" SiDateFin="true"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="61" V_Information="14" DonTabIndex="16"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                         <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV15" runat="server" DonTextMode="true"
                           V_PointdeVue="1" V_Objet="61" V_Information="15" V_SiDonneeDico="true"
                           EtiWidth="540px" DonWidth="538px" DonHeight="120px" DonTabIndex="17"
                           EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
           </asp:Table>
          </asp:TableCell>
        </asp:TableRow>     
       </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>