﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VNumTelephone.ascx.vb" Inherits="Virtualia.Net.VNumTelephone" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<style type="text/css">
    .validator_align {
        margin-top: 20px;
    }
</style>

<script type="text/javascript" lang="js"> 

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }



</script>

<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell ID="CellEtiquette">
            <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                Font-Bold="False" Font-Names="Calibri" Font-Size="Small"
                Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left;">
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadDropDownList runat="server" ID="DropDownList" Width="200px" Skin="Silk" EnableEmbeddedSkins="true" BackColor="#E2F5F1">
                <Items>
                <telerik:DropDownListItem Value="93" Text="Afghanistan (+93)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="355" Text="Albania (+355)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="213" Text="Algeria (+213)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="684" Text="American Samoa (+684)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="376" Text="Andorra (+376)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="244" Text="Angola (+244)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="264" Text="Anguilla (+264)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="672" Text="Antarctica (Casey) (+672)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="672" Text="Antarctica (Scott) (+672)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="268" Text="Antigua (+268)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="54" Text="Argentina (+54)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="374" Text="Armenia (+374)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="297" Text="Aruba (+297)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="247" Text="Ascension Islands (+247)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="0" Text="Atlantic Ocean (+0)" />
                <telerik:DropDownListItem Value="61" Text="Australia (+61)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="0" Text="Australia Territory (+0)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="43" Text="Austria (+43)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="994" Text="Azerbaijan (+994)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="242" Text="Bahamas (+242)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="973" Text="Bahrain (+973)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="880" Text="Bangladesh (+880)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="246" Text="Barbados (+246)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="268" Text="Barbuda (+268)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="375" Text="Belarus (+375)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="32" Text="Belgium (+32)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="501" Text="Belize (+501)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="229" Text="Benin (+229)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="441" Text="Bermuda (+441)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="975" Text="Bhutan (+975)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="591" Text="Bolivia (+591)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="387" Text="Bosnia Herzegovina (+387)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="267" Text="Botswana (+267)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="55" Text="Brazil (+55)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="284" Text="British Virgin Islands (+284)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="673" Text="Brunei (+673)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="359" Text="Bulgaria (+359)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="226" Text="Burkina Faso (+226)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="257" Text="Burundi (+257)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="855" Text="Cambodia (+855)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="237" Text="Cameroon (+237)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="1" Text="Canada (+1)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="238" Text="Cape Verde Island (+238)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="345" Text="Cayman Island (+345)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="236" Text="Central Africa Republic (+236)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="235" Text="Chad Republic (+235)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="56" Text="Chile (+56)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="86" Text="China (+86)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="61" Text="Christmas/Cocos (+61)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="57" Text="Colombia (+57)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="269" Text="Comoros (+269)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="242" Text="Congo (+242)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="682" Text="Cook Island (+682)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="506" Text="Costa Rica (+506)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="385" Text="Croatia (+385)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="53" Text="Cuba (+53)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="599" Text="Curacao (+599)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="357" Text="Cyprus (+357)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="420" Text="Czech Republic (+420)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="243" Text="Dem. Rep. of Congo (+243)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="45" Text="Denmark (+45)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="246" Text="Diego Garcia (+246)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="253" Text="Djibouti (+253)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="767" Text="Dominica (+767)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="809" Text="Dominican Republic (+809)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="670" Text="East Timor (+670)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="56" Text="Easter Island (+56)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="593" Text="Ecuador (+593)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="20" Text="Egypt (+20)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="503" Text="El Salvador (+503)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="240" Text="Equatorial Guinea (+240)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="291" Text="Eritrea (+291)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="372" Text="Estonia (+372)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="251" Text="Ethiopia (+251)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="298" Text="Faeroe Islands (+298)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="679" Text="Fiji Islands (+679)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="358" Text="Finland (+358)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Selected="True" Value="33" Text="France (+33)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="596" Text="French Antilles (+596)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="594" Text="French Guiana (+594)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="689" Text="French Polynesia (+689)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="241" Text="Gabon (+241)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="220" Text="Gambia (+220)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="995" Text="Georgia (+995)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="49" Text="Germany (+49)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="233" Text="Ghana (+233)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="350" Text="Gibraltar (+350)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="881" Text="Global Mobile Satellite System (GMSS) (+881)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="30" Text="Greece (+30)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="299" Text="Greenland (+299)" ></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="473" Text="Grenada (+473)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="590" Text="Guadeloupe (+590)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="671" Text="Guam (+671)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="502" Text="Guatemala (+502)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="224" Text="Guinea (+224)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="592" Text="Guyana (+592)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="509" Text="Haiti (+509)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="504" Text="Honduras (+504)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="852" Text="Hong Kong (+852)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="36" Text="Hungary (+36)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="354" Text="Iceland (+354)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="91" Text="India (+91)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="0" Text="Indian Ocean (+0)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="62" Text="Indonesia (+62)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="871" Text="Inmarsat (Atlantic Ocean - East) (+871)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="874" Text="Inmarsat (Atlantic Ocean - West) (+874)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="873" Text="Inmarsat (Indian Ocean) (+873)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="872" Text="Inmarsat (Pacific Ocean) (+872)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="870" Text="Inmarsat SNAC (+870)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="98" Text="Iran (+98)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="964" Text="Iraq (+964)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="353" Text="Ireland (+353)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="8817" Text="Iridium (Mobile Satellite service) (+8817)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="972" Text="Palestine (+972)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="39" Text="Italy (+39)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="225" Text="Ivory Coast (+225)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="876" Text="Jamaica (+876)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="81" Text="Japan (+81)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="962" Text="Jordan (+962)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="7" Text="Kazakhstan (+7)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="254" Text="Kenya (+254)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="686" Text="Kiribati (+686)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="7" Text="Kirighzia (+7)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="965" Text="Kuwait (+965)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="996" Text="Kyrgyz Republic (+996)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="7" Text="Kyrgyzstan (+7)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="856" Text="Laos (+856)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="371" Text="Latvia (+371)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="961" Text="Lebanon (+961)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="266" Text="Lesotho (+266)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="231" Text="Liberia (+231)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="218" Text="Libya (+218)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="423" Text="Liechtenstein (+423)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="370" Text="Lithuania (+370)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="352" Text="Luxembourg (+352)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="853" Text="Macao (+853)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="389" Text="Macedonia (+389)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="261" Text="Madagascar (+261)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="265" Text="Malawi (+265)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="60" Text="Malaysia (+60)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="960" Text="Maldives (+960)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="223" Text="Mali Republic (+223)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="356" Text="Malta (+356)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="692" Text="Marshall Islands (+692)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="596" Text="Martinique (+596)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="222" Text="Mauritania (+222)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="230" Text="Mauritius (+230)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="269" Text="Mayotte Island (+269)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="521" Text="Mexico Band 1 (+521)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="522" Text="Mexico Band 2 (+522)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="523" Text="Mexico Band 3 (+523)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="524" Text="Mexico Band 4 (+524)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="525" Text="Mexico Band 5 (+525)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="526" Text="Mexico Band 6 (+526)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="527" Text="Mexico Band 7 (+527)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="528" Text="Mexico Band 8 (+528)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="691" Text="Micronesia (+691)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="808" Text="Midway Island (+808)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="373" Text="Moldova (+373)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="377" Text="Monaco (+377)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="976" Text="Mongolia (+976)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="664" Text="Montserrat (+664)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="212" Text="Morocco (+212)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="258" Text="Mozambique (+258)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="95" Text="Myanmar (Burma) (+95)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="264" Text="Namibia (+264)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="674" Text="Nauru (+674)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="977" Text="Nepal (+977)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="31" Text="Netherlands (+31)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="599" Text="Netherlands Antilles (+599)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="869" Text="Nevis Island (+869)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="687" Text="New Caledonia (+687)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="64" Text="New Zealand (+64)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="505" Text="Nicaragua (+505)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="227" Text="Niger (+227)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="234" Text="Nigeria (+234)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="683" Text="Niue Island (+683)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="672" Text="Norfolk Island (+672)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="850" Text="North Korea (+850)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="670" Text="Northern Marianas Islands (Saipan, Rota, & Tinian) (+670)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="47" Text="Norway (+47)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="968" Text="Oman (+968)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="92" Text="Pakistan (+92)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="680" Text="Palau (+680)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="970" Text="Palestine (+970)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="809" Text="Palm Island (+809)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="507" Text="Panama (+507)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="675" Text="Papau New Guinea (+675)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="595" Text="Paraguay (+595)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="51" Text="Peru (+51)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="63" Text="Philippines (+63)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="48" Text="Poland (+48)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="351" Text="Portugal (+351)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="239" Text="Principe (+239)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="974" Text="Qatar (+974)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="262" Text="Reunion Island (+262)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="40" Text="Romania (+40)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="7" Text="Russia (+7)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="250" Text="Rwanda (+250)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="7" Text="Sakhalin (+7)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="378" Text="San Marino (+378)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="239" Text="Sao Tome (+239)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="966" Text="Saudi Arabia (+966)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="221" Text="Senegal Republic (+221)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="381" Text="Serbia (+381)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="248" Text="Seychelles (+248)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="232" Text="Sierra Leone (+232)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="65" Text="Singapore (+65)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="421" Text="Slovakia (+421)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="386" Text="Slovenia (+386)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="677" Text="Solomon Islands (+677)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="252" Text="Somalia (+252)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="27" Text="South Africa (+27)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="82" Text="South Korea (+82)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="34" Text="Spain (+34)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="94" Text="Sri Lanka (+94)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="290" Text="St. Helena (+290)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="869" Text="St. Kitts (+869)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="758" Text="St. Lucia (+758)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="508" Text="St. Pierre & Miquelon (+508)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="784" Text="St. Vincent & Grenadines (+784)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="249" Text="Sudan (+249)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="597" Text="Suriname (+597)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="268" Text="Swaziland (+268)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="46" Text="Sweden (+46)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="41" Text="Switzerland (+41)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="963" Text="Syria (+963)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="886" Text="Taiwan (+886)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="992" Text="Tajikistan (+992)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="255" Text="Tanzania (+255)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="66" Text="Thailand (+66)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="228" Text="Togo (+228)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="690" Text="Tokelau (+690)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="676" Text="Tonga (+676)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="868" Text="Trinidad/Tobago (+868)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="216" Text="Tunisia (+216)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="90" Text="Turkey (+90)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="993" Text="Turkmenistan (+993)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="649" Text="Turks/Caicos (+649)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="688" Text="Tuvalu Islands (+688)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="256" Text="Uganda (+256)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="380" Text="Ukraine (+380)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="971" Text="United Arab Emirates (+971)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="44" Text="United Kingdom (+44)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="878" Text="Universal Personal Telecommunications (UPT) (+878)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="598" Text="Uruguay (+598)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="340" Text="US Virgin Islands (+340)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="1" Text="USA (+1)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="998" Text="Uzbekistan (+998)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="678" Text="Vanuatu (+678)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="39" Text="Vatican City (+39)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="58" Text="Venezuela (+58)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="84" Text="Vietnam (+84)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="808" Text="Wake Island (+808)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="685" Text="West Samoa (+685)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="967" Text="Yemen Arab Rep. (+967)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="381" Text="Yugoslavia (+381)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="260" Text="Zambia (+260)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="255" Text="Zanzibar (+255)"></telerik:DropDownListItem>
                <telerik:DropDownListItem Value="263" Text="Zimbabwe (+263)>"></telerik:DropDownListItem>
                </Items>
            </Telerik:RadDropDownList>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:TextBox ID="Donnee" runat="server" Height="16px" Width="300px" MaxLength="0"
                            BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                            oncopy="return false;" onpaste="return false;" BorderWidth="2px" ForeColor="Black"
                            Visible="true" AutoPostBack="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                            Style="margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:RequiredFieldValidator ID="Validation_Donnee"
                            runat="server" ControlToValidate="Donnee" Enabled="false">
                        </asp:RequiredFieldValidator>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

        </asp:TableCell>
    </asp:TableRow>

</asp:Table>



