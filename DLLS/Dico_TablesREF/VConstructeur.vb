﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaREF
    Public Class VConstructeur
        Implements Virtualia.Systeme.IVConstructeur
        Private WsTypeModele As Integer
        Private WsPointdeVue As Integer = 1
        Private WsTypeParametre As Integer = 0

        Public Function V_NouvelleFiche(ByVal NumObjet As Integer, ByVal IdeDossier As Integer, ByVal LstDatas As List(Of String)) As Virtualia.Systeme.MetaModele.VIR_FICHE Implements Systeme.IVConstructeur.V_NouvelleFiche
            Dim FicheREF As Virtualia.Systeme.MetaModele.VIR_FICHE = Nothing
            Select Case WsPointdeVue
                Case VI.PointdeVue.PVueGeneral
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_LISTE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION_CIR
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_LISTE_CIR
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION_GEST
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_LISTE_GEST
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_LISTE_CIR
                    End Select
                Case VI.PointdeVue.PVueGrades
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            Select Case NumObjet
                                Case 1
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRADE
                                Case 2
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_AVANCEMENT_GRADE
                                Case 3
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRADE_CIR
                                Case 4
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRADE_DGCL
                                Case 5
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRADE_NNE
                            End Select
                        Case Else
                            Select Case NumObjet
                                Case 1
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_EMPLOI
                            End Select
                    End Select
                Case VI.PointdeVue.PVueGrilles
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRILLE
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_GRILLE_NNE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_CATEGORIE_NNE
                    End Select
                Case VI.PointdeVue.PVuePosition
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.POS_POSITION
                    End Select
                Case VI.PointdeVue.PVueAbsences
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ABS_ABSENCE
                    End Select
                Case VI.PointdeVue.PVueDirection
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ORG_DIRECTION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ORG_ANNUAIRE
                    End Select
                Case VI.PointdeVue.PVuePosteBud
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ORG_BUDGETAIRE
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ORG_DEFINITION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ORG_RATTACHEMENT
                    End Select
                Case VI.PointdeVue.PVueFormation
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_DESCRIPTIF
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_CARACTERISTIC
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_EVALUATION
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_SESSION
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_COUTS
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_FACTURE
                        Case 8
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS
                        Case 9
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FOR_INTERVENANT
                    End Select
                Case VI.PointdeVue.PVueEntreprise
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ENT_INTERLOCUTEUR
                    End Select
                Case VI.PointdeVue.PVuePrimes
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PRM_INDEMNITE
                    End Select
                Case VI.PointdeVue.PVuePaie
                    Select Case NumObjet
                        Case 1
                            'FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_GENERAL
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_FERIE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_OUVRABLE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_ZONE_RESIDENCE
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_INDICE_COMPL
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_INDICE100
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_SMIC
                        Case 8
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_PLAFOND_URSSAF
                        Case 9
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_RESIDENCE
                        Case 10
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_SFT
                        Case 11
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_SOLIDARITE
                        Case 12
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_INDEMNITE_KM
                        Case 13
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_COTIS_PRIVE
                        Case 14
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_CSG
                        Case 15
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_TRAVAIL
                        Case 16
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_CONGE_ANNUEL
                        Case 17
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_RDS
                        Case 18
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_MISSION
                        Case 19, 20
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_REGLE_TYPE_REMUN(NumObjet)
                        Case 21
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_MALADIE
                        Case 22
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_MIS_ETRANGER
                        Case 23
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_VACATION_EN
                        Case 24
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_COULEURS
                        Case 25
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_EXPLOITATION
                        Case 26
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_EXPLOITATION2
                        Case 27
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAI_EXPLOITATION3
                    End Select
                Case VI.PointdeVue.PVueMetier
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_METIER
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_EXERCICE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_QUALITE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_SAVOIR_FAIRE
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_DIPLOME
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_RATTACHEMENT
                        Case 8
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_ATTRIBUTION
                        Case 9
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_ACTIVITES
                        Case 10
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_EXTERNE
                        Case 11
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_INTERNE
                        Case 12
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_APTITUDES
                        Case 13
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MET_ENVIRONNEMENT
                    End Select
                Case VI.PointdeVue.PVueEtablissement
                    Select Case NumObjet
                        Case 1
                            Select Case WsTypeModele
                                Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE
                                Case Else
                                    FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE_PRIVE
                            End Select
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_HORAIRE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_ACCORD_RTT
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_REGLE_VALTEMPS
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_CONGE_ANNUEL
                        Case 6 'News
                            Return Nothing
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_DEBIT_CREDIT
                        Case 8
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_STRUCTURE
                        Case 9
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_PARAMETRES
                        Case 10
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_REGLE_EFFECTIF
                        Case 11
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_MANDATEMENT
                        Case 12
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_COMPTE_RENDU
                        Case 13
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_FONCTIONS
                        Case 14
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_FICHE_NAVETTE
                        Case 15
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ETA_INTRANET_ABSENCE
                    End Select
                Case VI.PointdeVue.PVueBaseHebdo 'Unités de Cycle
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_PREMIER
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_DEUXIEME
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_TROISIEME
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_QUATRIEME
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_CINQUIEME
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_SIXIEME
                        Case 8
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CYC_SEPTIEME
                    End Select
                Case VI.PointdeVue.PVueCycle 'Cycles de travail
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TRA_VALORISATION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TRA_ACCORD_RTT
                    End Select
                Case VI.PointdeVue.PVuePresences
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PRE_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PRE_ACCORD_RTT
                    End Select
                Case VI.PointdeVue.PVueDegres 'Degrés de compétence
                Case VI.PointdeVue.PVueQualites
                Case VI.PointdeVue.PVueValTemps
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.HOR_REGLE
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.HOR_VARIABLES
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.HOR_CONDITIONS_PRESENCE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.HOR_CONDITIONS_ABSENCE
                    End Select
                Case VI.PointdeVue.PVueItineraire
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ITI_DISTANCE
                    End Select
                Case VI.PointdeVue.PVueCommission
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ACT_COMMISSION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ACT_FONCTIONS
                    End Select
                Case VI.PointdeVue.PVueDicoCompetence
                Case VI.PointdeVue.PVueSavoirFaire
                Case VI.PointdeVue.PVueScriptCmc
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CMC_DESCRIPTIF
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CMC_SELECTION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CMC_VALEURS
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CMC_SQL
                    End Select
                Case VI.PointdeVue.PVueScriptStats
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.STATS_DESCRIPTIF
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.STATS_VALEURS
                    End Select
                Case VI.PointdeVue.PVueScriptExport
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EXPORT_DESCRIPTIF
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EXPORT_SELECTION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EXPORT_FILTRE
                    End Select
                Case VI.PointdeVue.PVueScriptImport
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.IMPORT_DESCRIPTIF
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.IMPORT_SELECTION
                    End Select
                Case VI.PointdeVue.PVueScriptEdition
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EDITION_DESCRIPTIF
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EDITION_SELECTION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EDITION_FILTRE
                    End Select
                Case VI.PointdeVue.PVueLiaisonWord
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LIAISON_EXPORT
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LIAISON_DOCUMENT
                    End Select
                Case VI.PointdeVue.PVueTranscodification
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CORRESP_DEFINITION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CORRESP_EXTERNES
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_CORRES_VIR_CIR
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.TAB_CORRES_VIR_CIR_GRD
                    End Select
                Case VI.PointdeVue.PVueMission
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MIS_CARACTERISTIC
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MIS_AFFECTATION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MIS_TRANSPORT
                    End Select
                Case VI.PointdeVue.PVuePerExterne
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EXT_PERSONNES
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.EXT_DEPLACEMENT
                    End Select
                Case VI.PointdeVue.PVueInterface
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.OUT_LOGICIEL
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.OUT_CONTEXTE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.OUT_DECISION
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.OUT_GAPAIE
                    End Select
                Case VI.PointdeVue.PVueSiteGeo
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.GEO_DESCRIPTIF
                    End Select
                Case VI.PointdeVue.PVueMonnaie
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MON_MONNAIE
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.MON_CONVERSION
                    End Select
                Case VI.PointdeVue.PVuePosteFct
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PST_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PST_SUIVI
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PST_ORGANIGRAMME
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PST_BUDGETAIRE
                    End Select
                Case VI.PointdeVue.PVueTransaction
                    Select Case NumObjet
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
                    End Select
                Case 42
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.FCT_SUPPORT
                    End Select
                Case VI.PointdeVue.PVueCommunesFr
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.VIL_COMMUNES
                    End Select
                Case VI.PointdeVue.PVuePays
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_DESCRIPTION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_JOURFERIE_FIXE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_JOURFERIE_MOBILE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_DROITS_CONGE
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_VOYAGE_CONGE
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_MIS_ETRANGER
                        Case 7
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.PAYS_TAUX_CHANGE
                    End Select
                Case VI.PointdeVue.PVueLolf
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_MISSION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_PROGRAMME
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_ACTION
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_AUTORISATION_EMPLOI
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_BOP
                        Case 6
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.LOLF_UO
                    End Select
                Case 48
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ONIC_MISSION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
                    End Select
                Case VI.PointdeVue.PVueUtilisateur
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.UTI_IDENTIFICATION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.UTI_FILTRE
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.UTI_PERIMETRE
                        Case 4
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.UTI_CRITERE_DOSSIER
                        Case 5
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.UTI_COULEUR_DOSSIER
                    End Select
                Case VI.PointdeVue.PVueRegles
                    Select Case NumObjet
                        Case VI.ObjetReg.ObaDefinition
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_DEFINITION
                        Case VI.ObjetReg.ObaRegle
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_REGLE
                        Case VI.ObjetReg.ObaDeclenchement
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_DECLENCHEMENT
                        Case VI.ObjetReg.ObaResultat
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_RESULTAT
                        Case VI.ObjetReg.ObaFormule
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_FORMULE
                        Case VI.ObjetReg.ObaCritere
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_CRITERE
                        Case VI.ObjetReg.ObaValeur
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.REG_VALEUR
                    End Select
                Case VI.PointdeVue.PVueConfiguration
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CONFIG_SECTION
                        Case 2
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CONFIG_SOUS_SECTION
                        Case 3
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.CONFIG_CLEF
                    End Select
                Case VI.PointdeVue.PVueSession
                    Select Case NumObjet
                        Case 1
                            FicheREF = New Virtualia.TablesObjet.ShemaREF.SESSION_CNX
                    End Select
            End Select

            If FicheREF Is Nothing Then
                Return Nothing
            End If
            If LstDatas Is Nothing Then
                FicheREF.Ide_Dossier = IdeDossier
                Return FicheREF
            End If
            Dim IndiceI As Integer
            Dim ChaineDatas As New System.Text.StringBuilder
            ChaineDatas.Append(IdeDossier.ToString & VI.Tild)
            For IndiceI = 0 To LstDatas.Count - 1
                If IndiceI = LstDatas.Count - 1 Then
                    ChaineDatas.Append(LstDatas(IndiceI))
                Else
                    ChaineDatas.Append(LstDatas(IndiceI) & VI.Tild)
                End If
            Next IndiceI
            FicheREF.ContenuTable = ChaineDatas.ToString
            Return FicheREF

        End Function

        Public Sub New(ByVal Modele As Integer, ByVal PointdeVue As Integer)
            WsTypeModele = Modele
            WsPointdeVue = PointdeVue
        End Sub

        Public Function V_NouvelleFiche(NumObjet As Integer, IdeDossier As Integer) As Systeme.MetaModele.VIR_FICHE Implements Systeme.IVConstructeur.V_NouvelleFiche
            Return V_NouvelleFiche(NumObjet, IdeDossier, Nothing)
        End Function

        Public WriteOnly Property V_Parametre As Integer Implements Systeme.IVConstructeur.V_Parametre
            Set(value As Integer)
                WsTypeParametre = value
            End Set
        End Property

        Public WriteOnly Property V_PointdeVue As Integer Implements Systeme.IVConstructeur.V_PointdeVue
            Set(value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public WriteOnly Property V_TypeduModele As Integer Implements Systeme.IVConstructeur.V_TypeduModele
            Set(value As Integer)
                WsTypeModele = value
            End Set
        End Property
    End Class
End Namespace