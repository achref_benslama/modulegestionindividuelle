﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Ressources.Datas

Namespace Registre
    Public Class ObjetConfiguration
        Implements IGestionVirFiche(Of CONFIG_SECTION)
        Private WsListeConfig As List(Of CONFIG_SECTION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property Valeur(ByVal Intitule As String) As String
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer
                Dim IndiceC As Integer
                Dim LstSouSections As List(Of CONFIG_SOUS_SECTION)
                Dim LstCles As List(Of CONFIG_CLEF)

                For IndiceI = 0 To WsListeConfig.Count - 1
                    LstSouSections = WsListeConfig.Item(IndiceI).ListedesSousSections
                    If LstSouSections IsNot Nothing Then
                        For IndiceK = 0 To LstSouSections.Count - 1
                            LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                            If LstCles IsNot Nothing Then
                                For IndiceC = 0 To LstCles.Count - 1
                                    If LstCles(IndiceC).Intitule = Intitule Then
                                        Return LstCles(IndiceC).Valeur
                                    End If
                                Next IndiceC
                            End If
                        Next IndiceK
                    End If
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property Valeur(ByVal NomSousSection As String, ByVal Intitule As String) As String
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer
                Dim IndiceC As Integer
                Dim LstSouSections As List(Of CONFIG_SOUS_SECTION)
                Dim LstCles As List(Of CONFIG_CLEF)

                For IndiceI = 0 To WsListeConfig.Count - 1
                    LstSouSections = WsListeConfig.Item(IndiceI).ListedesSousSections
                    If LstSouSections IsNot Nothing Then
                        For IndiceK = 0 To LstSouSections.Count - 1
                            If LstSouSections.Item(IndiceK).Intitule = NomSousSection Then
                                LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                                If LstCles IsNot Nothing Then
                                    For IndiceC = 0 To LstCles.Count - 1
                                        If LstCles(IndiceC).Intitule = Intitule Then
                                            Return LstCles(IndiceC).Valeur
                                        End If
                                    Next IndiceC
                                End If
                            End If
                        Next IndiceK
                    End If
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property Valeur(ByVal NomSection As String, ByVal NomSousSection As String, ByVal Intitule As String) As String
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer
                Dim IndiceC As Integer
                Dim LstSouSections As List(Of CONFIG_SOUS_SECTION)
                Dim LstCles As List(Of CONFIG_CLEF)

                For IndiceI = 0 To WsListeConfig.Count - 1
                    If WsListeConfig.Item(IndiceI).Intitule = NomSection Then
                        LstSouSections = WsListeConfig.Item(IndiceI).ListedesSousSections
                        If LstSouSections IsNot Nothing Then
                            For IndiceK = 0 To LstSouSections.Count - 1
                                Select Case NomSousSection
                                    Case Is <> ""
                                        If LstSouSections.Item(IndiceK).Intitule = NomSousSection Then
                                            LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                                            If LstCles IsNot Nothing Then
                                                For IndiceC = 0 To LstCles.Count - 1
                                                    If LstCles(IndiceC).Intitule = Intitule Then
                                                        Return LstCles(IndiceC).Valeur
                                                    End If
                                                Next IndiceC
                                            End If
                                        End If
                                    Case Else
                                        LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                                        If LstCles IsNot Nothing Then
                                            For IndiceC = 0 To LstCles.Count - 1
                                                If LstCles(IndiceC).Intitule = Intitule Then
                                                    Return LstCles(IndiceC).Valeur
                                                End If
                                            Next IndiceC
                                        End If
                                End Select
                            Next IndiceK
                        End If
                    End If
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property Valeur(ByVal NomCategorie As String, ByVal NomSection As String, ByVal NomSousSection As String, ByVal Intitule As String) As String
            Get
                Dim IndiceI As Integer
                Dim IndiceK As Integer
                Dim IndiceC As Integer
                Dim LstSouSections As List(Of CONFIG_SOUS_SECTION)
                Dim LstCles As List(Of CONFIG_CLEF)

                For IndiceI = 0 To WsListeConfig.Count - 1
                    If WsListeConfig.Item(IndiceI).Intitule = NomSection And WsListeConfig.Item(IndiceI).Categorie = NomCategorie Then
                        LstSouSections = WsListeConfig.Item(IndiceI).ListedesSousSections
                        If LstSouSections IsNot Nothing Then
                            For IndiceK = 0 To LstSouSections.Count - 1
                                Select Case NomSousSection
                                    Case Is <> ""
                                        If LstSouSections.Item(IndiceK).Intitule = NomSousSection Then
                                            LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                                            If LstCles IsNot Nothing Then
                                                For IndiceC = 0 To LstCles.Count - 1
                                                    If LstCles(IndiceC).Intitule = Intitule Then
                                                        Return LstCles(IndiceC).Valeur
                                                    End If
                                                Next IndiceC
                                            End If
                                        End If
                                    Case Else
                                        LstCles = LstSouSections.Item(IndiceK).ListedesClefs
                                        If LstCles IsNot Nothing Then
                                            For IndiceC = 0 To LstCles.Count - 1
                                                If LstCles(IndiceC).Intitule = Intitule Then
                                                    Return LstCles(IndiceC).Valeur
                                                End If
                                            Next IndiceC
                                        End If
                                End Select
                            Next IndiceK
                        End If
                    End If
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property ListeDesSections() As List(Of CONFIG_SECTION)
            Get
                Dim LstRes As List(Of CONFIG_SECTION)
                LstRes = (From instance In WsListeConfig Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Categorie Ascending, instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeSections() As Integer Implements IGestionVirFiche(Of CONFIG_SECTION).NombredItems
            Get
                Return WsListeConfig.Count
            End Get
        End Property

        Public Function Fiche_Section(ByVal Ide_Dossier As Integer) As CONFIG_SECTION Implements IGestionVirFiche(Of CONFIG_SECTION).GetFiche
            Return WsListeConfig.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Section(ByVal Intitule As String) As CONFIG_SECTION Implements IGestionVirFiche(Of CONFIG_SECTION).GetFiche
            Return WsListeConfig.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of CONFIG_SECTION).Actualiser
            WsListeConfig.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeConfig.Clear()
            Dim FicheSection As CONFIG_SECTION
            Dim FicheSousSection As CONFIG_SOUS_SECTION

            '** 1  Liste de toutes les Commissions
            WsListeConfig.AddRange(CreationCollectionVIRFICHE(Of CONFIG_SECTION).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeConfig.Count <= 0) Then
                Exit Sub
            End If


            '** 2  Liste de toutes les sous-sections
            Dim lstss As List(Of CONFIG_SOUS_SECTION) = CreationCollectionVIRFICHE(Of CONFIG_SOUS_SECTION).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstss.ForEach(Sub(f)
                              FicheSection = Fiche_Section(f.Ide_Dossier)
                              If FicheSection Is Nothing Then
                                  Exit Sub
                              End If

                              FicheSection.Ajouter_Paragraphe(f)
                          End Sub)

            '** 3  Liste de toutes les clefs
            Dim lstclee As List(Of CONFIG_CLEF) = CreationCollectionVIRFICHE(Of CONFIG_CLEF).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstclee.ForEach(Sub(f)
                                FicheSection = Fiche_Section(f.Ide_Dossier)
                                If (FicheSection Is Nothing) Then
                                    Return
                                End If

                                FicheSousSection = FicheSection.Sous_Section(f.ClefSousSection)
                                If (FicheSousSection Is Nothing) Then
                                    Return
                                End If

                                FicheSousSection.Ajouter_Clef(f)

                            End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeConfig = New List(Of CONFIG_SECTION)
            Call ConstituerListe()
        End Sub
    End Class
End Namespace

