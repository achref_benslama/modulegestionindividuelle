Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_REGLE
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsRegle As String
        Private WsRequete As String
        Private WsService As String
        Private WsObjet_1 As Integer = 0
        Private WsObjet_2 As Integer = 0
        Private WsEligibilite As String
        Private WsCondition_Collective As String

        Private WsNewFormule As REG_FORMULE

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Regle() As String
            Get
                Return WsRegle
            End Get
            Set(ByVal value As String)
                WsRegle = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Requete() As String
            Get
                Return WsRequete
            End Get
            Set(ByVal value As String)
                WsRequete = F_FormatAlpha(value, 500)
            End Set
        End Property

        Public Property Service() As String
            Get
                Return WsService
            End Get
            Set(ByVal value As String)
                WsService = F_FormatAlpha(value, 500)
            End Set
        End Property

        Public Property Objet_1() As Integer
            Get
                WsObjet_1 = 0
                WsObjet_2 = 0

                Dim ListeNumObjet As List(Of Integer)
                Dim K As Integer
                ListeNumObjet = New List(Of Integer)

                If WsNewFormule IsNot Nothing Then
                    If WsNewFormule.Liste_REG_CRITERE.Count > 0 Then

                        ListeNumObjet = (From instance In WsNewFormule.Liste_REG_CRITERE
                                         Order By instance.Objet Ascending
                                         Select instance.Objet).Distinct.ToList

                        For K = 0 To ListeNumObjet.Count - 1
                            If ListeNumObjet(K) <> 0 Then
                                If K = 0 Then
                                    WsObjet_1 = ListeNumObjet(K)
                                Else
                                    If WsObjet_1 <> ListeNumObjet(K) Then
                                        Objet_2 = ListeNumObjet(K)
                                    End If
                                End If
                            End If
                        Next K

                    End If
                End If

                Return WsObjet_1
            End Get
            Set(ByVal value As Integer)
                WsObjet_1 = value
            End Set
        End Property

        Public Property Objet_2() As Integer
            Get
                Return WsObjet_2
            End Get
            Set(ByVal value As Integer)
                WsObjet_2 = value
            End Set
        End Property

        Public Property Eligibilite() As String
            Get
                Return WsEligibilite
            End Get
            Set(ByVal value As String)
                WsEligibilite = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Condition_Collective() As String
            Get
                Return WsCondition_Collective
            End Get
            Set(ByVal value As String)
                WsCondition_Collective = F_FormatAlpha(value, 500)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Regle & VI.Tild)
                Chaine.Append(Requete & VI.Tild)
                Chaine.Append(Service & VI.Tild)
                Chaine.Append(Objet_1.ToString & VI.Tild)
                Chaine.Append(Objet_2.ToString & VI.Tild)
                Chaine.Append(Eligibilite & VI.Tild)
                Chaine.Append(Condition_Collective)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                Regle = TableauData(1)
                Requete = TableauData(2)
                Service = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                Objet_1 = CInt(TableauData(4))
                If TableauData(5) = "" Then TableauData(5) = "0"
                Objet_2 = CInt(TableauData(5))
                Eligibilite = TableauData(6)
                Condition_Collective = TableauData(7)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsRegle
                    Case 2
                        Return WsRequete
                    Case 3
                        Return WsService
                    Case 4
                        Return WsObjet_1.ToString
                    Case 5
                        Return WsObjet_2.ToString
                    Case 6
                        Return WsEligibilite
                    Case 7
                        Return WsCondition_Collective
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Property NewFormule() As REG_FORMULE
            Get
                Return WsNewFormule
            End Get
            Set(ByVal value As REG_FORMULE)
                WsNewFormule = value
            End Set
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone

            Dim CloneREG_REGLE As REG_REGLE = New REG_REGLE(Ide_Dossier.ToString & ContenuTable)

            If (WsNewFormule Is Nothing) Then
                Return CloneREG_REGLE
            End If

            CloneREG_REGLE.NewFormule = WsNewFormule.Clone()
            Return CloneREG_REGLE

        End Function

    End Class
End Namespace

