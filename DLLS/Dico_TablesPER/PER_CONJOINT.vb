Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CONJOINT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsNom As String
        Private WsPrenom As String
        Private WsProfession As String
        Private WsEmployeur As String
        Private WsNaissance As String
        Private WsObservations As String
        Private WsFonctionnaire As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CONJOINT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaConjoint
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 8
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                If WsSiMajuscule = True Then
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = value.ToUpper
                        Case Else
                            WsNom = Strings.Left(value.ToUpper, 35)
                    End Select
                Else
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = VirRhFonction.Lettre1Capi(value, 2)
                        Case Else
                            WsNom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 2)
                    End Select
                    MyBase.Clef = WsNom
                End If
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Profession() As String
            Get
                Return WsProfession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsProfession = value
                    Case Else
                        WsProfession = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Employeur() As String
            Get
                Return WsEmployeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEmployeur = value
                    Case Else
                        WsEmployeur = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Naissance() As String
            Get
                Return WsNaissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNaissance = value
                    Case Else
                        WsNaissance = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Fonctionnaire() As String
            Get
                Return WsFonctionnaire
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui", "1"
                        WsFonctionnaire = "Oui"
                    Case Else
                        WsFonctionnaire = "Non"
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Profession & VI.Tild)
                Chaine.Append(Employeur & VI.Tild)
                Chaine.Append(Naissance & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Fonctionnaire & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                Prenom = TableauData(2)
                Profession = TableauData(3)
                Employeur = TableauData(4)
                Naissance = TableauData(5)
                Observations = TableauData(6)
                Fonctionnaire = TableauData(7)
                MyBase.Certification = TableauData(8)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property VNomEtPrenom() As String
            Get
                Return Nom & Strings.Space(1) & Prenom
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<NomConjoint>" & VirRhFonction.ChaineXMLValide(Nom) & "</NomConjoint>" & vbCrLf)
                Chaine.Append("<PrenomConjoint>" & VirRhFonction.ChaineXMLValide(Prenom) & "</PrenomConjoint>" & vbCrLf)
                Chaine.Append("<ProfessionConjoint>" & VirRhFonction.ChaineXMLValide(Profession) & "</ProfessionConjoint>" & vbCrLf)
                Chaine.Append("<EmployeurConjoint>" & VirRhFonction.ChaineXMLValide(Employeur) & "</EmployeurConjoint>" & vbCrLf)
                Chaine.Append("<NaissanceConjoint>" & VirRhFonction.ChaineXMLValide(Naissance) & "</NaissanceConjoint>" & vbCrLf)
                Chaine.Append("<SiConjointFonctionnaire>" & Fonctionnaire & "</SiConjointFonctionnaire>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Sub New(ByVal SiMajuscule As Boolean)
            MyBase.New()
            WsSiMajuscule = SiMajuscule
        End Sub

        Public Sub New()
            MyBase.New()
            WsSiMajuscule = False
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace

