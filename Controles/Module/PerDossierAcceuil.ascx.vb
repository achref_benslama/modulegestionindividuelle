﻿Imports VI = Virtualia.Systeme.Constantes


Public Class PerDossierAcceuil
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNomStateIde As String = "MenuPERIde"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCivil
    Private WsNomTable As String = "PER_ETATCIVIL"
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsNomEtatCivil As String = ""
    Private WsPrenomEtatCivil As String = ""
    Private WsListeCourante As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
    Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
    Private wsfrm As Virtualia.Net.FrmInfosPersonnelles
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Enum Ivue As Integer
        IVProfile = 0
        IVPER = 1
    End Enum



    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property


    Private Sub PerDossierAcceuil_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim Ide_Dossier As Integer = V_Identifiant
        Dim DllExperte As Virtualia.Metier.Expertes.Partition
        Dim LstIde As List(Of Integer)


        'Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        'If Ide_Dossier > 0 Then
        '    Dossier = V_WebFonction.PointeurDossier(Ide_Dossier)
        'Else
        '    Dossier = Nothing
        'End If

        WsDossierPer = V_Contexte.DossierPER
        WsDossierPer.V_NvDossier_Clicked = False

        If WsDossierPer.FicheEtatCivil Is Nothing Then
            Etatcivil123.Text = ""
            Age.Text = ""
        Else
            Etatcivil123.Text = WsDossierPer.FicheEtatCivil.Qualite + " " + WsDossierPer.FicheEtatCivil.Nom + " " + WsDossierPer.FicheEtatCivil.Prenom
            Age.Text = Convert.ToInt32(WsDossierPer.FicheEtatCivil.V_Age(WsDossierPer.FicheEtatCivil.Datation_de_la_naissance)).ToString
        End If

        idevirtualia.Text = Ide_Dossier.ToString

        DllExperte = V_WebFonction.PointeurGlobal.PointeurDllExpert
        DllExperte.ClasseExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite) = VI.OptionInfo.DicoExport
        LstIde = New List(Of Integer)
        LstIde.Add(Ide_Dossier)
        Try

            present.Text = DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, 1002, Ide_Dossier,
                                        WsDossierPer.FichePosition.Date_de_Valeur, WsDossierPer.FichePosition.Date_de_Fin)
        Catch Ex As Exception
            present.Text = "Non renseigné"
        End Try


        If WsDossierPer.FicheAdressePro Is Nothing Then
            per_adressepro7.Text = ""
        Else
            per_adressepro7.Text = WsDossierPer.FicheAdressePro.Email
        End If

        If WsDossierPer.FicheCollectivite Is Nothing Then
            per_collectivite1.Text = ""
        Else
            per_collectivite1.Text = "Entrée le " + WsDossierPer.FicheCollectivite.Date_d_effet
        End If

        If WsDossierPer.FicheAffectation Is Nothing Then
            per_affectation1_2.Text = ""
        Else
            per_affectation1_2.Text = "Affecté le " + WsDossierPer.FicheAffectation.Date_de_Valeur + Chr(10) + WsDossierPer.FicheAffectation.Structure_de_1er_niveau
        End If

        If WsDossierPer.FicheAffectation Is Nothing Then
            per_affectation5.Text = ""
        Else
            per_affectation5.Text = WsDossierPer.FicheAffectation.Fonction_exercee
        End If

        If WsDossierPer.FicheAdressePro Is Nothing Then
            per_adressepro2.Text = ""
        Else
            per_adressepro2.Text = WsDossierPer.FicheAdressePro.Telephone_professionnel
        End If

        If WsDossierPer.FicheAdressePro Is Nothing Then
            per_adressepro9.Text = ""
        Else
            per_adressepro9.Text = WsDossierPer.FicheAdressePro.Sitegeo
        End If

        If WsDossierPer.FicheStatut Is Nothing Then
            per_statut1_2.Text = ""
            per_statut8.Text = ""

        Else
            per_statut1_2.Text = WsDossierPer.FicheStatut.Statut + " " + WsDossierPer.FicheStatut.Situation
            If WsDossierPer.FicheStatut.Date_de_Fin = "" Then
                per_statut8.Text = ""
            Else
                per_statut8.Text = "jusqu'au " + WsDossierPer.FicheStatut.Date_de_Fin
            End If
            Try
                DllExperte.ClasseExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaStatut) = VI.OptionInfo.DicoExport
                datestatut.Text = "Depuis le " + DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaStatut, 534, Ide_Dossier,
                                        WsDossierPer.FicheStatut.Date_de_Valeur, WsDossierPer.FicheStatut.Date_de_Fin)
            Catch Ex As Exception
                datestatut.Text = "Non renseigné"
            End Try
        End If


        If WsDossierPer.FichePosition Is Nothing Then
            per_position1_11_2.Text = ""
            per_position7.Text = ""
        Else
            per_position1_11_2.Text = WsDossierPer.FichePosition.Position + " " + WsDossierPer.FichePosition.Modaliteservice + " " + WsDossierPer.FichePosition.Taux_d_activite.ToString
            If WsDossierPer.FichePosition.Date_de_Fin = "" Then
                per_position7.Text = ""
            Else
                per_position7.Text = "jusqu'au " + WsDossierPer.FichePosition.Date_de_Fin
            End If
            Try
                DllExperte.ClasseExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite) = VI.OptionInfo.DicoExport
                datecalculposition.Text = "Depuis le " + DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, 933, Ide_Dossier,
                                        WsDossierPer.FichePosition.Date_de_Valeur, WsDossierPer.FichePosition.Date_de_Fin)
            Catch ex As Exception
                datecalculposition.Text = "Non Renseigné"
            End Try


        End If
        Try
            etp.Text = DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, 930, Ide_Dossier,
                                        WsDossierPer.FichePosition.Date_de_Valeur, WsDossierPer.FichePosition.Date_de_Fin)
        Catch ex As Exception
            etp.Text = "Non Renseigné"
        End Try


        If WsDossierPer.FicheGrade Is Nothing Then
            per_grade5.Text = ""
            per_grade1.Text = ""
            per_grade2_4.Text = ""



        Else
            DllExperte.ClasseExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade) = VI.OptionInfo.DicoExport
            Try
                depuisgrade1.Text = "Depuis le " + DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 553, Ide_Dossier,
                                        WsDossierPer.FicheGrade.Date_de_Valeur, WsDossierPer.FicheGrade.Date_de_Fin)
            Catch ex As Exception
                depuisgrade1.Text = "Non Renseigné"
            End Try


            per_grade5.Text = WsDossierPer.FicheGrade.Categorie
            per_grade1.Text = WsDossierPer.FicheGrade.Grade
            per_grade2_4.Text = WsDossierPer.FicheGrade.Echelon + WsDossierPer.FicheGrade.Indice_majore
            Try
                depuisgrade2_4.Text = " Depuis le " + DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 910, Ide_Dossier,
                                        WsDossierPer.FicheGrade.Date_de_Valeur, WsDossierPer.FicheGrade.Date_de_Fin)
            Catch ex As Exception
                depuisgrade2_4.Text = "Non Renseigné"
            End Try

        End If


    End Sub

    Public Overloads Property V_Identifiant As Integer
        Get
            If Me.ViewState(WsNomStateIde) Is Nothing Then
                Return 0
            End If
            Return CType(Me.ViewState(WsNomStateIde), Integer)
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), Integer) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property


    Protected Sub BtnDossierAll(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_visualiser_dossier.Click
        WsDossierPer = V_Contexte.DossierPER
        WsDossierPer.Dossier_entier_clicked = True
    End Sub

End Class