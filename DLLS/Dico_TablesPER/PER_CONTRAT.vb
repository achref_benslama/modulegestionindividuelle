﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CONTRAT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsTypeContrat As String
        Private WsObjetContrat As String
        Private WsCollege As String
        Private WsQualification As String
        Private WsPourcentageAnciennete As Integer = 0
        Private WsMotifRecrutement As String
        Private WsMotifFinContrat As String
        Private WsObservations As String
        Private WsCoefficientHie As Integer
        Private WsMode_de_remuneration As String
        Private WsRegime_de_cotisation As String
        Private WsDateCartePresse As String
        Private WsConvention As String
        Private WsBulletinModele As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CONTRAT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaStatut
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 16
            End Get
        End Property

        Public Property Type_de_Contrat() As String
            Get
                Return WsTypeContrat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeContrat = value
                    Case Else
                        WsTypeContrat = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsTypeContrat
            End Set
        End Property

        Public Property Objet_du_Contrat() As String
            Get
                Return WsObjetContrat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsObjetContrat = value
                    Case Else
                        WsObjetContrat = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property College() As String
            Get
                Return WsCollege
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCollege = value
                    Case Else
                        WsCollege = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Qualification() As String
            Get
                Return WsQualification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsQualification = value
                    Case Else
                        WsQualification = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Pourcentage_Prime_Anciennete() As Integer
            Get
                Return WsPourcentageAnciennete
            End Get
            Set(ByVal value As Integer)
                WsPourcentageAnciennete = value
            End Set
        End Property

        Public Property MotifRecrutement() As String
            Get
                Return WsMotifRecrutement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifRecrutement = value
                    Case Else
                        WsMotifRecrutement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property MotifFindeContrat() As String
            Get
                Return WsMotifFinContrat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifFinContrat = value
                    Case Else
                        WsMotifFinContrat = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property CoefficentHierarchique() As Integer
            Get
                Return WsCoefficientHie
            End Get
            Set(ByVal value As Integer)
                WsCoefficientHie = value
            End Set
        End Property

        Public Property Mode_de_Remuneration() As String
            Get
                Return WsMode_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMode_de_remuneration = value
                    Case Else
                        WsMode_de_remuneration = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Regime_de_Cotisation() As String
            Get
                Return WsRegime_de_cotisation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegime_de_cotisation = value
                    Case Else
                        WsRegime_de_cotisation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_de_CartePresse() As String
            Get
                Return WsDateCartePresse
            End Get
            Set(ByVal value As String)
                WsDateCartePresse = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property ConventionCollective() As String
            Get
                Return WsConvention
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsConvention = value
                    Case Else
                        WsConvention = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property BulletinModele() As String
            Get
                Return WsBulletinModele
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsBulletinModele = value
                    Case Else
                        WsBulletinModele = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Type_de_Contrat & VI.Tild)
                Chaine.Append(Objet_du_Contrat & VI.Tild)
                Chaine.Append(College & VI.Tild)
                Chaine.Append(Qualification & VI.Tild)
                Chaine.Append(Pourcentage_Prime_Anciennete.ToString & VI.Tild)
                Chaine.Append(MotifRecrutement & VI.Tild)
                Chaine.Append(MotifFindeContrat & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(CoefficentHierarchique & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Mode_de_Remuneration & VI.Tild)
                Chaine.Append(Regime_de_Cotisation & VI.Tild)
                Chaine.Append(Date_de_CartePresse & VI.Tild)
                Chaine.Append(ConventionCollective & VI.Tild)
                Chaine.Append(BulletinModele & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Type_de_Contrat = TableauData(2)
                Objet_du_Contrat = TableauData(3)
                College = TableauData(4)
                Qualification = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                Pourcentage_Prime_Anciennete = CInt(TableauData(6))
                MotifRecrutement = TableauData(7)
                MotifFindeContrat = TableauData(8)
                Observations = TableauData(9)
                If TableauData(10) = "" Then TableauData(10) = "0"
                CoefficentHierarchique = CInt(TableauData(10))
                MyBase.Date_de_Fin = TableauData(11)
                Mode_de_Remuneration = TableauData(12)
                Regime_de_Cotisation = TableauData(13)
                Date_de_CartePresse = TableauData(14)
                ConventionCollective = TableauData(15)
                BulletinModele = TableauData(16)
                MyBase.Certification = TableauData(17)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

