﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_EMPLOI
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsEmploiConventionnel As String
        Private WsSalaireMensuel As Double = 0
        Private WsSalaireAnnuel As Double = 0
        Private WsNombredePoints As Double = 0
        Private WsGroupeEmploi As String
        Private WsNombredeMoisSalaire As Double = 0
        Private WsEchelle_de_remuneration As String
        Private WsMotifEvolution As String
        Private WsDuree As String
        Private WsObservations As String
        Private WsFiliere As String
        Private WsCoefficientBase As Double = 0
        Private WsCategorie As String
        Private WsMonnaie As String
        Private WsResponsable As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_EMPLOI"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaGrade 'Spécifique Secteur Privé
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 16
            End Get
        End Property

        Public Property EmploiConventionnel() As String
            Get
                Return WsEmploiConventionnel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsEmploiConventionnel = value
                    Case Else
                        WsEmploiConventionnel = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsEmploiConventionnel
            End Set
        End Property

        Public Property SalaireMensuel() As Double
            Get
                Return WsSalaireMensuel
            End Get
            Set(ByVal value As Double)
                WsSalaireMensuel = value
            End Set
        End Property

        Public Property SalaireAnnuel() As Double
            Get
                Return WsSalaireAnnuel
            End Get
            Set(ByVal value As Double)
                WsSalaireAnnuel = value
            End Set
        End Property

        Public Property NombredePoints() As Double
            Get
                Return WsNombredePoints
            End Get
            Set(ByVal value As Double)
                WsNombredePoints = value
            End Set
        End Property

        Public Property GroupedEmploi() As String
            Get
                Return WsGroupeEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGroupeEmploi = value
                    Case Else
                        WsGroupeEmploi = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property NombredeMoisdeSalaire() As Double
            Get
                Return WsNombredeMoisSalaire
            End Get
            Set(ByVal value As Double)
                WsNombredeMoisSalaire = value
            End Set
        End Property

        Public Property Echelle_de_remuneration() As String
            Get
                Return WsEchelle_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEchelle_de_remuneration = value
                    Case Else
                        WsEchelle_de_remuneration = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property MotifdEvolution() As String
            Get
                Return WsMotifEvolution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifEvolution = value
                    Case Else
                        WsMotifEvolution = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Duree() As String
            Get
                Return WsDuree
            End Get
            Set(ByVal value As String)
                WsDuree = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property CoefficientdeBase() As Double
            Get
                Return WsCoefficientBase
            End Get
            Set(ByVal value As Double)
                WsCoefficientBase = value
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Monnaie() As String
            Get
                Return WsMonnaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsMonnaie = value
                    Case Else
                        WsMonnaie = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Responsable_Remuneration() As String
            Get
                Return WsResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsResponsable = value
                    Case Else
                        WsResponsable = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(EmploiConventionnel & VI.Tild)
                Chaine.Append(SalaireMensuel.ToString & VI.Tild)
                Chaine.Append(SalaireAnnuel.ToString & VI.Tild)
                Chaine.Append(NombredePoints.ToString & VI.Tild)
                Chaine.Append(GroupedEmploi & VI.Tild)
                Chaine.Append(NombredeMoisdeSalaire.ToString & VI.Tild)
                Chaine.Append(Echelle_de_remuneration & VI.Tild)
                Chaine.Append(MotifdEvolution & VI.Tild)
                Chaine.Append(VI.Tild) 'Durée
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(CoefficientdeBase.ToString & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Monnaie & VI.Tild)
                Chaine.Append(Responsable_Remuneration & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                EmploiConventionnel = TableauData(2)

                If TableauData(3) <> "" Then SalaireMensuel = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then SalaireAnnuel = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then NombredePoints = VirRhFonction.ConversionDouble(TableauData(5))

                GroupedEmploi = TableauData(6)

                If TableauData(7) <> "" Then NombredeMoisdeSalaire = VirRhFonction.ConversionDouble(TableauData(7))

                Echelle_de_remuneration = TableauData(8)
                MotifdEvolution = TableauData(9)
                Duree = ""
                Observations = TableauData(11)
                Filiere = TableauData(12)

                If TableauData(13) <> "" Then CoefficientdeBase = VirRhFonction.ConversionDouble(TableauData(13))

                Categorie = TableauData(14)
                Monnaie = TableauData(15)
                Responsable_Remuneration = TableauData(16)
                MyBase.Certification = TableauData(17)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property
        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

