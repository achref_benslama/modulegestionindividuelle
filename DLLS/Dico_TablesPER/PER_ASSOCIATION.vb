﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ASSOCIATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAssociation As String
        Private WsDate_d_Entree As String
        Private WsDate_de_depart As String
        Private WsMotif_de_depart As String
        Private WsRegion As String
        Private WsDepartement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ASSOCIATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 25 'Spécifique CCCA - BTP
            End Get
        End Property

        Public Property Association() As String
            Get
                Return WsAssociation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsAssociation = value
                    Case Else
                        WsAssociation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Date_d_Entree() As String
            Get
                Return WsDate_d_Entree
            End Get
            Set(ByVal value As String)
                WsDate_d_Entree = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_depart() As String
            Get
                Return WsDate_de_depart
            End Get
            Set(ByVal value As String)
                WsDate_de_depart = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = WsDate_de_depart
            End Set
        End Property

        Public Property Motif_de_depart() As String
            Get
                Return WsMotif_de_depart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotif_de_depart = value
                    Case Else
                        WsMotif_de_depart = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Region() As String
            Get
                Return WsRegion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegion = value
                    Case Else
                        WsRegion = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Departement() As String
            Get
                Return WsDepartement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDepartement = value
                    Case Else
                        WsDepartement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Association & VI.Tild)
                Chaine.Append(Date_d_Entree & VI.Tild)
                Chaine.Append(Date_de_depart & VI.Tild)
                Chaine.Append(Motif_de_depart & VI.Tild)
                Chaine.Append(Region & VI.Tild)
                Chaine.Append(Departement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Association = TableauData(1)
                Date_d_Entree = TableauData(2)
                Date_de_depart = TableauData(3)
                Motif_de_depart = TableauData(4)
                Region = TableauData(5)
                Departement = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



