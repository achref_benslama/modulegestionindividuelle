﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VSaisieCalendrier.ascx.vb" Inherits="Virtualia.Net.VSaisieCalendrier" %>

    <asp:Table ID="VCalendrier" runat="server" Height="322px" Width="268px" CellPadding="0" CellSpacing="0" BackColor="#5E9598"
            BorderStyle="Ridge" BorderColor="#B0E0D7" BorderWidth="4px" style="margin-top: 5px">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreTitre" runat="server">
                    <asp:TableRow>
                        <asp:TableCell ID="CellCalTitre" Visible="true">
                            <asp:Label ID="EtiTitre" runat="server" Height="20px" Width="220px" BackColor="#8DA8A3" BorderColor="#B0E0D7"
                                    ForeColor="#E9FDF9" BorderStyle="NotSet" BorderWidth="1px" Text="Calendrier Virtualia"
                                    Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                    style="margin-top: 3px; margin-left: 2px; text-indent:1px; text-align: center; vertical-align: top" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="right">
                            <asp:Button ID="CmdCancel" runat="server" Height="20px" Width="20px" BackColor="Transparent" ForeColor="Black"
                                BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" Text=" x" ToolTip="Fermer"
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false"
                                style="margin-top: 0px; text-indent:0px; text-align: center; vertical-align: middle" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left"  Height="22px">
                <asp:Table ID="CadreLstAnnee" runat="server" CellPadding="1" CellSpacing="0" > 
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="EtiLstAnnee" runat="server" Height="20px" Width="65px" Text="Année"
                                    BackColor="#8DA8A3" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                    BorderWidth="2px" ForeColor="#E9FDF9" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top:0px; margin-left:2px; font-style:oblique; text-indent:5px; text-align:center">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="CalLstAnnee" runat="server" Height="20px" Width="88px" AutoPostBack="true" BackColor="#EEECFD" ForeColor="#142425"
                            style="border-color: #B0E0D7; border-width: 2px; border-style: inset; display: table-cell; font-style: oblique;" >
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell Width="90px" HorizontalAlign="right" Visible="false">
                            <asp:Button ID="CmdOK" runat="server" Height="20px" Width="30px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="OK"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: center; vertical-align: middle" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left" Height="22px">
                <asp:Table ID="CadreLstMois" runat="server" CellPadding="1" CellSpacing="0" > 
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="EtiLstMois" runat="server" Height="20px" Width="65px" Text="Mois"
                                    BackColor="#8DA8A3" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                    BorderWidth="2px" ForeColor="#E9FDF9" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top:0px; margin-left:2px; font-style:oblique; text-indent:5px; text-align:center">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="CalLstMois" runat="server" Height="20px" Width="178px" AutoPostBack="true" BackColor="#EEECFD" ForeColor="#142425"
                            style="border-color: #B0E0D7; border-width: 2px; border-style: inset; display: table-cell; font-style: oblique;" >
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><div style="height: 3px; width: 175px"></div></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalLibelles" runat="server" Height="20px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="EtiLundi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiMardi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiMercredi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiJeudi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiVendredi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiSamedi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="EtiDimanche" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent: 1px; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><div style="height: 3px; width: 175px"></div></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine00" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM00" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM01" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM02" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM03" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM04" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM05" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM06" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM00" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM01" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM02" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM03" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM04" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM05" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM06" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine01" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM07" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM08" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM09" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM10" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM11" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM12" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM13" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM07" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM08" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM09" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM10" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM11" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM12" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM13" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine02" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM14" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM15" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM16" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM17" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM18" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM19" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM20" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM14" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM15" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM16" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM17" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM18" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM19" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM20" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine03" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM21" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM22" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM23" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM24" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM25" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM26" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM27" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM21" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM22" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM23" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM24" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM25" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM26" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM27" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine04" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM28" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM29" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM30" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM31" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM32" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM33" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM34" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM28" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM29" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM30" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM31" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM32" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM33" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM34" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CalSemaine05" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                            BorderStyle="None" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalAM35" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalAM36" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="CalDateSel00" runat="server" Height="17px" Width="178px" ForeColor="White"
                                    BorderStyle="Solid" BorderColor="#8DA8A3" BorderWidth="1px" BackColor="#124545"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-top: 1px; text-indent:-3px; text-align: center; vertical-align: top; border-bottom-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="CalPM35" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CalPM36" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Label ID="CalDateSel01" runat="server" Height="16px" Width="178px" ForeColor="White"
                                    BorderStyle="Solid" BorderColor="#8DA8A3" BorderWidth="1px" BackColor="#124545"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                    style="margin-bottom: 1px; text-indent:-3px; text-align: center; vertical-align: top; border-top-style: none" />
                            </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CalRadio" Height="60px" Visible="false">
                <asp:table ID="CadreOptionJour" runat="server" CellPadding="1" CellSpacing="0">
                     <asp:TableRow>
                         <asp:TableCell>
                             <asp:RadioButton ID="RadioV0" runat="server" Text="Le matin" Visible="true"
                                  AutoPostBack="true" GroupName="GroupeJour" ForeColor="#E9FDF9" Height="19px" Width="248px"
                                  BackColor="#8DA8A3" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                                  Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                                  text-indent: 5px; text-align: left; vertical-align: middle" />
                         </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                         <asp:TableCell>
                             <asp:RadioButton ID="RadioV1" runat="server" Text="Toute la journée" Checked="true"
                                  AutoPostBack="true" GroupName="GroupeJour" ForeColor="#E9FDF9" Height="19px" Width="248px"
                                  BackColor="#8DA8A3" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                                  Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                                  text-indent: 5px; text-align: left; vertical-align: middle" />
                        </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                         <asp:TableCell>
                             <asp:RadioButton ID="RadioV2" runat="server" Text="L'aprés-midi" 
                                  AutoPostBack="true" GroupName="GroupeJour" ForeColor="#E9FDF9" Height="19px" Width="248px"
                                  BackColor="#8DA8A3" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                                  Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                  style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                                  text-indent: 5px; text-align: left; vertical-align: middle" />
                         </asp:TableCell>
                    </asp:TableRow>
                </asp:table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Height="2px"></asp:TableCell>
        </asp:TableRow>
    </asp:Table>

