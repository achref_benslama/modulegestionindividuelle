﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace Session
    Public Class ObjetGlobal
        Inherits Virtualia.Net.Session.ObjetGlobalBase
        Private WsEnsemblePER As Virtualia.Net.Individuel.EnsembleDossiers = Nothing
        Private WsListeContextes As List(Of Virtualia.Net.Individuel.LocalNavigation) = Nothing

        Public ReadOnly Property ContexteSession(ByVal NoSession As String) As Virtualia.Net.Individuel.LocalNavigation
            Get
                Dim SessionUti As Virtualia.Net.Session.ObjetSession = MyBase.ItemSessionModule(NoSession)
                Dim ContexteUti As Virtualia.Net.Individuel.LocalNavigation

                If SessionUti Is Nothing Then
                    Return Nothing
                End If
                If WsListeContextes Is Nothing Then
                    WsListeContextes = New List(Of Virtualia.Net.Individuel.LocalNavigation)
                End If
                Try
                    ContexteUti = (From instance In WsListeContextes Select instance Where instance.VParent.V_IDSession = NoSession).First
                Catch ex As Exception
                    ContexteUti = Nothing
                End Try
                If ContexteUti Is Nothing Then
                    ContexteUti = New Virtualia.Net.Individuel.LocalNavigation(SessionUti)
                    WsListeContextes.Add(ContexteUti)
                End If
                Return ContexteUti
            End Get
        End Property
        Public ReadOnly Property EnsemblePER As Virtualia.Net.Individuel.EnsembleDossiers
            Get
                If WsEnsemblePER Is Nothing Then
                    Call Reinitialiser()
                    WsEnsemblePER = New Virtualia.Net.Individuel.EnsembleDossiers(Me)
                End If
                Return WsEnsemblePER
            End Get
        End Property

        Public Sub New(ByVal NomUti As String, ByVal NoBd As Integer)
            MyBase.New(NomUti, NoBd)
            MyBase.VirNomModule = "GestionIndividuelle"
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace