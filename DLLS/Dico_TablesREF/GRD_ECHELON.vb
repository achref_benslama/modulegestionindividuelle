Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_ECHELON
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIndex As Integer = 0
        Private WsIndiceBrut As String
        Private WsIndiceMajore As String
        Private WsDureeMini As String
        Private WsDureeMaxi As String
        Private WsDureeMoyenne As String
        Private WsChevron As String
        Private WsAliasEchelon As String
        Private WsIndicateurHE As String
        Private WsGroupeResidence As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_ECHELON"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrilles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property IndexListe() As Integer
            Get
                Return WsIndex
            End Get
            Set(ByVal value As Integer)
                WsIndex = value
            End Set
        End Property

        Public Property IndiceBrut() As String
            Get
                Return WsIndiceBrut
            End Get
            Set(ByVal value As String)
                WsIndiceBrut = value
            End Set
        End Property

        Public Property IndiceMajore() As String
            Get
                Return WsIndiceMajore
            End Get
            Set(ByVal value As String)
                WsIndiceMajore = value
            End Set
        End Property

        Public Property DureeMini() As String
            Get
                Return WsDureeMini
            End Get
            Set(ByVal value As String)
                WsDureeMini = value
            End Set
        End Property

        Public Property DureeMaxi() As String
            Get
                Return WsDureeMaxi
            End Get
            Set(ByVal value As String)
                WsDureeMaxi = value
            End Set
        End Property

        Public Property DureeMoyenne() As String
            Get
                Return WsDureeMoyenne
            End Get
            Set(ByVal value As String)
                WsDureeMoyenne = value
            End Set
        End Property

        Public Property Chevron() As String
            Get
                Return WsChevron
            End Get
            Set(ByVal value As String)
                WsChevron = value
            End Set
        End Property

        Public Property AliasEchelon() As String
            Get
                If WsAliasEchelon = "" And IndiceMajore <> "" Then
                    Return WsIndex.ToString
                Else
                    Return WsAliasEchelon
                End If
            End Get
            Set(ByVal value As String)
                WsAliasEchelon = value
            End Set
        End Property

        Public Property Indicateur_HE() As String
            Get
                Return WsIndicateurHE
            End Get
            Set(ByVal value As String)
                WsIndicateurHE = value
            End Set
        End Property

        Public Property GroupeResidence() As String
            Get
                Return WsGroupeResidence
            End Get
            Set(ByVal value As String)
                WsGroupeResidence = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(IndiceBrut & VI.Virgule)
                Chaine.Append(IndiceMajore & VI.Virgule)
                Chaine.Append(DureeMini & VI.Virgule)
                Chaine.Append(DureeMaxi & VI.Virgule)
                Chaine.Append(DureeMoyenne & VI.Virgule)
                Chaine.Append(Chevron & VI.Virgule)
                Chaine.Append(AliasEchelon & VI.Virgule)
                Chaine.Append(Indicateur_HE & VI.Virgule)
                Chaine.Append(GroupeResidence & VI.Virgule)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Virgule, -1)
                If TableauData.Count < 9 Then
                    ReDim Preserve TableauData(8)
                End If
                IndiceBrut = TableauData(0)
                IndiceMajore = TableauData(1)
                DureeMini = TableauData(2)
                DureeMaxi = TableauData(3)
                DureeMoyenne = TableauData(4)
                Chevron = TableauData(5)
                AliasEchelon = TableauData(6)
                Indicateur_HE = TableauData(7)
                GroupeResidence = TableauData(8)

                WsFicheLue = New StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Virgule)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property V_TableauData() As ArrayList
            Get
                Dim TableauW(0) As String
                Dim I As Integer
                Dim TabArray As New ArrayList
                TableauW = Strings.Split(ContenuTable, VI.Virgule, -1)
                For I = 0 To TableauW.Count - 1
                    TabArray.Add(TableauW(I))
                Next I
                Return TabArray
            End Get
            Set(ByVal value As ArrayList)
                Dim Chaine As New StringBuilder
                Dim I As Integer
                For I = 0 To value.Count - 1
                    Chaine.Append(value(I).ToString & VI.Virgule)
                Next I
                ContenuTable = Chaine.ToString
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

    End Class
End Namespace
