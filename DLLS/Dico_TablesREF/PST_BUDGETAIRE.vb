﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PST_BUDGETAIRE
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsPostebudgetaire As String
        Private WsIntitule As String
        Private WsCategorie As String
        Private WsFiliere As String
        Private WsGrade As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PST_BUDGETAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteFct
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Postebudgetaire() As String
            Get
                Return WsPostebudgetaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsPostebudgetaire = value
                    Case Else
                        WsPostebudgetaire = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Grade() As String
            Get
                Return WsGrade
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGrade = value
                    Case Else
                        WsGrade = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Postebudgetaire & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Grade)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Postebudgetaire = TableauData(2)
                Intitule = TableauData(3)
                Categorie = TableauData(4)
                Filiere = TableauData(5)
                Grade = TableauData(6)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
