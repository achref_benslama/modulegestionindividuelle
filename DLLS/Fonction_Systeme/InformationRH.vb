Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Donnees
        Public Class Information
            Private ReadOnly ObjetVirtualia As Virtualia.Systeme.MetaModele.Donnees.Objet
            Private WsIndex As Integer
            Private WsPointdeVue As Integer = 0
            Private WsObjet As Integer = 0
            Private WsNumero As Integer = 0
            Private WsIntitule As String
            Private WsNature As Integer = 0
            Private WsFormat As Integer = 0
            Private WsIndexSecondaire As Integer = 0
            Private WsClassement As Integer = 0
            Private WsRegleUnicite As Integer = 0
            Private WsNomInterne As String
            Private WsEtiquette As String
            Private WsEtiCadrage As Integer = 0
            Private WsPvueTable As Integer = 0
            Private WsTableReference As String
            Private WsDefault As String
            Private WsLgMaxi As Integer = 0
            Private WsLgSGBDR As Integer = 0
            Private WsZoneEdition(10) As Integer

            Public ReadOnly Property VParent() As Virtualia.Systeme.MetaModele.Donnees.Objet
                Get
                    Return ObjetVirtualia
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property Objet() As Integer
                Get
                    Return WsObjet
                End Get
                Set(ByVal value As Integer)
                    WsObjet = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public Property VNature() As Integer
                Get
                    Return WsNature
                End Get
                Set(ByVal value As Integer)
                    WsNature = value
                End Set
            End Property

            Public Property IndexSecondaire() As Integer
                Get
                    Return WsIndexSecondaire
                End Get
                Set(ByVal value As Integer)
                    WsIndexSecondaire = value
                End Set
            End Property

            Public Property Classement() As Integer
                Get
                    Return WsClassement
                End Get
                Set(ByVal value As Integer)
                    WsClassement = value
                End Set
            End Property

            Public Property RegleUnicite() As Integer
                Get
                    Return WsRegleUnicite
                End Get
                Set(ByVal value As Integer)
                    WsRegleUnicite = value
                End Set
            End Property

            Public Property Etiquette() As String
                Get
                    Return WsEtiquette
                End Get
                Set(ByVal value As String)
                    WsEtiquette = value
                End Set
            End Property

            Public Property NomInterne() As String
                Get
                    Return WsNomInterne
                End Get
                Set(ByVal value As String)
                    WsNomInterne = value
                End Set
            End Property

            Public Property Format() As Integer
                Get
                    Return WsFormat
                End Get
                Set(ByVal value As Integer)
                    WsFormat = value
                End Set
            End Property

            Public Property CadrageLabel() As Integer
                Get
                    Return WsEtiCadrage
                End Get
                Set(ByVal value As Integer)
                    WsEtiCadrage = value
                End Set
            End Property

            Public Property PointdeVueInverse() As Integer
                Get
                    Return WsPvueTable
                End Get
                Set(ByVal value As Integer)
                    WsPvueTable = value
                End Set
            End Property

            Public Property TabledeReference() As String
                Get
                    Return WsTableReference
                End Get
                Set(ByVal value As String)
                    WsTableReference = value
                End Set
            End Property

            Public Property ValeurDefaut() As String
                Get
                    Return WsDefault
                End Get
                Set(ByVal value As String)
                    WsDefault = value
                End Set
            End Property

            Public Property LongueurMaxi() As Integer
                Get
                    Return WsLgMaxi
                End Get
                Set(ByVal value As Integer)
                    WsLgMaxi = value
                End Set
            End Property

            Public Property LongueurSGBDR() As Integer
                Get
                    Return WsLgSGBDR
                End Get
                Set(ByVal value As Integer)
                    WsLgSGBDR = value
                End Set
            End Property

            Public Property ZoneEdition(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsZoneEdition.Count - 1
                            Return WsZoneEdition(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case 0 To WsZoneEdition.Count - 1
                            Select Case value
                                Case 0
                                    WsZoneEdition(Index) = 0
                                Case Else
                                    WsZoneEdition(Index) = 1
                            End Select
                    End Select
                End Set
            End Property

            Public Sub New(ByVal host As Virtualia.Systeme.MetaModele.Donnees.Objet)
                ObjetVirtualia = host
            End Sub
        End Class
    End Namespace
End Namespace
