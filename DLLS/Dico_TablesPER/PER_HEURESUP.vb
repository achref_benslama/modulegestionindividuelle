﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_HEURESUP
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '**** Secteur Public Heures rémunérées ****************************
        '** Tranche 1 = Jour - 14 Premières heures
        '** Tranche 2 = Jour - au delà des 14 premières heures (en principe Maxi 25)
        '** Tranche 3 = Dimanche et JF 14 premières heures
        '** Tranche 4 = Nuit - 14 premières heures
        '** Tranche 5 = Dimanche et JF - au delà des 14 premières heures
        '** Tranche 6 = Nuit - au delà des 14 premières heures
        '****
        Private WsTranche1_NbHeures As Double
        Private WsTranche1_Taux As Double
        Private WsTranche1_Montant As Double
        Private WsTranche2_NbHeures As Double
        Private WsTranche2_Taux As Double
        Private WsTranche2_Montant As Double
        Private WsTranche3_NbHeures As Double
        Private WsTranche3_Taux As Double
        Private WsTranche3_Montant As Double
        Private WsTranche4_NbHeures As Double
        Private WsTranche4_Taux As Double
        Private WsTranche4_Montant As Double
        Private WsMontantTotal As Double
        Private WsDate_Certification As String
        Private WsCommentaire As String
        Private WsTranche5_NbHeures As Double
        Private WsTranche5_Taux As Double
        Private WsTranche5_Montant As Double
        Private WsTranche6_NbHeures As Double
        Private WsTranche6_Taux As Double
        Private WsTranche6_Montant As Double
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_HEURESUP"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaHeuresSup
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 23
            End Get
        End Property

        Public Property Tranche1_Nombre() As Double
            Get
                Return WsTranche1_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche1_NbHeures = value
                MyBase.Clef = WsTranche1_NbHeures.ToString
            End Set
        End Property

        Public Property Tranche1_Taux() As Double
            Get
                Return WsTranche1_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche1_Taux = value
            End Set
        End Property

        Public Property Tranche1_Montant() As Double
            Get
                Return WsTranche1_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche1_Montant = value
            End Set
        End Property

        Public Property Tranche2_Nombre() As Double
            Get
                Return WsTranche2_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche2_NbHeures = value
            End Set
        End Property

        Public Property Tranche2_Taux() As Double
            Get
                Return WsTranche2_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche2_Taux = value
            End Set
        End Property

        Public Property Tranche2_Montant() As Double
            Get
                Return WsTranche2_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche2_Montant = value
            End Set
        End Property

        Public Property Tranche3_Nombre() As Double
            Get
                Return WsTranche3_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche3_NbHeures = value
            End Set
        End Property

        Public Property Tranche3_Taux() As Double
            Get
                Return WsTranche3_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche3_Taux = value
            End Set
        End Property

        Public Property Tranche3_Montant() As Double
            Get
                Return WsTranche3_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche3_Montant = value
            End Set
        End Property

        Public Property Tranche4_Nombre() As Double
            Get
                Return WsTranche4_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche4_NbHeures = value
            End Set
        End Property

        Public Property Tranche4_Taux() As Double
            Get
                Return WsTranche4_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche4_Taux = value
            End Set
        End Property

        Public Property Tranche4_Montant() As Double
            Get
                Return WsTranche4_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche4_Montant = value
            End Set
        End Property

        Public Property Tranche5_Nombre() As Double
            Get
                Return WsTranche5_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche5_NbHeures = value
            End Set
        End Property

        Public Property Tranche5_Taux() As Double
            Get
                Return WsTranche5_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche5_Taux = value
            End Set
        End Property

        Public Property Tranche5_Montant() As Double
            Get
                Return WsTranche5_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche5_Montant = value
            End Set
        End Property

        Public Property Tranche6_Nombre() As Double
            Get
                Return WsTranche5_NbHeures
            End Get
            Set(ByVal value As Double)
                WsTranche5_NbHeures = value
            End Set
        End Property

        Public Property Tranche6_Taux() As Double
            Get
                Return WsTranche5_Taux
            End Get
            Set(ByVal value As Double)
                WsTranche5_Taux = value
            End Set
        End Property

        Public Property Tranche6_Montant() As Double
            Get
                Return WsTranche5_Montant
            End Get
            Set(ByVal value As Double)
                WsTranche5_Montant = value
            End Set
        End Property

        Public Property MontantTotal() As Double
            Get
                Return WsMontantTotal
            End Get
            Set(ByVal value As Double)
                WsMontantTotal = value
            End Set
        End Property

        Public Property Date_Certification() As String
            Get
                Return WsDate_Certification
            End Get
            Set(ByVal value As String)
                WsDate_Certification = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Tranche1_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche1_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche1_Montant.ToString & VI.Tild)
                Chaine.Append(Tranche2_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche2_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche2_Montant.ToString & VI.Tild)
                Chaine.Append(Tranche3_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche3_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche3_Montant.ToString & VI.Tild)
                Chaine.Append(Tranche4_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche4_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche4_Montant.ToString & VI.Tild)
                Chaine.Append(MontantTotal.ToString & VI.Tild)
                Chaine.Append(Date_Certification & VI.Tild)
                Chaine.Append(Commentaire & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Tranche5_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche5_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche5_Montant.ToString & VI.Tild)
                Chaine.Append(Tranche6_Nombre.ToString & VI.Tild)
                Chaine.Append(Tranche6_Taux.ToString & VI.Tild)
                Chaine.Append(Tranche6_Montant.ToString & VI.Tild)
                Chaine.Append(MyBase.Certification & VI.Tild)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 24 Then
                    Exit Property
                End If
                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Tranche1_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(2)), 2)
                Tranche1_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(3)), 4)
                Tranche1_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(4)), 2)
                Tranche2_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(5)), 2)
                Tranche2_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(6)), 4)
                Tranche2_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(7)), 2)
                Tranche3_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(8)), 2)
                Tranche3_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(9)), 4)
                Tranche3_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(10)), 2)
                Tranche4_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(11)), 2)
                Tranche4_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(12)), 4)
                Tranche4_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(13)), 2)
                MontantTotal = Math.Round(VirRhFonction.ConversionDouble(TableauData(14)), 2)
                Date_Certification = TableauData(15)
                Commentaire = TableauData(16)
                MyBase.Date_de_Fin = TableauData(17)
                Tranche5_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(18)), 2)
                Tranche5_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(19)), 4)
                Tranche5_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(20)), 2)
                Tranche6_Nombre = Math.Round(VirRhFonction.ConversionDouble(TableauData(21)), 2)
                Tranche6_Taux = Math.Round(VirRhFonction.ConversionDouble(TableauData(22)), 4)
                Tranche6_Montant = Math.Round(VirRhFonction.ConversionDouble(TableauData(23)), 2)
                MyBase.Certification = TableauData(24)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public WriteOnly Property Indice_SecteurPublic(ByVal ValAnnuelleI100 As Double, ByVal TauxResidence As Double, _
                                                       ByVal IndicePlancher As Integer) As Integer
            Set(ByVal value As Integer)
                Dim BaseHoraire As Double
                Dim ValZone As Double

                If value < IndicePlancher Then
                    ValZone = IndicePlancher * TauxResidence
                Else
                    ValZone = value * TauxResidence
                End If
                BaseHoraire = Math.Round(((ValZone * (ValAnnuelleI100 / 100)) + (value * (ValAnnuelleI100 / 100))) / 1820, 2)

                If Tranche1_Taux = 0 Then
                    Tranche1_Taux = Math.Round(BaseHoraire * 1.25, 4) '14 Premières heures Jour
                    If Tranche1_Nombre > 0 And Tranche1_Montant = 0 Then
                        Tranche1_Montant = Tranche1_Nombre * Tranche1_Taux
                    End If
                End If
                If Tranche2_Taux = 0 Then
                    Tranche2_Taux = Math.Round(BaseHoraire * 1.27, 4) 'de 14 à 25 Jour
                    If Tranche2_Nombre > 0 And Tranche2_Montant = 0 Then
                        Tranche2_Montant = Tranche2_Nombre * Tranche2_Taux
                    End If
                End If
                If Tranche3_Taux = 0 Then
                    Tranche3_Taux = Math.Round(BaseHoraire * 1.25 * (1 + (2 / 3)), 4) 'Dimanche et JF 14 premières heures
                    If Tranche3_Nombre > 0 And Tranche3_Montant = 0 Then
                        Tranche3_Montant = Tranche3_Nombre * Tranche3_Taux
                    End If
                End If
                If Tranche4_Taux = 0 Then
                    Tranche4_Taux = Math.Round(BaseHoraire * 1.25 * 2, 4) 'Nuit - 14 premières heures
                    If Tranche4_Nombre > 0 And Tranche4_Montant = 0 Then
                        Tranche4_Montant = Tranche4_Nombre * Tranche4_Taux
                    End If
                End If
                If Tranche5_Taux = 0 Then
                    Tranche5_Taux = Math.Round(BaseHoraire * 1.27 * (1 + (2 / 3)), 4) 'Dimanche et JF - au delà des 14 premières heures
                    If Tranche5_Nombre > 0 And Tranche5_Montant = 0 Then
                        Tranche5_Montant = Tranche5_Nombre * Tranche5_Taux
                    End If
                End If
                If Tranche6_Taux = 0 Then
                    Tranche6_Taux = Math.Round(BaseHoraire * 1.27 * 2, 4) 'Nuit - au delà des 14 premières heures
                    If Tranche6_Nombre > 0 And Tranche6_Montant = 0 Then
                        Tranche6_Montant = Tranche6_Nombre * Tranche6_Taux
                    End If
                End If
                MontantTotal = Tranche1_Montant + Tranche2_Montant + Tranche3_Montant + Tranche4_Montant + Tranche5_Montant + Tranche6_Montant
            End Set
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateDebutHS>" & MyBase.Date_de_Valeur & "</DateDebutHS>" & vbCrLf)
                Chaine.Append("<DateFinHS>" & MyBase.Date_de_Fin & "</DateFinHS>" & vbCrLf)
                Chaine.Append("<DateCertificationHS>" & Date_Certification & "</DateCertificationHS>" & vbCrLf)
                Chaine.Append("<Tranche1_Nombre>" & Tranche1_Nombre.ToString & "</Tranche1_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche1_Taux>" & Tranche1_Taux.ToString & "</Tranche1_Taux>" & vbCrLf)
                Chaine.Append("<Tranche1_Montant>" & Tranche1_Montant.ToString & "</Tranche1_Montant>" & vbCrLf)
                Chaine.Append("<Tranche2_Nombre>" & Tranche2_Nombre.ToString & "</Tranche2_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche2_Taux>" & Tranche2_Taux.ToString & "</Tranche2_Taux>" & vbCrLf)
                Chaine.Append("<Tranche2_Montant>" & Tranche2_Montant.ToString & "</Tranche2_Montant>" & vbCrLf)
                Chaine.Append("<Tranche3_Nombre>" & Tranche3_Nombre.ToString & "</Tranche3_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche3_Taux>" & Tranche3_Taux.ToString & "</Tranche3_Taux>" & vbCrLf)
                Chaine.Append("<Tranche3_Montant>" & Tranche3_Montant.ToString & "</Tranche3_Montant>" & vbCrLf)
                Chaine.Append("<Tranche4_Nombre>" & Tranche4_Nombre.ToString & "</Tranche4_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche4_Taux>" & Tranche4_Taux.ToString & "</Tranche4_Taux>" & vbCrLf)
                Chaine.Append("<Tranche4_Montant>" & Tranche4_Montant.ToString & "</Tranche4_Montant>" & vbCrLf)
                Chaine.Append("<Tranche5_Nombre>" & Tranche5_Nombre.ToString & "</Tranche5_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche5_Taux>" & Tranche5_Taux.ToString & "</Tranche5_Taux>" & vbCrLf)
                Chaine.Append("<Tranche5_Montant>" & Tranche5_Montant.ToString & "</Tranche5_Montant>" & vbCrLf)
                Chaine.Append("<Tranche6_Nombre>" & Tranche6_Nombre.ToString & "</Tranche6_Nombre>" & vbCrLf)
                Chaine.Append("<Tranche6_Taux>" & Tranche6_Taux.ToString & "</Tranche6_Taux>" & vbCrLf)
                Chaine.Append("<Tranche6_Montant>" & Tranche6_Montant.ToString & "</Tranche6_Montant>" & vbCrLf)
                Chaine.Append("<TotalHS>" & MontantTotal.ToString & "</TotalHS>" & vbCrLf)
                Chaine.Append("<CommentaireHS>" & VirRhFonction.ChaineXMLValide(Commentaire) & "</CommentaireHS>" & vbCrLf)

                Return Chaine.ToString
                
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(Tranche1_Nombre.ToString)
                LstGrid.Add(Tranche1_Taux.ToString)
                LstGrid.Add(Tranche1_Montant.ToString)
                LstGrid.Add(Tranche2_Nombre.ToString)
                LstGrid.Add(Tranche2_Taux.ToString)
                LstGrid.Add(Tranche2_Montant.ToString)
                LstGrid.Add(Tranche3_Nombre.ToString)
                LstGrid.Add(Tranche3_Taux.ToString)
                LstGrid.Add(Tranche3_Montant.ToString)
                LstGrid.Add(Tranche4_Nombre.ToString)
                LstGrid.Add(Tranche4_Taux.ToString)
                LstGrid.Add(Tranche4_Montant.ToString)
                LstGrid.Add(Tranche5_Nombre.ToString)
                LstGrid.Add(Tranche5_Taux.ToString)
                LstGrid.Add(Tranche5_Montant.ToString)
                LstGrid.Add(Tranche6_Nombre.ToString)
                LstGrid.Add(Tranche6_Taux.ToString)
                LstGrid.Add(Tranche6_Montant.ToString)
                LstGrid.Add(MontantTotal.ToString)
                LstGrid.Add(Date_Certification)
                LstGrid.Add(Commentaire)
                LstGrid.Add(MyBase.Date_de_Fin)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                MyBase.Date_de_Valeur = value(0)
                Tranche1_Nombre = Val(value(1))
                Tranche1_Taux = Val(value(2))
                Tranche1_Montant = Val(value(3))
                Tranche2_Nombre = Val(value(4))
                Tranche2_Taux = Val(value(5))
                Tranche2_Montant = Val(value(6))
                Tranche3_Nombre = Val(value(7))
                Tranche3_Taux = Val(value(8))
                Tranche3_Montant = Val(value(9))
                Tranche4_Nombre = Val(value(10))
                Tranche4_Taux = Val(value(11))
                Tranche4_Montant = Val(value(12))
                Tranche5_Nombre = Val(value(13))
                Tranche5_Taux = Val(value(14))
                Tranche5_Montant = Val(value(15))
                Tranche6_Nombre = Val(value(16))
                Tranche6_Taux = Val(value(17))
                Tranche6_Montant = Val(value(18))
                MontantTotal = Val(value(19))
                Date_Certification = value(20)
                Commentaire = value(21)
                MyBase.Date_de_Fin = value(22)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures jusqu'à 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux 14 1ères heures"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant 14 1ères heures"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures > 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux > 14 heures"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant > 14 heures"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures Dim. et JF jusqu'à 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux Dimanche et JF jusqu'à 14"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant Dimanche et JF jusqu'à 14"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures de nuit jusqu'à 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux heures de nuit jusqu'à 14"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant heures de nuit jusqu'à 14"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures Dim. et JF au delà de 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux Dimanche et JF au delà de 14"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant Dimanche et JF au delà de 14"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre d'heures de nuit au delà de 14"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Taux heures de nuit au delà de 14"
            InfoBase.Longueur = 7
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant heures de nuit au delà de 14"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant total"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de certification"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            InfoBase.SiVisible = False
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Observations"
            InfoBase.Longueur = 250
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de Fin"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
