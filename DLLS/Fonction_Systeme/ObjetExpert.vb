Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Expertes
        Public Class ObjetExpert
            Inherits List(Of InformationExperte)
            Private ReadOnly WsPvueVirtualia As PointdevueExpert
            Private WsIndex As Integer
            Private WsPointdeVue As Integer
            Private WsNumero As Integer

            Public ReadOnly Property VParent() As PointdevueExpert
                Get
                    Return WsPvueVirtualia
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public ReadOnly Property NombredInformations() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As InformationExperte
                Get
                    If (Index < 0 And Index >= Count) Then
                        Return Nothing
                    End If
                    Return Me(Index)
                End Get
            End Property

            Public Function IndexInformation(ByVal NumInfo As Integer) As Integer
                Dim info As InformationExperte = (From it As InformationExperte In Me Where it.Numero = NumInfo Select it).FirstOrDefault()
                If (info Is Nothing) Then
                    Return 0
                End If
                Return IndexOf(info)
            End Function

            Public Function CreerUneInformation() As Integer
                Dim ObjetInfo As InformationExperte = New InformationExperte(Me)
                Add(ObjetInfo)
                ObjetInfo.PointdeVue = PointdeVue
                ObjetInfo.Objet = Numero
                ObjetInfo.VIndex = Count - 1
                Return Count - 1
            End Function

            Public Overloads Sub Remove()
                If Me.Count > 0 Then
                    RemoveAt(Me.Count - 1)
                End If
            End Sub

            Public Sub New(ByVal host As PointdevueExpert)
                WsPvueVirtualia = host
            End Sub
        End Class
    End Namespace
End Namespace