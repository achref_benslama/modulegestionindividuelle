﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_GENERAL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsEvaluateur As String
        Private WsService As String
        Private WsDate_de_Signature_Evaluateur As String
        Private WsDate_de_Signature_Evalue As String
        Private WsDescriptifPoste As String
        Private WsEmploiType As String
        Private WsActivitesPrincipales As String
        Private WsSiEncadrement As String
        Private WsNombreEncadres As Integer
        Private WsCritereSynthese As Integer
        Private WsIntituleSynthese As String
        Private WsAcquisExperience As String
        Private WsLitteralEvaluateur As String
        Private WsSiSouhaitEntretienCarriere As String
        Private WsSiFichePosteAdaptee As String
        Private WsActualisationFichePoste As String
        Private WsAppreciationFichePoste As String
        Private WsAptitudesFonctionSup As String
        Private WsSiUtilisationDIF As String
        Private WsDate_de_Signature_Formation_Evalue As String
        Private WsDate_de_Signature_Formation_Evaluateur As String
        Private WsSiManagerOKMajFichePoste As String
        Private WsDate_de_Signature_Autorite_Hie As String
        Private WsIdeManager As Integer
        Private WsSiConduiteProjet As String
        Private WsNombreEncadres_Cat_A As Integer
        Private WsNombreEncadres_Cat_B As Integer
        Private WsNombreEncadres_Cat_C As Integer
        Private WsSiFormationUrgente As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_GENERAL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienPro
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 30
            End Get
        End Property

        Public Property Evaluateur() As String
            Get
                Return WsEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsEvaluateur = value
                    Case Else
                        WsEvaluateur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Entite_Evaluation() As String
            Get
                Return WsService
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsService = value
                    Case Else
                        WsService = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Date_de_Signature_Evaluateur() As String
            Get
                Return WsDate_de_Signature_Evaluateur
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Evaluateur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Evalue() As String
            Get
                Return WsDate_de_Signature_Evalue
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Evalue = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Descriptif_Poste() As String
            Get
                Return WsDescriptifPoste
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDescriptifPoste = value
                    Case Else
                        WsDescriptifPoste = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Emploi_Type() As String
            Get
                Return WsEmploiType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsEmploiType = value
                    Case Else
                        WsEmploiType = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property ActivitesPrincipales() As String
            Get
                Return WsActivitesPrincipales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsActivitesPrincipales = value
                    Case Else
                        WsActivitesPrincipales = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property SiEncadrement() As String
            Get
                Return WsSiEncadrement
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiEncadrement = "Non"
                    Case Else
                        WsSiEncadrement = "Oui"
                End Select
            End Set
        End Property

        Public Property Nombre_Encadres() As Integer
            Get
                Return WsNombreEncadres
            End Get
            Set(ByVal value As Integer)
                WsNombreEncadres = value
            End Set
        End Property

        Public Property Critere_Synthese() As Integer
            Get
                Return WsCritereSynthese
            End Get
            Set(ByVal value As Integer)
                WsCritereSynthese = value
            End Set
        End Property

        Public Property Intitule_Synthese() As String
            Get
                Return WsIntituleSynthese
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleSynthese = value
                    Case Else
                        WsIntituleSynthese = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property AcquisExperience() As String
            Get
                Return WsAcquisExperience
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsAcquisExperience = value
                    Case Else
                        WsAcquisExperience = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property LitteralEvaluateur() As String
            Get
                Return WsLitteralEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsLitteralEvaluateur = value
                    Case Else
                        WsLitteralEvaluateur = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property SiSouhait_EntretienCarriere() As String
            Get
                Return WsSiSouhaitEntretienCarriere
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiSouhaitEntretienCarriere = "Non"
                    Case Else
                        WsSiSouhaitEntretienCarriere = "Oui"
                End Select
            End Set
        End Property

        Public Property SiFichedePoste_Adaptee() As String
            Get
                Return WsSiFichePosteAdaptee
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiFichePosteAdaptee = "Non"
                    Case Else
                        WsSiFichePosteAdaptee = "Oui"
                End Select
            End Set
        End Property

        Public Property Actualisation_FichedePoste() As String
            Get
                Return WsActualisationFichePoste
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsActualisationFichePoste = value
                    Case Else
                        WsActualisationFichePoste = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Appreciation_FichedePoste() As String
            Get
                Return WsAppreciationFichePoste
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsAppreciationFichePoste = value
                    Case Else
                        WsAppreciationFichePoste = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Aptitudes_Fonctions_Superieures() As String
            Get
                Return WsAptitudesFonctionSup
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsAptitudesFonctionSup = value
                    Case Else
                        WsAptitudesFonctionSup = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property SiUtilisation_du_DIF() As String
            Get
                Return WsSiUtilisationDIF
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiUtilisationDIF = "Non"
                    Case Else
                        WsSiUtilisationDIF = "Oui"
                End Select
            End Set
        End Property

        Public Property Date_de_Signature_Formation_Evalue() As String
            Get
                Return WsDate_de_Signature_Formation_Evalue
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Formation_Evalue = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Formation_Evaluateur() As String
            Get
                Return WsDate_de_Signature_Formation_Evaluateur
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Formation_Evaluateur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiManagerOK_PourMajFichedePoste() As String
            Get
                Return WsSiManagerOKMajFichePoste
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiManagerOKMajFichePoste = "Non"
                    Case Else
                        WsSiManagerOKMajFichePoste = "Oui"
                End Select
            End Set
        End Property

        Public Property Date_de_Signature_Autorite_Hierarchique() As String
            Get
                Return WsDate_de_Signature_Autorite_Hie
            End Get
            Set(ByVal value As String)
                WsDate_de_Signature_Autorite_Hie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Identifiant_Manager As Integer
            Get
                Return WsIdeManager
            End Get
            Set(value As Integer)
                WsIdeManager = value
            End Set
        End Property

        Public Property SiConduite_Projet() As String
            Get
                Return WsSiConduiteProjet
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiConduiteProjet = "Non"
                    Case Else
                        WsSiConduiteProjet = "Oui"
                End Select
            End Set
        End Property

        Public Property Nombre_Encadres_Categorie_A() As Integer
            Get
                Return WsNombreEncadres_Cat_A
            End Get
            Set(ByVal value As Integer)
                WsNombreEncadres_Cat_A = value
            End Set
        End Property

        Public Property Nombre_Encadres_Categorie_B() As Integer
            Get
                Return WsNombreEncadres_Cat_B
            End Get
            Set(ByVal value As Integer)
                WsNombreEncadres_Cat_B = value
            End Set
        End Property

        Public Property Nombre_Encadres_Categorie_C() As Integer
            Get
                Return WsNombreEncadres_Cat_C
            End Get
            Set(ByVal value As Integer)
                WsNombreEncadres_Cat_C = value
            End Set
        End Property

        Public Property SiFormation_Urgente() As String
            Get
                Return WsSiFormationUrgente
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiFormationUrgente = "Non"
                    Case Else
                        WsSiFormationUrgente = "Oui"
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Evaluateur & VI.Tild)
                Chaine.Append(Entite_Evaluation & VI.Tild)
                Chaine.Append(Date_de_Signature_Evaluateur & VI.Tild)
                Chaine.Append(Date_de_Signature_Evalue & VI.Tild)
                Chaine.Append(Descriptif_Poste & VI.Tild)
                Chaine.Append(Emploi_Type & VI.Tild)
                Chaine.Append(ActivitesPrincipales & VI.Tild)
                Chaine.Append(SiEncadrement & VI.Tild)
                Chaine.Append(Nombre_Encadres.ToString & VI.Tild)
                Chaine.Append(Critere_Synthese.ToString & VI.Tild)
                Chaine.Append(Intitule_Synthese & VI.Tild)
                Chaine.Append(AcquisExperience & VI.Tild)
                Chaine.Append(LitteralEvaluateur & VI.Tild)
                Chaine.Append(SiSouhait_EntretienCarriere & VI.Tild)
                Chaine.Append(SiFichedePoste_Adaptee & VI.Tild)
                Chaine.Append(Actualisation_FichedePoste & VI.Tild)
                Chaine.Append(Appreciation_FichedePoste & VI.Tild)
                Chaine.Append(Aptitudes_Fonctions_Superieures & VI.Tild)
                Chaine.Append(SiUtilisation_du_DIF & VI.Tild)
                Chaine.Append(Date_de_Signature_Formation_Evalue & VI.Tild)
                Chaine.Append(Date_de_Signature_Formation_Evaluateur & VI.Tild)
                Chaine.Append(SiManagerOK_PourMajFichedePoste & VI.Tild)
                Chaine.Append(Date_de_Signature_Autorite_Hierarchique & VI.Tild)
                Chaine.Append(Identifiant_Manager.ToString & VI.Tild)
                Chaine.Append(SiConduite_Projet & VI.Tild)
                Chaine.Append(Nombre_Encadres_Categorie_A.ToString & VI.Tild)
                Chaine.Append(Nombre_Encadres_Categorie_B.ToString & VI.Tild)
                Chaine.Append(Nombre_Encadres_Categorie_C.ToString & VI.Tild)
                Chaine.Append(SiFormation_Urgente & VI.Tild)
                Chaine.Append(MyBase.Certification)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 30 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Evaluateur = TableauData(2)
                Entite_Evaluation = TableauData(3)
                Date_de_Signature_Evaluateur = TableauData(4)
                Date_de_Signature_Evalue = TableauData(5)
                Descriptif_Poste = TableauData(6)
                Emploi_Type = TableauData(7)
                ActivitesPrincipales = TableauData(8)
                SiEncadrement = TableauData(9)
                If TableauData(10) <> "" Then Nombre_Encadres = CInt(VirRhFonction.ConversionDouble(TableauData(10)))
                If TableauData(11) <> "" Then Critere_Synthese = CInt(VirRhFonction.ConversionDouble(TableauData(11)))
                Intitule_Synthese = TableauData(12)
                AcquisExperience = TableauData(13)
                LitteralEvaluateur = TableauData(14)
                SiSouhait_EntretienCarriere = TableauData(15)
                SiFichedePoste_Adaptee = TableauData(16)
                Actualisation_FichedePoste = TableauData(17)
                Appreciation_FichedePoste = TableauData(18)
                Aptitudes_Fonctions_Superieures = TableauData(19)
                SiUtilisation_du_DIF = TableauData(20)
                Date_de_Signature_Formation_Evalue = TableauData(21)
                Date_de_Signature_Formation_Evaluateur = TableauData(22)
                SiManagerOK_PourMajFichedePoste = TableauData(23)
                Date_de_Signature_Autorite_Hierarchique = TableauData(24)
                If TableauData(25) <> "" Then Identifiant_Manager = CInt(VirRhFonction.ConversionDouble(TableauData(25)))
                SiConduite_Projet = TableauData(26)
                If TableauData(27) <> "" Then Nombre_Encadres_Categorie_A = CInt(VirRhFonction.ConversionDouble(TableauData(27)))
                If TableauData(28) <> "" Then Nombre_Encadres_Categorie_B = CInt(VirRhFonction.ConversionDouble(TableauData(28)))
                If TableauData(29) <> "" Then Nombre_Encadres_Categorie_C = CInt(VirRhFonction.ConversionDouble(TableauData(29)))
                SiFormation_Urgente = TableauData(30)
                MyBase.Certification = TableauData(31)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim Chaine As String
                Chaine = Date_de_Valeur & VI.PointVirgule & Evaluateur & VI.PointVirgule & Entite_Evaluation & VI.PointVirgule
                Chaine &= Date_de_Signature_Evaluateur & VI.PointVirgule & Date_de_Signature_Evalue
                Return Chaine
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
