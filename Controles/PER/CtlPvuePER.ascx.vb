﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPvuePER
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateIde As String = "MenuPERIde"

    Public Property V_Identifiant As Integer
        Get
            If Me.ViewState(WsNomStateIde) Is Nothing Then
                Return 0
            End If
            Return CType(Me.ViewState(WsNomStateIde), Integer)
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), Integer) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        ElseIf HPopupRefGrade.Value = "1" Then
            CellRefGrade.Visible = True
            PopupRefGrade.Show()
        Else
            CellReference.Visible = False
            Call AfficherFenetrePER()
        End If
    End Sub

    Private Sub AfficherFenetrePER()
        Dim Ide_Dossier As Integer = V_Identifiant
        If Ide_Dossier = 0 Then
            Exit Sub
        End If
        Select Case MultiPER.ActiveViewIndex
            Case 0
                PER_ETATCIVIL_1.Identifiant = Ide_Dossier
            Case 1
                PER_PIECE_IDENTITE1.V_Identifiant = V_Identifiant
            Case 2
                PER_DOMICILE_1.Identifiant = Ide_Dossier
            Case 3
                PER_DOMICILE_2.Identifiant = Ide_Dossier
            Case 4
                PER_RESIDENCE_CONGE.Identifiant = Ide_Dossier
            Case 5
                PER_ADRESSE_ETRANGER.Identifiant = Ide_Dossier
            Case 6
                PER_BANQUE_5.Identifiant = Ide_Dossier
            Case 7
                PER_ENFANT_3.Identifiant = Ide_Dossier
            Case 8
                PER_CONJOINT1.Identifiant = Ide_Dossier
            Case 9
                PER_APREVENIR2.Identifiant = Ide_Dossier
            Case 10
                PER_DECORATION.Identifiant = Ide_Dossier
            Case 11
                PER_DIPLOME.Identifiant = Ide_Dossier
            Case 12
                PER_QUALIFICATION.Identifiant = Ide_Dossier
            Case 13
                PER_SPECIALITE.Identifiant = Ide_Dossier
            Case 14
                PER_THESE.Identifiant = Ide_Dossier
            Case 15
                PER_EXPERIENCE.Identifiant = Ide_Dossier
            Case 16
                PER_LANG_ETRANGER.Identifiant = Ide_Dossier
            Case 17
                PER_ACQUIS.Identifiant = Ide_Dossier
            Case 18
                PER_STAGE.Identifiant = Ide_Dossier
            Case 19
                PER_LOISIR.Identifiant = Ide_Dossier
            Case 20
                PER_COLLECTIVITE_26.Identifiant = Ide_Dossier
            Case 21
                PER_IDENTIFICATION.Identifiant = Ide_Dossier
            Case 22
                PER_ADRESSEPRO_24.Identifiant = Ide_Dossier
            Case 23
                PER_AFFECTATION_17.Identifiant = Ide_Dossier
            Case 24
                PER_AFFECTATION2.Identifiant = Ide_Dossier
            Case 25
                PER_AFFECTATION3.Identifiant = Ide_Dossier
            Case 26
                PER_AFFECTATION4.Identifiant = Ide_Dossier
            Case 27
                PER_AFFECTATION5.Identifiant = Ide_Dossier
            Case 28
                PER_AFFECTATION6.Identifiant = Ide_Dossier
            Case 29
                PER_LOLF_92.Identifiant = Ide_Dossier
            Case 30
                PER_EMPLOIBUD.Identifiant = Ide_Dossier
            Case 31
                PER_ACTIANNEXE.Identifiant = Ide_Dossier
            Case 32
                PER_ACTI_INTERNE.Identifiant = Ide_Dossier
            Case 33
                PER_STATUT_12.Identifiant = Ide_Dossier
            Case 34
                PER_POSITION_13.Identifiant = Ide_Dossier
            Case 35
                PER_GRADE_14.Identifiant = Ide_Dossier
            Case 36
                PER_INTEGRATION.Identifiant = Ide_Dossier
            Case 37
                PER_BONIFICATION.Identifiant = Ide_Dossier
            Case 38
                PER_SANCTION.Identifiant = Ide_Dossier
            Case 39
                PER_STATUT_54.Identifiant = Ide_Dossier
            Case 40
                PER_POSITION_55.Identifiant = Ide_Dossier
            Case 41
                PER_GRADE_56.Identifiant = Ide_Dossier
            Case 42
                PER_SERVICECIVIL.Identifiant = Ide_Dossier
            Case 43
                PER_SERVICEMILITAIRE.Identifiant = Ide_Dossier
            Case 44
                PER_CUMULREM.Identifiant = Ide_Dossier
            Case 45
                PER_VISITE_MEDICAL.Identifiant = Ide_Dossier
            Case 46
                PER_HANDICAP.Identifiant = Ide_Dossier
            Case 47
                PER_VACCINATION.Identifiant = Ide_Dossier
            Case 48
                PER_PREVENTION.Identifiant = Ide_Dossier
            Case 49
                PER_ABSENCE_15.Identifiant = Ide_Dossier
            Case 50
                PER_AGENDA.Identifiant = Ide_Dossier
            Case 51
                PER_MANDAT.Identifiant = Ide_Dossier
            Case 52
                PER_AVANTAGE.Identifiant = Ide_Dossier
            Case 53
                PER_AFFILIATION.Identifiant = Ide_Dossier
            Case 54
                PER_MUTUELLE.Identifiant = Ide_Dossier
            Case 55
                PER_MATERIEL.Identifiant = Ide_Dossier
            Case 56
                PER_ANNOTATION.Identifiant = Ide_Dossier
            Case 57
                PER_DOCUMENT.Identifiant = Ide_Dossier
        End Select
    End Sub

    Private Sub PER_MENU_Menu_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles PER_MENU.Menu_Click
        If IsNumeric(e.Parametre) Then
            MultiPER.ActiveViewIndex = CInt(e.Parametre)
        End If
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler, RefGradeGrilles.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                MultiPER.SetActiveView(VueEtatCivil)
            Case "VuePieceIdentite"
                MultiPER.SetActiveView(VuePieceIdentite)
            Case "VueAdresse"
                MultiPER.SetActiveView(VueAdresse)
            Case "VueAdresse2"
                MultiPER.SetActiveView(VueAdresse2)
            Case "VueResidenceConge"
                MultiPER.SetActiveView(VueResidenceConge)
            Case "VueAdresseEtranger"
                MultiPER.SetActiveView(VueAdresseEtranger)
            Case "VueBanque"
                MultiPER.SetActiveView(VueBanque)
            Case "VueEnfant"
                MultiPER.SetActiveView(VueEnfant)
            Case "VueConjoint"
                MultiPER.SetActiveView(VueConjoint)
            Case "VuePrevenir"
                MultiPER.SetActiveView(VuePrevenir)
            Case "VueDecoration"
                MultiPER.SetActiveView(VueDecoration)
            Case "VueDiplome"
                MultiPER.SetActiveView(VueDiplome)
            Case "VueQualif"
                MultiPER.SetActiveView(VueQualif)
            Case "VueSpecialite"
                MultiPER.SetActiveView(VueSpecialite)
            Case "Vuethese"
                MultiPER.SetActiveView(Vuethese)
            Case "VueExperience"
                MultiPER.SetActiveView(VueExperience)
            Case "VueLangueEtrangeres"
                MultiPER.SetActiveView(VueLangueEtrangeres)
            Case "VueAcquis"
                MultiPER.SetActiveView(VueAcquis)
            Case "VueStage"
                MultiPER.SetActiveView(VueStage)
            Case "VueLoisir"
                MultiPER.SetActiveView(VueLoisir)
            Case "VueAdministration"
                MultiPER.SetActiveView(VueAdministration)
            Case "VueIdentification"
                MultiPER.SetActiveView(VueIdentification)
            Case "VueAdressePro"
                MultiPER.SetActiveView(VueAdressePro)
            Case "VueAffectation"
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAffectation2"
                MultiPER.SetActiveView(VueAffectation2)
            Case "VueAffectation3"
                MultiPER.SetActiveView(VueAffectation3)
            Case "VueAffectation4"
                MultiPER.SetActiveView(VueAffectation4)
            Case "VueAffectation5"
                MultiPER.SetActiveView(VueAffectation5)
            Case "VueAffectation6"
                MultiPER.SetActiveView(VueAffectation6)
            Case "VueLOLF"
                MultiPER.SetActiveView(VueLOLF)
            Case "VueEmploiBud"
                MultiPER.SetActiveView(VueEmploiBud)
            Case "VueActiAnnexe"
                MultiPER.SetActiveView(VueActiAnnexe)
            Case "VueActiInterne"
                MultiPER.SetActiveView(VueActiInterne)
            Case "VueStatut"
                MultiPER.SetActiveView(VueStatut)
            Case "VuePosition"
                MultiPER.SetActiveView(VuePosition)
            Case "VueGrade"
                MultiPER.SetActiveView(VueGrade)
            Case "VueIntegration"
                MultiPER.SetActiveView(VueIntegration)
            Case "VueBonification"
                MultiPER.SetActiveView(VueBonification)
            Case "VueSanction"
                MultiPER.SetActiveView(VueSanction)
            Case "VueStatutDetache"
                MultiPER.SetActiveView(VueStatutDetache)
            Case "VuePositionDetache"
                MultiPER.SetActiveView(VuePositionDetache)
            Case "VueGradeDetache"
                MultiPER.SetActiveView(VueGradeDetache)
            Case "VueServiceCivil"
                MultiPER.SetActiveView(VueServiceCivil)
            Case "VueArmee"
                MultiPER.SetActiveView(VueArmee)
            Case "VueCumulRem"
                MultiPER.SetActiveView(VueCumulRem)
            Case "VueMedical"
                MultiPER.SetActiveView(VueMedical)
            Case "VueHandicap"
                MultiPER.SetActiveView(VueHandicap)
            Case "VueVaccination"
                MultiPER.SetActiveView(VueVaccination)
            Case "VuePrevention"
                MultiPER.SetActiveView(VuePrevention)
            Case "VueAbsence"
                MultiPER.SetActiveView(VueAbsence)
            Case "VueAgenda"
                MultiPER.SetActiveView(VueAgenda)
            Case "VueMandat"
                MultiPER.SetActiveView(VueMandat)
            Case "VueAvantage"
                MultiPER.SetActiveView(VueAvantage)
            Case "VueAffiliation"
                MultiPER.SetActiveView(VueAffiliation)
            Case "VueMutuelle"
                MultiPER.SetActiveView(VueMutuelle)
            Case "VueMateriel"
                MultiPER.SetActiveView(VueMateriel)
            Case "VueAnnotation"
                MultiPER.SetActiveView(VueAnnotation)
            Case "VueDocument"
                MultiPER.SetActiveView(VueDocument)


        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) _
        Handles RefVirtualia.ValeurSelectionnee, RefGradeGrilles.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueEtatcivil"
                PER_ETATCIVIL_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueEtatCivil)
            Case "VuePieceIdentite"
                PER_PIECE_IDENTITE1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePieceIdentite)
            Case "VueAdresse"
                PER_DOMICILE_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdresse)
            Case "VueAdresse2"
                PER_DOMICILE_2.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdresse2)
            Case "VueResidenceConge"
                PER_RESIDENCE_CONGE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueResidenceConge)
            Case "VueAdresseEtranger"
                PER_ADRESSE_ETRANGER.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdresseEtranger)
            Case "VueBanque"
                PER_BANQUE_5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueBanque)
            Case "VueConjoint"
                PER_CONJOINT1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueConjoint)
            Case "VuePrevenir"
                PER_APREVENIR2.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePrevenir)
            Case "VueDecoration"
                PER_DECORATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueDecoration)
            Case "VueDiplome"
                PER_DIPLOME.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueDiplome)
            Case "VueQualif"
                PER_QUALIFICATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueQualif)
            Case "VueSpecialite"
                PER_SPECIALITE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueSpecialite)
            Case "Vuethese"
                PER_THESE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(Vuethese)
            Case "VueExperience"
                PER_EXPERIENCE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueExperience)
            Case "VueLangueEtrangeres"
                PER_LANG_ETRANGER.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueLangueEtrangeres)
            Case "VueAcquis"
                PER_ACQUIS.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAcquis)
            Case "VueStage"
                PER_STAGE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStage)
            Case "VueLoisir"
                PER_LOISIR.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueLoisir)
            Case "VueAdministration"
                PER_COLLECTIVITE_26.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdministration)
            Case "VueIdentification"
                PER_IDENTIFICATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueIdentification)
            Case "VueAdressePro"
                PER_ADRESSEPRO_24.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdressePro)
            Case "VueAffectation"
                PER_AFFECTATION_17.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAffectation2"
                PER_AFFECTATION2.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation2)
            Case "VueAffectation3"
                PER_AFFECTATION3.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation3)
            Case "VueAffectation4"
                PER_AFFECTATION4.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation4)
            Case "VueAffectation5"
                PER_AFFECTATION5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation5)
            Case "VueAffectation6"
                PER_AFFECTATION6.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation6)
            Case "VueLOLF"
                PER_LOLF_92.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueLOLF)
            Case "VueEmploiBud"
                PER_EMPLOIBUD.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueEmploiBud)
            Case "VueActiAnnexe"
                PER_ACTIANNEXE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueActiAnnexe)
            Case "VueActiInterne"
                PER_ACTI_INTERNE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueActiInterne)
            Case "VueStatut"
                PER_STATUT_12.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStatut)
            Case "VuePosition"
                PER_POSITION_13.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePosition)
            Case "VueGrade"
                PER_GRADE_14.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueGrade)
            Case "VueIntegration"
                PER_INTEGRATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueIntegration)
            Case "VueBonification"
                PER_BONIFICATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueBonification)
            Case "VueSanction"
                PER_SANCTION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueSanction)
            Case "VueStatutDetache"
                PER_STATUT_54.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStatutDetache)
            Case "VuePositionDetache"
                PER_POSITION_55.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePositionDetache)
            Case "VueGradeDetache"
                PER_GRADE_56.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueGradeDetache)
            Case "VueServiceCivil"
                PER_SERVICECIVIL.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueServiceCivil)
            Case "VueArmee"
                PER_SERVICEMILITAIRE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueArmee)
            Case "VueCumulRem"
                PER_CUMULREM.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueCumulRem)
            Case "VueMedical"
                PER_VISITE_MEDICAL.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueMedical)
            Case "VueHandicap"
                PER_HANDICAP.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueHandicap)
            Case "VueVaccination"
                PER_VACCINATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueVaccination)
            Case "VuePrevention"
                PER_PREVENTION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePrevention)
            Case "VueAbsence"
                PER_ABSENCE_15.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAbsence)
            Case "VueAgenda"
                PER_AGENDA.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAgenda)
            Case "VueMandat"
                PER_MANDAT.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueMandat)
            Case "VueAvantage"
                PER_AVANTAGE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAvantage)
            Case "VueAffiliation"
                PER_AFFILIATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffiliation)
            Case "VueMutuelle"
                PER_MUTUELLE.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueMutuelle)
            Case "VueMateriel"
                PER_MATERIEL.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueMateriel)
            Case "VueAnnotation"
                PER_ANNOTATION.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAnnotation)
            Case "VueDocument"
                PER_DOCUMENT.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueDocument)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_ETATCIVIL_1.AppelTable, PER_DOMICILE_1.AppelTable, PER_DOMICILE_2.AppelTable, PER_RESIDENCE_CONGE.AppelTable, PER_COLLECTIVITE_26.AppelTable, PER_LOLF_92.AppelTable,
        PER_AFFECTATION_17.AppelTable, PER_ADRESSEPRO_24.AppelTable, PER_AFFILIATION.AppelTable, PER_ABSENCE_15.AppelTable, PER_DECORATION.AppelTable, PER_SPECIALITE.AppelTable,
        PER_STATUT_12.AppelTable, PER_POSITION_13.AppelTable, PER_GRADE_14.AppelTable, PER_ADRESSE_ETRANGER.AppelTable, PER_APREVENIR2.AppelTable, PER_QUALIFICATION.AppelTable,
        PER_BANQUE_5.AppelTable, PER_STATUT_54.AppelTable, PER_POSITION_55.AppelTable, PER_GRADE_56.AppelTable, PER_CONJOINT1.AppelTable, PER_DIPLOME.AppelTable, PER_THESE.AppelTable,
        PER_EXPERIENCE.AppelTable, PER_LANG_ETRANGER.AppelTable, PER_AFFECTATION2.AppelTable, PER_AFFECTATION3.AppelTable, PER_AFFECTATION4.AppelTable, PER_AFFECTATION5.AppelTable,
        PER_AFFECTATION6.AppelTable, PER_EMPLOIBUD.AppelTable, PER_ACTIANNEXE.AppelTable, PER_ACTI_INTERNE.AppelTable, PER_INTEGRATION.AppelTable, PER_BONIFICATION.AppelTable,
        PER_SANCTION.AppelTable, PER_SERVICECIVIL.AppelTable, PER_SERVICEMILITAIRE.AppelTable, PER_CUMULREM.AppelTable, PER_VISITE_MEDICAL.AppelTable, PER_HANDICAP.AppelTable,
        PER_VACCINATION.AppelTable, PER_PREVENTION.AppelTable, PER_AGENDA.AppelTable, PER_MANDAT.AppelTable, PER_AVANTAGE.AppelTable, PER_MATERIEL.AppelTable, PER_ANNOTATION.AppelTable, PER_DOCUMENT.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtatCivil.ID
            Case VI.ObjetPer.ObaEtranger
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePieceIdentite.ID
            Case VI.ObjetPer.ObaAdresse
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdresse.ID
            Case VI.ObjetPer.ObaAdresse2
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdresse2.ID
            Case VI.ObjetPer.ObaAdresseConge
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueResidenceConge.ID
            Case VI.ObjetPer.ObaAdresseHorsFr
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdresseEtranger.ID
            Case VI.ObjetPer.ObaBanque
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueBanque.ID
            Case VI.ObjetPer.ObaEnfant
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEnfant.ID
            Case VI.ObjetPer.ObaConjoint
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueConjoint.ID
            Case VI.ObjetPer.ObaPrevenir
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePrevenir.ID
            Case VI.ObjetPer.ObaDecoration
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDecoration.ID
            Case VI.ObjetPer.ObaDiplome
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDiplome.ID
            Case VI.ObjetPer.ObaCompetence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueQualif.ID
            Case VI.ObjetPer.ObaSpecialite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSpecialite.ID
            Case VI.ObjetPer.ObaThese
                WebFct.PointeurContexte.SysRef_IDVueRetour = Vuethese.ID
            Case VI.ObjetPer.ObaExperience
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueExperience.ID
            Case VI.ObjetPer.ObaLangue
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLangueEtrangeres.ID
            Case VI.ObjetPer.ObaAcquis
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAcquis.ID
            Case VI.ObjetPer.ObaStageCv
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStage.ID
            Case VI.ObjetPer.ObaLoisir
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLoisir.ID
            Case VI.ObjetPer.ObaSociete
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdministration.ID
            Case VI.ObjetPer.ObaExterne
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueIdentification.ID
            Case VI.ObjetPer.ObaAdrPro
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdressePro.ID
            Case VI.ObjetPer.ObaOrganigramme
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation.ID
            Case VI.ObjetPer.ObaAffectation2nd
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation2.ID
            Case VI.ObjetPer.ObaAffectation3rd
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation3.ID
            Case VI.ObjetPer.ObaAffectation4rth
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation4.ID
            Case VI.ObjetPer.ObaAffectation5th
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation5.ID
            Case VI.ObjetPer.ObaAffectation6th
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation6.ID
            Case VI.ObjetPer.ObaLolf
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueLOLF.ID
            Case VI.ObjetPer.ObaPostebud
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEmploiBud.ID
            Case VI.ObjetPer.ObaActiAnnexes
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueActiAnnexe.ID
            Case VI.ObjetPer.ObaActiInternes
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueActiInterne.ID
            Case VI.ObjetPer.ObaStatut
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatut.ID
            Case VI.ObjetPer.ObaActivite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePosition.ID
            Case VI.ObjetPer.ObaGrade
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGrade.ID
            Case VI.ObjetPer.ObaIntegrationCorps
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueIntegration.ID
            Case VI.ObjetPer.ObaNotation
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueBonification.ID
            Case VI.ObjetPer.ObaSanction
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSanction.ID
            Case VI.ObjetPer.ObaStatutDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatutDetache.ID
            Case VI.ObjetPer.ObaPositionDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePositionDetache.ID
            Case VI.ObjetPer.ObaGradeDetache
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGradeDetache.ID
            Case VI.ObjetPer.ObaOrigine
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueServiceCivil.ID
            Case VI.ObjetPer.ObaArmee
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueArmee.ID
            Case VI.ObjetPer.ObaCumulRem
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCumulRem.ID
            Case VI.ObjetPer.ObaMedical
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueMedical.ID
            Case VI.ObjetPer.ObaHandicap
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueHandicap.ID
            Case VI.ObjetPer.ObaVaccination
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueVaccination.ID
            Case VI.ObjetPer.ObaPrevention
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePrevention.ID
            Case VI.ObjetPer.ObaAbsence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAbsence.ID
            Case VI.ObjetPer.ObaAgenda
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAgenda.ID
            Case VI.ObjetPer.ObaEligible
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueMandat.ID
            Case VI.ObjetPer.ObaAvantage
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAvantage.ID
            Case VI.ObjetPer.ObaCaisse
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffiliation.ID
            Case VI.ObjetPer.ObaMutuelle
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueMutuelle.ID
            Case VI.ObjetPer.ObaMateriel
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueMateriel.ID
            Case VI.ObjetPer.ObaMemento
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAnnotation.ID
            Case VI.ObjetPer.ObaDocuments
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDocument.ID
            Case Else
                Exit Sub
        End Select
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                If e.ControleAppelant = "Dontab01" Then
                    RefGradeGrilles.V_PointdeVue = e.PointdeVueInverse
                    RefGradeGrilles.V_NomTable = ""
                    RefGradeGrilles.V_Valeur_Selectionnee = e.NomdelaTable
                    RefGradeGrilles.V_Appelant(2, e.DatedeValeur) = e.ControleAppelant
                    HPopupRefGrade.Value = "1"
                    CellRefGrade.Visible = True
                    PopupRefGrade.Show()
                    Exit Sub
                End If
        End Select

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                If e.ControleAppelant = "Dontab06" Then
                    RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
                End If
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEnfant)
                    Case VI.ObjetPer.ObaDecoration
                        PER_DECORATION.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDecoration)
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaAffectation2nd
                        PER_AFFECTATION2.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation2)
                    Case VI.ObjetPer.ObaAffectation3rd
                        PER_AFFECTATION3.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation3)
                    Case VI.ObjetPer.ObaAffectation4rth
                        PER_AFFECTATION4.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation4)
                    Case VI.ObjetPer.ObaAffectation5th
                        PER_AFFECTATION5.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation5)
                    Case VI.ObjetPer.ObaAffectation6th
                        PER_AFFECTATION6.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation6)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdministration)
                    Case VI.ObjetPer.ObaLolf
                        PER_LOLF_92.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueLOLF)
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_54.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatutDetache)
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_55.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePositionDetache)
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_56.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGradeDetache)
                End Select
            Case Is = "SuppDossier"
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaCivil
                        PER_ETATCIVIL_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEtatCivil)
                    Case VI.ObjetPer.ObaEtranger
                        PER_PIECE_IDENTITE1.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePieceIdentite)
                    Case VI.ObjetPer.ObaEnfant
                        PER_ENFANT_3.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEnfant)
                    Case VI.ObjetPer.ObaDecoration
                        PER_DECORATION.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDecoration)
                    Case VI.ObjetPer.ObaAdresse
                        PER_DOMICILE_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdresse)
                    Case VI.ObjetPer.ObaBanque
                        PER_BANQUE_5.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueBanque)
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaAdrPro
                        PER_ADRESSEPRO_24.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdressePro)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAdministration)
                    Case VI.ObjetPer.ObaLolf
                        PER_LOLF_92.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueLOLF)
                    Case VI.ObjetPer.ObaStatutDetache
                        PER_STATUT_54.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatutDetache)
                    Case VI.ObjetPer.ObaPositionDetache
                        PER_POSITION_55.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePositionDetache)
                    Case VI.ObjetPer.ObaGradeDetache
                        PER_GRADE_56.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGradeDetache)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageDialogue, PER_PIECE_IDENTITE1.MessageDialogue, PER_DOMICILE_2.MessageDialogue, PER_ENFANT_3.MessageDialogue, PER_DOMICILE_1.MessageDialogue,
    PER_BANQUE_5.MessageDialogue, PER_STATUT_12.MessageDialogue, PER_POSITION_13.MessageDialogue, PER_ADRESSE_ETRANGER.MessageDialogue, PER_DIPLOME.MessageDialogue, PER_CONJOINT1.MessageDialogue,
    PER_DECORATION.MessageDialogue, PER_QUALIFICATION.MessageDialogue, PER_SPECIALITE.MessageDialogue, PER_THESE.MessageDialogue, PER_EXPERIENCE.MessageDialogue, PER_LANG_ETRANGER.MessageDialogue,
    PER_GRADE_14.MessageDialogue, PER_ABSENCE_15.MessageDialogue, PER_AFFECTATION_17.MessageDialogue, PER_ADRESSEPRO_24.MessageDialogue, PER_AFFILIATION.MessageDialogue, PER_COLLECTIVITE_26.MessageDialogue,
    PER_LOLF_92.MessageDialogue, PER_RESIDENCE_CONGE.MessageDialogue, PER_STATUT_54.MessageDialogue, PER_POSITION_55.MessageDialogue, PER_GRADE_56.MessageDialogue, PER_APREVENIR2.MessageDialogue,
    PER_ACQUIS.MessageDialogue, PER_STAGE.MessageDialogue, PER_IDENTIFICATION.MessageDialogue, PER_AFFECTATION2.MessageDialogue, PER_AFFECTATION3.MessageDialogue, PER_AFFECTATION4.MessageDialogue,
    PER_AFFECTATION5.MessageDialogue, PER_AFFECTATION6.MessageDialogue, PER_EMPLOIBUD.MessageDialogue, PER_ACTIANNEXE.MessageDialogue, PER_ACTI_INTERNE.MessageDialogue, PER_INTEGRATION.MessageDialogue,
    PER_BONIFICATION.MessageDialogue, PER_SANCTION.MessageDialogue, PER_SERVICECIVIL.MessageDialogue, PER_SERVICEMILITAIRE.MessageDialogue, PER_CUMULREM.MessageDialogue,
    PER_VISITE_MEDICAL.MessageDialogue, PER_HANDICAP.MessageDialogue, PER_VACCINATION.MessageDialogue, PER_PREVENTION.MessageDialogue, PER_AGENDA.MessageDialogue, PER_MANDAT.MessageDialogue,
    PER_AVANTAGE.MessageDialogue, PER_MATERIEL.MessageDialogue, PER_ANNOTATION.MessageDialogue, PER_DOCUMENT.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_ETATCIVIL_1.MessageSaisie, PER_ENFANT_3.MessageSaisie, PER_DOMICILE_1.MessageSaisie, PER_CONJOINT1.MessageSaisie, PER_QUALIFICATION.MessageSaisie,
    PER_BANQUE_5.MessageSaisie, PER_STATUT_12.MessageSaisie, PER_POSITION_13.MessageSaisie, PER_ADRESSEPRO_24.MessageSaisie, PER_DECORATION.MessageSaisie,
    PER_GRADE_14.MessageSaisie, PER_ABSENCE_15.MessageSaisie, PER_AFFECTATION_17.MessageSaisie, PER_ADRESSE_ETRANGER.MessageSaisie, PER_DIPLOME.MessageSaisie,
        PER_AFFILIATION.MessageSaisie, PER_COLLECTIVITE_26.MessageSaisie, PER_LOLF_92.MessageSaisie, PER_STATUT_54.MessageSaisie, PER_APREVENIR2.MessageSaisie,
    PER_POSITION_55.MessageSaisie, PER_GRADE_56.MessageSaisie, PER_RESIDENCE_CONGE.MessageSaisie, PER_PIECE_IDENTITE1.MessageSaisie, PER_DOMICILE_2.MessageSaisie,
    PER_SPECIALITE.MessageSaisie, PER_THESE.MessageSaisie, PER_EXPERIENCE.MessageSaisie, PER_LANG_ETRANGER.MessageSaisie, PER_ACQUIS.MessageSaisie, PER_STAGE.MessageSaisie,
    PER_IDENTIFICATION.MessageSaisie, PER_AFFECTATION2.MessageSaisie, PER_AFFECTATION3.MessageSaisie, PER_AFFECTATION4.MessageSaisie, PER_INTEGRATION.MessageSaisie,
    PER_AFFECTATION5.MessageSaisie, PER_AFFECTATION6.MessageSaisie, PER_EMPLOIBUD.MessageSaisie, PER_ACTIANNEXE.MessageSaisie, PER_ACTI_INTERNE.MessageSaisie,
    PER_BONIFICATION.MessageSaisie, PER_SANCTION.MessageSaisie, PER_SERVICECIVIL.MessageSaisie, PER_SERVICEMILITAIRE.MessageSaisie, PER_CUMULREM.MessageSaisie,
    PER_VISITE_MEDICAL.MessageSaisie, PER_HANDICAP.MessageSaisie, PER_VACCINATION.MessageSaisie, PER_PREVENTION.MessageSaisie, PER_AGENDA.MessageSaisie,
    PER_MANDAT.MessageSaisie, PER_AVANTAGE.MessageSaisie, PER_MATERIEL.MessageSaisie, PER_ANNOTATION.MessageSaisie, PER_DOCUMENT.MessageSaisie

        HPopupMsg.Value = "0"
        CellMessage.Visible = False
    End Sub

End Class