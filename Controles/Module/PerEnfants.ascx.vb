﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerEnfants
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerEnfants_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call LireLaFiche()
    End Sub

    'Protected Sub ItemChange(ByVal sender As Object, ByVal e As EventArgs) Handles myDropDown.SelectedIndexChanged
    '    Dim NoLigne As Integer
    '    Dim Ctl As Control
    '    Dim IndiceI As Integer = 0
    '    Dim VirControle As System.Web.UI.WebControls.TableRow
    '    Do
    '        Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Row_", IndiceI)
    '        If Ctl Is Nothing Then
    '            Exit Do
    '        End If
    '        NoLigne = CInt(Strings.Right(Ctl.ID, 1))
    '        VirControle = CType(Ctl, System.Web.UI.WebControls.TableRow)
    '        If myDropDown.SelectedIndex + 1 > NoLigne - 1 Then
    '            VirControle.Visible = True
    '        Else
    '            VirControle.Visible = False
    '            If NoLigne >= myDropDown.SelectedIndex + 1 Then
    '                If V_Contexte.DossierPER.ListeEnfants.Count >= NoLigne Then
    '                    For Each fiche In V_Contexte.DossierPER
    '                        If fiche.FicheLue = V_Contexte.DossierPER.ListeEnfants(NoLigne - 1).FicheLue Then
    '                            V_Contexte.DossierPER.Remove(fiche)
    '                            Exit For
    '                        End If
    '                    Next
    '                End If
    '            End If
    '            End If
    '            IndiceI += 1
    '    Loop

    'End Sub

    Private Sub LireLaFiche()
        Dim NoLigne As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False
        Dim VirControle As System.Web.UI.WebControls.TextBox
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Nom_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = ValeurLue(1, NoLigne - 1)
            VirControle.ReadOnly = SiLectureOnly
            VirControle.AutoPostBack = False
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Prenom_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = ValeurLue(2, NoLigne - 1)
            VirControle.ReadOnly = SiLectureOnly
            VirControle.AutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Date_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonText = ValeurLue(0, NoLigne - 1)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Radio_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.V_SiEnLectureSeule = SiLectureOnly 'WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadioH.V_SiAutoPostBack = True 'Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case ValeurLue(3, NoLigne - 1)
                Case Is = "Masculin"
                    VirRadioH.RadioGaucheCheck = True
                Case Is = "Féminin"
                    VirRadioH.RadioCentreCheck = True
                Case Else
                    VirRadioH.RadioGaucheCheck = False
                    VirRadioH.RadioCentreCheck = False
            End Select
            IndiceI += 1
        Loop

        Dim VirRadioC As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Acharge_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirRadioC = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioC.V_SiEnLectureSeule = SiLectureOnly 'WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1))
            VirRadioC.V_SiAutoPostBack = True 'Not (WsDossierPER.SiReadOnly(NumObjet, NumInfo, Strings.Mid(Ctl.ID, 7, 1)))
            Select Case ValeurLue(4, NoLigne - 1)
                Case Is = "Oui"
                    VirRadioC.RadioGaucheCheck = True
                Case Is = "Non"
                    VirRadioC.RadioCentreCheck = True
                Case Else
                    VirRadioC.RadioGaucheCheck = False
                    VirRadioC.RadioCentreCheck = False
            End Select
            IndiceI += 1
        Loop
    End Sub

    Private Sub Date_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Date_L1.ValeurChange, Date_L2.ValeurChange, Date_L3.ValeurChange,
        Date_L4.ValeurChange, Date_L5.ValeurChange, Date_L6.ValeurChange, Date_L7.ValeurChange, Date_L8.ValeurChange, Date_L9.ValeurChange

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 1))
        If ValeurLue(0, NoLigne - 1) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            TableauMaj(0, NoLigne - 1) = e.Valeur
            End If
    End Sub

    Private Sub Nom_TextChanged(sender As Object, e As EventArgs) Handles Nom_L1.TextChanged, Nom_L2.TextChanged, Nom_L3.TextChanged,
        Nom_L4.TextChanged, Nom_L5.TextChanged, Nom_L6.TextChanged, Nom_L7.TextChanged, Nom_L8.TextChanged, Nom_L9.TextChanged

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, TextBox).ID, 1))
        If ValeurLue(1, NoLigne - 1) <> CType(sender, TextBox).Text Then
            TableauMaj(1, NoLigne - 1) = CType(sender, TextBox).Text
        End If
    End Sub

    Private Sub Prenom_TextChanged(sender As Object, e As EventArgs) Handles Prenom_L1.TextChanged, Prenom_L2.TextChanged, Prenom_L3.TextChanged,
        Prenom_L4.TextChanged, Prenom_L5.TextChanged, Prenom_L6.TextChanged, Prenom_L7.TextChanged, Prenom_L8.TextChanged, Prenom_L9.TextChanged

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, TextBox).ID, 1))
        If ValeurLue(2, NoLigne - 1) <> CType(sender, TextBox).Text Then
            TableauMaj(2, NoLigne - 1) = CType(sender, TextBox).Text
        End If
    End Sub

    Private Sub Radio_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Radio_L1.ValeurChange, Radio_L2.ValeurChange, Radio_L3.ValeurChange,
        Radio_L4.ValeurChange, Radio_L5.ValeurChange, Radio_L6.ValeurChange, Radio_L7.ValeurChange, Radio_L8.ValeurChange, Radio_L9.ValeurChange

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 1))
        If ValeurLue(3, NoLigne - 1) <> e.Valeur Then
            TableauMaj(3, NoLigne - 1) = e.Valeur
        End If
    End Sub

    Private Sub Acharge_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Acharge_L1.ValeurChange, Acharge_L2.ValeurChange, Acharge_L3.ValeurChange,
        Acharge_L4.ValeurChange, Acharge_L5.ValeurChange, Acharge_L6.ValeurChange, Acharge_L7.ValeurChange, Acharge_L8.ValeurChange, Acharge_L9.ValeurChange

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 1))
        If ValeurLue(4, NoLigne - 1) <> e.Valeur Then
            TableauMaj(4, NoLigne - 1) = e.Valeur
        End If
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer, ByVal NoRang As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return ""
            End If
            If NoRang > LstFiches.Count - 1 Then
                Return ""
            End If
            Return LstFiches.Item(NoRang).V_TableauData(NoInfo).ToString
        End Get
    End Property

    Private WriteOnly Property TableauMaj(ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
        Set(value As String)
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Exit Property
                End Try
                If WsDossierPer Is Nothing Then
                    Exit Property
                End If
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
            Dim ConstructeurFiche As Virtualia.TablesObjet.ShemaPER.VConstructeur
            Dim FicheMAJ As Virtualia.Systeme.MetaModele.VIR_FICHE
            Dim TableauData As ArrayList

            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
                LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
            ElseIf NoRang > LstFiches.Count - 1 Then
                ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
                LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
            Else
                FicheMAJ = LstFiches.Item(NoRang)
            End If
            TableauData = FicheMAJ.V_TableauData
            TableauData(NumInfo) = value
            FicheMAJ.V_TableauData = TableauData
            WsDossierPer.ListeEnfants = LstFiches
        End Set
    End Property

    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property
End Class