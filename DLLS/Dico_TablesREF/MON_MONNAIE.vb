﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MON_MONNAIE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsLibelleLong As String
        Private WsSymbole As String
        Private WsSiCentimes As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MON_MONNAIE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMonnaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsLibelleLong
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLibelleLong = value
                    Case Else
                        WsLibelleLong = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Symbole() As String
            Get
                Return WsSymbole
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSymbole = value
                    Case Else
                        WsSymbole = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiPresence_Centimes() As String
            Get
                Return WsSiCentimes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiCentimes = value
                    Case Else
                        WsSiCentimes = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Symbole & VI.Tild)
                Chaine.Append(SiPresence_Centimes)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Symbole = TableauData(2)
                SiPresence_Centimes = TableauData(3)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
