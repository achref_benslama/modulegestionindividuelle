﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class STATS_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsLstValeurs As List(Of STATS_VALEURS)
        Private WsFicheLue As StringBuilder
        '
        Private WsPtdeVue As Integer
        Private WsIntitule As String
        Private WsUtilisateur As String
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsTypeGraphe As Integer
        Private WsStyleGraphe As Integer
        Private WsNumObjetReparti As Integer
        Private WsNumInfoReparti As Integer
        Private WsDateDebutExe As String
        Private WsDateFinExe As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "STATS_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptStats
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property PointdeVueStats() As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(ByVal value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Numero_ObjetSTATS() As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Numero_InfoSTATS() As Integer
            Get
                Return WsNumInfo
            End Get
            Set(ByVal value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property Graphe_Type() As Integer
            Get
                Return WsTypeGraphe
            End Get
            Set(ByVal value As Integer)
                WsTypeGraphe = value
            End Set
        End Property

        Public Property Graphe_Style() As Integer
            Get
                Return WsStyleGraphe
            End Get
            Set(ByVal value As Integer)
                WsStyleGraphe = value
            End Set
        End Property

        Public Property Numero_ObjetReparti() As Integer
            Get
                Return WsNumObjetReparti
            End Get
            Set(ByVal value As Integer)
                WsNumObjetReparti = value
            End Set
        End Property

        Public Property Numero_InfoReparti() As Integer
            Get
                Return WsNumInfoReparti
            End Get
            Set(ByVal value As Integer)
                WsNumInfoReparti = value
            End Set
        End Property

        Public Property DatedeDebutExe() As String
            Get
                Return WsDateDebutExe
            End Get
            Set(ByVal value As String)
                WsDateDebutExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatedeFinExe() As String
            Get
                Return WsDateFinExe
            End Get
            Set(ByVal value As String)
                WsDateFinExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(PointdeVueStats) & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetSTATS) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoSTATS) & VI.Tild)
                Chaine.Append(CStr(Graphe_Type) & VI.Tild)
                Chaine.Append(CStr(Graphe_Style) & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetReparti) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoReparti) & VI.Tild)
                Chaine.Append(DatedeDebutExe & VI.Tild)
                Chaine.Append(DatedeFinExe)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                PointdeVueStats = CInt(TableauData(1))
                Intitule = TableauData(2)
                Utilisateur = TableauData(3)
                For IndiceI = 4 To 9
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                Numero_ObjetSTATS = CInt(TableauData(4))
                Numero_InfoSTATS = CInt(TableauData(5))
                Graphe_Type = CInt(TableauData(6))
                Graphe_Style = CInt(TableauData(7))
                Numero_ObjetReparti = CInt(TableauData(8))
                Numero_InfoReparti = CInt(TableauData(9))
                DatedeDebutExe = TableauData(10)
                DatedeFinExe = TableauData(11)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesValeurs() As List(Of STATS_VALEURS)
            Get
                If WsLstValeurs Is Nothing Then
                    Return Nothing
                End If
                Return WsLstValeurs.OrderBy(Function(m) m.Numero_Rang).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Valeur(ByVal Fiche As STATS_VALEURS) As Integer
            If WsLstValeurs Is Nothing Then
                WsLstValeurs = New List(Of STATS_VALEURS)
            End If
            WsLstValeurs.Add(Fiche)
            Return WsLstValeurs.Count
        End Function

        Public Function Fiche_Selection(ByVal NoLigne As Integer) As STATS_VALEURS
            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Fiche As STATS_VALEURS In WsLstValeurs Where Fiche.Numero_Rang = NoLigne Select Fiche).FirstOrDefault()
        End Function
    End Class
End Namespace
