﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LIAISON_DOCUMENT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        Private WsIntitule As String
        Private WsLettreType As String
        Private WsClassement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LIAISON_DOCUMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLiaisonWord
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Lettre_Type() As String
            Get
                Return WsLettreType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsLettreType = value
                    Case Else
                        WsLettreType = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Classement() As String
            Get
                Return WsClassement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsClassement = value
                    Case Else
                        WsClassement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Lettre_Type & VI.Tild)
                Chaine.Append(Classement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
               
                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If
                If TableauData(0) = "" Then TableauData(0) = "0"
                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Lettre_Type = TableauData(2)
                Classement = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

