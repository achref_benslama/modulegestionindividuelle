Option Strict On
Option Explicit On
Option Compare Text
Namespace Parametrage
    Namespace BasesdeDonnees
        Public Class ParamSgbdConnexion
            Private ReadOnly ObjetDatabase As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Private WsIndex As Integer
            Private WsNumDataBase As Integer
            Private WsTypeCnx As String = "OLEDB"
            Private WsDsnODBC As String
            Private WsProvider As String 'Provider OLeDB ou Driver ODBC
            Private WsPrecision As String 'Précisions eventuelles sur chaines de connexion
            Private WsServeur As String = ""
            Private WsUtilisateur As String
            Private WsPassword As String = ""
            Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales

            Public ReadOnly Property VParent() As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
                Get
                    Return ObjetDatabase
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property NumDatabase() As Integer
                Get
                    Return WsNumDataBase
                End Get
                Set(ByVal value As Integer)
                    WsNumDataBase = value
                End Set
            End Property

            Public Property TypeCnx() As String
                Get
                    Return WsTypeCnx
                End Get
                Set(ByVal value As String)
                    Select Case value
                        Case "ADO", "ODBC", "OLEDB", "SQLSERVER"
                            WsTypeCnx = value
                        Case Else
                            WsTypeCnx = "OLEDB"
                    End Select
                End Set
            End Property

            Public Property DsnODBC() As String
                Get
                    Return WsDsnODBC
                End Get
                Set(ByVal value As String)
                    WsDsnODBC = value
                End Set
            End Property

            Public Property Provider() As String
                Get
                    Return WsProvider
                End Get
                Set(ByVal value As String)
                    WsProvider = value
                End Set
            End Property

            Public Property PrecisionSurConnexion() As String
                Get
                    Return WsPrecision
                End Get
                Set(ByVal value As String)
                    WsPrecision = value
                End Set
            End Property

            Public Property Serveur() As String
                Get
                    Return WsServeur
                End Get
                Set(ByVal value As String)
                    WsServeur = value
                End Set
            End Property

            Public Property Utilisateur() As String
                Get
                    Return WsUtilisateur
                End Get
                Set(ByVal value As String)
                    WsUtilisateur = value
                End Set
            End Property

            Public Property Password() As String
                Get
                    Select Case WsPassword.Length
                        Case Is = 48
                            Return WsRhFonction.DecryptageVirtualia(WsPassword)
                        Case Else
                            Return ""
                    End Select
                End Get
                Set(ByVal value As String)
                    Select Case value
                        Case Is = ""
                            WsPassword = ""
                        Case Else
                            WsPassword = WsRhFonction.CryptageVirtualia(value)
                    End Select
                End Set
            End Property

            Public ReadOnly Property PasswordStocke() As String
                Get
                    Return WsPassword
                End Get
            End Property

            Public WriteOnly Property ElementCompile(ByVal Balise As String) As String
                Set(ByVal value As String)
                    Select Case Balise
                        Case "DsnODBC"
                            DsnODBC = value
                        Case "Password"
                            WsPassword = value
                        Case "Precision"
                            PrecisionSurConnexion = value
                        Case "Provider"
                            Provider = value
                        Case "Serveur"
                            Serveur = value
                        Case "TypeCnx"
                            TypeCnx = value
                        Case "Utilisateur"
                            Utilisateur = value
                    End Select
                End Set
            End Property

            Public Sub New(ByVal host As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase)
                ObjetDatabase = host
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            End Sub

        End Class
    End Namespace
End Namespace

