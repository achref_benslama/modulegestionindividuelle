﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.Ressources.Datas
Imports Virtualia.Systeme.MetaModele.Predicats
Imports Virtualia.Systeme.MetaModele.Donnees
Imports System.Text

Namespace Datas
    Public Class EnsembleFiches
        Inherits List(Of VIR_FICHE)
        Private WsNomUtilisateur As String
        Private WsPointdeVue As Integer = VI.PointdeVue.PVueApplicatif
        Private WsNumeroObjet As Integer
        Private WsLstIde As List(Of Integer) = Nothing
        Private WsIdeDebut As Integer = 0
        Private WsIdeFin As Integer = 0
        Private WsLstFiltre As List(Of String) = Nothing

        Public WriteOnly Property NumeroObjet() As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = Nothing
                WsIdeDebut = 0
                WsIdeFin = 0
                Call ConstituerListe(False)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal SiTriDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = Nothing
                WsIdeDebut = 0
                WsIdeFin = 0
                Call ConstituerListe(SiTriDate)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal TabIde As List(Of Integer)) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = TabIde
                WsIdeDebut = 0
                WsIdeFin = 0
                Call ConstituerListe(False)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal TabIde As List(Of Integer), ByVal SiTriDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = TabIde
                WsIdeDebut = 0
                WsIdeFin = 0
                Call ConstituerListe(SiTriDate)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal IdeDebut As Integer, ByVal IdeFin As Integer) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = Nothing
                Call ConstituerListe(False)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal IdeDebut As Integer, ByVal IdeFin As Integer, ByVal SiTriDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = Nothing
                Call ConstituerListe(SiTriDate)
            End Set
        End Property

        Public WriteOnly Property NumeroObjet(ByVal TabFiltre As List(Of String), ByVal SiTriDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsNumeroObjet = value
                WsLstIde = Nothing
                WsIdeDebut = 0
                WsIdeFin = 0
                WsLstFiltre = TabFiltre
                Call ConstituerListe(SiTriDate)
            End Set
        End Property

        Public ReadOnly Property ListedesFiches() As List(Of VIR_FICHE)
            Get
                Return Me
            End Get
        End Property

        Public ReadOnly Property ListedesFiches(ByVal Ide_Dossier As Integer) As List(Of VIR_FICHE)
            Get
                Dim LstRes = New List(Of VIR_FICHE)
                Dim Predicat As New PredicateFiche(Ide_Dossier)
                LstRes = FindAll(AddressOf Predicat.SiIdentifiantOK)
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeFiches() As Integer
            Get
                Return Count
            End Get
        End Property

        Public ReadOnly Property TabIdentifiants() As List(Of Integer)
            Get
                If Count = 0 Then
                    Return Nothing
                End If

                Return ListeIdentifiants
            End Get
        End Property

        Public ReadOnly Property ListeIdentifiants() As List(Of Integer)
            Get
                If Count = 0 Then
                    Return Nothing
                End If
                Return (From instance In Me Select instance.Ide_Dossier Order By Ide_Dossier Ascending).ToList
            End Get
        End Property

        Private Sub ConstituerListe(ByVal SiTri As Boolean)
            Dim LstResultat As List(Of VIR_FICHE)
            '** Tout l'Objet
            If WsLstIde Is Nothing Then
                If WsIdeDebut > 0 And WsIdeFin > 0 Then
                    LstResultat = VirServiceServeur.LectureObjet_Plage_ToFiches(WsNomUtilisateur, WsPointdeVue, WsNumeroObjet, SiTri, WsIdeDebut, WsIdeFin)
                ElseIf WsLstFiltre Is Nothing Then
                    LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, WsPointdeVue, WsNumeroObjet, 0, SiTri)
                Else
                    LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, WsPointdeVue, WsNumeroObjet, 0, SiTri, WsLstFiltre)
                End If
            Else
                LstResultat = VirServiceServeur.LectureObjet_Plus_ToFiches(WsNomUtilisateur, WsPointdeVue, WsNumeroObjet, SiTri, WsLstIde)
            End If
            Me.Clear()
            If LstResultat Is Nothing Then
                Exit Sub
            End If
            Me.AddRange(LstResultat)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal PtdeVue As Integer)
            MyBase.New()
            WsNomUtilisateur = NomUtilisateur
            WsPointdeVue = PtdeVue
        End Sub
    End Class
End Namespace

