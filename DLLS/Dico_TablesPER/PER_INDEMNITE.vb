﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_INDEMNITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNatureRegime As String
        Private WsMontant As Double
        Private WsObservations As String
        Private WsModeCalcul As Integer
        Private WsPeriodicite As String
        Private WsRegroupement As String
        Private WsSiPartFixe As String
        Private WsMonnaieCompte As String
        Private WsMonnaiePaie As String
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_INDEMNITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaIndemnite
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 11
            End Get
        End Property

        Public Property NatureRegime() As String
            Get
                Return WsNatureRegime
            End Get
            Set(ByVal value As String)
                WsNatureRegime = F_FormatAlpha(value, 100)
                MyBase.Clef = WsNatureRegime
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return VirRhFonction.ConversionDouble(Strings.Format(WsMontant, "#0.00"))
            End Get
            Set(ByVal value As Double)
                WsMontant = VirRhFonction.ConversionDouble(F_FormatNumerique(value, 2))
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                WsObservations = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property ModedeCalcul() As Integer
            Get
                Return WsModeCalcul
            End Get
            Set(ByVal value As Integer)
                WsModeCalcul = CInt(F_FormatNumerique(value))
            End Set
        End Property

        Public Property Periodicite() As String
            Get
                Return WsPeriodicite
            End Get
            Set(ByVal value As String)
                WsPeriodicite = F_FormatAlpha(value, 20)
            End Set
        End Property

        Public Property Regroupement() As String
            Get
                Return WsRegroupement
            End Get
            Set(ByVal value As String)
                WsRegroupement = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property SiPartFixe() As String
            Get
                Return WsSiPartFixe
            End Get
            Set(ByVal value As String)
                WsSiPartFixe = value
            End Set
        End Property

        Public Property Monnaie_de_Compte() As String
            Get
                Return WsMonnaieCompte
            End Get
            Set(ByVal value As String)
                WsMonnaieCompte = F_FormatAlpha(value, 30)
            End Set
        End Property

        Public Property Monnaie_de_Paiement() As String
            Get
                Return WsMonnaiePaie
            End Get
            Set(ByVal value As String)
                WsMonnaiePaie = F_FormatAlpha(value, 30)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(NatureRegime & VI.Tild)
                Chaine.Append(Montant.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(ModedeCalcul.ToString & VI.Tild)
                Chaine.Append(Periodicite & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Regroupement & VI.Tild)
                Chaine.Append(SiPartFixe & VI.Tild)
                Chaine.Append(Monnaie_de_Compte & VI.Tild)
                Chaine.Append(Monnaie_de_Paiement & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                NatureRegime = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                Montant = VirRhFonction.ConversionDouble(TableauData(3))
                Observations = TableauData(4)
                If TableauData(5) = "" Then TableauData(5) = "0"
                ModedeCalcul = CInt(TableauData(5))
                Periodicite = TableauData(6)
                MyBase.Date_de_Fin = TableauData(7)
                Regroupement = TableauData(8)
                SiPartFixe = TableauData(9)
                Monnaie_de_Compte = TableauData(10)
                Monnaie_de_Paiement = TableauData(11)
                MyBase.Certification = TableauData(12)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateDebutPrime>" & MyBase.Date_de_Valeur & "</DateDebutPrime>" & vbCrLf)
                Chaine.Append("<DateFinPrime>" & MyBase.Date_de_Fin & "</DateFinPrime>" & vbCrLf)
                Chaine.Append("<IntitulePrime>" & NatureRegime & "</IntitulePrime>" & vbCrLf)
                Chaine.Append("<MontantPrime>" & Montant.ToString & "</MontantPrime>" & vbCrLf)
                Chaine.Append("<ModeCalculPrime>" & ModedeCalcul & "</ModeCalculPrime>" & vbCrLf)
                Chaine.Append("<PeriodicitePrime>" & Periodicite & "</PeriodicitePrime>" & vbCrLf)
                Chaine.Append("<ObservationsPrime>" & VirRhFonction.ChaineXMLValide(Observations) & "</ObservationsPrime>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(NatureRegime)
                LstGrid.Add(Montant.ToString)
                LstGrid.Add(Periodicite)
                LstGrid.Add(MyBase.Date_de_Fin)
                LstGrid.Add(Observations)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                MyBase.Date_de_Valeur = value(0)
                NatureRegime = value(1)
                Montant = VirRhFonction.ConversionDouble(value(2))
                Periodicite = value(3)
                MyBase.Date_de_Fin = value(4)
                Observations = value(5)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Intitule"
            InfoBase.Longueur = 100
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVuePrimes
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Montant"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal2"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Périodicité"
            InfoBase.Longueur = 20
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Périodicité"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de Fin"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Observations"
            InfoBase.Longueur = 250
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
