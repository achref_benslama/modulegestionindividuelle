﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CMC_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsLstSelections As List(Of CMC_SELECTION)
        Private WsFicheSql As CMC_SQL
        Private WsFicheLue As StringBuilder
        '
        Private WsPtdeVue As Integer
        Private WsIntitule As String
        Private WsUtilisateur As String
        Private WsOperateurLogique As Integer
        Private WsDateDebutExe As String
        Private WsDateFinExe As String
        Private WsSiScriptSql As Integer
        Private WsCategorieRH As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CMC_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptCmc
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property PointdeVueCMC() As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(ByVal value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property OperateurLogique() As Integer
            Get
                Return WsOperateurLogique
            End Get
            Set(ByVal value As Integer)
                WsOperateurLogique = value
            End Set
        End Property

        Public Property DatedeDebutExe() As String
            Get
                Return WsDateDebutExe
            End Get
            Set(ByVal value As String)
                WsDateDebutExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatedeFinExe() As String
            Get
                Return WsDateFinExe
            End Get
            Set(ByVal value As String)
                WsDateFinExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiScriptSql() As Integer
            Get
                Return WsSiScriptSql
            End Get
            Set(ByVal value As Integer)
                WsSiScriptSql = value
            End Set
        End Property

        Public Property CategorieRH() As Integer
            Get
                Return WsCategorieRH
            End Get
            Set(ByVal value As Integer)
                WsCategorieRH = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(PointdeVueCMC) & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(CStr(OperateurLogique) & VI.Tild)
                Chaine.Append(DatedeDebutExe & VI.Tild)
                Chaine.Append(DatedeFinExe & VI.Tild)
                Chaine.Append(CStr(SiScriptSql) & VI.Tild)
                Chaine.Append(CStr(CategorieRH))

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                PointdeVueCMC = CInt(TableauData(1))
                Intitule = TableauData(2)
                Utilisateur = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                OperateurLogique = CInt(TableauData(4))
                DatedeDebutExe = TableauData(5)
                DatedeFinExe = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                SiScriptSql = CInt(TableauData(7))
                If TableauData(8) = "" Then TableauData(8) = "0"
                CategorieRH = CInt(TableauData(8))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesSelections() As List(Of CMC_SELECTION)
            Get
                If WsLstSelections Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSelections.OrderBy(Function(m) m.Numero_Ligne).ToList
            End Get
        End Property

        Public Property Fiche_Sql() As CMC_SQL
            Get
                Return WsFicheSql
            End Get
            Set(ByVal value As CMC_SQL)
                WsFicheSql = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Selection(ByVal Fiche As CMC_SELECTION) As Integer
            If WsLstSelections Is Nothing Then
                WsLstSelections = New List(Of CMC_SELECTION)
            End If
            WsLstSelections.Add(Fiche)
            Return WsLstSelections.Count
        End Function

        Public Function Fiche_Selection(ByVal NoLigne As Integer) As CMC_SELECTION
            If ListedesSelections Is Nothing Then
                Return Nothing
            End If
            Return (From c As CMC_SELECTION In ListedesSelections Where c.Numero_Ligne = NoLigne Select c).FirstOrDefault()
        End Function
    End Class
End Namespace
