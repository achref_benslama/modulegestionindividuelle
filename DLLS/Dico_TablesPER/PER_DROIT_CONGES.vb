﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DROIT_CONGES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDroit_CA As Double = 0
        Private WsReport_CA As Double = 0
        Private WsDroit_Compteur1 As Double = 0
        Private WsDroit_Compteur2 As Double = 0
        Private WsDroit_Compteur3 As Double = 0
        Private WsDroit_ReposForfaitaire As String
        Private WsDroit_ReposCompensateur As String
        Private WsDroit_RTT As Double = 0
        Private WsObservation As String
        Private WsDebutPeriode_CA As String
        Private WsFinPeriode_CA As String
        Private WsDebutPeriode_RTT As String
        Private WsFinPeriode_RTT As String
        Private WsSiPasdeFractionnement As String
        Private WsSiDroitCA_AcquisMensuellement As String
        Private WsSiDroitRTT_AcquisMensuellement As String
        Private WsAnticipe_CA As Double = 0
        Private WsSiPasdeProrata As String
        Private WsPeriode_Compteur1 As String
        Private WsPeriode_Compteur2 As String
        Private WsPeriode_Compteur3 As String
        Private WsReport_RTT As Double = 0
        Private WsAnticipe_RTT As Double = 0
        'Traitement
        Private TsDebutFirstPeriodeLegaleCA As String
        Private TsFinFirstPeriodeLegaleCA As String
        Private TsDebutSecondPeriodeLegaleCA As String
        Private TsFinSecondPeriodeLegaleCA As String
        Private TsDateDebutCA As String
        Private TsDateFinCa As String
        Private TsDateDebutHP As String
        Private TsDateFinHP As String
        Private TsDateDebutHP1 As String
        Private TsDateFinHP1 As String
        Private TsDateDebutHP2 As String
        Private TsDateFinHP2 As String
        Private TsDateDebutARTT As String
        Private TsDateFinARTT As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DROIT_CONGES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDroits
            End Get
        End Property

        Public Property Droit_CA() As Double
            Get
                Return WsDroit_CA
            End Get
            Set(ByVal value As Double)
                WsDroit_CA = value
                MyBase.Clef = WsDroit_CA.ToString
            End Set
        End Property

        Public Property Report_CA() As Double
            Get
                Return WsReport_CA
            End Get
            Set(ByVal value As Double)
                WsReport_CA = value
            End Set
        End Property

        Public Property Droit_CompteurN1() As Double
            Get
                Return WsDroit_Compteur1
            End Get
            Set(ByVal value As Double)
                WsDroit_Compteur1 = value
            End Set
        End Property

        Public Property Droit_CompteurN2() As Double
            Get
                Return WsDroit_Compteur2
            End Get
            Set(ByVal value As Double)
                WsDroit_Compteur2 = value
            End Set
        End Property

        Public Property Droit_CompteurN3() As Double
            Get
                Return WsDroit_Compteur3
            End Get
            Set(ByVal value As Double)
                WsDroit_Compteur3 = value
            End Set
        End Property

        Public Property Droit_ReposForfaitaire() As String
            Get
                Return WsDroit_ReposForfaitaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDroit_ReposForfaitaire = value
                    Case Else
                        WsDroit_ReposForfaitaire = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Droit_ReposCompensateur() As String
            Get
                Return WsDroit_ReposCompensateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDroit_ReposCompensateur = value
                    Case Else
                        WsDroit_ReposCompensateur = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Droit_RTT() As Double
            Get
                Return WsDroit_RTT
            End Get
            Set(ByVal value As Double)
                WsDroit_RTT = value
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property DebutPeriode_CA() As String
            Get
                Return WsDebutPeriode_CA
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDebutPeriode_CA = value
                    Case Else
                        WsDebutPeriode_CA = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property FinPeriode_CA() As String
            Get
                Return WsFinPeriode_CA
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsFinPeriode_CA = value
                    Case Else
                        WsFinPeriode_CA = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property DebutPeriode_RTT() As String
            Get
                Return WsDebutPeriode_RTT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDebutPeriode_RTT = value
                    Case Else
                        WsDebutPeriode_RTT = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property FinPeriode_RTT() As String
            Get
                Return WsFinPeriode_RTT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsFinPeriode_RTT = value
                    Case Else
                        WsFinPeriode_RTT = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property SiPasdeFractionnement() As String
            Get
                Return WsSiPasdeFractionnement
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiPasdeFractionnement = "Non"
                    Case Is = "0", "Non"
                        WsSiPasdeFractionnement = "Non"
                    Case Else
                        WsSiPasdeFractionnement = "Oui"
                End Select
            End Set
        End Property

        Public Property SiDroitCA_AcquisMensuellement() As String
            Get
                Return WsSiDroitCA_AcquisMensuellement
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiDroitCA_AcquisMensuellement = "Non"
                    Case Is = "0", "Non"
                        WsSiDroitCA_AcquisMensuellement = "Non"
                    Case Else
                        WsSiDroitCA_AcquisMensuellement = "Oui"
                End Select
            End Set
        End Property

        Public Property SiDroitRTT_AcquisMensuellement() As String
            Get
                Return WsSiDroitRTT_AcquisMensuellement
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiDroitRTT_AcquisMensuellement = "Non"
                    Case Is = "0", "Non"
                        WsSiDroitRTT_AcquisMensuellement = "Non"
                    Case Else
                        WsSiDroitRTT_AcquisMensuellement = "Oui"
                End Select
            End Set
        End Property

        Public Property Anticipe_CA() As Double
            Get
                Return WsAnticipe_CA
            End Get
            Set(ByVal value As Double)
                WsAnticipe_CA = value
            End Set
        End Property

        Public Property SiPasdeProrata() As String
            Get
                Return WsSiPasdeProrata
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiPasdeProrata = "Non"
                    Case Is = "0", "Non"
                        WsSiPasdeProrata = "Non"
                    Case Else
                        WsSiPasdeProrata = "Oui"
                End Select
            End Set
        End Property

        Public Property Periode_CompteurN1() As String
            Get
                Return WsPeriode_Compteur1
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsPeriode_Compteur1 = "Non"
                    Case Is = "0", "Non"
                        WsPeriode_Compteur1 = "Non"
                    Case Else
                        WsPeriode_Compteur1 = "Oui"
                End Select
            End Set
        End Property

        Public Property Periode_CompteurN2() As String
            Get
                Return WsPeriode_Compteur2
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsPeriode_Compteur2 = "Non"
                    Case Is = "0", "Non"
                        WsPeriode_Compteur2 = "Non"
                    Case Else
                        WsPeriode_Compteur2 = "Oui"
                End Select
            End Set
        End Property

        Public Property Periode_CompteurN3() As String
            Get
                Return WsPeriode_Compteur3
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsPeriode_Compteur3 = "Non"
                    Case Is = "0", "Non"
                        WsPeriode_Compteur3 = "Non"
                    Case Else
                        WsPeriode_Compteur3 = "Oui"
                End Select
            End Set
        End Property

        Public Property Report_RTT() As Double
            Get
                Return WsReport_RTT
            End Get
            Set(ByVal value As Double)
                WsReport_RTT = value
            End Set
        End Property

        Public Property Anticipe_RTT() As Double
            Get
                Return WsAnticipe_RTT
            End Get
            Set(ByVal value As Double)
                WsAnticipe_RTT = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Select Case Droit_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_CA.ToString) & VI.Tild)
                End Select
                Select Case Report_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Report_CA.ToString) & VI.Tild)
                End Select
                Select Case Droit_CompteurN1
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_CompteurN1.ToString) & VI.Tild)
                End Select
                Select Case Droit_CompteurN2
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_CompteurN2.ToString) & VI.Tild)
                End Select
                Select Case Droit_CompteurN3
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_CompteurN3.ToString) & VI.Tild)
                End Select
                Chaine.Append(Droit_ReposForfaitaire & VI.Tild)
                Chaine.Append(Droit_ReposCompensateur & VI.Tild)
                Select Case Droit_RTT
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_RTT.ToString) & VI.Tild)
                End Select
                Chaine.Append(Observation & VI.Tild)
                Chaine.Append(DebutPeriode_CA & VI.Tild)
                Chaine.Append(FinPeriode_CA & VI.Tild)
                Chaine.Append(DebutPeriode_RTT & VI.Tild)
                Chaine.Append(FinPeriode_RTT & VI.Tild)
                Chaine.Append(SiPasdeFractionnement & VI.Tild)
                Chaine.Append(SiDroitCA_AcquisMensuellement & VI.Tild)
                Chaine.Append(SiDroitRTT_AcquisMensuellement & VI.Tild)
                Select Case Anticipe_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Anticipe_CA.ToString) & VI.Tild)
                End Select
                Chaine.Append(SiPasdeProrata & VI.Tild)
                Chaine.Append(Periode_CompteurN1 & VI.Tild)
                Chaine.Append(Periode_CompteurN2 & VI.Tild)
                Chaine.Append(Periode_CompteurN3 & VI.Tild)
                Select Case Report_RTT
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Report_RTT.ToString) & VI.Tild)
                End Select
                Select Case Anticipe_RTT
                    Case Is = 0
                        Chaine.Append("0")
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Anticipe_RTT.ToString))
                End Select
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 25 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)

                If TableauData(2) <> "" Then Droit_CA = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Report_CA = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Droit_CompteurN1 = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Droit_CompteurN2 = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Droit_CompteurN3 = VirRhFonction.ConversionDouble(TableauData(6))
                Droit_ReposForfaitaire = TableauData(7)
                Droit_ReposCompensateur = TableauData(8)

                If TableauData(9) <> "" Then Droit_RTT = VirRhFonction.ConversionDouble(TableauData(9))
                Observation = TableauData(10)
                DebutPeriode_CA = TableauData(11)
                FinPeriode_CA = TableauData(12)
                DebutPeriode_RTT = TableauData(13)
                FinPeriode_RTT = TableauData(14)
                SiPasdeFractionnement = TableauData(15)
                SiDroitCA_AcquisMensuellement = TableauData(16)
                SiDroitRTT_AcquisMensuellement = TableauData(17)

                If TableauData(18) <> "" Then Anticipe_CA = VirRhFonction.ConversionDouble(TableauData(18))
                SiPasdeProrata = TableauData(19)
                Periode_CompteurN1 = TableauData(20)
                Periode_CompteurN2 = TableauData(21)
                Periode_CompteurN3 = TableauData(22)

                If TableauData(23) <> "" Then Report_RTT = VirRhFonction.ConversionDouble(TableauData(23))
                If TableauData(24) <> "" Then Anticipe_RTT = VirRhFonction.ConversionDouble(TableauData(24))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public WriteOnly Property Date_Activation(ByVal PeriodeDebutHP As String, ByVal PeriodeFinHP As String) As String
            Set(ByVal value As String)
                Dim DateD As String
                Dim DateF As String
                '
                DateD = DebutPeriode_CA & "/" & Strings.Right(value, 4)
                DateF = FinPeriode_CA & "/" & Strings.Right(value, 4)

                Select Case VirRhDate.ComparerDates(DateD, DateF)
                    Case VI.ComparaisonDates.PlusGrand 'Cas du décalage ( exemple = secteur privé du 01/06 au 31/05)
                        Select Case VirRhDate.ComparerDates(value, DateD)
                            Case VI.ComparaisonDates.PlusPetit
                                TsDateDebutCA = DebutPeriode_CA & "/" & Strings.Trim(Val(Strings.Right(value, 4)) - 1)
                                TsDateFinCa = DateF
                            Case Else
                                TsDateDebutCA = DateD
                                TsDateFinCa = FinPeriode_CA & "/" & Strings.Trim(Val(Strings.Right(value, 4)) + 1)
                        End Select
                        TsDateDebutHP = PeriodeDebutHP & "/" & Strings.Right(TsDateDebutCA, 4)
                        TsDateFinHP = PeriodeFinHP & "/" & Strings.Right(TsDateDebutCA, 4)
                        Select Case VirRhDate.ComparerDates(TsDateDebutHP, TsDateDebutCA)
                            Case VI.ComparaisonDates.PlusPetit
                                TsDateDebutHP1 = VirRhDate.CalcDateMoinsJour(PeriodeFinHP & "/" & Strings.Right(TsDateDebutCA, 4), "1", "0")
                                TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(PeriodeDebutHP & "/" & Strings.Right(TsDateFinCa, 4), "0", "1")
                                TsDateDebutHP2 = ""
                                TsDateFinHP2 = ""
                            Case Else
                                TsDateDebutHP1 = TsDateDebutCA
                                TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(TsDateDebutHP, "0", "1")
                                TsDateDebutHP2 = VirRhDate.CalcDateMoinsJour(PeriodeFinHP & "/" & Strings.Right(TsDateDebutCA, 4), "1", "0")
                                TsDateFinHP2 = TsDateFinCa
                        End Select
                    Case Else  'Cas de l'année civile du 01/01 au 31/12
                        TsDateDebutCA = DateD
                        TsDateFinCa = DateF
                        TsDateDebutHP1 = TsDateDebutCA
                        TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(PeriodeDebutHP & "/" & Strings.Right(value, 4), "0", "1")
                        TsDateDebutHP2 = VirRhDate.CalcDateMoinsJour(PeriodeFinHP & "/" & Strings.Right(value, 4), "1", "0")
                        TsDateFinHP2 = TsDateFinCa
                        TsDateDebutHP = PeriodeDebutHP & "/" & Strings.Right(value, 4)
                        TsDateFinHP = PeriodeFinHP & "/" & Strings.Right(value, 4)
                End Select
                'Calcul de la période légale de prise de congés
                TsDebutFirstPeriodeLegaleCA = ""
                TsFinFirstPeriodeLegaleCA = ""
                TsDebutSecondPeriodeLegaleCA = ""
                TsFinSecondPeriodeLegaleCA = ""
                If TsDateDebutHP2 = "" And TsDateFinHP2 = "" Then
                    TsDebutFirstPeriodeLegaleCA = TsDateDebutCA
                    TsFinFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateDebutHP1, "0", "1")
                    TsDebutSecondPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateFinHP1, "1", "0")
                    TsFinSecondPeriodeLegaleCA = TsDateFinCa
                Else
                    TsDebutFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateFinHP1, "1", "0")
                    TsFinFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateDebutHP2, "0", "1")
                    TsDebutSecondPeriodeLegaleCA = ""
                    TsFinSecondPeriodeLegaleCA = ""
                End If
                'ARTT
                DateD = DebutPeriode_RTT & "/" & Strings.Right(value, 4)
                DateF = FinPeriode_RTT & "/" & Strings.Right(value, 4)
                Select Case VirRhDate.ComparerDates(DateD, DateF)
                    Case VI.ComparaisonDates.PlusGrand 'Cas du  01/06 au 31/05
                        Select Case VirRhDate.ComparerDates(value, DateD)
                            Case VI.ComparaisonDates.PlusPetit
                                TsDateDebutARTT = DebutPeriode_RTT & "/" & Strings.Trim(Val(Strings.Right(value, 4)) - 1)
                                TsDateFinARTT = DateF
                            Case Else
                                TsDateDebutARTT = DateD
                                TsDateFinARTT = FinPeriode_RTT & "/" & Strings.Trim(Val(Strings.Right(value, 4)) + 1)
                        End Select
                    Case Else  'Cas de l'année civile du 01/01 au 31/12
                        TsDateDebutARTT = DateD
                        TsDateFinARTT = DateF
                End Select
            End Set
        End Property

        Public ReadOnly Property Date_de_Debut_CA() As String
            Get
                Return TsDateDebutCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_CA() As String
            Get
                Return TsDateFinCa
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_PremierePeriode_Legale_CA() As String
            Get
                Return TsDebutFirstPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_PremierePeriode_Legale_CA() As String
            Get
                Return TsFinFirstPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_SecondePeriode_Legale_CA() As String
            Get
                Return TsDebutSecondPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_SecondePeriode_Legale_CA() As String
            Get
                Return TsFinSecondPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_RTT() As String
            Get
                Return TsDateDebutARTT
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_RTT() As String
            Get
                Return TsDateFinARTT
            End Get
        End Property

        Public ReadOnly Property Date_Debut_HorsPeriode() As String
            Get
                Return TsDateDebutHP
            End Get
        End Property

        Public ReadOnly Property Date_Fin_HorsPeriode() As String
            Get
                Return TsDateFinHP
            End Get
        End Property

        Public ReadOnly Property Date_Debut_T1_HorsPeriode() As String
            Get
                Return TsDateDebutHP1
            End Get
        End Property

        Public ReadOnly Property Date_Fin_T1_HorsPeriode() As String
            Get
                Return TsDateFinHP1
            End Get
        End Property

        Public ReadOnly Property Date_Debut_T2_HorsPeriode() As String
            Get
                Return TsDateDebutHP2
            End Get
        End Property

        Public ReadOnly Property Date_Fin_T2_HorsPeriode() As String
            Get
                Return TsDateFinHP2
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateValeurDroits>" & MyBase.Date_de_Valeur & "</DateValeurDroits>" & vbCrLf)
                Select Case Droit_CA
                    Case Is = 0
                        Chaine.Append("<DroitCA>" & "0" & "</DroitCA>" & vbCrLf)
                    Case Else
                        Chaine.Append("<DroitCA>" & VirRhFonction.VirgulePoint(Droit_CA.ToString) & "</DroitCA>" & vbCrLf)
                End Select
                Select Case Report_CA
                    Case Is = 0
                        Chaine.Append("<ReportCA>" & "0" & "</ReportCA>" & vbCrLf)
                    Case Else
                        Chaine.Append("<ReportCA>" & VirRhFonction.VirgulePoint(Report_CA.ToString) & "</ReportCA>" & vbCrLf)
                End Select
                Select Case Droit_CompteurN1
                    Case Is = 0
                        Chaine.Append("<DroitCompteurN1>" & "0" & "</DroitCompteurN1>" & vbCrLf)
                    Case Else
                        Chaine.Append("<DroitCompteurN1>" & VirRhFonction.VirgulePoint(Droit_CompteurN1.ToString) & "</DroitCompteurN1>" & vbCrLf)
                End Select
                Select Case Droit_CompteurN2
                    Case Is = 0
                        Chaine.Append("<DroitCompteurN2>" & "0" & "</DroitCompteurN2>" & vbCrLf)
                    Case Else
                        Chaine.Append("<DroitCompteurN2>" & VirRhFonction.VirgulePoint(Droit_CompteurN2.ToString) & "</DroitCompteurN2>" & vbCrLf)
                End Select
                Select Case Droit_CompteurN3
                    Case Is = 0
                        Chaine.Append("<DroitCompteurN3>" & "0" & "</DroitCompteurN3>" & vbCrLf)
                    Case Else
                        Chaine.Append("<DroitCompteurN3>" & VirRhFonction.VirgulePoint(Droit_CompteurN3.ToString) & "</DroitCompteurN3>" & vbCrLf)
                End Select
                Chaine.Append("<DroitReposForfaitaire>" & Droit_ReposForfaitaire & "</DroitReposForfaitaire>" & vbCrLf)
                Chaine.Append("<DroitReposCompensateur>" & Droit_ReposCompensateur & "</DroitReposCompensateur>" & vbCrLf)
                Select Case Droit_RTT
                    Case Is = 0
                        Chaine.Append("<DroitRTT>" & "0" & "</DroitRTT>" & vbCrLf)
                    Case Else
                        Chaine.Append("<DroitRTT>" & VirRhFonction.VirgulePoint(Droit_RTT.ToString) & "</DroitRTT>" & vbCrLf)
                End Select
                Chaine.Append("<DebutPeriodeCA>" & DebutPeriode_CA & "</DebutPeriodeCA>" & vbCrLf)
                Chaine.Append("<FinPeriodeCA>" & FinPeriode_CA & "</FinPeriodeCA>" & vbCrLf)
                Chaine.Append("<DebutPeriodeRTT>" & DebutPeriode_RTT & "</DebutPeriodeRTT>" & vbCrLf)
                Chaine.Append("<FinPeriodeRTT>" & FinPeriode_RTT & "</FinPeriodeRTT>" & vbCrLf)
                Chaine.Append("<SiPasdeFractionnement>" & SiPasdeFractionnement & "</SiPasdeFractionnement>" & vbCrLf)
                Chaine.Append("<SiDroitCAAcquisMensuellement>" & SiDroitCA_AcquisMensuellement & "</SiDroitCAAcquisMensuellement>" & vbCrLf)
                Chaine.Append("<SiDroitRTTAcquisMensuellement>" & SiDroitRTT_AcquisMensuellement & "</SiDroitRTTAcquisMensuellement>" & vbCrLf)
                Select Case Anticipe_CA
                    Case Is = 0
                        Chaine.Append("<AnticipeCA>" & "0" & "</AnticipeCA>" & vbCrLf)
                    Case Else
                        Chaine.Append("<AnticipeCA>" & VirRhFonction.VirgulePoint(Anticipe_CA.ToString) & "</AnticipeCA>" & vbCrLf)
                End Select
                Chaine.Append("<SiPasdeProrata>" & SiPasdeProrata & "</SiPasdeProrata>" & vbCrLf)
                Chaine.Append("<PeriodeCompteurN1>" & Periode_CompteurN1 & "</PeriodeCompteurN1>" & vbCrLf)
                Chaine.Append("<PeriodeCompteurN2>" & Periode_CompteurN2 & "</PeriodeCompteurN2>" & vbCrLf)
                Chaine.Append("<PeriodeCompteurN3>" & Periode_CompteurN3 & "</PeriodeCompteurN3>" & vbCrLf)
                Select Case Report_RTT
                    Case Is = 0
                        Chaine.Append("<ReportRTT>" & "0" & "</ReportRTT>" & vbCrLf)
                    Case Else
                        Chaine.Append("<ReportRTT>" & VirRhFonction.VirgulePoint(Report_RTT.ToString) & "</ReportRTT>" & vbCrLf)
                End Select
                Select Case Anticipe_RTT
                    Case Is = 0
                        Chaine.Append("<AnticipeRTT>" & "0" & "</AnticipeRTT>" & vbCrLf)
                    Case Else
                        Chaine.Append("<AnticipeRTT>" & VirRhFonction.VirgulePoint(Anticipe_RTT.ToString) & "</AnticipeRTT>" & vbCrLf)
                End Select

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Select Case Droit_CA
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Droit_CA.ToString)
                        End Select
                    Case 2
                        Select Case Report_CA
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Report_CA.ToString)
                        End Select
                    Case 3
                        Select Case Droit_CompteurN1
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Droit_CompteurN1.ToString)
                        End Select
                    Case 4
                        Select Case Droit_CompteurN2
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Droit_CompteurN2.ToString)
                        End Select
                    Case 5
                        Select Case Droit_CompteurN3
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Droit_CompteurN3.ToString)
                        End Select
                    Case 6
                        Return WsDroit_ReposForfaitaire
                    Case 7
                        Return WsDroit_ReposCompensateur
                    Case 8
                        Select Case Droit_RTT
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Droit_RTT.ToString)
                        End Select
                    Case 9
                        Return WsObservation
                    Case 10
                        Return WsDebutPeriode_CA
                    Case 11
                        Return WsFinPeriode_CA
                    Case 12
                        Return WsDebutPeriode_RTT
                    Case 13
                        Return WsFinPeriode_RTT
                    Case 14
                        Return WsSiPasdeFractionnement
                    Case 15
                        Return WsSiDroitCA_AcquisMensuellement
                    Case 16
                        Return WsSiDroitRTT_AcquisMensuellement
                    Case 17
                        Select Case Anticipe_CA
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Anticipe_CA.ToString)
                        End Select
                    Case 18
                        Return WsSiPasdeProrata
                    Case 19
                        Return WsPeriode_Compteur1
                    Case 20
                        Return WsPeriode_Compteur2
                    Case 21
                        Return WsPeriode_Compteur3
                    Case 22
                        Select Case Report_RTT
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Report_RTT.ToString)
                        End Select
                    Case 23
                        Select Case Anticipe_RTT
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Anticipe_RTT.ToString)
                        End Select
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(Droit_CA.ToString)
                LstGrid.Add(DebutPeriode_CA)
                LstGrid.Add(FinPeriode_CA)
                LstGrid.Add(SiPasdeFractionnement)
                LstGrid.Add(SiDroitCA_AcquisMensuellement)
                LstGrid.Add(Droit_RTT.ToString)
                LstGrid.Add(DebutPeriode_RTT)
                LstGrid.Add(FinPeriode_RTT)
                LstGrid.Add(SiDroitRTT_AcquisMensuellement)
                LstGrid.Add(Droit_CompteurN1.ToString)
                LstGrid.Add(Periode_CompteurN1)
                LstGrid.Add(Droit_CompteurN2.ToString)
                LstGrid.Add(Periode_CompteurN2)
                LstGrid.Add(Droit_CompteurN3.ToString)
                LstGrid.Add(Periode_CompteurN3)
                LstGrid.Add(Droit_ReposForfaitaire)
                LstGrid.Add(Droit_ReposCompensateur)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                MyBase.Date_de_Valeur = value(0)
                Droit_CA = VirRhFonction.ConversionDouble(value(1))
                DebutPeriode_CA = value(2)
                FinPeriode_CA = value(3)
                SiPasdeFractionnement = value(4)
                SiDroitCA_AcquisMensuellement = value(5)
                Droit_RTT = VirRhFonction.ConversionDouble(value(6))
                DebutPeriode_RTT = value(7)
                FinPeriode_RTT = value(8)
                SiDroitRTT_AcquisMensuellement = value(9)
                Droit_CompteurN1 = VirRhFonction.ConversionDouble(value(10))
                Periode_CompteurN1 = value(11)
                Droit_CompteurN2 = VirRhFonction.ConversionDouble(value(12))
                Periode_CompteurN2 = value(13)
                Droit_CompteurN3 = VirRhFonction.ConversionDouble(value(14))
                Periode_CompteurN3 = value(15)
                Droit_ReposForfaitaire = value(16)
                Droit_ReposCompensateur = value(17)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "01/01/AAAA"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Congés annuels"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de début"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "JJ/MM"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de fin"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "JJ/MM"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Pas de fractionnement ?"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Acquisition mensuelle des droits"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Journées ARTT"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de début"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "JJ/MM"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de fin"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "JJ/MM"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Acquisition mensuelle des droits"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Compteur N° 1"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de référence = RTT ?"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Compteur N° 2"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de référence = RTT ?"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Compteur N° 3"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Période de référence = RTT ?"
            InfoBase.Longueur = 3
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Booléen"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Droits à repos forfaitaire"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Heure"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Droits à repos compensateur"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Heure"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



