﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FOR_COUTS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsCoutpedagogique As Double = 0
        Private WsCoutintervenants As Double = 0
        Private WsCouthebergement As Double = 0
        Private WsCoutrestauration As Double = 0
        Private WsCoutlocation_materiel As Double = 0
        Private WsCoutlocation_salle As Double = 0
        Private WsPriseencharge_financiere As String
        Private WsMontant_priseencharge As Double = 0
        Private WsOrganismepayeur_coutpedagogique As String
        Private WsOrganismepayeur_coutnonpedagogique As String
        Private WsCoutdeplacement As Double = 0
        Private WsModalitecalcul_couts As String
        Private WsCoutsannexes_forfaitaires As Double = 0
        Private WsCoutsannexes As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_COUTS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property DateValeurcouts() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Coutpedagogique() As Double
            Get
                Return WsCoutpedagogique
            End Get
            Set(ByVal value As Double)
                WsCoutpedagogique = value
            End Set
        End Property

        Public Property Coutintervenants() As Double
            Get
                Return WsCoutintervenants
            End Get
            Set(ByVal value As Double)
                WsCoutintervenants = value
            End Set
        End Property

        Public Property Couthebergement() As Double
            Get
                Return WsCouthebergement
            End Get
            Set(ByVal value As Double)
                WsCouthebergement = value
            End Set
        End Property

        Public Property Coutrestauration() As Double
            Get
                Return WsCoutrestauration
            End Get
            Set(ByVal value As Double)
                WsCoutrestauration = value
            End Set
        End Property

        Public Property Coutlocation_materiel() As Double
            Get
                Return WsCoutlocation_materiel
            End Get
            Set(ByVal value As Double)
                WsCoutlocation_materiel = value
            End Set
        End Property

        Public Property Coutlocation_salle() As Double
            Get
                Return WsCoutlocation_salle
            End Get
            Set(ByVal value As Double)
                WsCoutlocation_salle = value
            End Set
        End Property

        Public Property Priseencharge_financiere() As String
            Get
                Return WsPriseencharge_financiere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsPriseencharge_financiere = value
                    Case Else
                        WsPriseencharge_financiere = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Montant_priseencharge() As Double
            Get
                Return WsMontant_priseencharge
            End Get
            Set(ByVal value As Double)
                WsMontant_priseencharge = value
            End Set
        End Property

        Public Property Organismepayeur_coutpedagogique() As String
            Get
                Return WsOrganismepayeur_coutpedagogique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganismepayeur_coutpedagogique = value
                    Case Else
                        WsOrganismepayeur_coutpedagogique = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Organismepayeur_coutnonpedagogique() As String
            Get
                Return WsOrganismepayeur_coutnonpedagogique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganismepayeur_coutnonpedagogique = value
                    Case Else
                        WsOrganismepayeur_coutnonpedagogique = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Coutdeplacement() As Double
            Get
                Return WsCoutdeplacement
            End Get
            Set(ByVal value As Double)
                WsCoutdeplacement = value
            End Set
        End Property

        Public Property Modalitecalcul_couts() As String
            Get
                Return WsModalitecalcul_couts
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsModalitecalcul_couts = value
                    Case Else
                        WsModalitecalcul_couts = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Coutsannexes_forfaitaires() As Double
            Get
                Return WsCoutsannexes_forfaitaires
            End Get
            Set(ByVal value As Double)
                WsCoutsannexes_forfaitaires = value
            End Set
        End Property

        Public Property Coutsannexes() As Double
            Get
                Return WsCoutsannexes
            End Get
            Set(ByVal value As Double)
                WsCoutsannexes = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Coutpedagogique.ToString & VI.Tild)
                Chaine.Append(Coutintervenants.ToString & VI.Tild)
                Chaine.Append(Couthebergement.ToString & VI.Tild)
                Chaine.Append(Coutrestauration.ToString & VI.Tild)
                Chaine.Append(Coutlocation_materiel.ToString & VI.Tild)
                Chaine.Append(Coutlocation_salle.ToString & VI.Tild)
                Chaine.Append(Priseencharge_financiere & VI.Tild)
                Chaine.Append(Montant_priseencharge.ToString & VI.Tild)
                Chaine.Append(Organismepayeur_coutpedagogique & VI.Tild)
                Chaine.Append(Organismepayeur_coutnonpedagogique & VI.Tild)
                Chaine.Append(Coutdeplacement.ToString & VI.Tild)
                Chaine.Append(Modalitecalcul_couts & VI.Tild)
                Chaine.Append(Coutsannexes_forfaitaires.ToString & VI.Tild)
                Chaine.Append(Coutsannexes.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Coutpedagogique = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Coutintervenants = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Couthebergement = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Coutrestauration = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Coutlocation_materiel = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Coutlocation_salle = VirRhFonction.ConversionDouble(TableauData(7))
                Priseencharge_financiere = TableauData(8)
                If TableauData(9) <> "" Then Montant_priseencharge = VirRhFonction.ConversionDouble(TableauData(9))
                Organismepayeur_coutpedagogique = TableauData(10)
                Organismepayeur_coutnonpedagogique = TableauData(11)
                If TableauData(12) <> "" Then Coutdeplacement = VirRhFonction.ConversionDouble(TableauData(12))
                Modalitecalcul_couts = TableauData(13)
                If TableauData(14) <> "" Then Coutsannexes_forfaitaires = VirRhFonction.ConversionDouble(TableauData(14))
                If TableauData(15) <> "" Then Coutsannexes = VirRhFonction.ConversionDouble(TableauData(15))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


