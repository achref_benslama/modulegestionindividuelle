﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_GROUPE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsGroupe As String
        Private WsDate_d_Entree As String
        Private WsDate_de_depart As String
        Private WsMotif_de_depart As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_GROUPE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 25 'Spécifique Secteur Privé
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Groupe() As String
            Get
                Return WsGroupe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsGroupe = value
                    Case Else
                        WsGroupe = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsGroupe
            End Set
        End Property

        Public Property Date_d_Entree() As String
            Get
                Return WsDate_d_Entree
            End Get
            Set(ByVal value As String)
                WsDate_d_Entree = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_depart() As String
            Get
                Return WsDate_de_depart
            End Get
            Set(ByVal value As String)
                WsDate_de_depart = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Motif_de_depart() As String
            Get
                Return WsMotif_de_depart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMotif_de_depart = value
                    Case Else
                        WsMotif_de_depart = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Groupe & VI.Tild)
                Chaine.Append(Date_d_Entree & VI.Tild)
                Chaine.Append(Date_de_depart & VI.Tild)
                Chaine.Append(Motif_de_depart & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Groupe = TableauData(1)
                Date_d_Entree = TableauData(2)
                Date_de_depart = TableauData(3)
                Motif_de_depart = TableauData(4)
                MyBase.Certification = TableauData(5)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property
        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



