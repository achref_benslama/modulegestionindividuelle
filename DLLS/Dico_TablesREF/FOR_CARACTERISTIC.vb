﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FOR_CARACTERISTIC
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsActionformation As String = ""
        Private WsNiveaudiplome_requis As String = ""
        Private WsQualification_acquise As String = ""
        Private WsMethodepedagogique As String = ""
        Private WsMoyens As String = ""
        Private WsAffectationorganigramme_niveau1 As String = ""
        Private WsNiveaustage As String = ""
        Private WsAffectationbudgetaire As String = ""
        Private WsContexte As String = ""
        Private WsTypestage As String = ""
        Private WsClassement As String = ""
        Private WsQualification_requise As String = ""
        Private WsAffectationorganigramme_niveau2 As String = ""
        Private WsAffectationetablissement As String = ""
        Private WsDatecommande As String = ""
        Private WsStatutcommande As String = ""
        Private WsReference_engagement As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_CARACTERISTIC"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Actionformation() As String
            Get
                Return WsActionformation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsActionformation = value
                    Case Else
                        WsActionformation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Niveaudiplome_requis() As String
            Get
                Return WsNiveaudiplome_requis
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNiveaudiplome_requis = value
                    Case Else
                        WsNiveaudiplome_requis = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Qualification_acquise() As String
            Get
                Return WsQualification_acquise
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsQualification_acquise = value
                    Case Else
                        WsQualification_acquise = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Methodepedagogique() As String
            Get
                Return WsMethodepedagogique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsMethodepedagogique = value
                    Case Else
                        WsMethodepedagogique = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Moyens() As String
            Get
                Return WsMoyens
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsMoyens = value
                    Case Else
                        WsMoyens = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Affectationorganigramme_niveau1() As String
            Get
                Return WsAffectationorganigramme_niveau1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsAffectationorganigramme_niveau1 = value
                    Case Else
                        WsAffectationorganigramme_niveau1 = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Niveaustage() As String
            Get
                Return WsNiveaustage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNiveaustage = value
                    Case Else
                        WsNiveaustage = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Affectationbudgetaire() As String
            Get
                Return WsAffectationbudgetaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsAffectationbudgetaire = value
                    Case Else
                        WsAffectationbudgetaire = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Contexte() As String
            Get
                Return WsContexte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsContexte = value
                    Case Else
                        WsContexte = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Typestage() As String
            Get
                Return WsTypestage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypestage = value
                    Case Else
                        WsTypestage = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Classement() As String
            Get
                Return WsClassement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsClassement = value
                    Case Else
                        WsClassement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Qualification_requise() As String
            Get
                Return WsQualification_requise
            End Get
            Set(ByVal value As String)
                WsQualification_requise = F_FormatAlpha(value, 100)
            End Set
        End Property

        Public Property Affectationorganigramme_niveau2() As String
            Get
                Return WsAffectationorganigramme_niveau2
            End Get
            Set(ByVal value As String)
                WsAffectationorganigramme_niveau2 = F_FormatAlpha(value, 100)
            End Set
        End Property

        Public Property Affectationetablissement() As String
            Get
                Return WsAffectationetablissement
            End Get
            Set(ByVal value As String)
                WsAffectationetablissement = F_FormatAlpha(value, 100)
            End Set
        End Property

        Public Property DateCommande() As String
            Get
                Return WsDatecommande
            End Get
            Set(ByVal value As String)
                WsDatecommande = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Statutcommande() As String
            Get
                Return WsStatutcommande
            End Get
            Set(ByVal value As String)
                WsStatutcommande = F_FormatAlpha(value, 80)
            End Set
        End Property

        Public Property Reference_engagement() As String
            Get
                Return WsReference_engagement
            End Get
            Set(ByVal value As String)
                WsReference_engagement = F_FormatAlpha(value, 40)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Actionformation & VI.Tild)
                Chaine.Append(Niveaudiplome_requis & VI.Tild)
                Chaine.Append(Qualification_acquise & VI.Tild)
                Chaine.Append(Methodepedagogique & VI.Tild)
                Chaine.Append(Moyens & VI.Tild)
                Chaine.Append(Affectationorganigramme_niveau1 & VI.Tild)
                Chaine.Append(Niveaustage & VI.Tild)
                Chaine.Append(Affectationbudgetaire & VI.Tild)
                Chaine.Append(Contexte & VI.Tild)
                Chaine.Append(Typestage & VI.Tild)
                Chaine.Append(Classement & VI.Tild)
                Chaine.Append(Qualification_requise & VI.Tild)
                Chaine.Append(Affectationorganigramme_niveau2 & VI.Tild)
                Chaine.Append(Affectationetablissement & VI.Tild)
                Chaine.Append(DateCommande & VI.Tild)
                Chaine.Append(Statutcommande & VI.Tild)
                Chaine.Append(Reference_engagement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)

                If (value Is Nothing) Then
                    Return
                End If
                If value = "" Then
                    Return
                End If

                Dim TableauData As String() = Strings.Split(value, VI.Tild, -1)

                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Actionformation = TableauData(1)
                Niveaudiplome_requis = TableauData(2)
                Qualification_acquise = TableauData(3)
                Methodepedagogique = TableauData(4)
                Moyens = TableauData(5)
                Affectationorganigramme_niveau1 = TableauData(6)
                Niveaustage = TableauData(7)
                Affectationbudgetaire = TableauData(8)
                Contexte = TableauData(9)
                Typestage = TableauData(10)
                Classement = TableauData(11)
                Qualification_requise = TableauData(12)
                Affectationorganigramme_niveau2 = TableauData(13)
                Affectationetablissement = TableauData(14)
                DateCommande = TableauData(15)
                Statutcommande = TableauData(16)
                Reference_engagement = TableauData(17)

                WsFicheLue = New StringBuilder()
                WsFicheLue.Append(VI.Tild)

                For Each s As String In TableauData
                    WsFicheLue.Append(s & VI.Tild)
                Next

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


