﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAYS_TAUX_CHANGE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsEnEuro As Double = 0
        Private WsInverse As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAYS_TAUX_CHANGE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 7
            End Get
        End Property

        Public Property TauxdeChange_Euro() As Double
            Get
                Return WsEnEuro
            End Get
            Set(ByVal value As Double)
                WsEnEuro = value
            End Set
        End Property

        Public Property TauxdeChange_Inverse() As Double
            Get
                Return WsInverse
            End Get
            Set(ByVal value As Double)
                WsInverse = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(TauxdeChange_Euro.ToString & VI.Tild)
                Chaine.Append(TauxdeChange_Inverse.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then TauxdeChange_Euro = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then TauxdeChange_Inverse = VirRhFonction.ConversionDouble(TableauData(3))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

