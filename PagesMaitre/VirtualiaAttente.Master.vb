﻿Public Class VirtualiaAttente
    Inherits System.Web.UI.MasterPage

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
        EtiTitreModule.Text = ""
    End Sub

    Private Sub HorlogeVirtuelle_Tick(sender As Object, e As EventArgs) Handles HorlogeVirtuelle.Tick
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

End Class