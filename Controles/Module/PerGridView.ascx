﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerGridView.ascx.vb" Inherits="Virtualia.Net.PerGridView" %>

<asp:Table ID="CadreGrid" runat="server" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:GridView ID="GridDonnee" runat="server" Caption="Virtualia"
                CaptionAlign="Left" CellPadding="2" BackColor="#B0E0D7" 
                ForeColor="#142425" Height="40px" HorizontalAlign="Left" Width="750px"
                AlternatingRowStyle-BorderStyle="None" BorderStyle="Inset" 
                Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True"
                AutoGenerateColumns="False" AutoGenerateSelectButton="True">
                <RowStyle BackColor="#DFFAF3" Font-Names="Trebuchet MS" Font-Italic="false" 
                          Font-Bold="false" Font-Size="Small" Height="18px"
                          HorizontalAlign="Left" VerticalAlign="Middle" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"/>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#225C59" ForeColor="White" Font-Bold="False" Font-Italic="True" 
                    Font-Names="Trebuchet MS" Font-Size="Small" HorizontalAlign="Left"
                    BorderStyle="Outset" BorderWidth="1px" Height="20px" VerticalAlign="Middle" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                  <asp:boundfield datafield="Colonne_1" headertext="Date de naissance"/>
                  <asp:boundfield datafield="Colonne_2" headertext="Prénom"/>
                  <asp:boundfield datafield="Colonne_3" headertext="Nom"/>
                  <asp:boundfield datafield="Colonne_4" headertext="A charge"/>
                </Columns>
            </asp:GridView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="HSelCoche" runat="server" Value="0" />
        </asp:TableCell>
   </asp:TableRow>
</asp:Table>