﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetCorrespondance
        Implements IGestionVirFicheModif(Of CORRESP_DEFINITION)

        Private WsListeTables As List(Of CORRESP_DEFINITION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesTables() As List(Of CORRESP_DEFINITION)
            Get
                Dim LstRes As List(Of CORRESP_DEFINITION)

                LstRes = (From instance In WsListeTables Select instance _
                                    Where instance.Ide_Dossier > 0 _
                                    Order By instance.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeTables() As Integer Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).NombredItems
            Get
                Return WsListeTables.Count
            End Get
        End Property

        Public Function Fiche_Definition(ByVal Intitule As String) As CORRESP_DEFINITION Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).GetFiche
            Return WsListeTables.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Function Fiche_Definition(ByVal Ide_Dossier As Integer) As CORRESP_DEFINITION Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).GetFiche
            Return WsListeTables.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).SupprimerItem
            Dim FicheDefinition As CORRESP_DEFINITION
            FicheDefinition = Fiche_Definition(Ide)
            If FicheDefinition IsNot Nothing Then
                WsListeTables.Remove(FicheDefinition)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide, "")
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of CORRESP_DEFINITION).Actualiser
            WsListeTables.Clear()
            ConstituerListe(0, "")
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer, ByVal CritereSel As String)
            WsListeTables.Clear()
            Dim FicheDefinition As CORRESP_DEFINITION

            '** 1  Liste de toutes les tables
            Dim lstdef As List(Of CORRESP_DEFINITION) = CreationCollectionVIRFICHE(Of CORRESP_DEFINITION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            If (CritereSel = "") Then
                WsListeTables.AddRange(lstdef)
            Else
                WsListeTables.AddRange((From it As CORRESP_DEFINITION In lstdef Where it.Intitule.Contains(CritereSel) Select it).ToList())
            End If
            If (WsListeTables.Count <= 0) Then
                Exit Sub
            End If

            '** 2  Liste de toutes les correspondances de valeur
            Dim lstext As List(Of CORRESP_EXTERNES) = CreationCollectionVIRFICHE(Of CORRESP_EXTERNES).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstext.ForEach(Sub(f)
                               FicheDefinition = Fiche_Definition(f.Ide_Dossier)
                               If FicheDefinition Is Nothing Then
                                   Exit Sub
                               End If
                               FicheDefinition.Ajouter_Valeurs(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeTables = New List(Of CORRESP_DEFINITION)
            ConstituerListe(Ide, "")
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Critere As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeTables = New List(Of CORRESP_DEFINITION)
            ConstituerListe(0, Critere)
        End Sub

    End Class
End Namespace
