﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class PerInfosCV
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub PerInfosCV_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If (Not Page.ClientScript.IsStartupScriptRegistered("AjaxFileUpload_change_text")) Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "AjaxFileTraduction", "AjaxFileUpload_change_text();", True)
        End If
    End Sub
End Class