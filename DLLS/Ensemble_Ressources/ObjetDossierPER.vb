﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele.Donnees

Namespace Datas
    Public Class ObjetDossierPER
        Inherits GenericDossier
        Private WsNom As String
        Private WsPrenom As String
        Private WsDateNai As String
        Private WsCritereIcone As String
        Private WsDateCritereIcone As String
        Private WsDateCritereTravail As Date
        Private WsChampSelection(10) As String 'Index = TypeArmoire
        '
        Private WsEtatLu(12) As Boolean

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                WsNom = value
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                WsPrenom = value
            End Set
        End Property

        Public Property Date_de_Naissance() As String
            Get
                Return WsDateNai
            End Get
            Set(ByVal value As String)
                WsDateNai = value
            End Set
        End Property

        Public Property CritereIcone() As String
            Get
                Return WsCritereIcone
            End Get
            Set(ByVal value As String)
                WsCritereIcone = value
            End Set
        End Property

        Public Property DateCritereIcone() As String
            Get
                Return WsDateCritereIcone
            End Get
            Set(ByVal value As String)
                WsDateCritereIcone = value
                If value <> "" Then
                    WsDateCritereTravail = System.Convert.ToDateTime(value)
                End If
            End Set
        End Property

        Public Property ChampsSelectionnes() As String()
            Get
                Return WsChampSelection
            End Get
            Set(ByVal value As String())
                WsChampSelection = value
            End Set
        End Property

        Public Property ChampSelectionne(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsChampSelection.Count - 1
                        Return WsChampSelection(Index)
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To WsChampSelection.Count - 1
                        WsChampSelection(Index) = value
                End Select
            End Set
        End Property

        Public Property EtatCategorieLu(ByVal Index As Integer) As Boolean
            Get
                Select Case Index
                    Case 0 To 12
                        Return WsEtatLu(Index)
                    Case Else
                        Return False
                End Select
            End Get
            Set(ByVal value As Boolean)
                WsEtatLu(Index) = value
            End Set
        End Property

        Public Function LireDossier(ByVal NomUtilisateur As String, ByVal ListeObjets As List(Of Integer), ByVal CategorieRh As Integer) As Boolean
            If EtatCategorieLu(CategorieRh) = True Then
                Return False
                Exit Function
            End If
            Call V_LireDossier(NomUtilisateur, ListeObjets)

            Dim FicheCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
            Dim IndiceO As Integer

            IndiceO = V_IndexTableDescendant("PER_ETATCIVIL")
            FicheCivil = CType(V_ListeDesFiches.Item(IndiceO), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
            WsNom = FicheCivil.Nom
            WsPrenom = FicheCivil.Prenom

            WsEtatLu(CategorieRh) = True
            Return True
        End Function

        Public Function SyntheseInfosPersonnelles(ByVal NomUtilisateur As String, ByVal RepRelatif As String) As String
            Dim ListeObjet = New List(Of Integer)
            Dim Cretour As Boolean
            Dim IndiceI As Integer
            Dim IndiceK As Integer
            Dim VTableau As New System.Text.StringBuilder

            ListeObjet.Add(VI.ObjetPer.ObaCivil)
            ListeObjet.Add(VI.ObjetPer.ObaAdresse)
            ListeObjet.Add(VI.ObjetPer.ObaEnfant)
            ListeObjet.Add(VI.ObjetPer.ObaBanque)
            ListeObjet.Add(VI.ObjetPer.ObaExterne)

            Cretour = LireDossier(NomUtilisateur, ListeObjet, VI.CategorieRH.InfosPersonnelles)

            Dim FichePer As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL = Nothing
            IndiceI = V_IndexTableDescendant("PER_ETATCIVIL")
            FichePer = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
            VTableau.Append(FichePer.Nom & Strings.Space(2) & FichePer.Prenom & VI.Tild)
            VTableau.Append(FichePer.Qualite & VI.Tild)
            VTableau.Append(FichePer.Nom & VI.Tild)
            VTableau.Append(FichePer.Prenom & VI.Tild)
            VTableau.Append(FichePer.Autre_prenom & VI.Tild)
            VTableau.Append(FichePer.Date_de_naissance & Strings.Space(1) & VirRhDates.CalcAge(FichePer.Date_de_naissance, VirRhDates.DateduJour, False) & " ans" & VI.Tild)
            VTableau.Append(FichePer.Lieu_de_naissance & VI.Tild)
            VTableau.Append(FichePer.Departement_ou_pays_de_naissan & VI.Tild)
            VTableau.Append(FichePer.Numero_d_identification_nation & VI.Tild)
            VTableau.Append(FichePer.Cle_securite_sociale & VI.Tild)

            Dim FicheAdr As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE = Nothing
            IndiceI = V_IndexTableDescendant("PER_DOMICILE")
            If IndiceI <> -1 Then
                FicheAdr = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE)
                VTableau.Append(FicheAdr.Numero_et_nom_de_la_rue & VI.Tild)
                VTableau.Append(FicheAdr.Complement_d_adresse & VI.Tild)
                VTableau.Append(FicheAdr.Code_postal & VI.Tild)
                VTableau.Append(FicheAdr.Ville & VI.Tild)
                VTableau.Append(FicheAdr.Numero_de_telephone & VI.Tild)
                VTableau.Append(FicheAdr.Numportable & VI.Tild)
                VTableau.Append(FicheAdr.Email & VI.Tild)
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild)
            End If

            Dim FicheRef As Virtualia.TablesObjet.ShemaPER.PER_REFEXTERNE = Nothing
            IndiceI = V_IndexTableDescendant("PER_REFEXTERNE")
            If IndiceI <> -1 Then
                FicheRef = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_REFEXTERNE)
                'If FicheRef.Photo <> "" Then
                'VTableau.Append(FicheRef.Photo & VI.Tild)
                'Else
                VTableau.Append(RepRelatif & "Images/General/silhouette.gif" & VI.Tild)
                'End If
            Else
                VTableau.Append(RepRelatif & "Images/General/silhouette.gif" & VI.Tild)
            End If

            VTableau.Append(FichePer.Situation_familiale & VI.Tild)

            Dim FicheEnf As Virtualia.TablesObjet.ShemaPER.PER_ENFANT
            IndiceK = 0
            Do
                IndiceI = V_IndexFiche(IndiceK, "PER_ENFANT")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheEnf = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ENFANT)
                If FicheEnf.Date_de_naissance <> "" Then
                    VTableau.Append(FicheEnf.Prenom & VI.Tild)
                    VTableau.Append(FicheEnf.Date_de_naissance & Strings.Space(1) & VirRhDates.CalcAge(FicheEnf.Date_de_naissance, VirRhDates.DateduJour, False) & " ans" & VI.Tild)
                End If
                IndiceK += 1
            Loop
            For IndiceI = IndiceK To 9
                VTableau.Append(VI.Tild & VI.Tild)
            Next IndiceI

            Dim FicheBnq As Virtualia.TablesObjet.ShemaPER.PER_BANQUE
            IndiceI = V_IndexTableDescendant("PER_BANQUE")
            If IndiceI <> -1 Then
                FicheBnq = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_BANQUE)
                VTableau.Append(FicheBnq.Code_Banque & VI.Tild)
                VTableau.Append(FicheBnq.Code_Guichet & VI.Tild)
                VTableau.Append(FicheBnq.NumeroduCompte & VI.Tild)
                VTableau.Append(FicheBnq.CleRIB & VI.Tild)
                VTableau.Append(FicheBnq.Domiciliation & VI.Tild)
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild)
            End If

            VTableau.Append(V_Identifiant.ToString & VI.Tild)

            If FicheRef IsNot Nothing Then
                VTableau.Append(FicheRef.Badge & VI.Tild)
                VTableau.Append(FicheRef.Matricule & VI.Tild)
                VTableau.Append(FicheRef.Numen & VI.Tild)
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild)
            End If

            VTableau.Append(VI.Tild)

            If FichePer.Autre_prenom <> "" Then
                VTableau.Append("Autres prénoms" & VI.Tild)
            Else
                VTableau.Append(VI.Tild)
            End If
            If FichePer.Date_de_naissance <> "" Then
                If FichePer.Sexe = "Féminin" Then
                    VTableau.Append("Née le" & VI.Tild)
                Else
                    VTableau.Append("Né le" & VI.Tild)
                End If
            End If
            If FichePer.Lieu_de_naissance <> "" Then
                VTableau.Append("à" & VI.Tild)
            Else
                VTableau.Append(VI.Tild)
            End If
            If FicheAdr IsNot Nothing Then
                If FicheAdr.Numero_de_telephone <> "" Then
                    VTableau.Append("Téléphone" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
                If FicheAdr.Numportable <> "" Then
                    VTableau.Append("Portable" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
                If FicheAdr.Email <> "" Then
                    VTableau.Append("Email" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild)
            End If
            If FicheRef IsNot Nothing Then
                If FicheRef.Badge <> "" Then
                    VTableau.Append("N° de badge" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
                If FicheRef.Matricule <> "" Then
                    VTableau.Append("Matricule paie" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
                If FicheRef.Numen <> "" Then
                    VTableau.Append("NUMEN" & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild)
            End If

            VTableau.Append(VI.SigneBarre)

            Return VTableau.ToString
        End Function

        Public Function SyntheseDiplomesCV(ByVal NomUtilisateur As String) As String
            Dim ListeObjet = New List(Of Integer)
            Dim Cretour As Boolean
            Dim IndiceI As Integer
            Dim IndiceK As Integer
            Dim VTableau As New System.Text.StringBuilder

            ListeObjet.Add(VI.ObjetPer.ObaCivil)
            ListeObjet.Add(VI.ObjetPer.ObaAdresse)
            ListeObjet.Add(VI.ObjetPer.ObaDiplome)
            ListeObjet.Add(VI.ObjetPer.ObaExperience)
            ListeObjet.Add(VI.ObjetPer.ObaAcquis)

            Cretour = LireDossier(NomUtilisateur, ListeObjet, VI.CategorieRH.Diplomes_Qualification)

            Dim FichePer As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL = Nothing
            IndiceI = V_IndexTableDescendant("PER_ETATCIVIL")
            FichePer = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
            VTableau.Append(FichePer.Qualite & Strings.Space(2) & FichePer.Nom & Strings.Space(2) & FichePer.Prenom & VI.Tild)
            If FichePer.Sexe = "Féminin" Then
                VTableau.Append("Née le " & FichePer.Date_de_naissance & VI.Tild)
            Else
                VTableau.Append("Né le " & FichePer.Date_de_naissance & VI.Tild)
            End If

            Dim FicheAdr As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE = Nothing
            IndiceI = V_IndexTableDescendant("PER_DOMICILE")
            If IndiceI <> -1 Then
                FicheAdr = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE)
                VTableau.Append(FicheAdr.Numero_et_nom_de_la_rue & VI.Tild)
                VTableau.Append(FicheAdr.Complement_d_adresse & VI.Tild)
                VTableau.Append(FicheAdr.Code_postal & Strings.Space(1) & FicheAdr.Ville & VI.Tild)
                VTableau.Append(FicheAdr.Email & VI.Tild)
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild & VI.Tild)
            End If

            Dim FicheDipl As Virtualia.TablesObjet.ShemaPER.PER_DIPLOME
            IndiceK = 0
            Do
                IndiceI = V_IndexFiche(IndiceK, "PER_DIPLOME")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheDipl = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_DIPLOME)
                If FicheDipl.Date_de_Valeur <> "" Then
                    VTableau.Append(FicheDipl.AnneeAcquisition & Strings.Space(1) & FicheDipl.Diplome & VI.Tild)
                End If
                IndiceK += 1
                If IndiceK > 5 Then
                    Exit Do
                End If
            Loop
            For IndiceI = IndiceK To 4
                VTableau.Append(VI.Tild)
            Next IndiceI

            Dim FicheExp As Virtualia.TablesObjet.ShemaPER.PER_EXPERIENCE_CV
            IndiceI = V_IndexTableDescendant("PER_EXPERIENCE_CV")
            If IndiceI <> -1 Then
                FicheExp = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_EXPERIENCE_CV)
                VTableau.Append(FicheExp.ExperienceProfessionnelle & VI.Tild)
            Else
                VTableau.Append(VI.Tild)
            End If

            Dim FicheAcquis As Virtualia.TablesObjet.ShemaPER.PER_ACQUIS_CV
            IndiceI = V_IndexTableDescendant("PER_ACQUIS_CV")
            If IndiceI <> -1 Then
                FicheAcquis = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ACQUIS_CV)
                VTableau.Append(FicheAcquis.AcquisTechnique & VI.Tild)
            Else
                VTableau.Append(VI.Tild)
            End If

            VTableau.Append(VI.SigneBarre)

            Return VTableau.ToString
        End Function

        Public Function SyntheseAffectation(ByVal NomUtilisateur As String, ByVal RepRelatif As String) As String
            Dim ListeObjet = New List(Of Integer)
            Dim Cretour As Boolean
            Dim IndiceI As Integer
            Dim VTableau As New System.Text.StringBuilder

            ListeObjet.Add(VI.ObjetPer.ObaCivil)
            ListeObjet.Add(VI.ObjetPer.ObaSociete)
            ListeObjet.Add(VI.ObjetPer.ObaOrganigramme)
            ListeObjet.Add(VI.ObjetPer.ObaAdrPro)
            ListeObjet.Add(VI.ObjetPer.ObaExterne)

            Cretour = LireDossier(NomUtilisateur, ListeObjet, VI.CategorieRH.AffectationsFonctionnelles)

            Dim FicheRef As Virtualia.TablesObjet.ShemaPER.PER_REFEXTERNE = Nothing
            IndiceI = V_IndexTableDescendant("PER_REFEXTERNE")
            If IndiceI <> -1 Then
                FicheRef = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_REFEXTERNE)
                'If FicheRef.Photo <> "" Then
                'VTableau.Append(FicheRef.Photo & VI.Tild)
                'Else
                VTableau.Append(RepRelatif & "Images/General/silhouette.gif" & VI.Tild)
                'End If
            Else
                VTableau.Append(RepRelatif & "Images/General/silhouette.gif" & VI.Tild)
            End If

            Dim FichePer As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL = Nothing
            IndiceI = V_IndexTableDescendant("PER_ETATCIVIL")
            FichePer = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
            VTableau.Append(FichePer.Nom & Strings.Space(2) & FichePer.Prenom & VI.Tild)

            Dim FicheOrg As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
            IndiceI = V_IndexTableDescendant("PER_AFFECTATION")
            If IndiceI <> -1 Then
                FicheOrg = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                If FicheOrg.Date_de_Valeur <> "" Then
                    VTableau.Append(FicheOrg.Fonction_exercee & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild)
            End If

            Dim FicheSoc As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE = Nothing
            IndiceI = V_IndexTableDescendant("PER_COLLECTIVITE")
            If IndiceI <> -1 Then
                FicheSoc = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE)
                If FicheSoc.Date_d_effet <> "" Then
                    VTableau.Append(FicheSoc.Administration & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild)
            End If

            IndiceI = V_IndexTableDescendant("PER_AFFECTATION")
            If IndiceI <> -1 Then
                FicheOrg = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                If FicheOrg.Date_de_Valeur <> "" Then
                    VTableau.Append(FicheOrg.Structure_de_rattachement & VI.Tild)
                    VTableau.Append(FicheOrg.Structure_d_affectation & VI.Tild)
                    VTableau.Append(FicheOrg.Structure_de_3e_niveau & VI.Tild)
                    VTableau.Append(FicheOrg.Structure_de_4e_niveau & VI.Tild)
                    VTableau.Append(FicheOrg.Structure_de_5e_niveau & VI.Tild)
                    VTableau.Append(FicheOrg.Structure_de_6e_niveau & VI.Tild)
                Else
                    VTableau.Append(VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild & VI.Tild)
            End If

            Dim FicheAdr As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO = Nothing
            IndiceI = V_IndexTableDescendant("PER_ADRESSEPRO")
            If IndiceI <> -1 Then
                FicheAdr = CType(Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO)
                VTableau.Append(FicheAdr.Email & VI.Tild)
                If FicheAdr.Telephone_professionnel <> "" Then
                    VTableau.Append("Tél: " & FicheAdr.Telephone_professionnel & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
                If FicheAdr.Numero_de_bureau <> "" Then
                    VTableau.Append("N° de bureau: " & FicheAdr.Numero_de_bureau & VI.Tild)
                Else
                    VTableau.Append(VI.Tild)
                End If
            Else
                VTableau.Append(VI.Tild & VI.Tild & VI.Tild)
            End If

            VTableau.Append(VI.SigneBarre)

            Return VTableau.ToString
        End Function

        Public Function HistoriquedeCarriere(ByVal NomUtilisateur As String, ByVal LstActivites As ArrayList, ByVal SiDoubleCarriere As Boolean) As ArrayList

            Dim Result As New ArrayList
            Dim ListeH1 As New HistoCarriere(NomUtilisateur, Me, LstActivites, SiDoubleCarriere)

            For Each lc As LigneCarriere In ListeH1.ListeHisto
                Result.Add(lc.LigneEditee)
            Next

            Return Result

        End Function

        Public Sub LireDossierRTT(ByVal NomUtilisateur As String)
            Dim ListeObjet = New List(Of Integer)
            Dim Cretour As Boolean

            ListeObjet.Add(VI.ObjetPer.ObaCivil)
            ListeObjet.Add(VI.ObjetPer.ObaSociete)
            ListeObjet.Add(VI.ObjetPer.ObaStatut)
            ListeObjet.Add(VI.ObjetPer.ObaActivite)
            ListeObjet.Add(VI.ObjetPer.ObaGrade)
            ListeObjet.Add(VI.ObjetPer.ObaExterne)
            ListeObjet.Add(VI.ObjetPer.ObaPresence)
            ListeObjet.Add(VI.ObjetPer.ObaAbsence)
            ListeObjet.Add(VI.ObjetPer.ObaFormation)
            ListeObjet.Add(VI.ObjetPer.ObaJournee)
            ListeObjet.Add(VI.ObjetPer.ObaValtemps)
            ListeObjet.Add(VI.ObjetPer.ObaFrais)
            ListeObjet.Add(VI.ObjetPer.ObaHebdoCET)
            ListeObjet.Add(VI.ObjetPer.ObaAnnualisation)
            ListeObjet.Add(VI.ObjetPer.ObaDroits)
            ListeObjet.Add(VI.ObjetPer.ObaOrganigramme)
            ListeObjet.Add(VI.ObjetPer.ObaSaisieIntranet)
            ListeObjet.Add(VI.ObjetPer.ObaModifPlanning)

            Cretour = LireDossier(NomUtilisateur, ListeObjet, VI.CategorieRH.TempsdeTravail)

        End Sub

        Public Sub New(ByVal Modele As ModeleRH)
            Me.New(Modele, 0)
        End Sub

        Public Sub New(ByVal Modele As ModeleRH, ByVal Ide As Integer)
            MyBase.New(Modele, VI.PointdeVue.PVueApplicatif, Ide)
        End Sub

    End Class
End Namespace
