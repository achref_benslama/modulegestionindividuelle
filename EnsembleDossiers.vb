﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Individuel
    Public Class EnsembleDossiers
        Inherits List(Of Individuel.DossierIndividu)
        Private WsPointeurGlobal As Session.ObjetGlobal
        Private WsDossierIndividu As Individuel.DossierIndividu
        Private WsFicheEtatCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL


        Public ReadOnly Property PointeurGlobal As Session.ObjetGlobal
            Get
                Return WsPointeurGlobal
            End Get
        End Property
        Public WriteOnly Property Identifiant(ByVal SiActualiser As Boolean) As Integer
            Set(ByVal value As Integer)
                Dim PerDossier As Individuel.DossierIndividu = ItemDossier(value)
                If PerDossier IsNot Nothing Then
                    If SiActualiser = True Then
                        Me.Remove(PerDossier)
                    Else
                        Exit Property
                    End If
                End If
                PerDossier = New Individuel.DossierIndividu(Me)
                PerDossier.Identifiant = value
                Me.Add(PerDossier)
            End Set
        End Property

        Public ReadOnly Property NatureObjet(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Integer
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return VI.TypeObjet.ObjetSimple
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dico = WsPointeurGlobal.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIdeInfo)
                If Dico.Count = 0 Then
                    Return VI.TypeObjet.ObjetSimple
                End If
                Return Dico.Item(0).PointeurModele.VParent.VNature
            End Get
        End Property

        Public Property ItemDossier(ByVal Ide As Integer) As Individuel.DossierIndividu
            Get
                Return Me.Find(Function(Recherche) Recherche.Identifiant = Ide)
            End Get
            Set(value As Individuel.DossierIndividu)
                WsDossierIndividu = value
            End Set
        End Property

        Public ReadOnly Property ObjetsDllIndividu() As List(Of Integer)
            Get
                Dim TabObjets As New List(Of Integer)
                TabObjets.Add(VI.ObjetPer.ObaCivil)
                TabObjets.Add(VI.ObjetPer.ObaConjoint)
                TabObjets.Add(VI.ObjetPer.ObaEnfant)
                TabObjets.Add(VI.ObjetPer.ObaAdresse)
                TabObjets.Add(VI.ObjetPer.ObaBanque)
                TabObjets.Add(VI.ObjetPer.ObaPrevenir)
                TabObjets.Add(VI.ObjetPer.ObaDocuments)
                TabObjets.Add(VI.ObjetPer.ObaDiplome)
                TabObjets.Add(VI.ObjetPer.ObaCompetence)
                TabObjets.Add(VI.ObjetPer.ObaLanguePratiquee)
                TabObjets.Add(VI.ObjetPer.ObaFormation)
                TabObjets.Add(VI.ObjetPer.ObaDIF)
                TabObjets.Add(VI.ObjetPer.ObaSociete)
                TabObjets.Add(VI.ObjetPer.ObaOrganigramme)
                TabObjets.Add(VI.ObjetPer.ObaAdrPro)
                TabObjets.Add(VI.ObjetPer.ObaStatut)
                TabObjets.Add(VI.ObjetPer.ObaActivite)
                TabObjets.Add(VI.ObjetPer.ObaGrade)
                TabObjets.Add(VI.ObjetPer.ObaOrigine)
                Return TabObjets
            End Get
        End Property

        Public Sub New(ByVal Host As Session.ObjetGlobal)
            WsPointeurGlobal = Host
        End Sub
    End Class
End Namespace
