﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class OUT_CONTEXTE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsParametre(24) As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "OUT_CONTEXTE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueInterface
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Parametre(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 24
                        Return WsParametre(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 24
                        Select Case value.Length
                            Case Is <= 30
                                WsParametre(Index) = value
                            Case Else
                                WsParametre(Index) = Strings.Left(value, 30)
                        End Select
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder
                Dim IndiceI As Integer
                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                For IndiceI = 0 To 24
                    Chaine.Append(Parametre(IndiceI) & VI.Tild)
                Next IndiceI

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 26 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                For IndiceI = 0 To 24
                    Parametre(IndiceI) = TableauData(IndiceI + 1)
                Next IndiceI

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
