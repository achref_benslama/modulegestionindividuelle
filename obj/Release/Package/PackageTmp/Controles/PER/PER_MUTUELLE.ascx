﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_MUTUELLE" Codebehind="PER_MUTUELLE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="270px" Width="690px" HorizontalAlign="Center"
    style="position:relative">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
                <asp:TableRow>
                   <asp:TableCell VerticalAlign="Bottom">
                     <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                     </asp:Button>
                   </asp:TableCell>
                </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelTitre" runat="server" Text="Affiliation Mutuelle" Height="20px" Width="330px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 4px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="TableauDonMutuelle" runat="server" Height="40px" CellPadding="0" Width="680px" 
            CellSpacing="0" HorizontalAlign="Left">
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                        V_PointdeVue="1" V_Objet="59" V_Information="1" V_SiDonneeDico="true"
                        EtiWidth="90px" DonWidth ="270px" DonTabIndex="1"/>
                </asp:TableCell>
                <asp:TableCell >
                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                        V_PointdeVue="1" V_Objet="59" V_Information="2" V_SiDonneeDico="true"
                        EtiWidth="170px" DonWidth ="90px" DonTabIndex="2"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VCoupleEtiDate ID="InfoD05" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="59" V_Information="5" DonTabIndex="3"/>
                </asp:TableCell>
                <asp:TableCell >
                    <Virtualia:VCoupleEtiDate ID="InfoD06" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="59" V_Information="6" DonTabIndex="4"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>  
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                    <asp:Label ID="EtiGarantie" runat="server" Text="Mutuelle - garanties souscrites"
                        Height="20px" Width="450px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; text-indent: 5px; text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server"
                        V_PointdeVue="1" V_Objet="59" V_Information="3" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth ="450px" DonTabIndex="5"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                        V_PointdeVue="1" V_Objet="59" V_Information="7" V_SiDonneeDico="true"
                        EtiWidth="150px" DonWidth ="450px" DonTabIndex="6"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VCoupleEtiDate ID="InfoD04" runat="server" TypeCalendrier="Standard" EtiWidth="150px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="59" V_Information="4" DonTabIndex="7"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
            </asp:TableRow> 
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>