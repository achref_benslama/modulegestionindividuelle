﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaAttente.master" AutoEventWireup="false" CodeBehind="FrmAttente.aspx.vb" 
    Inherits="Virtualia.Net.FrmAttente" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaAttente.master"  %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelAttente" runat="server">
        <ContentTemplate>
            <asp:Timer ID="TimerAttente" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" HorizontalAlign="Center" Width="90%" Height="800px"
                       Style="background-color:#1D1D1D ;margin-top: 1px;">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                            <ProgressTemplate>
                                <asp:Table ID="Cadre_Attente" runat="server" BackColor="Transparent">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Image ID="ImgAttente" runat="server" Height="400px" Width="800px" ImageUrl="~/Images/General/Chargement.gif" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ProgressTemplate>
                       </asp:UpdateProgress>
                    </asp:TableCell>
                 </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
