﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VFiltreListe
    
    '''<summary>
    '''Contrôle CadreFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFiltre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreChoix.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreChoix As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LstFiltres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstFiltres As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle DDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DDateDebut As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle DDateFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DDateFin As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle GridDonnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents GridDonnee As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Contrôle HSelFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelFiltre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HSelDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelDebut As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HSelFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelFin As Global.System.Web.UI.WebControls.HiddenField
End Class
