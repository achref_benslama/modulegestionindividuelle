﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EXPORT_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsLstSelections As List(Of EXPORT_SELECTION)
        Private WsFicheLue As StringBuilder
        '
        Private WsPtdeVue As Integer
        Private WsIntitule As String
        Private WsUtilisateur As String
        Private WsNomFichierSortie As String
        Private WsFormat As Integer
        Private WsSeparateur As Integer
        Private WsColonnes As Integer
        Private WsSiHistorique As Integer
        Private WsObjetFiltrant As Integer
        Private WsInfoFiltrante As Integer
        Private WsDateDebutExe As String
        Private WsDateFinExe As String
        Private WsNomFichierTranscodif As String
        Private WsCategorieRH As Integer
        Private WsScriptChaine As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EXPORT_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptExport
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property PointdeVueExport() As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(ByVal value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property NomFichierEnSortie() As String
            Get
                Return WsNomFichierSortie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNomFichierSortie = value
                    Case Else
                        WsNomFichierSortie = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property FormatSortie() As Integer
            Get
                Return WsFormat
            End Get
            Set(ByVal value As Integer)
                WsFormat = value
            End Set
        End Property

        Public Property Separateur() As Integer
            Get
                Return WsSeparateur
            End Get
            Set(ByVal value As Integer)
                WsSeparateur = value
            End Set
        End Property

        Public Property Colonnes() As Integer
            Get
                Return WsColonnes
            End Get
            Set(ByVal value As Integer)
                WsColonnes = value
            End Set
        End Property

        Public Property SiHistoriqueCoche() As Boolean
            Get
                If WsSiHistorique = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsSiHistorique = 0
                    Case True
                        WsSiHistorique = 1
                End Select
            End Set
        End Property

        Public Property ObjetFiltrant() As Integer
            Get
                Return WsObjetFiltrant
            End Get
            Set(ByVal value As Integer)
                WsObjetFiltrant = value
            End Set
        End Property

        Public Property InformationFiltrante() As Integer
            Get
                Return WsInfoFiltrante
            End Get
            Set(ByVal value As Integer)
                WsInfoFiltrante = value
            End Set
        End Property

        Public Property DatedeDebutExe() As String
            Get
                Return WsDateDebutExe
            End Get
            Set(ByVal value As String)
                WsDateDebutExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatedeFinExe() As String
            Get
                Return WsDateFinExe
            End Get
            Set(ByVal value As String)
                WsDateFinExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property NomFichierTranscodification() As String
            Get
                Return WsNomFichierTranscodif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNomFichierTranscodif = value
                    Case Else
                        WsNomFichierTranscodif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property CategorieRH() As Integer
            Get
                Return WsCategorieRH
            End Get
            Set(ByVal value As Integer)
                WsCategorieRH = value
            End Set
        End Property

        Public Property ScriptChaine() As String
            Get
                Return WsScriptChaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsScriptChaine = value
                    Case Else
                        WsScriptChaine = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(PointdeVueExport) & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(NomFichierEnSortie & VI.Tild)
                Chaine.Append(CStr(FormatSortie) & VI.Tild)
                Chaine.Append(CStr(Separateur) & VI.Tild)
                Chaine.Append(CStr(Colonnes) & VI.Tild)
                Chaine.Append(CStr(WsSiHistorique) & VI.Tild)
                Chaine.Append(CStr(ObjetFiltrant) & VI.Tild)
                Chaine.Append(CStr(InformationFiltrante) & VI.Tild)
                Chaine.Append(DatedeDebutExe & VI.Tild)
                Chaine.Append(DatedeFinExe & VI.Tild)
                Chaine.Append(NomFichierTranscodification & VI.Tild)
                Chaine.Append(CStr(CategorieRH) & VI.Tild)
                Chaine.Append(ScriptChaine)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                PointdeVueExport = CInt(TableauData(1))
                Intitule = TableauData(2)
                Utilisateur = TableauData(3)
                NomFichierEnSortie = TableauData(4)
                For IndiceI = 5 To 10
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                FormatSortie = CInt(TableauData(5))
                Separateur = CInt(TableauData(6))
                Colonnes = CInt(TableauData(7))
                SiHistoriqueCoche = CInt(TableauData(8))
                ObjetFiltrant = CInt(TableauData(9))
                InformationFiltrante = CInt(TableauData(10))
                DatedeDebutExe = TableauData(11)
                DatedeFinExe = TableauData(12)
                NomFichierTranscodification = TableauData(13)
                If TableauData(14) = "" Then TableauData(14) = "0"
                CategorieRH = CInt(TableauData(14))
                ScriptChaine = TableauData(15)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesSelections() As List(Of EXPORT_SELECTION)
            Get
                If WsLstSelections Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSelections.OrderBy(Function(m) m.Numero_Ligne).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Selection(ByVal Fiche As EXPORT_SELECTION) As Integer
            If WsLstSelections Is Nothing Then
                WsLstSelections = New List(Of EXPORT_SELECTION)
            End If
            WsLstSelections.Add(Fiche)
            Return WsLstSelections.Count
        End Function

        Public Function Fiche_Selection(ByVal NoLigne As Integer) As EXPORT_SELECTION

            If WsLstSelections Is Nothing Then
                Return Nothing
            End If

            Return (From it As EXPORT_SELECTION In WsLstSelections Where it.Numero_Ligne = NoLigne Select it).FirstOrDefault()

        End Function
    End Class
End Namespace
