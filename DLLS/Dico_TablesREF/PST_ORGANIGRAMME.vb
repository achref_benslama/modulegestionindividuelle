﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PST_ORGANIGRAMME
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNiveau1 As String
        Private WsNiveau2 As String
        Private WsNiveau3 As String
        Private WsNiveau4 As String
        Private WsNiveau5 As String
        Private WsNiveau6 As String
        Private WsSitegeographique As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PST_ORGANIGRAMME"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteFct
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Niveau1() As String
            Get
                Return WsNiveau1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau1 = value
                    Case Else
                        WsNiveau1 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Niveau2() As String
            Get
                Return WsNiveau2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau2 = value
                    Case Else
                        WsNiveau2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Niveau3() As String
            Get
                Return WsNiveau3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau3 = value
                    Case Else
                        WsNiveau3 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Niveau4() As String
            Get
                Return WsNiveau4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau4 = value
                    Case Else
                        WsNiveau4 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Niveau5() As String
            Get
                Return WsNiveau5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau5 = value
                    Case Else
                        WsNiveau5 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Niveau6() As String
            Get
                Return WsNiveau6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNiveau6 = value
                    Case Else
                        WsNiveau6 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Sitegeographique() As String
            Get
                Return WsSitegeographique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsSitegeographique = value
                    Case Else
                        WsSitegeographique = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Niveau1 & VI.Tild)
                Chaine.Append(Niveau2 & VI.Tild)
                Chaine.Append(Niveau3 & VI.Tild)
                Chaine.Append(Niveau4 & VI.Tild)
                Chaine.Append(Niveau5 & VI.Tild)
                Chaine.Append(Niveau6 & VI.Tild)
                Chaine.Append(Sitegeographique)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Niveau1 = TableauData(2)
                Niveau2 = TableauData(3)
                Niveau3 = TableauData(4)
                Niveau4 = TableauData(5)
                Niveau5 = TableauData(6)
                Niveau6 = TableauData(7)
                Sitegeographique = TableauData(8)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
