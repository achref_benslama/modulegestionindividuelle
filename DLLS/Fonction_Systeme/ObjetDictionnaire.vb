﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Outils
        Public Class ObjetDictionnaire
            Private WsIndex As Integer
            Private WsSiExperte As Boolean
            Private WsPointdeVue As Integer
            Private WsNoObjet As Integer
            Private WsNoInfo As Integer
            Private WsIntitule As String
            Private WsDonNature As Integer
            Private WsDonFormat As Integer
            Private WsSiMaj As Boolean
            Private WsSiCmc As Boolean
            Private WsSiStats As Boolean
            Private WsSiImprimer As Boolean
            Private WsSiExport As Boolean
            Private WsSiImport As Boolean

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property SiExperte() As Boolean
                Get
                    Return WsSiExperte
                End Get
                Set(ByVal value As Boolean)
                    WsSiExperte = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property Objet() As Integer
                Get
                    Return WsNoObjet
                End Get
                Set(ByVal value As Integer)
                    WsNoObjet = value
                End Set
            End Property

            Public Property Information() As Integer
                Get
                    Return WsNoInfo
                End Get
                Set(ByVal value As Integer)
                    WsNoInfo = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public Property NatureDonnee() As Integer
                Get
                    Return WsDonNature
                End Get
                Set(ByVal value As Integer)
                    WsDonNature = value
                End Set
            End Property

            Public Property FormatDonnee() As Integer
                Get
                    Return WsDonFormat
                End Get
                Set(ByVal value As Integer)
                    WsDonFormat = value
                End Set
            End Property

            Public Property SiMaj() As Boolean
                Get
                    Return WsSiMaj
                End Get
                Set(ByVal value As Boolean)
                    WsSiMaj = value
                End Set
            End Property

            Public Property SiCMC() As Boolean
                Get
                    Return WsSiCmc
                End Get
                Set(ByVal value As Boolean)
                    WsSiCmc = value
                End Set
            End Property

            Public Property SiStats() As Boolean
                Get
                    Return WsSiStats
                End Get
                Set(ByVal value As Boolean)
                    WsSiStats = value
                End Set
            End Property

            Public Property SiExport() As Boolean
                Get
                    Return WsSiExport
                End Get
                Set(ByVal value As Boolean)
                    WsSiExport = value
                End Set
            End Property

            Public Property SiImport() As Boolean
                Get
                    Return WsSiImport
                End Get
                Set(ByVal value As Boolean)
                    WsSiImport = value
                End Set
            End Property

            Public Property SiImprimer() As Boolean
                Get
                    Return WsSiImprimer
                End Get
                Set(ByVal value As Boolean)
                    WsSiImprimer = value
                End Set
            End Property

            Public Sub New()
                MyBase.New()
            End Sub
        End Class
    End Namespace
End Namespace

