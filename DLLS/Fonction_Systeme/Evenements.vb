﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Evenements
    Public Class FicheSelectionEventArgs
        Inherits EventArgs
        Private WsFicheSel As Virtualia.Systeme.MetaModele.VIR_FICHE = Nothing
        Private WsID_Appelant As String = ""

        Public Sub New(ByVal Appelant_ID As String, ByVal Fiche As Virtualia.Systeme.MetaModele.VIR_FICHE)
            WsFicheSel = Fiche
        End Sub

        Public ReadOnly Property ControleAppelant() As String
            Get
                Return WsID_Appelant
            End Get
        End Property

        Public ReadOnly Property Fiche_Selectionnee As Virtualia.Systeme.MetaModele.VIR_FICHE
            Get
                Return WsFicheSel
            End Get
        End Property
    End Class

    Public Class ArmoireSelectionEventArgs
        Inherits EventArgs
        Private WsLstCriteres As List(Of String)
        Private WsLstIde As List(Of Integer)
        Private WsChoixMenu As String

        Public Sub New(ByVal Choix As String, ByVal ListeCriteres As List(Of String), ByVal ListeIdentifiants As List(Of Integer))
            WsChoixMenu = Choix
            WsLstCriteres = ListeCriteres
            WsLstIde = ListeIdentifiants
        End Sub

        Public Sub New(ByVal Choix As String, ByVal ListeCriteres As List(Of String))
            WsChoixMenu = Choix
            WsLstCriteres = ListeCriteres
            WsLstIde = Nothing
        End Sub

        Public Sub New(ByVal Choix As String, ByVal ListeIdentifiants As List(Of Integer))
            WsChoixMenu = Choix
            WsLstCriteres = Nothing
            WsLstIde = ListeIdentifiants
        End Sub

        Public ReadOnly Property Choix_Menu As String
            Get
                Return WsChoixMenu
            End Get
        End Property

        Public ReadOnly Property Liste_Criteres As List(Of String)
            Get
                Return WsLstCriteres
            End Get
        End Property

        Public ReadOnly Property Liste_Identifiants As List(Of Integer)
            Get
                Return WsLstIde
            End Get
        End Property
    End Class

    Public Class DonneeChangeEventArgs
        Inherits EventArgs
        Private WsData As String
        Private WsParam As String
        Private WsTableau As ArrayList

        Public Sub New(ByVal Param As String, ByVal Donnee As String)
            WsParam = Param
            WsData = Donnee
        End Sub

        Public Sub New(ByVal Donnee As String)
            WsData = Donnee
        End Sub

        Public Sub New(ByVal Tableau As ArrayList)
            WsTableau = Tableau
        End Sub

        Public ReadOnly Property Valeur() As String
            Get
                Return WsData
            End Get
        End Property

        Public ReadOnly Property TableauValeurs() As ArrayList
            Get
                Return WsTableau
            End Get
        End Property

        Public ReadOnly Property Parametre() As String
            Get
                Return WsParam
            End Get
        End Property
    End Class

    Public Class AppelTableEventArgs
        Inherits EventArgs
        Private WsPointdeVue As Integer
        Private WsNomTable As String
        Private WsID_Appelant As String
        Private WsNumObjetAppelant As Integer
        Private WsDateValeur As String

        Public Sub New(ByVal Appelant_ID As String, ByVal NumObjetAppelant As Integer, ByVal Pvue As Integer, ByVal NomTable As String, ByVal DateValeur As String)
            WsID_Appelant = Appelant_ID
            WsNumObjetAppelant = NumObjetAppelant
            WsPointdeVue = Pvue
            WsNomTable = NomTable
            WsDateValeur = DateValeur
        End Sub

        Public Sub New(ByVal Appelant_ID As String, ByVal NumObjetAppelant As Integer, ByVal Pvue As Integer, ByVal NomTable As String)
            WsID_Appelant = Appelant_ID
            WsNumObjetAppelant = NumObjetAppelant
            WsPointdeVue = Pvue
            WsNomTable = NomTable
            WsDateValeur = ""
        End Sub

        Public ReadOnly Property ControleAppelant() As String
            Get
                Return WsID_Appelant
            End Get
        End Property

        Public ReadOnly Property ObjetAppelant() As Integer
            Get
                Return WsNumObjetAppelant
            End Get
        End Property

        Public ReadOnly Property PointdeVueInverse() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property NomdelaTable() As String
            Get
                Return WsNomTable
            End Get
        End Property

        Public ReadOnly Property DatedeValeur() As String
            Get
                Return WsDateValeur
            End Get
        End Property
    End Class

    Public Class ValeurSelectionneeEventArgs
        Inherits EventArgs
        Private WsID_Appelant As String
        Private WsNoObjet_Appelant As Integer
        Private WsPtdeVue As Integer
        Private WsNomTable As String
        Private WsIdentifiant As Integer
        Private WsValeur As String
        Private WsCode As String

        Public Sub New(ByVal Appelant_ID As String, ByVal NumObjet As Integer, ByVal Pvue As Integer, ByVal NomTable As String, ByVal Ide As Integer, ByVal ValeurSel As String, ByVal CodeSel As String)
            WsID_Appelant = Appelant_ID
            WsNoObjet_Appelant = NumObjet
            WsPtdeVue = Pvue
            WsNomTable = NomTable
            WsIdentifiant = Ide
            WsValeur = ValeurSel
            WsCode = CodeSel
        End Sub

        Public ReadOnly Property ControleAppelant() As String
            Get
                Return WsID_Appelant
            End Get
        End Property

        Public ReadOnly Property ObjetAppelant() As Integer
            Get
                Return WsNoObjet_Appelant
            End Get
        End Property

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPtdeVue
            End Get
        End Property

        Public ReadOnly Property NomdelaTable() As String
            Get
                Return WsNomTable
            End Get
        End Property

        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property Valeur() As String
            Get
                Return WsValeur
            End Get
        End Property

        Public ReadOnly Property Code() As String
            Get
                Return WsCode
            End Get
        End Property
    End Class

    Public Class DossierClickEventArgs
        Inherits EventArgs
        Private WsIdeDossier As Integer
        Private WsValeur As String
        Private WsProfondeur As Integer

        Public Sub New(ByVal Ide As Integer, ByVal Valeur As String, ByVal Niveau As Integer)
            WsIdeDossier = Ide
            WsValeur = Valeur
            WsProfondeur = Niveau
        End Sub

        Public Sub New(ByVal Ide As Integer, ByVal Valeur As String)
            WsIdeDossier = Ide
            WsValeur = Valeur
            WsProfondeur = 0
        End Sub

        Public Sub New(ByVal Ide As Integer)
            WsIdeDossier = Ide
            WsValeur = ""
            WsProfondeur = 0
        End Sub

        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdeDossier
            End Get
        End Property

        Public ReadOnly Property Valeur() As String
            Get
                Return WsValeur
            End Get
        End Property

        Public ReadOnly Property Profondeur() As Integer
            Get
                Return WsProfondeur
            End Get
        End Property
    End Class

    Public Class AppelAlbumClickEventArgs
        Inherits EventArgs
        Private WsPointdeVue As Integer
        Private WsOutil As Integer

        Public Sub New(ByVal PtdeVue As Integer, ByVal SelOutil As Integer)
            WsPointdeVue = PtdeVue
            WsOutil = SelOutil
        End Sub

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property Outil() As Integer
            Get
                Return WsOutil
            End Get
        End Property
    End Class

    Public Class MessageRetourEventArgs
        Inherits EventArgs
        Private WsIDEmetteur As String
        Private WsNumObjet As Integer = 0
        Private WsMsg_Reponse As String
        Private WsMsg_Datas As String = ""

        Public Sub New(ByVal IDEmetteur As String, ByVal NumObjet As Integer, ByVal Datas As String, ByVal Reponse As String)
            WsIDEmetteur = IDEmetteur
            WsNumObjet = NumObjet
            WsMsg_Reponse = Reponse
            WsMsg_Datas = Datas
        End Sub

        Public ReadOnly Property Emetteur() As String
            Get
                Return WsIDEmetteur
            End Get
        End Property

        Public ReadOnly Property ReponseMsg() As String
            Get
                Return WsMsg_Reponse
            End Get
        End Property

        Public ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public ReadOnly Property ChaineDatas() As String
            Get
                Return WsMsg_Datas
            End Get
        End Property

    End Class

    Public Class MessageSaisieEventArgs
        Inherits EventArgs
        Private WsIDEmetteur As String
        Private WsNumObjet As Integer = 0
        Private WsMsg_Nature As String
        Private WsMsg_Titre As String
        Private WsMsg_Contenu As String
        Private WsMsg_Datas As String = ""

        Public Sub New(ByVal IDEmetteur As String, ByVal Nature As String, ByVal Contenu As String)
            WsIDEmetteur = IDEmetteur
            WsMsg_Nature = Nature
            WsMsg_Titre = ""
            WsMsg_Contenu = Contenu
        End Sub

        Public Sub New(ByVal IDEmetteur As String, ByVal NumObjet As Integer, ByVal Datas As String, ByVal Nature As String, ByVal Titre As String, ByVal Contenu As String)
            WsIDEmetteur = IDEmetteur
            WsNumObjet = NumObjet
            WsMsg_Nature = Nature
            WsMsg_Titre = Titre
            WsMsg_Contenu = Contenu
            WsMsg_Datas = Datas
        End Sub

        Public ReadOnly Property Emetteur() As String
            Get
                Return WsIDEmetteur
            End Get
        End Property

        Public ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public ReadOnly Property ChaineDatas() As String
            Get
                Return WsMsg_Datas
            End Get
        End Property

        Public ReadOnly Property NatureMessage() As String
            Get
                Return WsMsg_Nature
            End Get
        End Property

        Public ReadOnly Property ContenuMessage() As String
            Get
                Return WsMsg_Contenu
            End Get
        End Property

        Public ReadOnly Property TitreMessage() As String
            Get
                Return WsMsg_Titre
            End Get
        End Property
    End Class

    Public Class ExecuterEventArgs
        Inherits EventArgs
        Private WsNomTable As String
        Private WsNoPage As Integer

        Public Sub New(ByVal NomTable As String, ByVal NoPage As Integer)
            WsNomTable = NomTable
            WsNoPage = NoPage
        End Sub

        Public ReadOnly Property NumeroPage() As Integer
            Get
                Return WsNoPage
            End Get
        End Property

        Public ReadOnly Property NomdelaTable() As String
            Get
                Return WsNomTable
            End Get
        End Property

    End Class

    Public Class DureeTacheEventArgs
        Inherits EventArgs
        Private WsHeureDebut As String
        Private WsHeureFin As String
        Private WsMsgCount As Integer

        Public Sub New(ByVal Hdebut As String, ByVal HFin As String, ByVal Cpt As Integer)
            WsHeureDebut = Hdebut
            WsHeureFin = HFin
            WsMsgCount = Cpt
        End Sub

        Public ReadOnly Property HeuredeDebut() As String
            Get
                Return WsHeureDebut
            End Get
        End Property

        Public ReadOnly Property HeuredeFin() As String
            Get
                Return WsHeureFin
            End Get
        End Property

        Public ReadOnly Property Compteur() As Integer
            Get
                Return WsMsgCount
            End Get
        End Property
    End Class

    Public Class AppelChoixEventArgs
        Inherits EventArgs
        Private WsValeurs As ArrayList
        Private WsNumLigne As Integer
        Private WsAppelant As String

        Public Sub New(ByVal IndexLigne As Integer, ByVal ListeValeurs As ArrayList, ByVal TypeAppel As String)
            WsValeurs = ListeValeurs
            WsNumLigne = IndexLigne
            WsAppelant = TypeAppel
        End Sub

        Public Sub New(ByVal IndexLigne As Integer, ByVal ListeValeurs As ArrayList)
            WsValeurs = ListeValeurs
            WsNumLigne = IndexLigne
            WsAppelant = ""
        End Sub

        Public Sub New(ByVal ListeValeurs As ArrayList)
            WsValeurs = ListeValeurs
            WsNumLigne = 0
        End Sub

        Public ReadOnly Property Valeurs() As ArrayList
            Get
                Return WsValeurs
            End Get
        End Property

        Public ReadOnly Property IndiceLigne() As Integer
            Get
                Return WsNumLigne
            End Get
        End Property

        Public ReadOnly Property Appelant() As String
            Get
                Return WsAppelant
            End Get
        End Property
    End Class

    Public Class ScriptResultatEventArgs
        Inherits EventArgs
        Private WsLibels As List(Of String)
        Private WsColonnes As List(Of String)
        Private WsValeurs As List(Of String)
        Private WsTabCentrage As List(Of Integer)
        Private WsTitre As String

        Public Sub New(ByVal Libels As List(Of String), ByVal LibCols As List(Of String), ByVal Valeurs As List(Of String), ByVal TabCentrage As List(Of Integer))
            WsLibels = Libels
            WsColonnes = LibCols
            WsValeurs = Valeurs
            WsTabCentrage = TabCentrage
            WsTitre = ""
        End Sub

        Public Sub New(ByVal Libels As List(Of String), ByVal LibCols As List(Of String), ByVal Valeurs As List(Of String), ByVal TabCentrage As List(Of Integer), _
                       ByVal Intitule As String)
            WsLibels = Libels
            WsColonnes = LibCols
            WsValeurs = Valeurs
            WsTabCentrage = TabCentrage
            WsTitre = Intitule
        End Sub

        Public ReadOnly Property Libelles() As List(Of String)
            Get
                Return WsLibels
            End Get
        End Property

        Public ReadOnly Property IntitulesColonnes() As List(Of String)
            Get
                Return WsColonnes
            End Get
        End Property

        Public ReadOnly Property Valeurs() As List(Of String)
            Get
                Return WsValeurs
            End Get
        End Property

        Public ReadOnly Property Centrage_Colonne() As List(Of Integer)
            Get
                Return WsTabCentrage
            End Get
        End Property

        Public ReadOnly Property Titre() As String
            Get
                Return WsTitre
            End Get
        End Property

    End Class

    Public Class ListeSgbdEventArgs
        Inherits EventArgs
        Private WsNoBd As Integer
        Private WsNom As String
        Private WsIntitule As String
        Private WsFormatDates As Integer
        Private WsSchema As String
        Private WsCarDecimal As String
        Private WsTypeSgbd As Integer
        Private WsTypeSgbdAlpha As String
        Private WsDsnOdbc As String
        Private WsUtilisateur As String
        Private WsMotPasse As String
        Private WsServeurBd As String
        Private WsStatus As String

        Public Sub New(ByVal NoBd As Integer, ByVal Nom As String, ByVal Intitule As String, ByVal FormatDates As Integer, ByVal Shema As String, ByVal CarDecimal As String, ByVal TypeSgbd As Integer, ByVal LibSgbd As String, ByVal Dsn As String, ByVal Serveur As String, ByVal Uti As String, ByVal Pw As String, ByVal Statut As String)
            WsNoBd = NoBd
            WsNom = Nom
            WsIntitule = Intitule
            WsFormatDates = FormatDates
            WsSchema = Shema
            WsCarDecimal = CarDecimal
            WsTypeSgbd = TypeSgbd
            WsTypeSgbdAlpha = LibSgbd
            WsDsnOdbc = Dsn
            WsServeurBd = Serveur
            WsUtilisateur = Uti
            WsMotPasse = Pw
            WsStatus = Statut
        End Sub

        Public ReadOnly Property NumeroBd() As Integer
            Get
                Return WsNoBd
            End Get
        End Property

        Public ReadOnly Property NomBd() As String
            Get
                Return WsNom
            End Get
        End Property

        Public ReadOnly Property IntituleBd() As String
            Get
                Return WsIntitule
            End Get
        End Property

        Public ReadOnly Property FormatDates() As Integer
            Get
                Return WsFormatDates
            End Get
        End Property

        Public ReadOnly Property Schema() As String
            Get
                Return WsSchema
            End Get
        End Property

        Public ReadOnly Property SymboleDecimal() As String
            Get
                Return WsCarDecimal
            End Get
        End Property

        Public ReadOnly Property TypeduSgbd() As Integer
            Get
                Return WsTypeSgbd
            End Get
        End Property

        Public ReadOnly Property TypeduSgbdAlpha() As String
            Get
                Return WsTypeSgbdAlpha
            End Get
        End Property

        Public ReadOnly Property DsnODBC() As String
            Get
                Return WsDsnOdbc
            End Get
        End Property

        Public ReadOnly Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
        End Property

        Public ReadOnly Property MotdePasse() As String
            Get
                Return WsMotPasse
            End Get
        End Property

        Public ReadOnly Property ServeurDB() As String
            Get
                Return WsServeurBd
            End Get
        End Property

        Public ReadOnly Property Statut() As String
            Get
                Return WsStatus
            End Get
        End Property

    End Class

    Public Class Liste_RefreshEventArgs
        Inherits EventArgs
        Private WsPointdeVue As Integer

        Public Sub New(ByVal PtdeVue As Integer)
            WsPointdeVue = PtdeVue
        End Sub

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

    End Class

    Public Class CritereSelectionneEventArgs
        Inherits EventArgs
        Public Const ISelection As Integer = 0
        Public Const IExit As Integer = 1

        Private WsPointdeVue As Integer
        Private WsIdeDossier As Integer
        Private WsIdeCritere As Integer
        Private WsTypeEvenement As Integer

        Public Sub New(ByVal PtdeVue As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = 0
            WsIdeCritere = 0
            WsTypeEvenement = ISelection
        End Sub

        Public Sub New(ByVal PtdeVue As Integer, ByVal IdeDossier As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = IdeDossier
            WsIdeCritere = 0
            WsTypeEvenement = ISelection
        End Sub

        Public Sub New(ByVal PtdeVue As Integer, ByVal IdeDossier As Integer, ByVal IdeCritere As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = IdeDossier
            WsIdeCritere = IdeCritere
            WsTypeEvenement = ISelection
        End Sub

        Public Sub New(ByVal PtdeVue As Integer, ByVal IdeDossier As Integer, ByVal IdeCritere As Integer, ByVal ITypeEvenement As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = IdeDossier
            WsIdeCritere = IdeCritere
            WsTypeEvenement = ITypeEvenement
        End Sub

        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdeDossier
            End Get
        End Property

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property IdeCritere() As Integer
            Get
                Return WsIdeCritere
            End Get
        End Property

        Public ReadOnly Property TypeEvenement() As Integer
            Get
                Return WsTypeEvenement
            End Get
        End Property

    End Class

    Public Class CritereAjouterEventArgs
        Inherits EventArgs

        Private WsPointdeVue As Integer
        Private WsIdeDossier As Integer

        Public Sub New(ByVal PtdeVue As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = 0
        End Sub

        Public Sub New(ByVal PtdeVue As Integer, ByVal IdeDossier As Integer)
            WsPointdeVue = PtdeVue
            WsIdeDossier = IdeDossier
        End Sub


        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdeDossier
            End Get
        End Property

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

    End Class

    Public Class DictionnaireEventArgs
        Inherits EventArgs
        Private WsVIndex As Integer
        Private WsPointdeVue As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer

        Public ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public ReadOnly Property NumeroInfo() As Integer
            Get
                Return WsNumInfo
            End Get
        End Property

        Public ReadOnly Property IndexListe() As Integer
            Get
                Return WsVIndex
            End Get
        End Property

        Public Sub New(ByVal ValeurIndex As Integer)
            WsVIndex = ValeurIndex
        End Sub

        Public Sub New(ByVal Ptdevue As Integer, ByVal Objet As Integer, ByVal Info As Integer)
            WsPointdeVue = Ptdevue
            WsNumObjet = Objet
            WsNumInfo = Info
        End Sub

    End Class

End Namespace