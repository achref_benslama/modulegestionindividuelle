﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPrime
        Implements IGestionVirFicheModif(Of PRM_INDEMNITE)

        Private WsListePrimes As List(Of PRM_INDEMNITE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesAbsences() As List(Of PRM_INDEMNITE)
            Get
                Dim LstRes As List(Of PRM_INDEMNITE)
                LstRes = (From instance In WsListePrimes Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredItems() As Integer Implements IGestionVirFicheModif(Of PRM_INDEMNITE).NombredItems
            Get
                Return WsListePrimes.Count
            End Get
        End Property

        Public Function Fiche_Prime(ByVal Ide_Dossier As Integer) As PRM_INDEMNITE Implements IGestionVirFicheModif(Of PRM_INDEMNITE).GetFiche
            Return WsListePrimes.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Prime(ByVal Intitule As String) As PRM_INDEMNITE Implements IGestionVirFicheModif(Of PRM_INDEMNITE).GetFiche

            Return WsListePrimes.Find(Function(m) m.Intitule = Intitule)

        End Function

        Public Function Fiche_Prime_ParReference(ByVal Code As String) As PRM_INDEMNITE
            Return WsListePrimes.Find(Function(m) m.Reference = Code)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of PRM_INDEMNITE).SupprimerItem
            Dim FicheInd As PRM_INDEMNITE
            FicheInd = Fiche_Prime(Ide)
            If FicheInd IsNot Nothing Then
                WsListePrimes.Remove(FicheInd)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of PRM_INDEMNITE).AjouterItem
            SupprimerItem(Ide)
            Actualiser()
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of PRM_INDEMNITE).Actualiser
            WsListePrimes.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListePrimes.Clear()
            '** 1  Liste de toutes les valeurs
            WsListePrimes.AddRange(CreationCollectionVIRFICHE(Of PRM_INDEMNITE).GetCollection(WsNomUtilisateur, VirServiceServeur))
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListePrimes = New List(Of PRM_INDEMNITE)
            Call ConstituerListe()
        End Sub

    End Class
End Namespace


