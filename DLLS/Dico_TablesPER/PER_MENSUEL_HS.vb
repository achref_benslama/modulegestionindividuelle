﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MENSUEL_HS
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsNbHeures_Jour_20h As Double
        Private WsNbHeures_Jour_22h As Double
        Private WsNbHeures_Nuit As Double
        Private WsNbHeures_Trajet_HS As Double
        Private WsNbHeures_JF_20h As Double
        Private WsNbHeures_JF_22h As Double
        Private WsNbHeures_JF_Nuit As Double
        Private WsNbHeures_Trajet_JF As Double
        '
        Private WsDate_DemandeHeures As String
        Private WsObservations_Demande As String
        Private WsDate_Visa_Manager As String
        Private WsSignataire_Manager As String
        Private WsVisa_Manager As String
        Private WsObservations_Manager As String
        Private WsDate_Visa_GRH As String
        Private WsSignataire_GRH As String
        Private WsVisa_GRH As String
        Private WsObservations_GRH As String
        '
        Private WsNbHeuresTotalesDemandees As Double
        Private WsNbHeuresDemandees_Jour_T1 As Double
        Private WsNbHeuresDemandees_Nuit_T1 As Double
        Private WsNbHeuresDemandees_JF_T1 As Double
        Private WsNbHeuresDemandees_Jour_T2 As Double
        Private WsNbHeuresDemandees_Nuit_T2 As Double
        Private WsNbHeuresDemandees_JF_T2 As Double
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MENSUEL_HS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaMensuelHS
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 26
            End Get
        End Property

        Public Property TotalHeures_Jour_Jusqua20h() As Double
            Get
                Return WsNbHeures_Jour_20h
            End Get
            Set(ByVal value As Double)
                WsNbHeures_Jour_20h = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Jour_de20a22h() As Double
            Get
                Return WsNbHeures_Jour_22h
            End Get
            Set(ByVal value As Double)
                WsNbHeures_Jour_22h = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Nuit() As Double
            Get
                Return WsNbHeures_Nuit
            End Get
            Set(ByVal value As Double)
                WsNbHeures_Nuit = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Trajet_HS() As Double
            Get
                Return WsNbHeures_Trajet_HS
            End Get
            Set(ByVal value As Double)
                WsNbHeures_Trajet_HS = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Astreinte_Jusqua20h() As Double
            Get
                Return WsNbHeures_JF_20h
            End Get
            Set(ByVal value As Double)
                WsNbHeures_JF_20h = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Astreinte_de20a22h() As Double
            Get
                Return WsNbHeures_JF_22h
            End Get
            Set(ByVal value As Double)
                WsNbHeures_JF_22h = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Astreinte_Nuit() As Double
            Get
                Return WsNbHeures_JF_Nuit
            End Get
            Set(ByVal value As Double)
                WsNbHeures_JF_Nuit = Math.Round(value, 2)
            End Set
        End Property

        Public Property TotalHeures_Trajet_Astreinte() As Double
            Get
                Return WsNbHeures_Trajet_JF
            End Get
            Set(ByVal value As Double)
                WsNbHeures_Trajet_JF = Math.Round(value, 2)
            End Set
        End Property

        Public Property Date_Demande() As String
            Get
                Return WsDate_DemandeHeures
            End Get
            Set(ByVal value As String)
                WsDate_DemandeHeures = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Observations_Demande() As String
            Get
                Return WsObservations_Demande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations_Demande = value
                    Case Else
                        WsObservations_Demande = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Date_Visa_Manager() As String
            Get
                Return WsDate_Visa_Manager
            End Get
            Set(ByVal value As String)
                WsDate_Visa_Manager = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Signataire_Manager() As String
            Get
                Return WsSignataire_Manager
            End Get
            Set(ByVal value As String)
                WsSignataire_Manager = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Nature_Visa_Manager() As String
            Get
                Return WsVisa_Manager
            End Get
            Set(ByVal value As String)
                WsVisa_Manager = F_FormatAlpha(value, 30)
            End Set
        End Property

        Public Property Observations_Manager() As String
            Get
                Return WsObservations_Manager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations_Manager = value
                    Case Else
                        WsObservations_Manager = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Date_Visa_GRH() As String
            Get
                Return WsDate_Visa_GRH
            End Get
            Set(ByVal value As String)
                WsDate_Visa_GRH = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Signataire_GRH() As String
            Get
                Return WsSignataire_GRH
            End Get
            Set(ByVal value As String)
                WsSignataire_GRH = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Nature_Visa_GRH() As String
            Get
                Return WsVisa_GRH
            End Get
            Set(ByVal value As String)
                WsVisa_GRH = F_FormatAlpha(value, 30)
            End Set
        End Property

        Public Property Observations_GRH() As String
            Get
                Return WsObservations_GRH
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations_GRH = value
                    Case Else
                        WsObservations_GRH = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Totales_DemandeAPayer() As Double
            Get
                Return WsNbHeuresTotalesDemandees
            End Get
            Set(ByVal value As Double)
                WsNbHeuresTotalesDemandees = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_Jour_T1() As Double
            Get
                Return WsNbHeuresDemandees_Jour_T1
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_Jour_T1 = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_Nuit_T1() As Double
            Get
                Return WsNbHeuresDemandees_Nuit_T1
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_Nuit_T1 = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_JF_T1() As Double
            Get
                Return WsNbHeuresDemandees_JF_T1
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_JF_T1 = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_Jour_T2() As Double
            Get
                Return WsNbHeuresDemandees_Jour_T2
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_Jour_T2 = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_Nuit_T2() As Double
            Get
                Return WsNbHeuresDemandees_Nuit_T2
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_Nuit_T2 = Math.Round(value, 2)
            End Set
        End Property

        Public Property NombreHeures_DemandeAPayer_JF_T2() As Double
            Get
                Return WsNbHeuresDemandees_JF_T2
            End Get
            Set(ByVal value As Double)
                WsNbHeuresDemandees_JF_T2 = Math.Round(value, 2)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(TotalHeures_Jour_Jusqua20h.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Jour_de20a22h.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Nuit.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Trajet_HS.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Astreinte_Jusqua20h.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Astreinte_de20a22h.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Astreinte_Nuit.ToString & VI.Tild)
                Chaine.Append(TotalHeures_Trajet_Astreinte.ToString & VI.Tild)
                Chaine.Append(Date_Demande & VI.Tild)
                Chaine.Append(Observations_Demande & VI.Tild)
                Chaine.Append(Date_Visa_Manager & VI.Tild)
                Chaine.Append(Signataire_Manager & VI.Tild)
                Chaine.Append(Nature_Visa_Manager & VI.Tild)
                Chaine.Append(Observations_Manager & VI.Tild)
                Chaine.Append(Date_Visa_GRH & VI.Tild)
                Chaine.Append(Signataire_GRH & VI.Tild)
                Chaine.Append(Nature_Visa_GRH & VI.Tild)
                Chaine.Append(Observations_GRH & VI.Tild)
                Chaine.Append(NombreHeures_Totales_DemandeAPayer.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_Jour_T1.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_Nuit_T1.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_JF_T1.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_Jour_T2.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_Nuit_T2.ToString & VI.Tild)
                Chaine.Append(NombreHeures_DemandeAPayer_JF_T2.ToString & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 28 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                TotalHeures_Jour_Jusqua20h = VirRhFonction.ConversionDouble(TableauData(2), 2)
                TotalHeures_Jour_de20a22h = VirRhFonction.ConversionDouble(TableauData(3), 2)
                TotalHeures_Nuit = VirRhFonction.ConversionDouble(TableauData(4), 2)
                TotalHeures_Trajet_HS = VirRhFonction.ConversionDouble(TableauData(5), 2)
                TotalHeures_Astreinte_Jusqua20h = VirRhFonction.ConversionDouble(TableauData(6), 2)
                TotalHeures_Astreinte_de20a22h = VirRhFonction.ConversionDouble(TableauData(7), 2)
                TotalHeures_Astreinte_Nuit = VirRhFonction.ConversionDouble(TableauData(8), 2)
                TotalHeures_Trajet_Astreinte = VirRhFonction.ConversionDouble(TableauData(9), 2)
                Date_Demande = TableauData(10)
                Observations_Demande = TableauData(11)
                Date_Visa_Manager = TableauData(12)
                Signataire_Manager = TableauData(13)
                Nature_Visa_Manager = TableauData(14)
                Observations_Manager = TableauData(15)
                Date_Visa_GRH = TableauData(16)
                Signataire_GRH = TableauData(17)
                Nature_Visa_GRH = TableauData(18)
                Observations_GRH = TableauData(19)
                NombreHeures_Totales_DemandeAPayer = VirRhFonction.ConversionDouble(TableauData(20), 2)
                NombreHeures_DemandeAPayer_Jour_T1 = VirRhFonction.ConversionDouble(TableauData(21), 2)
                NombreHeures_DemandeAPayer_Nuit_T1 = VirRhFonction.ConversionDouble(TableauData(22), 2)
                NombreHeures_DemandeAPayer_JF_T1 = VirRhFonction.ConversionDouble(TableauData(23), 2)
                NombreHeures_DemandeAPayer_Jour_T2 = VirRhFonction.ConversionDouble(TableauData(24), 2)
                NombreHeures_DemandeAPayer_Nuit_T2 = VirRhFonction.ConversionDouble(TableauData(25), 2)
                NombreHeures_DemandeAPayer_JF_T2 = VirRhFonction.ConversionDouble(TableauData(26), 2)
                MyBase.Certification = TableauData(27)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public ReadOnly Property TotalHeures_Jour_T1() As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Jour_Jusqua20h + TotalHeures_Jour_de20a22h
                If ZCalc > 14 Then
                    ZCalc = 14
                End If
                Return ZCalc
            End Get
        End Property

        Public ReadOnly Property TotalHeures_Nuit_T1() As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Nuit + TotalHeures_Astreinte_Nuit
                If ZCalc > 14 Then
                    ZCalc = 14
                End If
                Return ZCalc
            End Get
        End Property

        Public ReadOnly Property TotalHeures_JF_T1() As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Astreinte_Jusqua20h + TotalHeures_Astreinte_de20a22h + TotalHeures_Trajet_Astreinte
                If ZCalc > 14 Then
                    ZCalc = 14
                End If
                Return ZCalc
            End Get
        End Property

        Public ReadOnly Property TotalHeures_Jour_T2() As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Jour_Jusqua20h + TotalHeures_Jour_de20a22h
                If ZCalc <= 14 Then
                    Return 0
                End If
                Return ZCalc - 14
            End Get
        End Property

        Public ReadOnly Property TotalHeures_Nuit_T2() As Double
            Get
                Dim ZCalc As Double
                ZCalc = ZCalc = TotalHeures_Nuit + TotalHeures_Astreinte_Nuit
                If ZCalc <= 14 Then
                    Return 0
                End If
                Return ZCalc - 14
            End Get
        End Property

        Public ReadOnly Property TotalHeures_JF_T2() As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Astreinte_Jusqua20h + TotalHeures_Astreinte_de20a22h + TotalHeures_Trajet_Astreinte
                If ZCalc <= 14 Then
                    Return 0
                End If
                Return ZCalc - 14
            End Get
        End Property

        Public ReadOnly Property NombreHeuresCompensables_Jour(Optional ByVal SiPrevu As Boolean = False) As Double
            Get
                Dim ZCalc As Double
                Dim Zde20a22 As Double
                ZCalc = TotalHeures_Jour_Jusqua20h + TotalHeures_Trajet_HS
                ZCalc += TotalHeures_Jour_de20a22h * 1.5
                If SiExisteHeuresRemunerees = False And SiPrevu = False Then
                    Return ZCalc
                End If
                ZCalc = TotalHeures_Jour_Jusqua20h + TotalHeures_Trajet_HS - (NombreHeures_DemandeAPayer_Jour_T1 + NombreHeures_DemandeAPayer_Jour_T2)
                Zde20a22 = TotalHeures_Jour_de20a22h
                If ZCalc < 0 Then
                    Zde20a22 += ZCalc
                    ZCalc = 0
                End If
                Return ZCalc + (Zde20a22 * 1.5)
            End Get
        End Property

        Public ReadOnly Property NombreHeuresCompensables_Nuit(Optional ByVal SiPrevu As Boolean = False) As Double
            Get
                Dim ZCalc As Double
                ZCalc = TotalHeures_Nuit_T1 + TotalHeures_Nuit_T2
                If SiExisteHeuresRemunerees = False And SiPrevu = False Then
                    Return ZCalc * 2
                End If
                Return (ZCalc - (NombreHeures_DemandeAPayer_Nuit_T1 + NombreHeures_DemandeAPayer_Nuit_T2)) * 2
            End Get
        End Property

        Public ReadOnly Property NombreHeuresCompensables_JF(Optional ByVal SiPrevu As Boolean = False) As Double
            Get
                Dim ZCalc As Double
                Dim ZJusqua20 As Double = TotalHeures_Astreinte_Jusqua20h
                Dim Zde20a22 As Double = TotalHeures_Astreinte_de20a22h

                ZCalc = ZJusqua20 * 1.25
                ZCalc += Zde20a22 * 1.5
                ZCalc += TotalHeures_Trajet_Astreinte
                If SiExisteHeuresRemunerees = False And SiPrevu = False Then
                    Return ZCalc
                End If
                ZCalc = TotalHeures_Trajet_Astreinte - (NombreHeures_DemandeAPayer_JF_T1 + NombreHeures_DemandeAPayer_JF_T2)
                If ZCalc < 0 Then
                    ZJusqua20 += ZCalc
                    ZCalc = 0
                End If
                If ZJusqua20 < 0 Then
                    Zde20a22 += ZJusqua20
                    ZJusqua20 = 0
                End If
                ZCalc += ZJusqua20 * 1.25
                ZCalc += Zde20a22 * 1.5
                Return ZCalc
            End Get
        End Property

        Public ReadOnly Property TotalHeuresCompensables(ByVal SiPrevu As Boolean) As Double
            Get
                Return NombreHeuresCompensables_Jour(SiPrevu) + NombreHeuresCompensables_Nuit(SiPrevu) + NombreHeuresCompensables_JF(SiPrevu)
            End Get
        End Property

        Public ReadOnly Property TotalHeuresRemunerees(ByVal SiPrevu As Boolean) As Double
            Get
                If SiExisteHeuresRemunerees = False And SiPrevu = False Then
                    Return 0
                End If
                Return NombreHeures_Totales_DemandeAPayer
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property SiExisteHeuresRemunerees As Boolean
            Get
                If Date_Demande = "" Or Date_Visa_Manager = "" Or Date_Visa_GRH = "" Then
                    Return False
                End If
                If Nature_Visa_Manager.StartsWith("Refus") Or Nature_Visa_GRH.StartsWith("Refus") Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

