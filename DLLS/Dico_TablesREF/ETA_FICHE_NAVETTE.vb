﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_FICHE_NAVETTE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsDateValidation As String
        Private WsEmetteur As String
        Private WsDatePriseEnCompte As String
        Private WsLibelleSujet As String
        Private WsLibelleAction As String
        Private WsObservations As String
        Private WsNomFichier As String
        Private WsMoiePaie As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_FICHE_NAVETTE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 14
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value
            End Set
        End Property

        Public Property DatedeValidation() As String
            Get
                Return WsDateValidation
            End Get
            Set(ByVal value As String)
                WsDateValidation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Emetteur() As String
            Get
                Return WsEmetteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsEmetteur = value
                    Case Else
                        WsEmetteur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property DatedePriseEnCompte() As String
            Get
                Return WsDatePriseEnCompte
            End Get
            Set(ByVal value As String)
                WsDatePriseEnCompte = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Libelle_Sujet() As String
            Get
                Return WsLibelleSujet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsLibelleSujet = value
                    Case Else
                        WsLibelleSujet = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Libelle_Action() As String
            Get
                Return WsLibelleAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsLibelleAction = value
                    Case Else
                        WsLibelleAction = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property NomFichier() As String
            Get
                Return WsNomFichier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNomFichier = value
                    Case Else
                        WsNomFichier = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property MoisPaie() As String
            Get
                Return WsMoiePaie
            End Get
            Set(ByVal value As String)
                WsMoiePaie = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(DatedeValidation & VI.Tild)
                Chaine.Append(Emetteur & VI.Tild)
                Chaine.Append(DatedePriseEnCompte & VI.Tild)
                Chaine.Append(Libelle_Sujet & VI.Tild)
                Chaine.Append(Libelle_Action & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(NomFichier & VI.Tild)
                Chaine.Append(MoisPaie)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Rang = CInt(TableauData(2))
                DatedeValidation = TableauData(3)
                Emetteur = TableauData(4)
                DatedePriseEnCompte = TableauData(5)
                Libelle_Sujet = TableauData(6)
                Libelle_Action = TableauData(7)
                Observations = TableauData(8)
                NomFichier = TableauData(9)
                MoisPaie = TableauData(10)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
