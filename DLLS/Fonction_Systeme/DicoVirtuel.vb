﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace MetaModele
    Namespace Outils
        Public Class DictionnaireVirtuel
            Private WsEtiquette As String
            Private WsLgMaxi As Integer
            Private WsNatureDonnee As String
            Private WsFormatDonnee As String
            Private WsIdeTable As Integer
            Private WsNomTable As String
            Private WsPointdeVue As Integer
            Private WsSiObligatoire As Boolean = False
            Private WsSiVisible As Boolean = True
            Private WsSiNonSaisissable As Boolean = False
            Private WsTag As String = ""
            Private WsNumInfoReel As Integer
            Private WsTooltip As String = ""

            Public Property Etiquette As String
                Get
                    Return WsEtiquette
                End Get
                Set(ByVal value As String)
                    WsEtiquette = value
                End Set
            End Property

            Public Property Longueur As Integer
                Get
                    Return WsLgMaxi
                End Get
                Set(ByVal value As Integer)
                    WsLgMaxi = value
                End Set
            End Property

            Public Property NatureDonnee As String
                Get
                    Return WsNatureDonnee
                End Get
                Set(ByVal value As String)
                    WsNatureDonnee = value
                End Set
            End Property

            Public Property FormatDonnee As String
                Get
                    Return WsFormatDonnee
                End Get
                Set(ByVal value As String)
                    WsFormatDonnee = value
                End Set
            End Property

            Public Property IdentifiantTable As Integer
                Get
                    Return WsIdeTable
                End Get
                Set(ByVal value As Integer)
                    WsIdeTable = value
                End Set
            End Property

            Public Property NomTable As String
                Get
                    Return WsNomTable
                End Get
                Set(ByVal value As String)
                    WsNomTable = value
                End Set
            End Property

            Public Property PointdeVue As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property SiObligatoire As Boolean
                Get
                    Return WsSiObligatoire
                End Get
                Set(ByVal value As Boolean)
                    WsSiObligatoire = value
                End Set
            End Property

            Public Property SiVisible As Boolean
                Get
                    Return WsSiVisible
                End Get
                Set(ByVal value As Boolean)
                    WsSiVisible = value
                End Set
            End Property

            Public Property SiNonSaisissable As Boolean
                Get
                    Return WsSiNonSaisissable
                End Get
                Set(ByVal value As Boolean)
                    WsSiNonSaisissable = value
                End Set
            End Property

            Public Property NumeroInfoReel As Integer
                Get
                    Return WsNumInfoReel
                End Get
                Set(ByVal value As Integer)
                    WsNumInfoReel = value
                End Set
            End Property

            Public Property ToolTipAide As String
                Get
                    Return WsTooltip
                End Get
                Set(ByVal value As String)
                    WsTooltip = value
                End Set
            End Property

            Public Property TagGestion As String
                Get
                    Return WsTag
                End Get
                Set(ByVal value As String)
                    WsTag = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace
