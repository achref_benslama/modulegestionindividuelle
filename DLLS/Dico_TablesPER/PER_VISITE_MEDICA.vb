﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_VISITE_MEDICA
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDateVisite As String
        Private WsType As String
        Private WsHeure As String
        Private WsResultat As String
        Private WsRythmeSurveillance As String
        Private WsDateConvocation As String
        Private WsCentre As String
        Private WsMedecin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_VISITE_MEDICA"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaMedical
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 8
            End Get
        End Property

        Public Property Date_Visite() As String
            Get
                Return WsDateVisite
            End Get
            Set(ByVal value As String)
                WsDateVisite = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Type_Visite() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsType = value
                    Case Else
                        WsType = Strings.Left(value, 40)
                        MyBase.Clef = WsType
                End Select
            End Set
        End Property

        Public Property Heure_Visite() As String
            Get
                Return WsHeure
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure = value
                    Case Else
                        WsHeure = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Resultat() As String
            Get
                Return WsResultat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsResultat = value
                    Case Else
                        WsResultat = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Rythme_Surveillance() As String
            Get
                Return WsRythmeSurveillance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsRythmeSurveillance = value
                    Case Else
                        WsRythmeSurveillance = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Date_Convocation() As String
            Get
                Return WsDateConvocation
            End Get
            Set(ByVal value As String)
                WsDateConvocation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Centre_Medical() As String
            Get
                Return WsCentre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCentre = value
                    Case Else
                        WsCentre = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Medecin() As String
            Get
                Return WsMedecin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsMedecin = value
                    Case Else
                        WsMedecin = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Visite & VI.Tild)
                Chaine.Append(Type_Visite & VI.Tild)
                Chaine.Append(Heure_Visite & VI.Tild)
                Chaine.Append(Resultat & VI.Tild)
                Chaine.Append(Rythme_Surveillance & VI.Tild)
                Chaine.Append(Date_Convocation & VI.Tild)
                Chaine.Append(Centre_Medical & VI.Tild)
                Chaine.Append(Medecin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Visite = TableauData(1)
                Type_Visite = TableauData(2)
                Heure_Visite = TableauData(3)
                Resultat = TableauData(4)
                Rythme_Surveillance = TableauData(5)
                Date_Convocation = TableauData(6)
                Centre_Medical = TableauData(7)
                Medecin = TableauData(8)
                MyBase.Certification = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

