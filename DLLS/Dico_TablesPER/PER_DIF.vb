﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DIF
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDateEffet As String
        Private WsDIF As String
        Private WsNbHeuresEffectuees As String
        Private WsSoldeAnnuel As String
        Private WsNbHeuresSurTempsdeTravail As String
        Private WsNbHeuresHorsTempsdeTravail As String
        Private WsCumulHeuresEffectuees As String
        Private WsSoldeDIF As String
        Private WsCumulSurTempsdeTravail As String
        Private WsCumulHorsTempsdeTravail As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DIF"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 93
            End Get
        End Property

        Public Property Date_d_Effet() As String
            Get
                Return WsDateEffet
            End Get
            Set(ByVal value As String)
                WsDateEffet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDateEffet
            End Set
        End Property

        Public Property DIF() As String
            Get
                Return WsDIF
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDIF = value
                    Case Else
                        WsDIF = Strings.Left(value, 9)
                End Select
                MyBase.Clef = WsDIF
            End Set
        End Property

        Public Property NombreHeures_Effectuees() As String
            Get
                Return WsNbHeuresEffectuees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeuresEffectuees = value
                    Case Else
                        WsNbHeuresEffectuees = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property SoldeAnnuel() As String
            Get
                Return WsSoldeAnnuel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsSoldeAnnuel = value
                    Case Else
                        WsSoldeAnnuel = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Sur_TempsTravail() As String
            Get
                Return WsNbHeuresSurTempsdeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeuresSurTempsdeTravail = value
                    Case Else
                        WsNbHeuresSurTempsdeTravail = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Hors_TempsTravail() As String
            Get
                Return WsNbHeuresHorsTempsdeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeuresHorsTempsdeTravail = value
                    Case Else
                        WsNbHeuresHorsTempsdeTravail = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Cumulees() As String
            Get
                Return WsCumulHeuresEffectuees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsCumulHeuresEffectuees = value
                    Case Else
                        WsCumulHeuresEffectuees = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Solde_DIF() As String
            Get
                Return WsSoldeDIF
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsSoldeDIF = value
                    Case Else
                        WsSoldeDIF = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Cumul_Sur_TempsTravail() As String
            Get
                Return WsCumulSurTempsdeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsCumulSurTempsdeTravail = value
                    Case Else
                        WsCumulSurTempsdeTravail = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Cumul_Hors_TempsTravail() As String
            Get
                Return WsCumulHorsTempsdeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsCumulHorsTempsdeTravail = value
                    Case Else
                        WsCumulHorsTempsdeTravail = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_Effet & VI.Tild)
                Chaine.Append(DIF & VI.Tild)
                Chaine.Append(NombreHeures_Effectuees & VI.Tild)
                Chaine.Append(SoldeAnnuel & VI.Tild)
                Chaine.Append(NombreHeures_Sur_TempsTravail & VI.Tild)
                Chaine.Append(NombreHeures_Hors_TempsTravail & VI.Tild)
                Chaine.Append(NombreHeures_Cumulees & VI.Tild)
                Chaine.Append(Solde_DIF & VI.Tild)
                Chaine.Append(Cumul_Sur_TempsTravail & VI.Tild)
                Chaine.Append(Cumul_Hors_TempsTravail)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                DIF = TableauData(2)
                NombreHeures_Effectuees = TableauData(3)
                SoldeAnnuel = TableauData(4)
                NombreHeures_Sur_TempsTravail = TableauData(5)
                NombreHeures_Hors_TempsTravail = TableauData(6)
                NombreHeures_Cumulees = TableauData(7)
                Solde_DIF = TableauData(8)
                Cumul_Sur_TempsTravail = TableauData(9)
                Cumul_Hors_TempsTravail = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDateEffet
                    Case 1
                        Return WsDIF
                    Case 2
                        Return WsNbHeuresEffectuees
                    Case 3
                        Return WsSoldeAnnuel
                    Case 4
                        Return WsNbHeuresSurTempsdeTravail
                    Case 5
                        Return WsNbHeuresHorsTempsdeTravail
                    Case 6
                        Return WsCumulHeuresEffectuees
                    Case 7
                        Return WsSoldeDIF
                    Case 8
                        Return WsCumulSurTempsdeTravail
                    Case 9
                        Return WsCumulHorsTempsdeTravail
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(DIF)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & MyBase.Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                MyBase.Date_de_Valeur = value(0)
                DIF = value(2)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Année"
            InfoBase.Longueur = 4
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = "01/01/AAAA"
            InfoBase.IdentifiantTable = 0
            InfoBase.NomTable = ""
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "DIF Annuel (Par défaut 20 Heures)"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Heure"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


