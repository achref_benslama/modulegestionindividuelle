﻿Public Interface IVConstructeur
    WriteOnly Property V_TypeduModele As Integer
    WriteOnly Property V_PointdeVue As Integer
    WriteOnly Property V_Parametre As Integer

    Function V_NouvelleFiche(ByVal NumObjet As Integer, ByVal IdeDossier As Integer) As Virtualia.Systeme.MetaModele.VIR_FICHE

    Function V_NouvelleFiche(ByVal NumObjet As Integer, ByVal IdeDossier As Integer, ByVal LstDatas As List(Of String)) As Virtualia.Systeme.MetaModele.VIR_FICHE
End Interface
