﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_STOCKOPTION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNombreOptions As Integer
        Private WsPrixMarche As Double
        Private WsNumeroPlan As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_STOCKOPTION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPEA 'Spécifique Secteur Privé
            End Get
        End Property

        Public Property NombreOptions() As Integer
            Get
                Return WsNombreOptions
            End Get
            Set(ByVal value As Integer)
                WsNombreOptions = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Prix_du_Marche() As Double
            Get
                Return WsPrixMarche
            End Get
            Set(ByVal value As Double)
                WsPrixMarche = value
            End Set
        End Property

        Public Property Numero_Plan() As String
            Get
                Return WsNumeroPlan
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNumeroPlan = value
                    Case Else
                        WsNumeroPlan = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(NombreOptions.ToString & VI.Tild)
                Chaine.Append(Prix_du_Marche.ToString & VI.Tild)
                Chaine.Append(Numero_Plan)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                NombreOptions = CInt(TableauData(2))
                If TableauData(3) = "" Then TableauData(3) = "0"
                Prix_du_Marche = VirRhFonction.ConversionDouble(TableauData(3))
                Numero_Plan = TableauData(4)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsNombreOptions.ToString
                    Case 2
                        Return WsPrixMarche.ToString
                    Case 3
                        Return WsNumeroPlan
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
