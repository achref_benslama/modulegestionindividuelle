Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ETATCIVIL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean = False
        '
        Private WsQualite As String
        Private WsNom As String
        Private WsPrenom As String
        Private WsDate_de_naissance As String
        Private WsLieu_de_naissance As String
        Private WsDepartement_ou_pays_de_naissan As String
        Private WsNationalite As String
        Private WsSexe As String
        Private WsSituation_familiale As String
        Private WsDate_de_la_situation As String
        Private WsNumero_d_identification_nation As String
        Private WsCle_securite_sociale As String
        Private WsPatronyme As String
        Private WsAutre_prenom As String
        Private WsDatation_de_la_naissance As String
        Private WsDatevalidationnir As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ETATCIVIL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaCivil
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 18
            End Get
        End Property

        Public Property Qualite() As String
            Get
                Return WsQualite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 12
                        WsQualite = value
                    Case Else
                        WsQualite = Strings.Left(value, 12)
                End Select
            End Set
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                If WsSiMajuscule = True Then
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = value.ToUpper
                        Case Else
                            WsNom = Strings.Left(value.ToUpper, 35)
                    End Select
                Else
                    Select Case value.Length
                        Case Is <= 35
                            WsNom = VirRhFonction.Lettre1Capi(value, 2)
                        Case Else
                            WsNom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 2)
                    End Select
                    MyBase.Clef = WsNom
                End If
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Date_de_naissance() As String
            Get
                Return WsDate_de_naissance
            End Get
            Set(ByVal value As String)
                WsDate_de_naissance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Lieu_de_naissance() As String
            Get
                Return WsLieu_de_naissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsLieu_de_naissance = value
                    Case Else
                        WsLieu_de_naissance = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Departement_ou_pays_de_naissan() As String
            Get
                Return WsDepartement_ou_pays_de_naissan
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsDepartement_ou_pays_de_naissan = value
                    Case Else
                        WsDepartement_ou_pays_de_naissan = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Nationalite() As String
            Get
                Return WsNationalite
            End Get
            Set(ByVal value As String)
                WsNationalite = value
            End Set
        End Property

        Public Property Sexe() As String
            Get
                Return WsSexe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsSexe = value
                    Case Else
                        WsSexe = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property Situation_familiale() As String
            Get
                Return WsSituation_familiale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsSituation_familiale = value
                    Case Else
                        WsSituation_familiale = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Date_de_la_situation() As String
            Get
                Return WsDate_de_la_situation
            End Get
            Set(ByVal value As String)
                WsDate_de_la_situation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_d_identification_nation() As String
            Get
                Return WsNumero_d_identification_nation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_d_identification_nation = value
                    Case Else
                        WsNumero_d_identification_nation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Cle_securite_sociale() As String
            Get
                Return WsCle_securite_sociale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case 1
                        WsCle_securite_sociale = "0" & value
                    Case Else
                        WsCle_securite_sociale = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Patronyme() As String
            Get
                Return WsPatronyme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPatronyme = value
                    Case Else
                        WsPatronyme = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Autre_prenom() As String
            Get
                Return WsAutre_prenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsAutre_prenom = value
                    Case Else
                        WsAutre_prenom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Private ReadOnly Property Age() As String
            Get
                Return V_Age(VI.OptionInfo.DicoRecherche, VirRhDate.DateduJour)
            End Get
        End Property

        Public Property Datation_de_la_naissance() As String
            Get
                Return WsDatation_de_la_naissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsDatation_de_la_naissance = value
                    Case Else
                        WsDatation_de_la_naissance = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Datevalidationnir() As String
            Get
                Return WsDatevalidationnir
            End Get
            Set(ByVal value As String)
                WsDatevalidationnir = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Qualite & VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Date_de_naissance & VI.Tild)
                Chaine.Append(Lieu_de_naissance & VI.Tild)
                Chaine.Append(Departement_ou_pays_de_naissan & VI.Tild)
                Chaine.Append(Nationalite & VI.Tild)
                Chaine.Append(Sexe & VI.Tild)
                Chaine.Append(Situation_familiale & VI.Tild)
                Chaine.Append(Date_de_la_situation & VI.Tild)
                Chaine.Append(Numero_d_identification_nation & VI.Tild)
                Chaine.Append(Cle_securite_sociale & VI.Tild)
                Chaine.Append(Patronyme & VI.Tild)
                Chaine.Append(Autre_prenom & VI.Tild)
                Chaine.Append(VI.Tild) 'Age
                Chaine.Append(Datation_de_la_naissance & VI.Tild)
                Chaine.Append(Datevalidationnir & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 19 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Qualite = TableauData(1)
                Nom = TableauData(2)
                Prenom = TableauData(3)
                Date_de_naissance = TableauData(4)
                Lieu_de_naissance = TableauData(5)
                Departement_ou_pays_de_naissan = TableauData(6)
                Nationalite = TableauData(7)
                Sexe = TableauData(8)
                Situation_familiale = TableauData(9)
                Date_de_la_situation = TableauData(10)
                Numero_d_identification_nation = TableauData(11)
                Select Case TableauData(12).Length
                    Case 1
                        Cle_securite_sociale = "0" & TableauData(12)
                    Case Else
                        Cle_securite_sociale = TableauData(12)
                End Select
                TableauData(12) = Cle_securite_sociale

                Patronyme = TableauData(13)
                Autre_prenom = TableauData(14)
                'TableauData(15) - age
                Datation_de_la_naissance = TableauData(16)
                Datevalidationnir = TableauData(17)
                MyBase.Certification = TableauData(18)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_Age(ByVal Methode As Integer, ByVal DateValeur As String) As String
            Get
                Dim Champ As String
                If DateValeur = "" Then
                    DateValeur = VirRhDate.DateduJour
                End If
                Select Case Methode
                    Case VI.OptionInfo.DicoStats
                        Champ = VirRhDate.CalcAge(Date_de_naissance, DateValeur, True)
                        Champ = Math.Round(VirRhFonction.ConversionDouble(Champ), 2).ToString
                    Case Else
                        Champ = VirRhDate.CalcAge(Date_de_naissance, DateValeur, False)
                        Champ = Math.Round(VirRhFonction.ConversionDouble(Champ), 0).ToString
                End Select
                Return Champ
            End Get
        End Property

        Public ReadOnly Property V_Age(ByVal DateValeur As String) As Double
            Get
                Dim Champ As String
                If DateValeur = "" Then
                    DateValeur = VirRhDate.DateduJour
                End If
                Champ = VirRhDate.CalcAge(Date_de_naissance, DateValeur, True)
                Return Math.Round(VirRhFonction.ConversionDouble(Champ), 2)
            End Get
        End Property

        Public ReadOnly Property V_Anniversaire(ByVal DateValeur As String) As String
            Get
                If DateValeur = "" Then
                    DateValeur = VirRhDate.DateduJour
                End If
                Select Case Strings.Left(Date_de_naissance, 5)
                    Case Is = Strings.Left(DateValeur, 5)
                        Return "Oui"
                End Select
                Return "Non"
            End Get
        End Property

        Public ReadOnly Property V_NomEtPrenom() As String
            Get
                Return Nom & Strings.Space(1) & Prenom
            End Get
        End Property

        Public ReadOnly Property V_PrenomEtNom() As String
            Get
                Return Prenom & Strings.Space(1) & Nom
            End Get
        End Property

        Public ReadOnly Property V_NomMarital() As String
            Get
                Select Case Sexe
                    Case Is = "Masculin"
                        Return ""
                End Select
                Return Nom
            End Get
        End Property

        Public ReadOnly Property V_Patronyme() As String
            Get
                Select Case Patronyme
                    Case Is <> ""
                        Return Patronyme
                End Select
                Return Nom
            End Get
        End Property

        Public ReadOnly Property V_NIREdite() As String
            Get
                Dim WNir As String
                Select Case Len(Numero_d_identification_nation)
                    Case Is < 7
                        Return Numero_d_identification_nation
                End Select
                WNir = Strings.Left(Numero_d_identification_nation, 1) & Strings.Space(1) & _
                    Strings.Mid(Numero_d_identification_nation, 2, 2) & Strings.Space(1) & _
                    Strings.Mid(Numero_d_identification_nation, 4, 2) & Strings.Space(1) & _
                    Strings.Mid(Numero_d_identification_nation, 6, 2) & Strings.Space(1)
                Select Case Len(Numero_d_identification_nation)
                    Case Is = 13
                        WNir &= Strings.Mid(Numero_d_identification_nation, 8, 3) & Strings.Space(1) & _
                            Strings.Right(Numero_d_identification_nation, 3)
                    Case Else
                        WNir &= Strings.Right(Numero_d_identification_nation, Len(Numero_d_identification_nation) - 7)
                End Select
                WNir &= Strings.Space(1) & Cle_securite_sociale
                Return WNir
            End Get
        End Property

        Public ReadOnly Property V_NIRComplet() As String
            Get
                Dim WNir As String = Numero_d_identification_nation.Replace(Strings.Space(1), "")
                WNir &= Cle_securite_sociale
                Return WNir
            End Get
        End Property

        Public ReadOnly Property V_NomClasse() As String
            Get
                Dim ChaineW As String
                Dim ChaineEditee As String

                ChaineW = VirRhFonction.Lettre1Capi(Nom, 2)
                ChaineEditee = ChaineW
                Select Case Len(ChaineW)
                    Case Is > 3
                        Select Case Strings.Left(ChaineW, 2)
                            Case Is = "d'"
                                ChaineEditee = Strings.Right(ChaineW, Len(ChaineW) - 2) & " (d')"
                        End Select
                        Select Case Strings.Left(ChaineW, 3)
                            Case Is = "de "
                                ChaineEditee = Strings.Right(ChaineW, Len(ChaineW) - 3) & " (de)"
                        End Select
                        Select Case Strings.Left(ChaineW, 3)
                            Case Is = "du "
                                ChaineEditee = Strings.Right(ChaineW, Len(ChaineW) - 3) & " (du)"
                        End Select
                End Select
                Return ChaineEditee
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<Nom>" & Nom & "</Nom>" & vbCrLf)
                Chaine.Append("<Prenom>" & Prenom & "</Prenom>" & vbCrLf)
                Chaine.Append("<Qualite>" & Qualite & "</Qualite>" & vbCrLf)
                Chaine.Append("<DateNaissance>" & Date_de_naissance & "</DateNaissance>" & vbCrLf)
                Chaine.Append("<Lieu_de_naissance>" & Lieu_de_naissance & "</Lieu_de_naissance>" & vbCrLf)
                Chaine.Append("<DepartementNaissance>" & Departement_ou_pays_de_naissan & "</DepartementNaissance>" & vbCrLf)
                Chaine.Append("<Nationalite>" & Nationalite & "</Nationalite>" & vbCrLf)
                Chaine.Append("<Sexe>" & Sexe & "</Sexe>" & vbCrLf)
                Chaine.Append("<Situation_familiale>" & Situation_familiale & "</Situation_familiale>" & vbCrLf)
                Chaine.Append("<Date_de_la_situation>" & Date_de_la_situation & "</Date_de_la_situation>" & vbCrLf)
                Chaine.Append("<Patronyme>" & Patronyme & "</Patronyme>" & vbCrLf)
                Chaine.Append("<Autre_prenom>" & Autre_prenom & "</Autre_prenom>" & vbCrLf)
                Chaine.Append("<NumeroInsee>" & Numero_d_identification_nation & "</NumeroInsee>" & vbCrLf)
                Chaine.Append("<CleInsee>" & Cle_securite_sociale & "</CleInsee>" & vbCrLf)

                Return Chaine.ToString
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property SiOK_DateNaissance(ByVal NouvelleDate As String) As Boolean
            Get
                If NouvelleDate = "" Then
                    Return False
                End If
                Dim AgeNum As Integer
                Dim AgeAlpha As String = V_Age(VI.OptionInfo.DicoRecherche, "")
                AgeAlpha = VirRhDate.CalcAge(NouvelleDate, VirRhDate.DateduJour, False)
                Select Case AgeAlpha.Length
                    Case Is > 3
                        Return False
                End Select
                If AgeAlpha = "" Then
                    Return False
                End If
                AgeNum = CInt(AgeAlpha)
                If AgeNum < 16 Then
                    Return False
                End If
                If AgeNum > 99 Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public ReadOnly Property SiOK_DateNaissance() As Boolean
            Get
                Dim AgeNum As Integer
                Dim AgeAlpha As String = V_Age(VI.OptionInfo.DicoRecherche, "")
                If AgeAlpha = "" Then
                    Return False
                End If
                AgeNum = CInt(AgeAlpha)
                If AgeNum < 16 Then
                    Return False
                End If
                If AgeNum > 99 Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public ReadOnly Property SiOK_NIR(ByVal NouveauNir As String, ByVal NouvelleCle As String) As Boolean
            Get
                If NouveauNir = "" Then
                    Return True
                End If
                If NouvelleCle = "" And NouveauNir <> "" Then
                    Return True
                End If
                Return VirRhFonction.VerificationCleUrssaf(NouveauNir, NouvelleCle)
            End Get
        End Property


        Public ReadOnly Property SiOK_NumNIR(ByVal Numero As String, ByVal Sexe As String, ByVal DatNai As String, ByVal Departement As String) As Boolean
            Get

                Return VirRhFonction.VerificationNoUrssaf(Numero, Sexe, DatNai, Departement)
            End Get
        End Property

        Public ReadOnly Property SiOK_NIR() As Boolean
            Get
                If Numero_d_identification_nation = "" Then
                    Return True
                End If
                If Cle_securite_sociale = "" Then
                    Return True
                End If
                Return VirRhFonction.VerificationCleUrssaf(Numero_d_identification_nation, Cle_securite_sociale)
            End Get
        End Property

        Public Property SiNom_En_Majuscule() As Boolean
            Get
                Return WsSiMajuscule
            End Get
            Set(ByVal value As Boolean)
                WsSiMajuscule = value
            End Set
        End Property

        Public Sub New(ByVal SiMajuscule As Boolean)
            MyBase.New()
            WsSiMajuscule = SiMajuscule
        End Sub
        Public Sub New()
            Me.New(False)
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace

