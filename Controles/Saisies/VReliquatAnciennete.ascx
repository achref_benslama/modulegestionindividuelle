﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VReliquatAnciennete" Codebehind="VReliquatAnciennete.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Textbox.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<asp:Table ID="VAnciennete" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                    style="margin-bottom: 20px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>          
            <Telerik:RadTextBox ID="Annees" runat="server" Font-Size="Medium" style="padding-left: 10px;" TextMode="SingleLine" Font-Names="Calibri"
                    Width="70px" DonBackColor="WhiteSmoke" ToolTip="Nombre d'années de reliquat"> 
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Mois" runat="server" Font-Size="Medium" style="padding-left: 10px;" TextMode="SingleLine" Font-Names="Calibri"
                   Width="70px" DonBackColor="WhiteSmoke" ToolTip="Nombre de mois de reliquat">  
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Jours" runat="server" Font-Size="Medium" style="padding-left: 10px;" TextMode="SingleLine" Font-Names="Calibri"
                   Width="70px" DonBackColor="WhiteSmoke" ToolTip="Nombre de jours de reliquat">  
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="DateAnciennete" runat="server" Font-Size="Medium" style="padding-left: 10px;" TextMode="SingleLine" Font-Names="Calibri"
                  Width="140px" DonBackColor="WhiteSmoke"> 
            </Telerik:RadTextBox>       
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>