﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.MetaModele
Namespace WebService
    Public Interface IServiceServeur
        Function LectureObjet_ToListeChar(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal Ide As Integer, ByVal SiTri As Boolean, _
                                                 Optional ByVal Filtres As List(Of String) = Nothing) As List(Of String)

        Function LectureObjet_ToFiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal Ide As Integer, ByVal SiTri As Boolean, _
                                                 Optional ByVal Filtres As List(Of String) = Nothing) As List(Of VIR_FICHE)

        Function LectureDossier_ToListeChar(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal Ide As Integer, _
                                                 ByVal SiTri As Boolean, ByVal ListeNoObjets As List(Of Integer)) As List(Of String)

        Function LectureDossier_ToFiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal Ide As Integer, _
                                                 ByVal SiTri As Boolean, ByVal ListeNoObjets As List(Of Integer)) As List(Of VIR_FICHE)

        Function LectureObjet_Plage_ToListeChar(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal SiTri As Boolean, ByVal IdeDebut As Integer, ByVal IdeFin As Integer) As List(Of String)

        Function LectureObjet_Plage_ToFiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal SiTri As Boolean, ByVal IdeDebut As Integer, ByVal IdeFin As Integer) As List(Of VIR_FICHE)

        Function LectureObjet_Plus_ToListeChar(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal SiTri As Boolean, ByVal ListeIde As List(Of Integer)) As List(Of String)

        Function LectureObjet_Plus_ToFiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal SiTri As Boolean, ByVal ListeIde As List(Of Integer)) As List(Of VIR_FICHE)

        Function RequeteSql_ToListeChar(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal OrdreSql As String) As List(Of String)

        Function RequeteSql_ToListeType(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal OrdreSql As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)

        Function RequeteSql_ToFiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal OrdreSql As String) As List(Of VIR_FICHE)

        Function MiseAjour_Fiche(ByVal NomUtiSgbd As String, ByVal PVue As Integer, ByVal NoObjet As Integer, ByVal Ide As Integer, ByVal CodeMaj As String,
                                                 ByVal ValeursLues As String, ByVal ValeursMaj As String,
                                                 Optional ByVal SiJournaliser As Boolean = True) As Boolean

        Function Inserer_Lot_Fiches(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal Ide As Integer, _
                                    ByVal ValeursMaj As String, Optional ByVal SiJournaliser As Boolean = True) As Integer

        Function MajSqlDirecte(ByVal NomUtiSgbd As String, ByVal OrdreSql As String) As Boolean

        Function MajSqlDirecte(ByVal NomUtiSgbd As String, ByVal FluxSql As List(Of String)) As Integer

        Function ObtenirUnCompteur(ByVal NomUtiSgbd As String, ByVal NomTable As String, ByVal Categorie As String) As Integer

        Function ChangerBasedeDonneesCourante(ByVal NomUtiSgbd As String, ByVal NoBd As Integer) As Boolean

        Function OuvertureSession(ByVal NomUtiSgbd As String, ByVal Pw_Sha As String, ByVal NoBd As Integer) As Integer

        Function FermetureSession(ByVal NomUtiSgbd As String) As Boolean

        Function LireProfilUtilisateurVirtualia(ByVal NomUtiSgbd As String, ByVal NomGestionnaire As String, _
                                                ByVal NoBd As Integer) As Virtualia.Net.ServiceServeur.UtilisateurType

        Function ListeUtilisateurs(ByVal Nature As String, ByVal NoBd As Integer) As List(Of Virtualia.Net.ServiceServeur.UtilisateurType)

        ReadOnly Property InformationLogiciel() As Virtualia.Net.ServiceServeur.ProduitType

        Function ObtenirUnFichierParametre(ByVal NomUtiSgbd As String, ByVal Categorie As String) As Byte()

        Function Journaliser_Maj(ByVal NomUtiSgbd As String, ByVal Pvue As Integer, ByVal NoObjet As Integer, _
                                                 ByVal Ide As Integer, ByVal CodeMaj As String, ByVal ContenuFiche As String) As Boolean

        Function RequeteSqlExterne_ToListeChar(ByVal NomUtiClient As String, ByVal NomUtiSgbd As String, ByVal PwSgbdDecrypte As String, _
                                                        ByVal FileDsn As String, ByVal TypeduSgbd As Integer, ByVal Requete As String) As List(Of String)

        Function RequeteSqlExterne_ToListeType(ByVal NomUtiClient As String, ByVal NomUtiSgbd As String, ByVal PwSgbdDecrypte As String, _
                                                        ByVal FileDsn As String, ByVal TypeduSgbd As Integer, ByVal Requete As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)

        Function MiseAjour_SqlExterne(ByVal NomUtiClient As String, ByVal NomUtiSgbd As String, ByVal PwSgbdDecrypte As String, _
                                                      ByVal FileDsn As String, ByVal TypeduSgbd As Integer, ByVal Requete As String, _
                                                      Optional ByVal FluxRequetes As List(Of String) = Nothing) As Boolean

        ReadOnly Property Instance_ModeleRH() As Virtualia.Systeme.MetaModele.Donnees.ModeleRH

        ReadOnly Property Instance_Database() As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd

        ReadOnly Property Instance_ExperteRH() As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH

        ReadOnly Property Instance_ConfigV4() As Virtualia.Systeme.Configuration.FichierConfig

        ReadOnly Property IPServeur() As String

        ReadOnly Property NomDNSServeur() As String

    End Interface
End Namespace