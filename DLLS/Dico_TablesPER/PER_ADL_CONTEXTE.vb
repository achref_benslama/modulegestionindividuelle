﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ADL_CONTEXTE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsMonnaieCompte_Principal As String
        Private WsMonnaieCompte_Alternative As String
        Private WsMensuel_SalaireBrut As Double = 0
        Private WsMensuel_MonnaieCompte_SalaireBrut As String
        Private WsMensuel_CotisationsSalariales As Double = 0
        Private WsMensuel_MonnaieCompte_PartSalarie As String
        Private WsMensuel_CotisationsPatronales As Double = 0
        Private WsMensuel_MonnaieCompte_PartPatronale As String
        Private WsMensuel_Primes As Double = 0
        Private WsMensuel_MonnaieCompte_Primes As String
        Private WsAnnuel_Remuneration_Nette As Double = 0
        Private WsAnnuel_MonnaieCompte_Net As String
        Private WsAnnuel_CotisationsSalariales As Double = 0
        Private WsAnnuel_MonnaieCompte_PartSalarie As String
        Private WsAnnuel_CotisationsPatronales As Double = 0
        Private WsAnnuel_MonnaieCompte_PartPatronale As String
        Private WsAnnuel_Primes As Double = 0
        Private WsAnnuel_MonnaieCompte_Primes As String
        Private WsAnnuel_HeuresSupp As Double = 0
        Private WsAnnuel_MonnaieCompte_HeuresSupp As String
        Private WsAnnuel_DepensesMedicales As Double = 0
        Private WsAnnuel_MonnaieCompte_DepensesMedicales As String
        Private WsAnnuel_ComplementaireAT As Double = 0
        Private WsAnnuel_MonnaieCompte_ComplementaireAT As String
        Private WsAnnuel_ComplementaireMaladie As Double = 0
        Private WsAnnuel_MonnaieCompte_Maladie As String
        Private WsAnnuel_ComplementaireRetraite As Double = 0
        Private WsAnnuel_MonnaieCompte_Retraite As String
        Private WsMotifModification As String
        Private WsObservation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ADL_CONTEXTE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 89
            End Get
        End Property

        Public Property MonnaieCompte_Principal() As String
            Get
                Return WsMonnaieCompte_Principal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMonnaieCompte_Principal = value
                    Case Else
                        WsMonnaieCompte_Principal = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property MonnaieCompte_Alternative() As String
            Get
                Return WsMonnaieCompte_Alternative
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMonnaieCompte_Alternative = value
                    Case Else
                        WsMonnaieCompte_Alternative = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_SalaireBrut_Mensuel() As Double
            Get
                Return WsMensuel_SalaireBrut
            End Get
            Set(ByVal value As Double)
                WsMensuel_SalaireBrut = value
            End Set
        End Property

        Public Property MonnaieCompte_SalaireBrut_Mensuel() As String
            Get
                Return WsMensuel_MonnaieCompte_SalaireBrut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMensuel_MonnaieCompte_SalaireBrut = value
                    Case Else
                        WsMensuel_MonnaieCompte_SalaireBrut = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_PartSalarial_Mensuel() As Double
            Get
                Return WsMensuel_CotisationsSalariales
            End Get
            Set(ByVal value As Double)
                WsMensuel_CotisationsSalariales = value
            End Set
        End Property

        Public Property MonnaieCompte_PartSalarial_Mensuel() As String
            Get
                Return WsMensuel_MonnaieCompte_PartSalarie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMensuel_MonnaieCompte_PartSalarie = value
                    Case Else
                        WsMensuel_MonnaieCompte_PartSalarie = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_PartPatronale_Mensuel() As Double
            Get
                Return WsMensuel_CotisationsPatronales
            End Get
            Set(ByVal value As Double)
                WsMensuel_CotisationsPatronales = value
            End Set
        End Property

        Public Property MonnaieCompte_PartPatronale_Mensuel() As String
            Get
                Return WsMensuel_MonnaieCompte_PartPatronale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMensuel_MonnaieCompte_PartPatronale = value
                    Case Else
                        WsMensuel_MonnaieCompte_PartPatronale = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_Primes_Mensuel() As Double
            Get
                Return WsMensuel_Primes
            End Get
            Set(ByVal value As Double)
                WsMensuel_Primes = value
            End Set
        End Property

        Public Property MonnaieCompte_Primes_Mensuel() As String
            Get
                Return WsMensuel_MonnaieCompte_Primes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsMensuel_MonnaieCompte_Primes = value
                    Case Else
                        WsMensuel_MonnaieCompte_Primes = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_Net_Annuel() As Double
            Get
                Return WsAnnuel_Remuneration_Nette
            End Get
            Set(ByVal value As Double)
                WsAnnuel_Remuneration_Nette = value
            End Set
        End Property

        Public Property MonnaieCompte_Net_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_Net
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_Net = value
                    Case Else
                        WsAnnuel_MonnaieCompte_Net = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_PartSalarial_Annuel() As Double
            Get
                Return WsAnnuel_CotisationsSalariales
            End Get
            Set(ByVal value As Double)
                WsAnnuel_CotisationsSalariales = value
            End Set
        End Property

        Public Property MonnaieCompte_PartSalarial_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_PartSalarie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_PartSalarie = value
                    Case Else
                        WsAnnuel_MonnaieCompte_PartSalarie = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_PartPatronale_Annuel() As Double
            Get
                Return WsAnnuel_CotisationsPatronales
            End Get
            Set(ByVal value As Double)
                WsAnnuel_CotisationsPatronales = value
            End Set
        End Property

        Public Property MonnaieCompte_PartPatronale_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_PartPatronale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_PartPatronale = value
                    Case Else
                        WsAnnuel_MonnaieCompte_PartPatronale = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_Primes_Annuel() As Double
            Get
                Return WsAnnuel_Primes
            End Get
            Set(ByVal value As Double)
                WsAnnuel_Primes = value
            End Set
        End Property

        Public Property MonnaieCompte_Primes_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_Primes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_Primes = value
                    Case Else
                        WsAnnuel_MonnaieCompte_Primes = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_HeuresSupp_Annuel() As Double
            Get
                Return WsAnnuel_HeuresSupp
            End Get
            Set(ByVal value As Double)
                WsAnnuel_HeuresSupp = value
            End Set
        End Property

        Public Property MonnaieCompte_HeuresSupp_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_HeuresSupp
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_HeuresSupp = value
                    Case Else
                        WsAnnuel_MonnaieCompte_HeuresSupp = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_DepensesMedicales_Annuel() As Double
            Get
                Return WsAnnuel_DepensesMedicales
            End Get
            Set(ByVal value As Double)
                WsAnnuel_DepensesMedicales = value
            End Set
        End Property

        Public Property MonnaieCompte_DepensesMedicales_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_DepensesMedicales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_DepensesMedicales = value
                    Case Else
                        WsAnnuel_MonnaieCompte_DepensesMedicales = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_ComplementaireAT_Annuel() As Double
            Get
                Return WsAnnuel_ComplementaireAT
            End Get
            Set(ByVal value As Double)
                WsAnnuel_ComplementaireAT = value
            End Set
        End Property

        Public Property MonnaieCompte_ComplementaireAT_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_ComplementaireAT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_ComplementaireAT = value
                    Case Else
                        WsAnnuel_MonnaieCompte_ComplementaireAT = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_ComplementaireMaladie_Annuel() As Double
            Get
                Return WsAnnuel_ComplementaireMaladie
            End Get
            Set(ByVal value As Double)
                WsAnnuel_ComplementaireMaladie = value
            End Set
        End Property

        Public Property MonnaieCompte_ComplementaireMaladie_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_Maladie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_Maladie = value
                    Case Else
                        WsAnnuel_MonnaieCompte_Maladie = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Montant_ComplementaireRetraite_Annuel() As Double
            Get
                Return WsAnnuel_ComplementaireRetraite
            End Get
            Set(ByVal value As Double)
                WsAnnuel_ComplementaireRetraite = value
            End Set
        End Property

        Public Property MonnaieCompte_ComplementaireRetraite_Annuel() As String
            Get
                Return WsAnnuel_MonnaieCompte_Retraite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsAnnuel_MonnaieCompte_Retraite = value
                    Case Else
                        WsAnnuel_MonnaieCompte_Retraite = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Motif_Modification() As String
            Get
                Return WsMotifModification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifModification = value
                    Case Else
                        WsMotifModification = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(MonnaieCompte_Principal & VI.Tild)
                Chaine.Append(MonnaieCompte_Alternative & VI.Tild)
                Select Case Montant_SalaireBrut_Mensuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_SalaireBrut_Mensuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_SalaireBrut_Mensuel & VI.Tild)
                Select Case Montant_PartSalarial_Mensuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_PartSalarial_Mensuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_PartSalarial_Mensuel & VI.Tild)
                Select Case Montant_PartPatronale_Mensuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_PartPatronale_Mensuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_PartPatronale_Mensuel & VI.Tild)
                Select Case Montant_Primes_Mensuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Primes_Mensuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_Primes_Mensuel & VI.Tild)
                Select Case Montant_Net_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Net_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_Net_Annuel & VI.Tild)
                Select Case Montant_PartSalarial_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_PartSalarial_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_PartSalarial_Mensuel & VI.Tild)
                Select Case Montant_PartPatronale_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_PartPatronale_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_PartSalarial_Annuel & VI.Tild)
                Select Case Montant_Primes_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Primes_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_Primes_Annuel & VI.Tild)
                Select Case Montant_HeuresSupp_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_HeuresSupp_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_HeuresSupp_Annuel & VI.Tild)
                Select Case Montant_DepensesMedicales_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_DepensesMedicales_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_DepensesMedicales_Annuel & VI.Tild)
                Select Case Montant_ComplementaireAT_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_ComplementaireAT_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_ComplementaireAT_Annuel & VI.Tild)
                Select Case Montant_ComplementaireMaladie_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_ComplementaireMaladie_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_ComplementaireMaladie_Annuel & VI.Tild)
                Select Case Montant_ComplementaireRetraite_Annuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_ComplementaireRetraite_Annuel.ToString) & VI.Tild)
                End Select
                Chaine.Append(MonnaieCompte_ComplementaireRetraite_Annuel & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Motif_Modification & VI.Tild)
                Chaine.Append(Observation)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 33 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                MonnaieCompte_Principal = TableauData(2)
                MonnaieCompte_Alternative = TableauData(3)
                If TableauData(4) <> "" Then Montant_SalaireBrut_Mensuel = VirRhFonction.ConversionDouble(TableauData(4))
                MonnaieCompte_SalaireBrut_Mensuel = TableauData(5)
                If TableauData(6) <> "" Then Montant_PartSalarial_Mensuel = VirRhFonction.ConversionDouble(TableauData(6))
                MonnaieCompte_PartSalarial_Mensuel = TableauData(7)
                If TableauData(8) <> "" Then Montant_PartPatronale_Mensuel = VirRhFonction.ConversionDouble(TableauData(8))
                MonnaieCompte_PartPatronale_Mensuel = TableauData(9)
                If TableauData(10) <> "" Then Montant_Primes_Mensuel = VirRhFonction.ConversionDouble(TableauData(10))
                MonnaieCompte_Primes_Mensuel = TableauData(11)
                If TableauData(12) <> "" Then Montant_Net_Annuel = VirRhFonction.ConversionDouble(TableauData(12))
                MonnaieCompte_Net_Annuel = TableauData(13)
                If TableauData(14) <> "" Then Montant_PartSalarial_Annuel = VirRhFonction.ConversionDouble(TableauData(14))
                MonnaieCompte_PartSalarial_Annuel = TableauData(15)
                If TableauData(16) <> "" Then Montant_PartPatronale_Annuel = VirRhFonction.ConversionDouble(TableauData(16))
                MonnaieCompte_PartPatronale_Annuel = TableauData(17)
                If TableauData(18) <> "" Then Montant_Primes_Annuel = VirRhFonction.ConversionDouble(TableauData(18))
                MonnaieCompte_Primes_Annuel = TableauData(19)
                If TableauData(20) <> "" Then Montant_HeuresSupp_Annuel = VirRhFonction.ConversionDouble(TableauData(20))
                MonnaieCompte_HeuresSupp_Annuel = TableauData(21)
                If TableauData(22) <> "" Then Montant_DepensesMedicales_Annuel = VirRhFonction.ConversionDouble(TableauData(22))
                MonnaieCompte_DepensesMedicales_Annuel = TableauData(23)
                If TableauData(24) <> "" Then Montant_ComplementaireAT_Annuel = VirRhFonction.ConversionDouble(TableauData(24))
                MonnaieCompte_ComplementaireAT_Annuel = TableauData(25)
                If TableauData(26) <> "" Then Montant_ComplementaireMaladie_Annuel = VirRhFonction.ConversionDouble(TableauData(26))
                MonnaieCompte_ComplementaireMaladie_Annuel = TableauData(27)
                If TableauData(28) <> "" Then Montant_ComplementaireRetraite_Annuel = VirRhFonction.ConversionDouble(TableauData(28))
                MonnaieCompte_ComplementaireRetraite_Annuel = TableauData(29)
                MyBase.Date_de_Fin = TableauData(30)
                Motif_Modification = TableauData(31)
                Observation = TableauData(32)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
