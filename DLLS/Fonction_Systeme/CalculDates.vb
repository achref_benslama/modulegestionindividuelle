Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Fonctions
    Public Class CalculDates
        Private LibelleMois(12) As String
        Private LibelleJour(7) As String
        Private WsPays As Integer = 1
        Private WsPaques As String
        Private WsAscension As String
        Private WsPentecote As String
        'Separateur des dates
        Private WsSeparateurDate As String
        'Jours RH
        Private WsJourOuvrable As Integer
        Private WsJourRepos As Integer
        '
        Private Function NombreJours_Base360(ByVal DateDebut As String, ByVal DateFin As String) As Integer
            If DateDebut = "" Or Datefin = "" Then
                Return 0
            End If
            Dim MM As Integer
            Dim NbJours As Integer = 0

            If CDate(DateFin).Year - CDate(DateDebut).Year > 0 Then
                NbJours = (CDate(DateFin).Year - CDate(DateDebut).Year) * 360
                DateDebut = "01/01/" & Strings.Right(DateFin, 4)
            End If
            MM = CDate(DateDebut).Month
            If CDate(DateDebut).Day > 1 Then
                NbJours -= CDate(DateDebut).Day - 1
            End If
            Do
                If MM = CDate(DateFin).Month Then
                    If MM = 2 Then
                        If CDate(DateFin).Day >= 28 Then
                            NbJours += 30
                        Else
                            NbJours += CDate(DateFin).Day
                        End If
                    Else
                        If CDate(DateFin).Day >= 30 Then
                            NbJours += 30
                        Else
                            NbJours += CDate(DateFin).Day
                        End If
                    End If
                Else
                    NbJours += 30
                End If
                MM += 1
                If MM > CDate(DateFin).Month Then
                    Exit Do
                End If
            Loop
            Return NbJours
        End Function

        Public Property Pays() As Integer
            Get
                If WsPays = 0 Then
                    WsPays = VI.Pays.France
                End If
                Return WsPays
            End Get
            Set(ByVal value As Integer)
                WsPays = CInt(VI.ParametreApplication("Pays"))
            End Set
        End Property

        Public ReadOnly Property AnneeBissextile(ByVal Annee As Integer) As Boolean
            Get
                ' Detection d'une annee bissextile ;
                ' si l'annee est bissextile : AnneeBissextile=True
                If Annee Mod 4 = 0 And Annee Mod 100 = 0 Then
                    Select Case Annee Mod 400
                        Case Is = 0
                            Return True
                    End Select
                End If
                If Annee Mod 4 = 0 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property ClairDate(ByVal Date1 As String, ByVal AvecleJour As Boolean) As String
            Get
                ' conversion d'une date (JJ/MM/AAAA)
                ' en date sous la forme :
                ' "Mercredi 6 novembre 1991" (string)
                Dim Wd As Integer
                Dim J1 As String
                Dim Chaine As String = ""

                Date1 = Date1.TrimStart
                If Date1 = "" Then
                    Return ""
                End If
                J1 = Str(CInt(Strings.Left(Date1, 2)))
                Wd = Weekday(DateValue(Date1), Microsoft.VisualBasic.FirstDayOfWeek.Monday)

                If CInt(Strings.Left(Date1, 2)) = 1 Then J1 = Strings.Space(1) & "1er"
                Select Case AvecleJour
                    Case True
                        Chaine = LibelleJour(Wd) & J1 & Strings.Space(1) & LibelleMois(CInt(Strings.Mid(Date1, 4, 2))) & Strings.Space(1) & CInt(Strings.Right(Date1, 4)).ToString.Trim
                    Case Else
                        Chaine = J1.TrimStart & Strings.Space(1) & LibelleMois(CInt(Strings.Mid(Date1, 4, 2))).ToUpper & Strings.Space(1) & CInt(Strings.Right(Date1, 4)).ToString.Trim
                End Select
                Return Chaine.ToLower
            End Get
        End Property

        Public ReadOnly Property MoisEnClair(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 1 To 12
                        Return LibelleMois(Index)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property IndexduMois(ByVal Intitule As String) As Integer
            Get
                Dim IndiceI As Integer
                For IndiceI = 1 To 12
                    If LibelleMois(IndiceI) = Intitule Then
                        Return IndiceI
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property JourEnClair(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 1 To 7
                        Return LibelleJour(Index)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property IndexduJour(ByVal Intitule As String) As Integer
            Get
                Dim IndiceI As Integer
                For IndiceI = 1 To 7
                    If LibelleJour(IndiceI) = Intitule Then
                        Return IndiceI
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property SiJourFerie(ByVal ArgumentDate As String) As Boolean
            Get
                Dim JJ As Integer
                Dim MM As Integer
                Dim AA As Integer
                Dim NbOr As Integer
                Dim Epacte As Double
                Dim Jour As Date
                Dim PLune As Date
                Dim Paques As Date
                Dim Ascension As Date
                Dim Pentecote As Date

                Jour = CDate(DateEditee(ArgumentDate))
                JJ = Day(Jour) : MM = Month(Jour) : AA = Year(Jour)
                NbOr = (AA Mod 19) + 1

                Epacte = (11 * NbOr - (3 + Int((2 + Int(AA / 100)) * 3 / 7))) Mod 30
                Select Case FormatdelaDate
                    Case VI.FormatDatesSysteme.DateFrance10, VI.FormatDatesSysteme.DateFrance8
                        PLune = System.DateTime.FromOADate(CDate("19/04/" & AA).ToOADate - ((Epacte + 6) Mod 30))
                    Case Else
                        PLune = System.DateTime.FromOADate(CDate("04/19/" & AA).ToOADate - ((Epacte + 6) Mod 30))
                End Select
                Select Case Epacte
                    Case Is = 24
                        PLune = System.DateTime.FromOADate(PLune.ToOADate - 1)
                End Select
                If Epacte = 25 And (AA >= 1900 And AA < 2200) Then PLune = System.DateTime.FromOADate(PLune.ToOADate - 1)

                Paques = System.DateTime.FromOADate(PLune.ToOADate - Weekday(PLune, Microsoft.VisualBasic.FirstDayOfWeek.Sunday) + Microsoft.VisualBasic.FirstDayOfWeek.Monday + 7)
                Ascension = System.DateTime.FromOADate(Paques.ToOADate + 38)
                Pentecote = System.DateTime.FromOADate(Ascension.ToOADate + 11)

                WsPaques = DateStandardVirtualia(CStr(Paques))
                WsAscension = DateStandardVirtualia(CStr(Ascension))
                WsPentecote = DateStandardVirtualia(CStr(Pentecote))

                'Les jours feries communs No�l et jour de l'an
                If JJ = 1 And MM = 1 Then Return True
                If JJ = 25 And MM = 12 Then Return True
                'Le 6 Janvier
                If JJ = 6 And MM = 1 Then
                    Select Case Pays
                        Case Is = VI.Pays.Allemagne, VI.Pays.Espagne, VI.Pays.Italie
                            Return True
                    End Select
                End If
                'Le 26 Decembre
                If JJ = 26 And MM = 12 Then
                    Select Case Pays
                        Case Is = VI.Pays.Angleterre, VI.Pays.Allemagne, VI.Pays.Espagne, VI.Pays.Italie, VI.Pays.Portugal, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                'Le 1er mai
                If JJ = 1 And MM = 5 Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Allemagne, VI.Pays.Italie, VI.Pays.Hollande, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                Select Case Pays
                    Case Is = VI.Pays.France
                        'Le 8 Mai et le 14 juillet
                        If JJ = 8 And MM = 5 Then Return True
                        If JJ = 14 And MM = 7 Then Return True
                    Case Is = VI.Pays.Angleterre
                        'Les 13 Avril, 7 et 28 Mai et 27 Ao�t
                        If JJ = 13 And MM = 4 Then Return True
                        If JJ = 7 And MM = 5 Then Return True
                        If JJ = 28 And MM = 5 Then Return True
                        If JJ = 27 And MM = 8 Then Return True
                    Case Is = VI.Pays.Allemagne
                        'Les 13 Avril, 14 Juin et 3 Octobre
                        If JJ = 13 And MM = 4 Then Return True
                        If JJ = 14 And MM = 6 Then Return True
                        If JJ = 3 And MM = 10 Then Return True
                    Case Is = VI.Pays.Espagne
                        'Les 13 Avril, 6 et 8 decembre
                        If JJ = 13 And MM = 4 Then Return True
                        If JJ = 6 And MM = 12 Then Return True
                        If JJ = 8 And MM = 12 Then Return True
                    Case Is = VI.Pays.Italie
                        'Le 8 decembre
                        If (JJ = 8 And MM = 12) Then Return True
                    Case Is = VI.Pays.Portugal
                        'Les 27 Fevrier, 13 Avril, 5 Octobre , 1 et 8 decembre
                        If JJ = 27 And MM = 2 Then Return True
                        If JJ = 13 And MM = 4 Then Return True
                        If JJ = 5 And MM = 10 Then Return True
                        If JJ = 1 And MM = 12 Then Return True
                        If JJ = 8 And MM = 12 Then Return True
                    Case Is = VI.Pays.Hollande
                        'Le 30 avril
                        If (JJ = 30 And MM = 4) Then Return True
                    Case Is = VI.Pays.Belgique
                        'Le 21 Juillet
                        If (JJ = 21 And MM = 7) Then Return True
                    Case Is = VI.Pays.EtatsUnis
                        'Les 15 Janvier,19 Fevrier,13 Avril, 28 Mai, 4 Juillet, 3 Septembre, 8 Octobre, 22 Novembre
                        If JJ = 15 And MM = 1 Then Return True
                        If JJ = 19 And MM = 2 Then Return True
                        If JJ = 13 And MM = 4 Then Return True
                        If JJ = 28 And MM = 5 Then Return True
                        If JJ = 4 And MM = 7 Then Return True
                        If JJ = 3 And MM = 9 Then Return True
                        If JJ = 8 And MM = 10 Then Return True
                        If JJ = 22 And MM = 11 Then Return True
                    Case Is = VI.Pays.Luxembourg
                        'Le 23 Juin
                        If (JJ = 23 And MM = 6) Then Return True
                End Select
                'Le 15 Aout
                If JJ = 15 And MM = 8 Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Espagne, VI.Pays.Italie, VI.Pays.Portugal, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                'Le 1er Novembre
                If JJ = 1 And MM = 11 Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Allemagne, VI.Pays.Espagne, VI.Pays.Italie, VI.Pays.Portugal, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                'Le 11 Novembre
                If JJ = 11 And MM = 11 Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Belgique, VI.Pays.EtatsUnis
                            Return True
                    End Select
                End If
                'P�ques
                If JJ = Day(Paques) And MM = Month(Paques) Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Angleterre, VI.Pays.Allemagne, VI.Pays.Espagne, VI.Pays.Italie, VI.Pays.Portugal, VI.Pays.Hollande, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                If JJ = Day(Ascension) And MM = Month(Ascension) Then
                    Select Case Pays
                        Case Is = VI.Pays.France, VI.Pays.Allemagne, VI.Pays.Italie, VI.Pays.Portugal, VI.Pays.Hollande, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                If JJ = Day(Pentecote) And MM = Month(Pentecote) Then
                    Select Case Pays
                        Case Is = VI.Pays.France
                            Select Case AA
                                Case Is < 2005
                                    Return True
                                Case Is > 2008
                                    Return True
                            End Select
                        Case Is = VI.Pays.Allemagne, VI.Pays.Portugal, VI.Pays.Hollande, VI.Pays.Belgique, VI.Pays.Luxembourg
                            Return True
                    End Select
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property LundidePaques(ByVal Annee As String) As String
            Get
                Dim Test As Boolean
                Test = SiJourFerie("01/06/" & Annee)
                Return WsPaques
            End Get
        End Property

        Public ReadOnly Property LundidePentecote(ByVal Annee As String) As String
            Get
                Dim Test As Boolean
                Test = SiJourFerie("01/06/" & Annee)
                Return WsPentecote
            End Get
        End Property

        Public ReadOnly Property JeudidelAscension(ByVal Annee As String) As String
            Get
                Dim Test As Boolean
                Test = SiJourFerie("01/06/" & Annee)
                Return WsAscension
            End Get
        End Property

        Public ReadOnly Property ListeDesJoursFeries(ByVal Annee As String, ByVal Mois As Integer) As String
            Get
                Dim IMois As Integer
                Dim IDebut As Integer
                Dim IFin As Integer
                Dim IJour As Integer
                Dim INombre As Integer
                Dim Chaine As New System.Text.StringBuilder
                Select Case Mois
                    Case 1 To 12
                        IDebut = Mois
                        IFin = Mois
                    Case Else
                        IDebut = 1
                        IFin = 12
                End Select
                For IMois = IDebut To IFin
                    Select Case IMois
                        Case 2
                            INombre = 28
                        Case 1, 3, 5, 7, 8, 10, 12
                            INombre = 31
                        Case Else
                            INombre = 30
                    End Select
                    For IJour = 1 To INombre
                        Select Case SiJourFerie(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee)
                            Case True
                                Chaine.Append(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee & VI.Tild)
                        End Select
                    Next IJour
                Next IMois
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property NombredeJoursFeries(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String

                TableauData = Strings.Split(ListeDesJoursFeries(Annee, Mois), VI.Tild, -1)
                Try
                    Return TableauData.Count - 1
                Catch E As Exception
                    Return 0
                End Try
            End Get
        End Property

        Public ReadOnly Property SiJourOuvre(ByVal ArgumentDate As String, ByVal SiTenirCompteJourFerie As Boolean) As Boolean
            Get
                Dim Wd As Integer
                Try
                    Wd = Weekday(DateValue(ArgumentDate), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                Catch ex As Exception
                    Wd = Weekday(DateValue(DateStandardVirtualia(ArgumentDate)), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                End Try
                Select Case Wd
                    Case Is = VI.Jours.Dimanche
                        Return False
                    Case Is = VI.Jours.Samedi
                        Return False
                    Case Else
                        Select Case SiTenirCompteJourFerie
                            Case False
                                Return True
                            Case True
                                Select Case SiJourFerie(ArgumentDate)
                                    Case True
                                        Return False
                                    Case False
                                        Return True
                                End Select
                        End Select
                End Select
                Return True
            End Get
        End Property

        Public ReadOnly Property SiJourOuvrable(ByVal ArgumentDate As String, ByVal SiTenirCompteJourFerie As Boolean) As Boolean
            Get
                Dim Wd As Integer

                Try
                    Wd = Weekday(DateValue(ArgumentDate), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                Catch ex As Exception
                    Wd = Weekday(DateValue(DateStandardVirtualia(ArgumentDate)), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                End Try
                Select Case Wd
                    Case Is = VI.Jours.Dimanche
                        Return False
                    Case Else
                        Select Case SiTenirCompteJourFerie
                            Case False
                                Return True
                            Case True
                                Select Case SiJourFerie(ArgumentDate)
                                    Case True
                                        Return False
                                    Case False
                                        Return True
                                End Select
                        End Select
                End Select
                Return True
            End Get
        End Property

        Public ReadOnly Property NombredeJoursOuvres(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvres(Annee, Mois, False), VI.Tild, -1)
                Return TableauData.Count - 1
            End Get
        End Property

        Public ReadOnly Property NombredeJoursTravailles(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvres(Annee, Mois, True), VI.Tild, -1)
                Return TableauData.Count - 1
            End Get
        End Property

        Public ReadOnly Property NombredeJoursOuvrables(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvrables(Annee, Mois, False), VI.Tild, -1)
                Return TableauData.Count - 1
            End Get
        End Property

        Public ReadOnly Property ListeDesJoursOuvres(ByVal Annee As String, ByVal Mois As Integer, ByVal SiTravaille As Boolean) As String
            Get
                Dim IMois As Integer
                Dim IDebut As Integer
                Dim IFin As Integer
                Dim IJour As Integer
                Dim INombre As Integer
                Dim Chaine As New System.Text.StringBuilder
                Select Case Mois
                    Case 1 To 12
                        IDebut = Mois
                        IFin = Mois
                    Case Else
                        IDebut = 1
                        IFin = 12
                End Select
                For IMois = IDebut To IFin
                    Select Case IMois
                        Case 2
                            INombre = 28
                        Case 1, 3, 5, 7, 8, 10, 12
                            INombre = 31
                        Case Else
                            INombre = 30
                    End Select
                    For IJour = 1 To INombre
                        Select Case SiJourOuvre(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee, SiTravaille)
                            Case True
                                Chaine.Append(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee & VI.Tild)
                        End Select
                    Next IJour
                Next IMois
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeDesJoursOuvrables(ByVal Annee As String, ByVal Mois As Integer, ByVal SiTravaille As Boolean) As String
            Get
                Dim IMois As Integer
                Dim IDebut As Integer
                Dim IFin As Integer
                Dim IJour As Integer
                Dim INombre As Integer
                Dim Chaine As New System.Text.StringBuilder
                Select Case Mois
                    Case 1 To 12
                        IDebut = Mois
                        IFin = Mois
                    Case Else
                        IDebut = 1
                        IFin = 12
                End Select

                For IMois = IDebut To IFin
                    Select Case IMois
                        Case 2
                            INombre = 28
                        Case 1, 3, 5, 7, 8, 10, 12
                            INombre = 31
                        Case Else
                            INombre = 30
                    End Select
                    For IJour = 1 To INombre
                        Select Case SiJourOuvrable(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee, SiTravaille)
                            Case True
                                Chaine.Append(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee & VI.Tild)
                        End Select
                    Next IJour
                Next IMois
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property FormatdelaDate() As Integer
            Get
                Dim Resultat As Integer
                Dim TestDate As Date
                Dim ChaineDate As String

                Resultat = VI.FormatDatesSysteme.DateFrance10
                Try
                    TestDate = DateValue("25/12/2001")
                Catch ex As Exception
                    Try
                        TestDate = DateValue("12/25/2001")
                    Catch ex2 As Exception
                        Return VI.FormatDatesSysteme.DateFrance10
                    End Try
                End Try
                ChaineDate = CStr(TestDate)
                Select Case CInt(Strings.Left(CStr(TestDate), 2))
                    Case Is = 25
                        Resultat = VI.FormatDatesSysteme.DateFrance10
                        Select Case ChaineDate.Length
                            Case Is = 8
                                Resultat = VI.FormatDatesSysteme.DateFrance8
                        End Select
                    Case Is = 12
                        Resultat = VI.FormatDatesSysteme.DateUS10
                        Select Case ChaineDate.Length
                            Case Is = 8
                                Resultat = VI.FormatDatesSysteme.DateUS8
                                Select Case DateValue("01/01/01").ToShortDateString.Length
                                    Case Is < 8
                                        Resultat = VI.FormatDatesSysteme.DateUS5
                                End Select
                        End Select
                    Case Is = 20
                        Resultat = VI.FormatDatesSysteme.DateInverse10
                    Case Is = 1
                        Resultat = VI.FormatDatesSysteme.DateInverse8
                End Select
                WsSeparateurDate = VI.Slash
                Select Case Strings.InStr(CStr(TestDate), VI.Tiret)
                    Case Is > 0
                        WsSeparateurDate = VI.Tiret
                    Case Else
                        Select Case Strings.InStr(CStr(TestDate), Strings.Space(1))
                            Case Is > 0
                                WsSeparateurDate = Strings.Space(1)
                            Case Else
                                Select Case Strings.InStr(CStr(TestDate), VI.PointFinal)
                                    Case Is > 0
                                        WsSeparateurDate = VI.PointFinal
                                End Select
                        End Select
                End Select
                Return Resultat
            End Get
        End Property

        Public ReadOnly Property DateduJour() As String
            Get
                Dim TableauData(0) As String
                Dim TableauDataDate(0) As String
                Dim JDate As String
                Dim FormatLocal As Integer
                Dim ChaineDate As String = ""

                FormatLocal = FormatdelaDate
                'FormatLocal = 3
                JDate = CStr(System.DateTime.Now)
                ChaineDate = Format(System.DateTime.Now, "d")
                Select Case FormatLocal
                    Case VI.FormatDatesSysteme.DateFrance10
                        ChaineDate = Strings.Left(JDate, 10)
                    Case VI.FormatDatesSysteme.DateFrance8
                        ChaineDate = Strings.Left(JDate, 2) & VI.Slash & Strings.Mid(JDate, 4, 2) _
                            & VI.Slash & Strings.Format(Year(System.DateTime.Now), "0000")
                    Case VI.FormatDatesSysteme.DateUS10, VI.FormatDatesSysteme.DateUS8
                        ChaineDate = Strings.Mid(JDate, 4, 2) & VI.Slash & Strings.Left(JDate, 2) & VI.Slash _
                        & Strings.Format(Year(System.DateTime.Now), "0000")
                        ''****AKR
                        If ChaineDate.Contains("//") Then
                            TableauDataDate = Strings.Split(JDate, WsSeparateurDate, -1)
                            Select Case TableauDataDate.Count
                                Case Is = 3
                                    ChaineDate = Strings.Format(CInt(TableauDataDate(1)), "00") & VI.Slash _
                                    & Strings.Format(CInt(TableauDataDate(0)), "00") & VI.Slash _
                                    & Strings.Format(Year(System.DateTime.Now), "0000")
                            End Select
                        End If
                        ''****

                    Case VI.FormatDatesSysteme.DateInverse10
                        ChaineDate = Strings.Mid(JDate, 9, 2) & VI.Slash & Strings.Mid(JDate, 6, 2) & VI.Slash & Strings.Left(JDate, 4)
                    Case VI.FormatDatesSysteme.DateInverse8
                        ChaineDate = Strings.Mid(JDate, 7, 2) & VI.Slash & Strings.Mid(JDate, 4, 2) _
                            & VI.Slash & Strings.Format(Year(System.DateTime.Now), "0000")
                    Case VI.FormatDatesSysteme.DateUS5
                        Select Case WsSeparateurDate
                            Case Is <> ""
                                TableauData = Strings.Split(JDate, WsSeparateurDate, -1)
                                Select Case TableauData.Count
                                    Case Is = 3
                                        ChaineDate = Strings.Format(CInt(TableauData(1)), "00") & VI.Slash _
                                            & Strings.Format(CInt(TableauData(0)), "00") & VI.Slash _
                                            & Strings.Format(Year(System.DateTime.Now), "0000")
                                End Select
                        End Select
                End Select

                Return ChaineDate
            End Get
        End Property

        Public ReadOnly Property DateTypee(ByVal ArgumentDate As String, Optional ByVal SiMax As Boolean = False) As Date
            Get
                Dim Chaine As String
                If ArgumentDate.Length <= 10 Then
                    Chaine = DateSaisieVerifiee(ArgumentDate)
                Else
                    Chaine = DateSaisieVerifiee(Strings.Left(ArgumentDate, 10))
                End If
                If Chaine = "" Then
                    If SiMax = True Then
                        Return System.DateTime.MaxValue
                    Else
                        Return System.DateTime.MinValue
                    End If
                End If
                Try
                    Return CDate(Chaine)
                Catch ex As Exception
                    If SiMax = True Then
                        Return System.DateTime.MaxValue
                    Else
                        Return System.DateTime.MinValue
                    End If
                End Try
            End Get
        End Property

        Public ReadOnly Property DateSaisieVerifiee(ByVal ArgumentDate As String) As String
            Get
                ' verification de la validitee d'une date
                ' fournie sous une des formes suivantes:
                '    JJ?MM?SSAA ou JJMMSSAA,
                ' l'annee est comprise entre 1753 et 2078
                ' resultat sous la forme JJ/MM/AAAA
                Dim TableauData(0) As String
                Dim CharSep As String
                Dim FormatLocal As Integer
                Dim Erreur As Integer
                Dim Jour As Integer
                Dim Mois As Integer
                Dim Annee As Integer

                Try
                    If ArgumentDate = "" Or ArgumentDate = "//" Then
                        Return ""
                    End If
                    FormatLocal = FormatdelaDate
                    'FormatLocal = 3
                    CharSep = ""
                    Select Case Strings.InStr(ArgumentDate, VI.Slash)
                        Case Is > 0
                            CharSep = VI.Slash
                        Case Else
                            Select Case Strings.InStr(ArgumentDate, VI.Tiret)
                                Case Is > 0
                                    CharSep = VI.Tiret
                                Case Else
                                    Select Case Strings.InStr(ArgumentDate, " ")
                                        Case Is > 0
                                            CharSep = Strings.Space(1)
                                        Case Else
                                            Select Case Strings.InStr(ArgumentDate, VI.PointFinal)
                                                Case Is > 0
                                                    CharSep = VI.PointFinal
                                            End Select
                                    End Select
                            End Select
                    End Select
                    Select Case CharSep
                        Case Is <> ""
                            TableauData = Strings.Split(ArgumentDate, CharSep, -1)
                            If TableauData.Count = 3 Then
                                If TableauData(0) = "" Or TableauData(1) = "" Or TableauData(2) = "" Then
                                    Return ""
                                End If
                            End If
                    End Select
                    Erreur = 0
                    Select Case FormatLocal
                        Case VI.FormatDatesSysteme.DateFrance10, VI.FormatDatesSysteme.DateFrance8
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 2))
                                        Case 8
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case 10
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 4, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Jour = CInt(TableauData(0))
                                            Mois = CInt(TableauData(1))
                                            Annee = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                        Case VI.FormatDatesSysteme.DateUS10, VI.FormatDatesSysteme.DateUS8, VI.FormatDatesSysteme.DateUS5
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Mois = CInt(Strings.Left(ArgumentDate, 2))
                                            Jour = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 2))
                                        Case 8
                                            Mois = CInt(Strings.Left(ArgumentDate, 2))
                                            Jour = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case 10
                                            Mois = CInt(Strings.Left(ArgumentDate, 2))
                                            Jour = CInt(Strings.Mid(ArgumentDate, 4, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Mois = CInt(TableauData(0))
                                            Jour = CInt(TableauData(1))
                                            Annee = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                        Case VI.FormatDatesSysteme.DateInverse10, VI.FormatDatesSysteme.DateInverse8
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 2))
                                        Case 8
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 5, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 4))
                                        Case 10
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 6, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Annee = CInt(TableauData(0))
                                            Mois = CInt(TableauData(1))
                                            Jour = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                    End Select
                    If Jour < 1 Or Jour > 31 Then Erreur += 1
                    If Mois < 1 Or Mois > 12 Then Erreur += 1
                    Select Case Annee
                        Case Is < 100
                            Annee = Annee + Siecledelannee(Annee)
                    End Select
                    If Annee < 1753 Or Annee > 2200 Then Erreur += 1
                    Select Case Mois
                        Case 4, 6, 9, 11
                            If Jour = 31 Then Jour = 30
                        Case 2
                            Select Case AnneeBissextile(Annee)
                                Case False
                                    If Jour > 28 Then Jour = 28
                                Case True
                                    If Jour > 29 Then Jour = 29
                            End Select
                    End Select
                    Select Case Erreur
                        Case Is > 0
                            Return ""
                    End Select
                    Select Case FormatLocal
                        Case VI.FormatDatesSysteme.DateFrance10
                            Return Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Annee, "0000")
                        Case VI.FormatDatesSysteme.DateFrance8
                            Return Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case VI.FormatDatesSysteme.DateUS10
                            Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Annee, "0000")
                        Case VI.FormatDatesSysteme.DateUS8
                            Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case VI.FormatDatesSysteme.DateInverse10
                            Return Strings.Format(Annee, "0000") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                        Case VI.FormatDatesSysteme.DateInverse8
                            Return Strings.Right(Strings.Format(Annee, "0000"), 2) & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                        Case VI.FormatDatesSysteme.DateUS5
                            Return Strings.Format(Mois, "#0") & WsSeparateurDate & Strings.Format(Jour, "#0") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case Else
                            Return ""
                    End Select
                Catch ex As Exception
                    Return ""
                End Try
            End Get
        End Property

        Public ReadOnly Property DateSaisieTypeeFr(ByVal ArgumentDate As String) As String
            Get
                ' verification de la validitee d'une date entr�e en fran�ais
                ' fournie sous une des formes suivantes:
                '    JJ?MM?SSAA ou JJMMSSAA,
                ' l'annee est comprise entre 1753 et 2078
                ' resultat sous la forme JJ/MM/AAAA
                Dim TableauData(0) As String
                Dim CharSep As String
                Dim FormatLocal As Integer
                Dim Erreur As Integer
                Dim Jour As Integer
                Dim Mois As Integer
                Dim Annee As Integer

                Try
                    If ArgumentDate = "" Or ArgumentDate = "//" Then
                        Return ""
                    End If
                    FormatLocal = FormatdelaDate
                    'FormatLocal = 3
                    CharSep = ""
                    Select Case Strings.InStr(ArgumentDate, VI.Slash)
                        Case Is > 0
                            CharSep = VI.Slash
                        Case Else
                            Select Case Strings.InStr(ArgumentDate, VI.Tiret)
                                Case Is > 0
                                    CharSep = VI.Tiret
                                Case Else
                                    Select Case Strings.InStr(ArgumentDate, " ")
                                        Case Is > 0
                                            CharSep = Strings.Space(1)
                                        Case Else
                                            Select Case Strings.InStr(ArgumentDate, VI.PointFinal)
                                                Case Is > 0
                                                    CharSep = VI.PointFinal
                                            End Select
                                    End Select
                            End Select
                    End Select
                    Select Case CharSep
                        Case Is <> ""
                            TableauData = Strings.Split(ArgumentDate, CharSep, -1)
                            If TableauData.Count = 3 Then
                                If TableauData(0) = "" Or TableauData(1) = "" Or TableauData(2) = "" Then
                                    Return ""
                                End If
                            End If
                    End Select
                    Erreur = 0
                    Select Case FormatLocal
                        Case VI.FormatDatesSysteme.DateFrance10, VI.FormatDatesSysteme.DateFrance8
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 2))
                                        Case 8
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case 10
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 4, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Jour = CInt(TableauData(0))
                                            Mois = CInt(TableauData(1))
                                            Annee = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                        Case VI.FormatDatesSysteme.DateUS10, VI.FormatDatesSysteme.DateUS8, VI.FormatDatesSysteme.DateUS5
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 2))
                                        Case 8
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case 10
                                            Jour = CInt(Strings.Left(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 4, 2))
                                            Annee = CInt(Strings.Right(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Jour = CInt(TableauData(0))
                                            Mois = CInt(TableauData(1))
                                            Annee = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                        Case VI.FormatDatesSysteme.DateInverse10, VI.FormatDatesSysteme.DateInverse8
                            Select Case CharSep
                                Case Is = ""
                                    Select Case ArgumentDate.Length
                                        Case 6
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 3, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 2))
                                        Case 8
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 5, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 4))
                                        Case 10
                                            Jour = CInt(Strings.Right(ArgumentDate, 2))
                                            Mois = CInt(Strings.Mid(ArgumentDate, 6, 2))
                                            Annee = CInt(Strings.Left(ArgumentDate, 4))
                                        Case Else
                                            Erreur += 1
                                    End Select
                                Case Else
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Jour = CInt(TableauData(0))
                                            Mois = CInt(TableauData(1))
                                            Annee = CInt(TableauData(2))
                                        Case Else
                                            Erreur += 1
                                    End Select
                            End Select
                    End Select
                    If Jour < 1 Or Jour > 31 Then Erreur += 1
                    If Mois < 1 Or Mois > 12 Then Erreur += 1
                    Select Case Annee
                        Case Is < 100
                            Annee = Annee + Siecledelannee(Annee)
                    End Select
                    If Annee < 1753 Or Annee > 2200 Then Erreur += 1
                    Select Case Mois
                        Case 4, 6, 9, 11
                            If Jour = 31 Then Jour = 30
                        Case 2
                            Select Case AnneeBissextile(Annee)
                                Case False
                                    If Jour > 28 Then Jour = 28
                                Case True
                                    If Jour > 29 Then Jour = 29
                            End Select
                    End Select
                    Select Case Erreur
                        Case Is > 0
                            Return ""
                    End Select
                    Select Case FormatLocal
                        Case VI.FormatDatesSysteme.DateFrance10
                            Return Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Annee, "0000")
                        Case VI.FormatDatesSysteme.DateFrance8
                            Return Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case VI.FormatDatesSysteme.DateUS10
                            Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Annee, "0000")
                        Case VI.FormatDatesSysteme.DateUS8
                            Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case VI.FormatDatesSysteme.DateInverse10
                            Return Strings.Format(Annee, "0000") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                        Case VI.FormatDatesSysteme.DateInverse8
                            Return Strings.Right(Strings.Format(Annee, "0000"), 2) & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                        Case VI.FormatDatesSysteme.DateUS5
                            Return Strings.Format(Mois, "#0") & WsSeparateurDate & Strings.Format(Jour, "#0") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                        Case Else
                            Return ""
                    End Select
                Catch ex As Exception
                    Return ""
                End Try
            End Get
        End Property


        Public ReadOnly Property DateStandardVirtualia(ByVal ArgumentDate As String) As String
            Get
                Dim TableauData(0) As String
                Dim FormatLocal As Integer
                Dim ChaineDate As String
                Dim Resultat As String

                If ArgumentDate Is Nothing AndAlso ArgumentDate = "" Then
                    Return ""
                End If
                Try
                    FormatLocal = FormatdelaDate
                    'ArgumentDate = "09/17/2018"
                    'FormatLocal = 3
                    ChaineDate = ArgumentDate
                    Resultat = ""
                    Select Case FormatLocal
                        Case VI.FormatDatesSysteme.DateFrance10
                            Select Case ArgumentDate.Length
                                Case Is = 10
                                    Resultat = ArgumentDate
                                Case Else
                                    Resultat = DateSaisieVerifiee(ArgumentDate)
                            End Select
                        Case VI.FormatDatesSysteme.DateFrance8
                            Select Case ArgumentDate.Length
                                Case Is <> 8
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Resultat = Strings.Left(ChaineDate, 2) & VI.Slash & Strings.Mid(ChaineDate, 4, 2) & VI.Slash & Strings.Format(CInt(Strings.Right(ChaineDate, 2)) + Siecledelannee(CInt(Strings.Right(ChaineDate, 2))), "0000")
                        Case VI.FormatDatesSysteme.DateUS10
                            Select Case ArgumentDate.Length
                                Case Is <> 10
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Resultat = Strings.Mid(ChaineDate, 4, 2) & VI.Slash & Strings.Left(ChaineDate, 2) & VI.Slash & Strings.Right(ChaineDate, 4)
                        Case VI.FormatDatesSysteme.DateUS8
                            Select Case ArgumentDate.Length
                                Case Is <> 8
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Resultat = Strings.Mid(ChaineDate, 4, 2) & VI.Slash & Strings.Left(ChaineDate, 2) & VI.Slash & Strings.Format(CInt(Strings.Right(ChaineDate, 2)) + Siecledelannee(CInt(Strings.Right(ChaineDate, 2))), "0000")
                        Case VI.FormatDatesSysteme.DateInverse10
                            Select Case ArgumentDate.Length
                                Case Is <> 10
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Resultat = Strings.Right(ChaineDate, 2) & VI.Slash & Strings.Mid(ChaineDate, 6, 2) & VI.Slash & Strings.Left(ChaineDate, 4)
                        Case VI.FormatDatesSysteme.DateInverse8
                            Select Case ArgumentDate.Length
                                Case Is <> 8
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Resultat = Strings.Right(ChaineDate, 2) & VI.Slash & Strings.Mid(ChaineDate, 4, 2) & VI.Slash & Strings.Format(CInt(Strings.Left(ChaineDate, 2)) + Siecledelannee(CInt(Strings.Left(ChaineDate, 2))), "0000")
                        Case VI.FormatDatesSysteme.DateUS5
                            Select Case ArgumentDate.Length
                                Case Is <> 8
                                    ChaineDate = DateSaisieVerifiee(ArgumentDate)
                            End Select
                            Select Case WsSeparateurDate
                                Case Is <> ""
                                    TableauData = Strings.Split(ChaineDate, WsSeparateurDate, -1)
                                    Select Case TableauData.Count
                                        Case Is = 3
                                            Resultat = Strings.Format(CInt(TableauData(1)), "00") & VI.Slash & Strings.Format(CInt(TableauData(0)), "00") & Strings.Format(CInt(TableauData(2)) + Siecledelannee(CInt(TableauData(2))), "0000")
                                    End Select
                            End Select
                    End Select
                    Select Case Resultat.Trim.Length
                        Case Is = 10
                            Return Resultat
                        Case Else
                            Return ""
                    End Select
                Catch ex As Exception
                    Return ""
                End Try
            End Get
        End Property

        Public ReadOnly Property DateEditee(ByVal DateStd As String) As String
            Get
                Dim FormatLocal As Integer
                Dim Jour As Integer
                Dim Mois As Integer
                Dim Annee As Integer

                If DateStd = "" Then
                    Return ""
                End If
                FormatLocal = FormatdelaDate
                'FormatLocal = 3
                Jour = CInt(Strings.Left(DateStd, 2))
                Mois = CInt(Strings.Mid(DateStd, 4, 2))
                Annee = CInt(Strings.Right(DateStd, 4))
                Select Case FormatLocal
                    Case VI.FormatDatesSysteme.DateFrance10
                        Return DateStd
                    Case VI.FormatDatesSysteme.DateFrance8
                        Return Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                    Case VI.FormatDatesSysteme.DateUS10
                        Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Format(Annee, "0000")
                    Case VI.FormatDatesSysteme.DateUS8
                        Return Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                    Case VI.FormatDatesSysteme.DateInverse10
                        Return Strings.Format(Annee, "0000") & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                    Case VI.FormatDatesSysteme.DateInverse8
                        Return Strings.Right(Strings.Format(Annee, "0000"), 2) & WsSeparateurDate & Strings.Format(Mois, "00") & WsSeparateurDate & Strings.Format(Jour, "00")
                    Case VI.FormatDatesSysteme.DateUS5
                        Return Strings.Format(Mois, "#0") & WsSeparateurDate & Strings.Format(Jour, "#0") & WsSeparateurDate & Strings.Right(Strings.Format(Annee, "0000"), 2)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Private ReadOnly Property Siecledelannee(ByVal Anneesur2car As Integer) As Integer
            Get
                Select Case Anneesur2car
                    Case Is >= 50
                        Return 1900
                    Case Else
                        Return 2000
                End Select
            End Get
        End Property

        Public ReadOnly Property Semaine_Numero(ByVal ArgumentDate As String) As Integer
            Get
                If DateSaisieVerifiee(ArgumentDate) = "" Then
                    Return 0
                End If
                Dim NoSemaine As Integer
                Dim DateOrigine As Date
                Dim Cal As System.Globalization.GregorianCalendar
                Dim K As Integer

                DateOrigine = CDate(ArgumentDate)
                '****
                'Return DatePart(DateInterval.WeekOfYear, DateOrigine, FirstDayOfWeek.Monday, FirstWeekOfYear.FirstFourDays)
                '****
                Cal = New System.Globalization.GregorianCalendar(System.Globalization.GregorianCalendarTypes.Localized)
                NoSemaine = Cal.GetWeekOfYear(DateOrigine, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
                If NoSemaine < 53 Then
                    Return NoSemaine
                End If
                If DateOrigine.Month = 1 Then
                    Return 53
                End If
                For K = 1 To 6
                    If Cal.GetDayOfWeek(CDate(Strings.Format(K, "00") & "/01/" & CDate(ArgumentDate).Year + 1)) = DayOfWeek.Thursday Then
                        Return 1
                    End If
                Next K
                Return 53
            End Get
        End Property

        Private ReadOnly Property Semaine_Date(ByVal NoSemaine As Integer, ByVal DateSource As String, ByVal SiDateFin As Boolean) As String
            Get
                Dim K As Integer
                Dim JJ As Integer
                Dim MM As Integer
                Dim AA As Integer
                Dim RelVal As Double
                Dim DateTestee As Date
                Dim SemaineTestee As Integer
                Dim IndiceDebut As Integer = 0
                Dim DateRes As String = ""

                JJ = 1
                MM = 1
                AA = CDate(DateSource).Year
                RelVal = DateSerial(AA, MM, JJ).ToOADate
                IndiceDebut = (NoSemaine - 1) * 7
                For K = IndiceDebut To 366
                    RelVal = DateSerial(AA, MM, JJ + K).ToOADate
                    DateTestee = System.DateTime.FromOADate(RelVal)
                    SemaineTestee = Semaine_Numero(DateStandardVirtualia(CStr(DateTestee)))
                    Select Case SemaineTestee
                        Case Is = NoSemaine
                            Select Case SiDateFin
                                Case True
                                    DateRes = Semaine_Date_Fin(DateStandardVirtualia(CStr(DateTestee)))
                                Case False
                                    DateRes = Semaine_Date_Debut(DateStandardVirtualia(CStr(DateTestee)))
                            End Select
                            Exit For
                    End Select
                Next K
                If NoSemaine = 1 Or NoSemaine >= 52 Then
                    If DateRes = "" Then
                        DateRes = "01/01/" & AA
                    End If
                    If CDate(DateRes) < CDate(DateSource) Then
                        RelVal = DateSerial(AA + 1, MM, JJ).ToOADate
                        For K = 0 To 366
                            RelVal = DateSerial(AA + 1, MM, JJ + K).ToOADate
                            DateTestee = System.DateTime.FromOADate(RelVal)
                            SemaineTestee = Semaine_Numero(DateStandardVirtualia(CStr(DateTestee)))
                            Select Case SemaineTestee
                                Case Is = NoSemaine
                                    Select Case SiDateFin
                                        Case True
                                            Return Semaine_Date_Fin(DateStandardVirtualia(CStr(DateTestee)))
                                        Case False
                                            Return Semaine_Date_Debut(DateStandardVirtualia(CStr(DateTestee)))
                                    End Select
                            End Select
                        Next K
                    End If
                End If
                Return DateRes
            End Get
        End Property

        Public ReadOnly Property Semaine_Date_Debut(ByVal NoSemaine As Integer, ByVal DateSource As String) As String
            Get
                Return Semaine_Date(NoSemaine, DateSource, False)
            End Get
        End Property

        Public ReadOnly Property Semaine_Date_Debut(ByVal ArgumentDate As String) As String
            Get
                Dim JJ As Integer
                Dim MM As Integer
                Dim AA As Integer
                Dim DateRes As Date
                Dim Wd As Integer

                If DateSaisieVerifiee(ArgumentDate) = "" Then
                    Return ""
                End If
                JJ = Day(CDate(ArgumentDate))
                MM = Month(CDate(ArgumentDate))
                AA = Year(CDate(ArgumentDate))
                Wd = Weekday(CDate(ArgumentDate), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                DateRes = DateSerial(AA, MM, (JJ - (Wd - 1)))
                Return DateStandardVirtualia(DateRes.ToShortDateString)
            End Get
        End Property

        Public ReadOnly Property Semaine_Date_Fin(ByVal NoSemaine As Integer, ByVal DateSource As String) As String
            Get
                Return Semaine_Date(NoSemaine, DateSource, True)
            End Get
        End Property

        Public ReadOnly Property Semaine_Date_Fin(ByVal ArgumentDate As String) As String
            Get
                Dim JJ As Integer
                Dim MM As Integer
                Dim AA As Integer
                Dim DateRes As Date
                Dim Wd As Integer

                If DateSaisieVerifiee(ArgumentDate) = "" Then
                    Return ""
                End If
                JJ = CDate(ArgumentDate).Day
                MM = CDate(ArgumentDate).Month
                AA = CDate(ArgumentDate).Year
                Wd = Weekday(CDate(ArgumentDate), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                DateRes = DateSerial(AA, MM, (JJ + (7 - Wd)))
                Return DateStandardVirtualia(DateRes.ToShortDateString)
            End Get
        End Property

        Public Function CalcAge(ByVal Date1 As String, ByVal Date2 As String, ByVal Precision As Boolean) As String
            ' calcul de l'�ge � partir de la date de naissance
            ' sous la forme JJ/MM/AAAA (string)
            ' resultat sous la forme d'une cha�ne (string)
            Dim JJ As Single
            Dim MM As Single
            Dim SsAa As Single
            Dim Wdn As Double
            Dim Wage As Double
            Dim Wdj As String

            Select Case Date1.Length
                Case Is = 8
                    Date1 = Strings.Left(Date1, 6) & Strings.Format(Siecledelannee(CInt(Strings.Right(Date1, 2))) + CInt(Strings.Right(Date1, 2)), "0000")
            End Select
            Date1 = Date1.TrimStart
            JJ = CInt(Strings.Left(Date1, 2)) : MM = CInt(Strings.Mid(Date1, 4, 2)) : SsAa = CInt(Strings.Right(Date1, 4))

            Wdn = (SsAa * 10000) + ((MM * 100) + JJ)
            Select Case Date2
                Case Is = ""
                    Date2 = DateduJour
            End Select
            Wdj = Strings.Right(Date2, 4) & Strings.Mid(Date2, 4, 2) & Strings.Left(Date2, 2)

            Wage = (CInt(Wdj) - Wdn) / 10000

            Select Case Precision
                Case True
                    Select Case CInt(Strings.Mid(Wdj, 5, 2)) - MM
                        Case Is >= 0
                            Wdn = CInt(Strings.Mid(Wdj, 5, 2)) - MM
                            Return Int(Wage).ToString.Trim & "." & Strings.Format((Wdn / 12) * 100, "00")
                        Case Is < 0
                            Wdn = MM - CInt(Strings.Mid(Wdj, 5, 2))
                            Return Int(Wage).ToString.Trim & "." & Strings.Format((1 - (Wdn / 12)) * 100, "00")
                            Exit Function
                    End Select
            End Select
            Return Int(Wage).ToString.Trim
        End Function

        Public Function CalcDateMoinsDuree(ByVal Date1 As String, ByVal Duree1 As String, ByVal Duree2 As String) As String
            ' Ajouter � une date, sous la forme JJ/MM/AAAA (string),
            ' une duree (AAAAMMJJ) puis retrancher une autre duree (AAAAMMJJ);
            ' Le resultat est une date JJ/MM/AAAA (string).
            ' le calcul est effectue sur les bases suivantes:
            '     une annee = 360 jours, un mois = 30 jours;
            Dim J1 As Single
            Dim M1 As Single
            Dim A1 As Single
            Dim WsDate1 As Single
            Dim Ju1 As Single
            Dim Mu1 As Single
            Dim Au1 As Single
            Dim WsDuree1 As Single
            Dim Ju2 As Single
            Dim Mu2 As Single
            Dim Au2 As Single
            Dim WsDuree2 As Single
            Dim WsQuant As Single
            Dim JJ As Single
            Dim MM As Single
            Dim AA As Single

            Date1 = Date1.TrimStart
            J1 = CInt(Strings.Left(Date1, 2))
            M1 = CInt(Strings.Mid(Date1, 4, 2))
            A1 = CInt(Strings.Right(Date1, 4))

            If J1 = 31 Then J1 = 30
            WsDate1 = ((A1 * 360) + ((M1 * 30) + J1))

            Duree1 = Duree1.TrimStart
            Ju1 = CInt(Strings.Right(Duree1, 2))
            Mu1 = CInt(Strings.Mid(Duree1, 5, 2))
            Au1 = CInt(Strings.Left(Duree1, 4))
            If Ju1 = 31 Then Ju1 = 30
            WsDuree1 = ((Au1 * 360) + ((Mu1 * 30) + Ju1))

            Duree2 = Duree2.TrimStart
            Ju2 = CInt(Strings.Right(Duree2, 2))
            Mu2 = CInt(Strings.Mid(Duree2, 5, 2))
            Au2 = CInt(Strings.Left(Duree2, 4))
            If Ju2 = 31 Then Ju2 = 30
            WsDuree2 = ((Au2 * 360) + ((Mu2 * 30) + Ju2))

            WsQuant = WsDate1 + WsDuree1 - WsDuree2

            Do
                Select Case WsQuant
                    Case Is >= 360
                        AA += 1
                        WsQuant = WsQuant - 360
                    Case Else
                        Exit Do
                End Select
            Loop
            Do
                Select Case WsQuant
                    Case Is >= 30
                        MM += 1
                        WsQuant = WsQuant - 30
                    Case Else
                        Exit Do
                End Select
            Loop
            JJ = WsQuant
            Return Strings.Format(JJ, "00") & VI.Slash & Strings.Format(MM, "00") & VI.Slash & Strings.Format(AA, "0000")
        End Function

        Public Function CalcDateMoinsJour(ByVal Date1 As String, ByVal PlusJour As String, ByVal MoinsJour As String) As String

            ' Ajouter � une date, sous la forme JJ/MM/AAAA (string),
            ' un nombre de jours (9999) puis retrancher un autre
            ' nombre de jours (9999);
            ' Le resultat est une date JJ/MM/AAAA (string).
            ' le calcul est effectue sur les bases suivantes:
            '     une annee = 360 jours, un mois = 30 jours;
            ' Utilise les fonctions : AnneeBissextile et CalcQuantieme.
            Dim Indice As Integer
            Dim WsQuant As Double
            Dim WsQuant1 As Double
            Dim Dsa As Double
            Dim Rsa As Double
            Dim AA As Double
            Dim MM As Double
            Dim JJ As Double
            Dim Bis As Double
            Dim NbJourdumois(12) As Integer

            Select Case PlusJour.Length
                Case 0
                    Return ""
                    Exit Function
            End Select
            Select Case MoinsJour.Length
                Case 0
                    Return ""
                    Exit Function
            End Select

            Date1 = Date1.TrimStart
            WsQuant1 = CalcQuantieme(Date1)

            Select Case CInt(PlusJour)
                Case Is < 0, Is > 9999
                    Return ""
                    Exit Function
            End Select
            Select Case CInt(MoinsJour)
                Case Is < 0, Is > 9999
                    Return ""
                    Exit Function
            End Select

            WsQuant = WsQuant1 + CInt(PlusJour) - CInt(MoinsJour)
            Dsa = Convert.ToInt64(WsQuant) \ 1461
            Rsa = CInt(WsQuant Mod 1461)
            If Rsa = 0 Then
                AA = (Dsa * 4) - 1 : MM = 12 : JJ = 31
                Return Strings.Format(JJ, "00") & VI.Slash & Strings.Format(MM, "00") & VI.Slash & Strings.Format(AA, "0000")
                Exit Function
            End If
            ' incrementation de l'annee
            AA = Dsa * 4
            Bis = 366
            Do
                Select Case Rsa
                    Case Is <= Bis
                        Exit Do
                End Select
                Rsa = Rsa - Bis : AA += 1 : Bis = 365
            Loop
            For Indice = 1 To 12
                Select Case Indice
                    Case 1, 3, 5, 7, 8, 10, 12 : NbJourdumois(Indice) = 31
                    Case 4, 6, 9, 11 : NbJourdumois(Indice) = 30
                    Case Else : NbJourdumois(Indice) = 28
                End Select
            Next Indice
            If AnneeBissextile(CInt(AA)) = True Then NbJourdumois(2) = 29
            ' incrementation du mois
            For Indice = 1 To 12
                MM += 1
                If Rsa <= NbJourdumois(Indice) Then Exit For
                Rsa = Rsa - NbJourdumois(Indice)
            Next Indice
            ' incrementation des jours
            Select Case Rsa
                Case 0
                    JJ = 1
                Case Else
                    JJ = Rsa
            End Select

            ' interpretation du resultat
            Return Strings.Format(JJ, "00") & VI.Slash & Strings.Format(MM, "00") & VI.Slash & Strings.Format(AA, "0000")
        End Function

        Public Function CalcDateSerial(ByVal ArgumentDate As String, ByVal ArgumentPas As Integer) As Integer
            Dim AA As Integer
            Dim MM As Integer
            Dim JJ As Integer
            Dim RelVal As Integer
            AA = 0 : MM = 0 : JJ = 0
            RelVal = 0
            '
            If ArgumentDate = "" Then
                Return 0
                Exit Function
            End If
            JJ = CInt(Strings.Left(ArgumentDate, 2))
            MM = CInt(Strings.Mid(ArgumentDate, 4, 2))
            AA = CInt(Strings.Right(ArgumentDate, 4))
            RelVal = CInt(DateSerial(AA, MM, JJ + ArgumentPas).ToOADate)
            Return RelVal
        End Function

        Public Function CalcHeure(ByVal Param1 As String, ByVal Param2 As String, ByVal Param3 As Integer) As String
            '
            ' Calculs
            '
            ' 1) � partir d'une heure + ou - une duree
            '    Param1 = "HH:MM"
            '    Param2 = "HH h MM"
            '
            ' 2) ajouter ou soustraire des durees
            '    Param1 = "HH h MM"
            '    Param2 = "HH h MM"
            '
            ' Resultat sous la forme "HH:MM" (pour le cas 1)
            ' Resultat sous la forme " HH h MM"   (pour le cas 2 si positif)
            ' Resultat sous la forme "-HH h MM"   (pour le cas 2 si negatif)
            '
            ' Param3 = True  : ajouter
            ' Param3 = False : soustraire
            ' Param3 = 1     : Transformer une duree en minutes
            ' Param3 = 2     : Transformer des minutes en duree
            ' Param3 = 3     : Transformer des minutes en une heure
            ' Param3 = 4     : Calculer un nombre de jours
            '                  en equivalent horaire journalier de reference
            '                  � partir de deux durees en minutes
            '                  Param1 = Valeur � transformer en equivalent jours
            '                  Param2 = Horaire journalier de reference en minutes
            '
            Dim WModeCalcul As Integer
            WModeCalcul = 0
            Dim WHDebut As String
            WHDebut = ""
            Dim WHFin As String
            WHFin = ""
            Dim WMinDebut As Integer
            WMinDebut = 0
            Dim WMinFin As Integer
            WMinFin = 0
            Dim WDiffMin As Integer
            WDiffMin = 0
            Dim WNbH As Integer
            WNbH = 0
            Dim WNbJ As Integer
            WNbJ = 0
            Dim WNbM As Integer
            WNbM = 0
            Dim WDecim As Double
            WDecim = 0
            Dim WWHeure As String
            WWHeure = "00"
            Dim WWMinute As String
            WWMinute = "00"
            Dim WWJour As String
            WWJour = ""
            Dim WWFraction As String
            WWFraction = ""
            Dim P As Integer
            P = 0
            Dim L As Integer
            L = 0
            Dim WSigne As String
            WSigne = ""
            '
            Select Case Strings.InStr(Param1, ":")
                Case Is > 0
                    If VerifHeure(Param2, False) = "" Then
                        Return ""
                        Exit Function
                    Else
                        Return VerifHeure(Param1, True)
                        WModeCalcul = 1
                        Exit Function
                    End If
                Case Else
                    Select Case Strings.InStr(Param1, "h")
                        Case Is > 0
                            Select Case Param3
                                Case 1
                                    If VerifHeure(Param1, False) = "" Then
                                        Return ""
                                        Exit Function
                                    End If
                                    WModeCalcul = 3
                                Case Else
                                    If VerifHeure(Param2, False) = "" Then
                                        Return ""
                                        Exit Function
                                    End If
                                    WModeCalcul = 2
                            End Select
                        Case Else
                            If Param1 <> "" Then
                                Select Case Param3
                                    Case 2
                                        WModeCalcul = 4
                                    Case 3
                                        WModeCalcul = 5
                                    Case 4
                                        If Param2 <> "" Then
                                            WModeCalcul = 6
                                            If (CInt(Param2) = 0) Or (CInt(Param1) = 0) Then
                                                Return ""
                                                Exit Function
                                            End If
                                        Else
                                            Return ""
                                            Exit Function
                                        End If
                                    Case Else
                                        Return ""
                                        Exit Function
                                End Select
                            Else
                                Return ""
                                Exit Function
                            End If
                    End Select
            End Select
            '
            Select Case WModeCalcul
                '
                Case 1 'Ajouter ou soustraire une duree � une heure
                    '
                    WHDebut = VerifHeure(Param1, True)
                    WHFin = VerifHeure(Param2, False)
                    If (WHDebut = "") Or (WHFin = "") Then
                        Return ""
                        Exit Function
                    End If
                    '
                    L = CInt(Strings.InStr(WHFin, "h"))
                    '
                    WMinDebut = (CInt(Strings.Left(WHDebut, 2)) * 60) + CInt(Strings.Right(WHDebut, 2))
                    WMinFin = (CInt(Strings.Left(WHFin, L - 2)) * 60) + CInt(Strings.Right(WHFin, WHFin.Length - (L + 1)))
                    '
                    Select Case Param3
                        Case 1 ' ajouter mais pas au del� d'une journee complete
                            WDiffMin = WMinFin + WMinDebut
                            If WDiffMin > 1439 Then
                                WDiffMin = 1439
                            End If
                        Case 0 ' soustraire
                            WDiffMin = WMinDebut - WMinFin
                            Select Case WDiffMin
                                Case Is < 0
                                    WDiffMin = 0
                            End Select
                    End Select
                    '
                    ' Mise en forme du resultat
                    '
                    Do
                        Select Case WDiffMin
                            Case Is > 59
                                WDiffMin = WDiffMin - 60
                                WNbH += 1
                            Case Else
                                WNbM = WDiffMin
                                Exit Do
                        End Select
                    Loop
                    '
                    WWHeure = Strings.Format(WNbH, "00")
                    WWMinute = Strings.Format(WNbM, "00")
                    '
                    Return WWHeure & ":" & WWMinute
                    '
                Case 2 'Additions ou soustractions de durees
                    '
                    WHDebut = VerifHeure(Param1, False)
                    WHFin = VerifHeure(Param2, False)
                    If (WHDebut = "") Or (WHFin = "") Then
                        Return ""
                        Exit Function
                    End If
                    '
                    P = CInt(Strings.InStr(WHDebut, "h"))
                    L = CInt(Strings.InStr(WHFin, "h"))
                    '
                    WMinDebut = (CInt(Strings.Left(WHDebut, P - 2)) * 60) + CInt(Strings.Right(WHDebut, WHDebut.Length - (P + 1)))
                    WMinFin = (CInt(Strings.Left(WHFin, L - 2)) * 60) + CInt(Strings.Right(WHFin, WHFin.Length - (L + 1)))
                    '
                    Select Case Param3
                        Case 1 ' ajouter
                            WDiffMin = WMinFin + WMinDebut
                        Case 0 ' soustraire
                            WDiffMin = WMinDebut - WMinFin
                            Select Case WDiffMin
                                Case Is >= 0
                                    WSigne = ""
                                Case Is < 0
                                    WSigne = "-"
                                    WDiffMin = -WDiffMin
                            End Select
                    End Select
                    '
                    ' Mise en forme du resultat
                    '
                    Do
                        Select Case WDiffMin
                            Case Is > 59
                                WDiffMin = WDiffMin - 60
                                WNbH += 1
                            Case Else
                                WNbM = WDiffMin
                                Exit Do
                        End Select
                    Loop
                    '
                    WWMinute = Strings.Format(WNbM, "00")
                    '
                    Return WSigne & WNbH.ToString.Trim & " h " & WWMinute
                    '
                Case 3 ' Transformer une duree en minutes
                    '
                    WHDebut = VerifHeure(Param1, False)
                    If WHDebut = "" Then
                        Return ""
                        Exit Function
                    End If
                    '
                    P = CInt(Strings.InStr(WHDebut, "h"))
                    '
                    WMinDebut = (CInt(Strings.Left(WHDebut, P - 2)) * 60) + CInt(Strings.Right(WHDebut, WHDebut.Length - (P + 1)))
                    '
                    Return WMinDebut.ToString.Trim
                    '
                Case 4 ' Transformer des minutes en duree
                    '
                    WMinDebut = CInt(Param1)
                    WDiffMin = WMinDebut
                    '
                    ' Mise en forme du resultat
                    '
                    Do
                        Select Case WDiffMin
                            Case Is > 59
                                WDiffMin = WDiffMin - 60
                                WNbH += 1
                            Case Else
                                WNbM = WDiffMin
                                Exit Do
                        End Select
                    Loop
                    '
                    WWMinute = Strings.Format(WNbM, "00")
                    '
                    Return WNbH.ToString.Trim & " h " & WWMinute
                    '
                Case 5 ' Transformer des minutes en une heure
                    '
                    WMinDebut = CInt(Param1)
                    WDiffMin = WMinDebut
                    '
                    ' Mise en forme du resultat
                    '
                    Do
                        Select Case WDiffMin
                            Case Is > 59
                                WDiffMin = WDiffMin - 60
                                WNbH += 1
                            Case Else
                                WNbM = WDiffMin
                                Exit Do
                        End Select
                    Loop
                    '
                    WWHeure = Strings.Format(WNbH, "00")
                    WWMinute = Strings.Format(WNbM, "00")
                    '
                    Return WWHeure & ":" & WWMinute
                    '
                Case 6 'Nombre de jours de reference � partir de 2 durees en minutes
                    '
                    WMinDebut = CInt(Param1)
                    WMinFin = CInt(Param2)
                    '
                    Do
                        Select Case WMinDebut
                            Case Is > (WMinFin - 1)
                                WMinDebut = WMinDebut - WMinFin
                                WNbJ += 1
                            Case Else
                                WNbM = WMinDebut
                                Exit Do
                        End Select
                    Loop
                    '
                    ' Mise en forme du resultat
                    '
                    WWJour = WNbJ.ToString.Trim
                    If WNbM > 0 Then
                        WDecim = (WNbM / WMinFin) * 100
                        WWFraction = "," & Int(WDecim).ToString.Trim
                        If Int(WDecim) < 10 Then
                            WWFraction = "," & "0" & Int(WDecim).ToString.Trim
                        End If
                        If Int(WDecim).ToString.Trim = "0" Then
                            WWFraction = ""
                        End If
                    Else
                        WWFraction = ""
                    End If
                    '
                    Return WWJour & WWFraction
                    '
                Case Else
                    Return ""
            End Select
        End Function

        Public Function CalcJour(ByVal Date1 As String, ByVal Date2 As String) As String
            Dim firstDate As Date
            Dim secondDate As Date
            Dim Result As Long
            Try
                firstDate = CDate(Date1)
                secondDate = CDate(Date2)
                Result = DateDiff(DateInterval.Day, firstDate, secondDate) + 1
                Return Result.ToString
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Function CalcQuantieme(ByVal ArgumentDate As String) As Integer
            ' calcul du nombre de jours d'une date;
            ' le calcul tient compte des mois � 28,29,30 et 31 jours
            ' le resultat est un nombre de jour
            ' Utilise la fonction : AnneeBissextile.

            Dim Indice As Integer
            Dim TotalNbJour As Integer
            Dim Annee As Integer
            Dim Mois As Integer
            Dim Jour As Integer
            Dim Bissextile As Boolean
            Dim Reste As Integer
            Dim NbJourAnnee As Integer
            Dim NbJourdumois(12) As Integer

            For Indice = 1 To 12
                Select Case Indice
                    Case 1, 3, 5, 7, 8, 10, 12 : NbJourdumois(Indice) = 31
                    Case 4, 6, 9, 11 : NbJourdumois(Indice) = 30
                    Case Else : NbJourdumois(Indice) = 28
                End Select
            Next Indice
            TotalNbJour = 0
            Select Case ArgumentDate
                Case Is = ""
                    CalcQuantieme = 0
                    Exit Function
            End Select
            Try
                Annee = Year(DateValue(ArgumentDate))
                Mois = Month(DateValue(ArgumentDate))
                Jour = Day(DateValue(ArgumentDate))
            Catch
                Annee = Year(DateValue(DateStandardVirtualia(ArgumentDate)))
                Mois = Month(DateValue(DateStandardVirtualia(ArgumentDate)))
                Jour = Day(DateValue(DateStandardVirtualia(ArgumentDate)))
            End Try

            Bissextile = AnneeBissextile(Annee)
            Reste = CInt(Annee Mod 4)
            TotalNbJour = (Annee \ 4) * 1461
            Select Case Reste
                Case Is > 0
                    TotalNbJour = TotalNbJour + 366
                    For Indice = 2 To Reste
                        TotalNbJour = TotalNbJour + 365
                    Next
            End Select
            If Bissextile = True Then
                NbJourdumois(2) = 29
            End If
            NbJourAnnee = 0
            Select Case Mois
                Case Is > 1
                    For Indice = 1 To Mois - 1
                        NbJourAnnee = NbJourAnnee + NbJourdumois(Indice)
                    Next Indice
            End Select
            NbJourAnnee = NbJourAnnee + Jour
            Return TotalNbJour + NbJourAnnee
        End Function

        Public Function SiChevauchementDates(ByVal Date1Debut As String, ByVal Date1Fin As String, ByVal Date2Debut As String, ByVal Date2Fin As String) As Boolean
            Dim DebutDebut As Integer
            Dim DebutFin As Integer
            Dim FinDebut As Integer
            Dim FinFin As Integer

            If (Date1Debut = "") Or (Date1Fin = "") Or (Date2Debut = "") Or (Date2Fin = "") Then
                Return False
            End If
            DebutDebut = ComparerDates(Date1Debut, Date2Debut)
            DebutFin = ComparerDates(Date1Debut, Date2Fin)
            FinDebut = ComparerDates(Date1Fin, Date2Debut)
            FinFin = ComparerDates(Date1Fin, Date2Fin)
            If (DebutDebut = VI.ComparaisonDates.PlusGrand) And (DebutFin = VI.ComparaisonDates.PlusPetit) Then
                Return True
            ElseIf (FinDebut = VI.ComparaisonDates.PlusGrand) And (FinFin = VI.ComparaisonDates.PlusPetit) Then
                Return True
            ElseIf (DebutDebut = VI.ComparaisonDates.PlusPetit) And (FinFin = VI.ComparaisonDates.PlusGrand) Then
                Return True
            End If
            Return False
        End Function

        Public Function SiChevauchementHoraire(ByVal DebutPlage1 As String, ByVal FinPlage1 As String, ByVal DebutPlage2 As String, ByVal FinPlage2 As String) As Boolean
            Dim WMode As Integer
            WMode = 0
            '
            ' DebutPlage2 et FinPlage2 sont obligatoire , ainsi que DebutPlage1
            '
            If DebutPlage2 <> "" Then
                DebutPlage2 = VerifHeure(DebutPlage2, True)
            End If
            If FinPlage2 <> "" Then
                FinPlage2 = VerifHeure(FinPlage2, True)
            End If
            If (DebutPlage2 = "") Or (FinPlage2 = "") Then
                Return False
            End If
            '
            If DebutPlage1 <> "" Then
                DebutPlage1 = VerifHeure(DebutPlage1, True)
            End If
            If FinPlage1 <> "" Then
                FinPlage1 = VerifHeure(FinPlage1, True)
            End If
            Select Case DebutPlage1
                Case ""
                    Return False
                Case Else
                    Select Case FinPlage1
                        Case ""
                            WMode = 1
                        Case Else
                            WMode = 2
                    End Select
            End Select
            '
            Select Case WMode
                Case 1 'Comparaison d'un horaire avec une plage horaire
                    Select Case DebutPlage1
                        Case Is >= DebutPlage2
                            Select Case DebutPlage1
                                Case Is <= FinPlage2
                                    Return True
                            End Select
                    End Select
                Case 2 'Comparaison de deux plages horaire
                    If (FinPlage1 < DebutPlage2) Or (DebutPlage1 > FinPlage2) Then
                        Return False
                    Else
                        Return True
                    End If
            End Select
            Return False
        End Function

        Public Function ComparerDates(ByVal Premiere As String, ByVal Deuxieme As String) As Integer
            '***  Cette fonction compare deux dates et renvoie un code retour
            '     indiquant si il y a Egalite ou difference, plus petite
            '     ou plus grande, entre celles-ci
            '                                                            ****
            Dim TestPremiere As String
            Dim TestFirst As String
            Dim TestDeuxieme As String
            Dim TestSecond As String

            Select Case Deuxieme
                Case Is = ""
                    Select Case Premiere.Length
                        Case Is > 10
                            Deuxieme = DateduJour & Format(System.DateTime.Now, "t")
                        Case Else
                            Deuxieme = DateduJour
                    End Select
            End Select
            TestPremiere = Strings.Mid(Premiere, 7, 4) & Strings.Mid(Premiere, 4, 2) & Strings.Left(Premiere, 2)
            TestDeuxieme = Strings.Mid(Deuxieme, 7, 4) & Strings.Mid(Deuxieme, 4, 2) & Strings.Left(Deuxieme, 2)
            Select Case TestPremiere
                Case Is = TestDeuxieme
                    ComparerDates = VI.ComparaisonDates.Egalite
                    Select Case Premiere.Length
                        Case Is > 10
                            TestFirst = Strings.Mid(Premiere, 11, 2) & Strings.Right(Premiere, 2)
                            TestSecond = Strings.Mid(Deuxieme, 11, 2) & Strings.Right(Deuxieme, 2)
                            Select Case TestFirst
                                Case Is = TestSecond
                                    Return VI.ComparaisonDates.Egalite
                                Case Is > TestSecond
                                    Return VI.ComparaisonDates.PlusGrand
                                Case Is < TestSecond
                                    Return VI.ComparaisonDates.PlusPetit
                            End Select
                    End Select
                Case Is > TestDeuxieme
                    Return VI.ComparaisonDates.PlusGrand
                Case Else
                    Return VI.ComparaisonDates.PlusPetit
            End Select
        End Function

        Public Function ComparerHeures(ByVal Premiere As String, ByVal Deuxieme As String) As Integer
            '***  Cette fonction compare deux heures et renvoie un code retour
            '     indiquant si il y a Egalite ou difference, plus petite
            '     ou plus grande, entre celles-ci
            Dim PremiereHH As String
            Dim PremiereMM As String
            Dim PremiereSS As String
            Dim DeuxiemeHH As String
            Dim DeuxiemeMM As String
            Dim DeuxiemeSS As String
            Dim PremiereTest As String
            Dim DeuxiemeTest As String

            If Deuxieme = "" Then Deuxieme = Format(System.DateTime.Now, "T")
            PremiereHH = Strings.Left(Premiere, 2) : PremiereMM = Strings.Mid(Premiere, 4, 2) : PremiereSS = Strings.Right(Premiere, 2)
            PremiereTest = PremiereHH & PremiereMM & PremiereSS
            DeuxiemeHH = Strings.Left(Deuxieme, 2)
            DeuxiemeMM = Strings.Mid(Deuxieme, 4, 2)
            DeuxiemeSS = Strings.Right(Deuxieme, 2)
            DeuxiemeTest = DeuxiemeHH & DeuxiemeMM & DeuxiemeSS
            If PremiereTest = DeuxiemeTest Then
                Return VI.ComparaisonDates.Egalite
            ElseIf PremiereTest > DeuxiemeTest Then
                Return VI.ComparaisonDates.PlusGrand
            Else
                Return VI.ComparaisonDates.PlusPetit
            End If
        End Function

        Public Function DureeDates365(ByVal Date1 As String, ByVal Date2 As String) As String
            ' calcul de la duree entre deux dates JJ/MM/AAAA (string),
            ' le calcul tient compte des mois � 28,29,30 et 31 jours.
            ' le calcul est effectue sur les bases suivantes:
            '     une annee = 365 jours, un mois = 30 jours;
            ' le resultat est sous la forme SSAAMMJJ (string).
            ' Utilise la fonction : CalcQuantieme.
            Dim JJ As Single
            Dim MM As Single
            Dim AA As Single
            Dim WsQuant As Single
            Dim WsQuant1 As Single
            Dim WsQuant2 As Single

            Date1 = Date1.Trim
            Date2 = Date2.Trim

            WsQuant1 = CalcQuantieme(Date1)
            WsQuant2 = CalcQuantieme(Date2)
            Select Case WsQuant1
                Case Is > WsQuant2
                    WsQuant = WsQuant1 - WsQuant2
                Case Else
                    WsQuant = WsQuant2 - WsQuant1
            End Select
            Do
                If WsQuant >= 365 Then
                    AA += 1
                    WsQuant = WsQuant - 365
                Else
                    Exit Do
                End If
            Loop
            Do
                If WsQuant >= 30 Then
                    MM += 1
                    WsQuant = WsQuant - 30
                Else
                    Exit Do
                End If
            Loop
            JJ = WsQuant
            Return Strings.Format((AA * 10000) + ((MM * 100) + JJ), "00000000")
        End Function

        Public Function DureeHeure(ByVal HeureDebut As String, ByVal HeureFin As String) As String
            ' Calcul de la duree en heures et minutes
            ' entre deux heures
            ' fournies sous la forme suivante : HH:MM
            '
            ' resultat sous la forme HH h MM
            ' HH compris entre 0 et 23
            ' MM compris entre 0 et 59
            '
            ' Si HeureDebut > HeureFin : HeureFin concerne le lendemain
            '
            '
            Dim WHDebut As String
            WHDebut = ""
            Dim WHFin As String
            WHFin = ""
            Dim WMinDebut As Integer
            WMinDebut = 0
            Dim WMinFin As Integer
            WMinFin = 0
            Dim WDiffMin As Integer
            WDiffMin = 0
            Dim WNbH As Integer
            WNbH = 0
            Dim WNbM As Integer
            WNbM = 0
            Dim WWHeure As String
            WWHeure = "00"
            Dim WWMinute As String
            WWMinute = "00"
            '
            DureeHeure = "00 h 00"
            '
            WHDebut = VerifHeure(HeureDebut, True)
            WHFin = VerifHeure(HeureFin, True)
            If (WHDebut = "") Or (WHFin = "") Then
                Exit Function
            End If
            '
            WMinDebut = (CInt(Strings.Left(WHDebut, 2)) * 60) + CInt(Strings.Right(WHDebut, 2))
            WMinFin = (CInt(Strings.Left(WHFin, 2)) * 60) + CInt(Strings.Right(WHFin, 2))
            '
            Select Case WMinDebut
                Case Is < WMinFin
                    WDiffMin = WMinFin - WMinDebut
                Case Is > WMinFin
                    WDiffMin = 1440 - (WMinDebut - WMinFin)
                Case Is = WMinFin
                    Exit Function
            End Select
            '
            ' Mise en forme du resultat
            '
            Do
                Select Case WDiffMin
                    Case Is > 59
                        WDiffMin = WDiffMin - 60
                        WNbH += 1
                    Case Else
                        WNbM = WDiffMin
                        Exit Do
                End Select
            Loop
            '
            WWHeure = Strings.Format(WNbH, "00")
            WWMinute = Strings.Format(WNbM, "00")
            '
            Return WNbH.ToString.Trim & " h " & WWMinute
        End Function

        Public Function DureePlus(ByVal Duree1 As String, ByVal Duree2 As String) As String
            ' Additionne � une duree initiale, une autre duree,
            ' toutes deux exprimees sous la forme SSAAMMJJ (string).
            ' Le resultat est sous la forme SSAAMMJJ (string).
            Dim J1 As Integer
            Dim M1 As Integer
            Dim A1 As Integer
            Dim J2 As Integer
            Dim M2 As Integer
            Dim A2 As Integer
            Dim JJ As Integer
            Dim MM As Integer
            Dim AA As Integer

            Duree1 = Duree1.Trim
            Duree2 = Duree2.Trim
            If Duree1 = "" Or Duree2 = "" Then
                Return "00000000"
                Exit Function
            End If
            If Duree1.Length < 8 And Duree1.Length > 0 Then
                Duree1 = Strings.Format(Val(Duree1), "00000000")
            End If
            If Duree2.Length < 8 And Duree2.Length > 0 Then
                Duree2 = Strings.Format(Val(Duree2), "00000000")
            End If
            Try
                J1 = CInt(Strings.Right(Duree1, 2))
                M1 = CInt(Strings.Mid(Duree1, 5, 2))
                A1 = CInt(Strings.Left(Duree1, 4))

                J2 = CInt(Strings.Right(Duree2, 2))
                M2 = CInt(Strings.Mid(Duree2, 5, 2))
                A2 = CInt(Strings.Left(Duree2, 4))

                JJ = J1 + J2 : MM = M1 + M2 : AA = A1 + A2

                If JJ >= 30 Then JJ = JJ - 30 : MM += 1
                If MM >= 12 Then MM = MM - 12 : AA += 1
                Return Strings.Format((AA * 10000) + ((MM * 100) + JJ), "00000000")
            Catch ex As Exception
                Return "00000000"
            End Try
        End Function

        Public Function JourDuree(ByVal NbrJours As Integer) As String
            Dim Resultat As Integer
            Dim NbAa As Integer
            Dim NbrJAa As Integer
            Dim ResteJour As Integer
            Dim NbMm As Integer
            Dim NbrJMm As Integer
            JourDuree = "000000"
            If NbrJours = 0 Then Exit Function
            '
            Resultat = NbrJours
            '
            NbAa = Resultat \ 360
            NbrJAa = NbAa * 360
            ResteJour = Resultat - NbrJAa
            '
            NbMm = ResteJour \ 30
            NbrJMm = NbMm * 30
            ResteJour = ResteJour - NbrJMm
            '
            Return Strings.Format(NbAa, "00") & Strings.Format(NbMm, "00") & Strings.Format(ResteJour, "00")
        End Function

        Public Function MiseEnFormeDate(ByVal InDate As String, ByVal InFormat As String, ByVal Sens As Boolean) As String
            Dim JJ As String
            Dim MM As String
            Dim AA As String
            Dim SS As String

            JJ = ""
            MM = ""
            AA = ""
            SS = ""
            InDate = InDate.Trim
            Select Case InDate
                Case ""
                    Return ""
                    Exit Function
            End Select
            Select Case Sens
                Case False 'Export
                    JJ = Strings.Left(InDate, 2)
                    MM = Strings.Mid(InDate, 4, 2)
                    AA = Strings.Right(InDate, 2)
                    SS = Strings.Mid(InDate, 7, 2)
                    Select Case InFormat
                        Case "jj/mm/ssaa"
                            Return JJ & VI.Slash & MM & VI.Slash & SS & AA
                        Case "jj/mm/aa"
                            Return JJ & VI.Slash & MM & VI.Slash & AA
                        Case "aammjj"
                            Return AA & MM & JJ
                        Case "ssaammjj"
                            Return SS & AA & MM & JJ
                        Case "mm-dd-yyyy", "mm-dd-ssaa"
                            Return MM & "-" & JJ & "-" & SS & AA
                        Case "mm-dd-yy"
                            Return MM & "-" & JJ & "-" & AA
                        Case "jjmmssaa"
                            Return JJ & MM & SS & AA
                        Case Is = "ssaa-mm-jj"
                            Return SS & AA & "-" & MM & "-" & JJ
                    End Select
                Case True 'Import
                    Select Case InFormat
                        Case "jj/mm/ssaa"
                            JJ = Strings.Left(InDate, 2)
                            MM = Strings.Mid(InDate, 4, 2)
                            AA = Strings.Right(InDate, 2)
                            SS = Strings.Mid(InDate, 7, 2)
                        Case "mm-dd-yyyy", "mm-dd-ssaa"
                            MM = Strings.Left(InDate, 2)
                            JJ = Strings.Mid(InDate, 4, 2)
                            AA = Strings.Right(InDate, 2)
                            SS = Strings.Mid(InDate, 7, 2)
                        Case "jj/mm/aa"
                            JJ = Strings.Left(InDate, 2)
                            MM = Strings.Mid(InDate, 4, 2)
                            AA = Strings.Right(InDate, 2)
                            Select Case AA
                                Case Is > CStr(30)
                                    SS = "19"
                                Case Else
                                    SS = Strings.Left(CStr(Year(System.DateTime.Now)), 2)
                            End Select
                        Case "mm-dd-yy"
                            MM = Strings.Left(InDate, 2)
                            JJ = Strings.Mid(InDate, 4, 2)
                            AA = Strings.Right(InDate, 2)
                            Select Case AA
                                Case Is > CStr(30)
                                    SS = "19"
                                Case Else
                                    SS = Strings.Left(CStr(Year(System.DateTime.Now)), 2)
                            End Select
                        Case "aammjj"
                            JJ = Strings.Right(InDate, 2)
                            MM = Strings.Mid(InDate, 3, 2)
                            AA = Strings.Left(InDate, 2)
                            Select Case AA
                                Case Is > CStr(30)
                                    SS = "19"
                                Case Else
                                    SS = Strings.Left(CStr(Year(System.DateTime.Now)), 2)
                            End Select
                        Case "ssaammjj"
                            Select Case InDate.Length
                                Case 8
                                    JJ = Strings.Right(InDate, 2)
                                    MM = Strings.Mid(InDate, 5, 2)
                                    AA = Strings.Mid(InDate, 3, 2)
                                    SS = Strings.Left(InDate, 2)
                                Case 14 'As/400 format condense
                                    JJ = Strings.Right(InDate, 2)
                                    If Strings.Left(JJ, 1) = Strings.Space(1) Then JJ = "0" & Strings.Right(JJ, 1)
                                    MM = Strings.Mid(InDate, 9, 2)
                                    If Strings.Left(MM, 1) = Strings.Space(1) Then MM = "0" & Strings.Right(MM, 1)
                                    AA = Strings.Mid(InDate, 5, 2)
                                    SS = Strings.Left(InDate, 2)
                            End Select
                        Case "ssaa--mm--jj"
                            JJ = Strings.Right(InDate, 2)
                            MM = Strings.Mid(InDate, 7, 2)
                            AA = Strings.Mid(InDate, 3, 2)
                            SS = Strings.Left(InDate, 2)
                        Case "jjmmssaa"
                            JJ = Strings.Left(InDate, 2)
                            MM = Strings.Mid(InDate, 3, 2)
                            SS = Strings.Mid(InDate, 5, 2)
                            AA = Strings.Right(InDate, 2)
                    End Select
                    If JJ = "00" And MM = "00" And AA = "00" And SS = "00" Then
                        Return ""
                        Exit Function
                    End If
                    If JJ < "01" Or JJ > "31" Then JJ = "01"
                    If MM < "01" Or MM > "12" Then MM = "01"
                    Select Case CInt(SS)
                        Case 0
                            Return ""
                        Case Else
                            Return JJ & VI.Slash & MM & VI.Slash & SS & AA
                            Exit Function
                    End Select
            End Select
            Return ""
        End Function

        Public Function ProrataDuree(ByVal Duree As String, ByVal Pourcent As String) As String
            Dim JJ As Integer = 0
            Dim MM As Integer = 0
            Dim AA As Integer = 0
            Dim NbjDuree As Long
            Dim Resultat As Long
            Dim ResteJour As Long
            Dim Pourcentage As Integer
            Dim NbAa As Long
            Dim NbMm As Long
            Dim NbrJAa As Long
            Dim NbrJMm As Long

            If Pourcent = "100" Then
                Return Duree
            End If
            If Not (IsNumeric(Pourcent)) Then
                Return Duree
            End If
            '
            If IsNumeric(Strings.Right(Duree, 2)) Then
                JJ = CInt(Strings.Right(Duree, 2))
            End If
            If IsNumeric(Strings.Mid(Duree, 5, 2)) Then
                MM = CInt(Strings.Mid(Duree, 5, 2))
            End If
            If IsNumeric(Strings.Mid(Duree, 3, 2)) Then
                AA = CInt(Strings.Mid(Duree, 3, 2))
            End If
            '
            NbjDuree = (AA * 360) + (MM * 30) + JJ
            Pourcentage = CInt(Pourcent.Trim)
            Resultat = (NbjDuree * Pourcentage) \ 100
            '
            NbAa = Resultat \ 360
            NbrJAa = NbAa * 360
            ResteJour = Resultat - NbrJAa
            '
            NbMm = ResteJour \ 30
            NbrJMm = NbMm * 30
            ResteJour -= NbrJMm
            '
            Return "00" & Strings.Format(NbAa, "00") & Strings.Format(NbMm, "00") & Strings.Format(ResteJour, "00")
        End Function

        Public Function VerifHeure(ByVal ArgumentHeure As String, ByVal SiModeHeure As Boolean) As String
            ' Verification de la validite d'un format heure (Mode = True)
            ' fourni sous une des formes suivantes :
            '    HH?MM ou HHMM ou H ou HH,  heures et minutes
            '
            ' resultat sous la forme HH:MM
            ' HH compris entre 0 et 24
            ' MM compris entre 0 et 59
            '
            ' Verification de la validite d'un format duree (Mode = False)
            ' fourni sous une des formes suivantes :
            '    HH MM   ou HHhMM heures et minutes
            '
            ' resultat sous la forme HH h MM
            ' HH compris entre 0 et 2028
            ' MM compris entre 0 et 59
            '
            Dim WHeure As Integer = 0
            Dim WMinute As Integer = 0
            Dim WWHeure As String = "00"
            Dim WWMinute As String = "00"
            Dim WCaractere As String = ""
            Dim I As Integer = 0
            Dim P As Integer = 0
            Dim IndicNegatif As String = ""

            If ArgumentHeure Is Nothing Then
                Return ""
            End If
            Try
                Select Case SiModeHeure
                    Case True ' Heure
                        '
                        If ArgumentHeure.Length = 5 Then
                            WHeure = CInt(Strings.Left(ArgumentHeure, 2))
                            WMinute = CInt(Strings.Right(ArgumentHeure, 2))
                        Else
                            If ArgumentHeure.Length = 4 Then
                                WHeure = CInt(Strings.Left(ArgumentHeure, 2))
                                WMinute = CInt(Strings.Right(ArgumentHeure, 2))
                            Else
                                If (ArgumentHeure.Length = 2) Or (ArgumentHeure.Length = 1) Then
                                    WHeure = CInt(ArgumentHeure)
                                    WMinute = 0
                                Else
                                    Return ""
                                End If
                            End If
                        End If
                        '
                        For I = 1 To ArgumentHeure.Length
                            If Not ((ArgumentHeure.Length = 5) And (I = 3)) Then
                                WCaractere = Strings.Mid(ArgumentHeure, I, 1)
                                If (WCaractere < "0" Or WCaractere > "9") Then
                                    Return ""
                                End If
                            End If
                        Next I
                        '
                        If WHeure < 0 Or WHeure > 24 Then
                            Return ""
                        End If
                        '
                        If WMinute < 0 Or WMinute > 59 Then
                            Return ""
                        End If
                        '
                        WWHeure = Strings.Format(WHeure, "00")
                        WWMinute = Strings.Format(WMinute, "00")
                        '
                        Return WWHeure & ":" & WWMinute
                        '
                    Case False ' Duree
                        '
                        If Strings.Left(ArgumentHeure, 1) = "-" Then
                            IndicNegatif = "-"
                            ArgumentHeure = Strings.Right(ArgumentHeure, ArgumentHeure.Length - 1)
                        End If
                        If Strings.InStr(ArgumentHeure, "h") > 1 Then
                            P = Strings.InStr(ArgumentHeure, "h")
                            WHeure = CInt(Strings.Left(ArgumentHeure, P - 1))
                            WMinute = CInt(Strings.Right(ArgumentHeure, ArgumentHeure.Length - P))
                        Else
                            If Strings.InStr(ArgumentHeure, " ") > 1 Then
                                P = Strings.InStr(ArgumentHeure, " ")
                                WHeure = CInt(Strings.Left(ArgumentHeure, P - 1))
                                WMinute = CInt(Strings.Right(ArgumentHeure, ArgumentHeure.Length - P))
                            Else
                                WHeure = CInt(ArgumentHeure)
                                WMinute = 0
                            End If
                        End If
                        '
                        For I = 1 To ArgumentHeure.Length
                            WCaractere = Strings.Mid(ArgumentHeure, I, 1)
                            If (WCaractere < "0" Or WCaractere > "9") Then
                                If Not ((WCaractere = " ") Or (WCaractere = "h")) Then
                                    Return ""
                                End If
                            End If
                        Next I
                        '
                        If WHeure < 0 Or WHeure > 2029 Then
                            Return ""
                        End If
                        '
                        If WMinute < 0 Or WMinute > 59 Then
                            Return ""
                        End If
                        '
                        WWMinute = Strings.Format(WMinute, "00")
                        '
                        If IndicNegatif = "-" Then
                            Return "-" & WHeure.ToString.Trim & " h " & WWMinute
                        Else
                            Return WHeure.ToString.Trim & " h " & WWMinute
                        End If
                End Select
            Catch ex As Exception
                Return ""
            End Try
            Return ""
        End Function

        Public Function DureeDates360(ByVal DateDebut As String, ByVal DateFin As String) As String
            'V2.0  Toute la fonction
            ' calcul de la duree entre deux dates de format JJ/MM/AAAA (string),
            ' le calcul est effectue sur les bases suivantes:
            '     une annee = 360 jours, un mois = 30 jours
            '     en date anniversaire
            ' le resultat est sous la forme SSAAMMJJ (string)
            ' Utilise la fonction CalDateAvancement
            '
            Dim WDate1 As String
            Dim WDate2 As String
            Dim WEcart As Integer
            Dim WEcartDate1 As Integer
            Dim WEcartDate2 As Integer
            Dim WDureeCalculee As Double
            Dim WDureeFormatee As String
            Dim WAA As Double
            Dim WMM As Double
            Dim WJJ As Double
            Dim WDateAvancement As String
            Dim WDateAnniversaire As String
            DureeDates360 = "00000000"
            If DateDebut = "" Or DateFin = "" Then Exit Function
            Select Case ComparerDates(DateDebut, DateFin)
                Case VI.ComparaisonDates.PlusGrand, VI.ComparaisonDates.Egalite
                    Exit Function
            End Select
            '
            WDate1 = "01/" & Strings.Right(DateDebut, 7)
            WEcartDate1 = 0
            Select Case CInt(Strings.Left(DateDebut, 2))
                Case 1 To 30
                    WEcartDate1 = CInt(Strings.Left(DateDebut, 2))
                Case 31
                    WEcartDate1 = 30
            End Select
            Select Case Strings.Left(DateDebut, 5)
                Case "28/02", "29/02"
                    WEcartDate1 = 30
            End Select
            '
            WDate2 = "01/" & Strings.Right(DateFin, 7)
            WEcartDate2 = 0
            Select Case CInt(Strings.Left(DateFin, 2))
                Case 1 To 30
                    WEcartDate2 = CInt(Strings.Left(DateFin, 2))
                Case 31
                    WEcartDate2 = 30
            End Select
            Select Case Strings.Left(DateFin, 5)
                Case "28/02", "29/02"
                    WEcartDate2 = 30
            End Select
            '
            WAA = 0
            WMM = 0
            WDateAnniversaire = WDate1
            '
            Do
                WDateAvancement = CalculerDateAvancement(WDateAnniversaire, "000100", "000000", "000000") '1 mois
                Select Case ComparerDates(WDateAvancement, WDate2)
                    Case VI.ComparaisonDates.PlusGrand
                        Exit Do
                    Case Else
                        WDateAnniversaire = WDateAvancement
                End Select
                WMM += 1
                Select Case WMM
                    Case Is > 11
                        WAA += 1
                        WMM = 0
                End Select
            Loop
            '
            WEcart = WEcartDate2 - WEcartDate1
            WJJ = 0
            Select Case WEcart
                Case 0 To 29
                    WJJ = WEcart
                Case Is > 29
                    WJJ = 0
                    WMM += 1
                    Select Case WMM
                        Case Is > 11
                            WAA += 1
                            WMM = 0
                    End Select
                Case Is < 0
                    WMM = WMM - 1
                    Select Case WMM
                        Case Is < 0
                            WAA = WAA - 1
                            Select Case WAA
                                Case Is < 0
                                    WAA = 0
                            End Select
                            WMM = 11
                    End Select
                    WJJ = 30 + WEcart
            End Select
            '
            WDureeCalculee = ((WAA * 10000) + ((WMM * 100) + WJJ))
            WDureeFormatee = Strings.Format(WDureeCalculee, "00000000")
            DureeDates360 = WDureeFormatee
            ''
        End Function

        Public Function CalculerDateAvancement(ByVal DateEchelon As String, ByVal DureeEchelon As String, ByVal DureeInterr As String, ByVal DureeReliq As String) As String
            ' Calcul de la date d'avancement d'echelon  (JJ/MM/SSAA) ou (JJMMSSAA)
            ' � partir de : 1 - la date du dernier avancement d'echelon - DateEchelon (JJ/MM/SSAA)
            '               2 - la duree dans l'echelon  - DureeEchelon (AAMMJJ)
            '               3 - la duree des interruptions de carriere - DureeInterr (AAMMJJ)
            '               4 - la duree des reliquats d'anciennete - DureeReliq (AAMMJJ)
            ' bases de calcul : . 1 unite MM = 30 unites JJ , 1 unite AA = 12 unites MM
            '                   . S'il n'y a pas d'avancement : CalculerDateAvancement = ""
            ' 2 methodes de calcul
            '    1 - en annees/mois/jours
            '    2 - en jours calendaires
            '
            Dim DEAA As Integer
            Dim DEMM As Integer
            Dim DEJJ As Integer
            Dim DIAA As Integer
            Dim DIMM As Integer
            Dim DIJJ As Integer
            Dim DRAA As Integer
            Dim DRMM As Integer
            Dim DRJJ As Integer
            Dim DUAA As Integer
            Dim DUMM As Integer
            Dim DUJJ As Integer
            Dim DTESSAA As Integer
            Dim DTEMM As Integer
            Dim DTEJJ As Integer
            Dim DAVSSAA As Integer
            Dim DAVMM As Integer
            Dim DAVJJ As Integer
            Dim MMDAV As String
            Dim JJDAV As String
            Dim MaxJJ As Integer
            Dim DifJJ As Integer
            Dim DateAvancement As String

            ' *************************** Contr�le des parametres ********************
            Select Case DateEchelon
                Case Is = ""
                    Return ""
            End Select
            Select Case DureeEchelon.Length
                Case Is <> 6
                    DureeEchelon = "000000"
            End Select
            Select Case DureeInterr
                Case Is = ""
                    DureeInterr = "000000"
                Case Else
                    Select Case DureeInterr.Length
                        Case Is <> 6
                            DureeInterr = "000000"
                    End Select
            End Select
            Select Case DureeReliq
                Case Is = ""
                    DureeReliq = "000000"
                Case Else
                    Select Case DureeReliq.Length
                        Case Is <> 6
                            DureeReliq = "000000"
                    End Select
            End Select
            '********************  Initialisations *********************
            DEAA = CInt(Strings.Left(DureeEchelon, 2))
            DEMM = CInt(Strings.Mid(DureeEchelon, 3, 2))
            DEJJ = CInt(Strings.Right(DureeEchelon, 2))

            DIAA = CInt(Strings.Left(DureeInterr, 2))
            DIMM = CInt(Strings.Mid(DureeInterr, 3, 2))
            DIJJ = CInt(Strings.Right(DureeInterr, 2))

            DRAA = CInt(Strings.Left(DureeReliq, 2))
            DRMM = CInt(Strings.Mid(DureeReliq, 3, 2))
            DRJJ = CInt(Strings.Right(DureeReliq, 2))

            DateAvancement = ""
            ' A - Calcul de la duree resultante (AAMMJJ) = DureeEchelon + DureeInterr - DureeReliq
            DUAA = DEAA + DIAA - DRAA
            DUMM = DEMM + DIMM - DRMM
            DUJJ = DEJJ + DIJJ - DRJJ
            Do
                Select Case DUJJ
                    Case Is > 29
                        DUJJ = DUJJ - 30
                        DUMM += 1
                    Case Else
                        If DUJJ >= 0 And DUJJ < 30 Then
                            Exit Do
                        End If
                        If DUJJ < 0 And DUJJ > -30 Then
                            DUJJ = DUJJ + 31
                            DUMM = DUMM - 1
                            Exit Do
                        End If
                        Select Case DUJJ
                            Case Is < -29
                                DUJJ = DUJJ + 31
                                DUMM = DUMM - 1
                        End Select
                End Select
            Loop
            Do
                Select Case DUMM
                    Case Is > 11
                        DUMM = DUMM - 12
                        DUAA += 1
                    Case Else
                        If DUMM >= 0 And DUMM < 12 Then
                            Exit Do
                        End If
                        If DUMM < 0 And DUMM > -12 Then
                            DUMM += 12
                            DUAA -= 1
                            Exit Do
                        End If
                        Select Case DUMM
                            Case Is < -11
                                DUMM += 12
                                DUAA -= 1
                        End Select
                End Select
            Loop
            Select Case DureeEchelon
                Case Is <> "000000"
                    Select Case DUAA
                        Case Is < 0
                            Return ""
                            Exit Function
                    End Select
            End Select
            '*************** B - Calcul de la date d'avancement (JJ/MM/SSAA) = DateEchelon + Duree Resultante
            DTESSAA = CInt(Strings.Right(DateEchelon, 4))
            DTEMM = CInt(Strings.Mid(DateEchelon, 4, 2))
            DTEJJ = CInt(Strings.Left(DateEchelon, 2))

            DAVSSAA = DTESSAA + DUAA

            DAVMM = DTEMM + DUMM
            Do
                Select Case DAVMM
                    Case Is > 12
                        DAVMM = DAVMM - 12
                        DAVSSAA += 1
                    Case Else
                        Exit Do
                End Select
            Loop

            DAVJJ = DTEJJ + DUJJ
            Do
                Select Case DAVMM
                    Case 1, 3, 5, 7, 8, 10, 12
                        MaxJJ = 31
                    Case 4, 6, 9, 11
                        MaxJJ = 30
                    Case 2
                        Select Case AnneeBissextile(DAVSSAA)
                            Case True
                                MaxJJ = 29
                            Case False
                                MaxJJ = 28
                        End Select
                End Select
                Select Case DAVJJ
                    Case Is > MaxJJ
                        Select Case DAVJJ
                            Case Is < 32
                                Select Case DUJJ
                                    Case Is <> 0
                                        DAVJJ = DAVJJ - MaxJJ
                                        DAVMM += 1
                                        Exit Do
                                    Case Else
                                        DAVJJ = MaxJJ
                                        Exit Do
                                End Select
                            Case Else
                                DifJJ = 31 - MaxJJ
                                DAVJJ = DAVJJ - MaxJJ - DifJJ
                                DAVMM += 1
                        End Select
                        Select Case DAVMM
                            Case Is > 12
                                DAVMM = DAVMM - 12
                                DAVSSAA += 1
                        End Select
                    Case Else
                        Exit Do
                End Select
            Loop

            MMDAV = Str(DAVMM)
            JJDAV = Str(DAVJJ)
            Select Case DAVMM
                Case Is < 10
                    Mid(MMDAV, 1, 1) = "0"
            End Select
            Select Case DAVJJ
                Case Is < 10
                    Mid(JJDAV, 1, 1) = "0"
            End Select

            DateAvancement = JJDAV.TrimStart & VI.Slash & MMDAV.TrimStart & VI.Slash & DAVSSAA.ToString.Trim

            Select Case ComparerDates(DateEchelon, DateAvancement)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                    Return DateAvancement
                Case Else
                    Select Case DureeEchelon
                        Case Is = "000000"
                            Return DateAvancement
                    End Select
            End Select
            Return ""
        End Function

        Public Function ConvHeuresJour(ByVal NombreHeures As Double, ByVal RegleConversion As Double, ByVal UniteMinimale As Double) As Double
            Dim Resultat As Double

            ConvHeuresJour = 0
            Select Case NombreHeures
                Case Is <= 0
                    Exit Function
            End Select
            Select Case RegleConversion
                Case Is <= 0
                    Exit Function
            End Select
            Resultat = NombreHeures / RegleConversion
            Select Case UniteMinimale
                Case Is >= 0
                    Select Case NombreHeures
                        Case Is < (RegleConversion / 2)
                            Select Case NombreHeures
                                Case Is > 0
                                    Resultat = UniteMinimale
                            End Select
                    End Select
            End Select
            ConvHeuresJour = Resultat
        End Function

        Public ReadOnly Property ConvHeuresEnCentieme(ByVal Valeur As String) As String
            Get
                Dim Pos As Integer
                Dim HH As Integer
                Dim Mn As Single
                Dim Chaine As String

                Chaine = Valeur.Trim
                If Chaine = "" Then
                    Return ""
                End If
                Pos = Strings.InStr(Chaine, "h")
                If Pos = 0 Then
                    Pos = Strings.InStr(Chaine, ":")
                End If
                If Pos = 0 Then
                    Return ""
                End If
                If IsNumeric(Strings.Left(Chaine, Pos - 1).Trim) Then
                    HH = CInt(Strings.Left(Chaine, Pos - 1).Trim)
                End If
                If IsNumeric(Strings.Left(Chaine, Pos - 1).Trim) Then
                    Mn = CSng(CInt(Strings.Right(Chaine, 2).Trim) * (100 / 60) * 10)
                End If
                Chaine = HH.ToString & "." & CInt(Mn).ToString
                Return Strings.Format(Val(Chaine), "0.00")
            End Get
        End Property

        Public Function ConvertCentiemeEnHeureMinute(ByVal NbCentiemes As Double) As String
            If NbCentiemes = 0 Then
                Return ""
            End If
            Dim NbMinutes As Double = NbCentiemes * 60
            Dim NBHeures As Long = CInt(NbMinutes) \ 60
            NbMinutes -= NBHeures * 60
            Return NBHeures & " h " & Strings.Format(NbMinutes, "00")
        End Function

        Public ReadOnly Property HeureJour(Optional ByVal SiSeconde As Boolean = False) As String
            Get
                Dim Chaine As String = Strings.Format(Hour(System.DateTime.Now), "00") & ":"
                Chaine &= Strings.Format(Minute(System.DateTime.Now), "00")
                If SiSeconde = True Then
                    Chaine &= ":" & Strings.Format(Second(System.DateTime.Now), "00")
                End If
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property HeureTypee(ByVal ArgumentDate As String, ByVal ArgumentHeure As String) As System.DateTime
            Get
                Dim TableauW(0) As String
                Dim DateValeur As Date = DateTypee(ArgumentDate)
                Dim Heure As Integer
                Dim Mn As Integer
                If ArgumentHeure = "" Then
                    Return New System.DateTime(DateValeur.Year, DateValeur.Month, DateValeur.Day, 0, 0, 0)
                End If
                Try
                    TableauW = Strings.Split(ArgumentHeure, ":", -1)
                    Heure = CInt(TableauW(0))
                    If Heure > 23 Then
                        Heure = 0
                        DateValeur = DateTypee(ArgumentDate).AddDays(1)
                    End If
                    Mn = CInt(TableauW(1))
                    If Mn > 60 Then
                        Mn = 0
                    End If
                    Return New System.DateTime(DateValeur.Year, DateValeur.Month, DateValeur.Day, Heure, Mn, 0)
                Catch ex As Exception
                    Return New System.DateTime(DateValeur.Year, DateValeur.Month, DateValeur.Day, 0, 0, 0)
                End Try
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            LibelleMois(1) = "Janvier"
            LibelleMois(2) = "F�vrier"
            LibelleMois(3) = "Mars"
            LibelleMois(4) = "Avril"
            LibelleMois(5) = "Mai"
            LibelleMois(6) = "Juin"
            LibelleMois(7) = "Juillet"
            LibelleMois(8) = "Ao�t"
            LibelleMois(9) = "Septembre"
            LibelleMois(10) = "Octobre"
            LibelleMois(11) = "Novembre"
            LibelleMois(12) = "D�cembre"

            LibelleJour(1) = "Lundi"
            LibelleJour(2) = "Mardi"
            LibelleJour(3) = "Mercredi"
            LibelleJour(4) = "Jeudi"
            LibelleJour(5) = "Vendredi"
            LibelleJour(6) = "Samedi"
            LibelleJour(7) = "Dimanche"
            '
            WsSeparateurDate = VI.Slash
            WsJourOuvrable = VI.Jours.Samedi
            WsJourRepos = VI.Jours.Dimanche
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            Erase LibelleMois
            Erase LibelleJour
        End Sub

    End Class
End Namespace

