﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Module ModuleShema
    Private WsRhDate As Virtualia.Systeme.Fonctions.CalculDates = Nothing
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales = Nothing
    Private WsRhAnnexe12 As Virtualia.TablesObjet.ShemaDGFIP.DicoAnnexe12

    Public ReadOnly Property VirRhDate() As Virtualia.Systeme.Fonctions.CalculDates
        Get
            If WsRhDate Is Nothing Then
                WsRhDate = New Virtualia.Systeme.Fonctions.CalculDates
            End If
            Return WsRhDate
        End Get
    End Property

    Public ReadOnly Property VirRhFonction() As Virtualia.Systeme.Fonctions.Generales
        Get
            If WsRhFonction Is Nothing Then
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            End If
            Return WsRhFonction
        End Get
    End Property

    Public ReadOnly Property VirRhAnnexe12 As Virtualia.TablesObjet.ShemaDGFIP.DicoAnnexe12
        Get
            If WsRhAnnexe12 Is Nothing Then
                WsRhAnnexe12 = New Virtualia.TablesObjet.ShemaDGFIP.DicoAnnexe12
            End If
            Return WsRhAnnexe12
        End Get
    End Property

    Public Function MiseEnFormeAnciennete(ByVal Methode As Integer, ByVal Chaine As String) As String
        Dim AnsDur As String
        Dim MoisDur As String
        Dim JoursDur As String
        Dim Libelle As String
        Dim NombreAns As Double

        Select Case Methode
            Case VI.OptionInfo.DicoCmc, VI.OptionInfo.DicoStats, VI.OptionInfo.DicoRecherche, VI.OptionInfo.DicoImpression
                Select Case Chaine.Length
                    Case Is = 6
                        Chaine = "00" & Chaine
                End Select
            Case Else
                Return Chaine
                Exit Function
        End Select
        Select Case Chaine
            Case Is = "00000000"
                Return ""
                Exit Function
        End Select
        Select Case Methode
            Case VI.OptionInfo.DicoRecherche, VI.OptionInfo.DicoImpression
                AnsDur = ""
                Select Case Strings.Mid(Chaine, 3, 2)
                    Case Is <> "00"
                        Select Case Val(Strings.Mid(Chaine, 3, 2))
                            Case 0, 1
                                Libelle = Strings.Space(1) & "an" & Strings.Space(1)
                            Case Else
                                Libelle = Strings.Space(1) & "ans" & Strings.Space(1)
                        End Select
                        AnsDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 3, 2)))) & Libelle
                End Select
                MoisDur = ""
                Select Case Strings.Mid(Chaine, 5, 2)
                    Case Is <> "00"
                        MoisDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 5, 2)))) & Strings.Space(1) & "mois" & Strings.Space(1)
                End Select
                JoursDur = ""
                Select Case Strings.Mid(Chaine, 7, 2)
                    Case Is <> "00"
                        JoursDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 7, 2)))) & Strings.Space(1) & "jours" & Strings.Space(1)
                End Select
                Return AnsDur & MoisDur & JoursDur
                Exit Function
            Case VI.OptionInfo.DicoCmc, VI.OptionInfo.DicoStats
                AnsDur = ""
                Select Case Strings.Mid(Chaine, 3, 2)
                    Case Is <> "00"
                        AnsDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 3, 2))))
                End Select
                MoisDur = ""
                Select Case Strings.Mid(Chaine, 5, 2)
                    Case Is <> "00"
                        MoisDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 5, 2))))
                End Select
                JoursDur = ""
                Select Case Strings.Mid(Chaine, 7, 2)
                    Case Is <> "00"
                        JoursDur = Strings.LTrim(Str(Val(Strings.Mid(Chaine, 7, 2))))
                End Select
                NombreAns = Val(AnsDur) + (((Val(MoisDur) * 30) + Val(JoursDur)) / 365)
                Return Strings.Format(NombreAns, "###.0")
                Exit Function
        End Select
        Return Chaine
    End Function

    Public ReadOnly Property ConversionLettreIban(ByVal Caractere As String) As String
        Get
            Select Case Caractere
                Case "A"
                    Return "10"
                Case "B"
                    Return "11"
                Case "C"
                    Return "12"
                Case "D"
                    Return "13"
                Case "E"
                    Return "14"
                Case "F"
                    Return "15"
                Case "G"
                    Return "16"
                Case "H"
                    Return "17"
                Case "I"
                    Return "18"
                Case "J"
                    Return "19"
                Case "K"
                    Return "20"
                Case "L"
                    Return "21"
                Case "M"
                    Return "22"
                Case "N"
                    Return "23"
                Case "O"
                    Return "24"
                Case "P"
                    Return "25"
                Case "Q"
                    Return "26"
                Case "R"
                    Return "27"
                Case "S"
                    Return "28"
                Case "T"
                    Return "29"
                Case "U"
                    Return "30"
                Case "V"
                    Return "31"
                Case "W"
                    Return "32"
                Case "X"
                    Return "33"
                Case "Y"
                    Return "34"
                Case "Z"
                    Return "35"
                Case Else
                    Return Caractere
            End Select
        End Get
    End Property

End Module