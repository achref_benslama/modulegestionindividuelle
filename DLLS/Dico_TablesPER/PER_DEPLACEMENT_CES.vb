﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DEPLACEMENT_CES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Voyage_Aller As String
        Private WsDate_Voyage_Retour As String
        Private WsTransport As String
        Private WsDate_Emission As String
        Private WsParcours As String
        Private WsReferenceFacture As String
        Private WsDate_Facture As String
        Private WsReferenceRemboursement As String
        Private WsMontant As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DEPLACEMENT_CES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaFrais 'Spécifique Conseil Economique et Social
            End Get
        End Property

        Public Property Date_Aller_Voyage() As String
            Get
                Return WsDate_Voyage_Aller
            End Get
            Set(ByVal value As String)
                WsDate_Voyage_Aller = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Retour_Voyage() As String
            Get
                Return WsDate_Voyage_Retour
            End Get
            Set(ByVal value As String)
                WsDate_Voyage_Retour = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Transport() As String
            Get
                Return WsTransport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsTransport = value
                    Case Else
                        WsTransport = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Date_Emission() As String
            Get
                Return WsDate_Emission
            End Get
            Set(ByVal value As String)
                WsDate_Emission = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Parcours() As String
            Get
                Return WsParcours
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsParcours = value
                    Case Else
                        WsParcours = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Reference_Facture() As String
            Get
                Return WsReferenceFacture
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsReferenceFacture = value
                    Case Else
                        WsReferenceFacture = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Date_Facture() As String
            Get
                Return WsDate_Facture
            End Get
            Set(ByVal value As String)
                WsDate_Facture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference_Remboursement() As String
            Get
                Return WsReferenceRemboursement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsReferenceRemboursement = value
                    Case Else
                        WsReferenceRemboursement = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Date_Aller_Voyage & VI.Tild)
                Chaine.Append(Date_Retour_Voyage & VI.Tild)
                Chaine.Append(Transport & VI.Tild)
                Chaine.Append(Date_Emission & VI.Tild)
                Chaine.Append(Parcours & VI.Tild)
                Chaine.Append(Reference_Facture & VI.Tild)
                Chaine.Append(Date_Facture & VI.Tild)
                Chaine.Append(Reference_Facture & VI.Tild)
                Select Case Montant
                    Case Is = 0
                        Chaine.Append("0")
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant.ToString))
                End Select

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Date_Aller_Voyage = TableauData(2)
                Date_Retour_Voyage = TableauData(3)
                Transport = TableauData(4)
                Date_Emission = TableauData(5)
                Parcours = TableauData(6)
                Reference_Facture = TableauData(7)
                Date_Facture = TableauData(8)
                Reference_Remboursement = TableauData(9)
                If TableauData(10) = "" Then
                    TableauData(10) = "0"
                End If
                TableauData(10) = VirRhFonction.MontantStocke(TableauData(10))
                Montant = VirRhFonction.ConversionDouble(TableauData(10))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
