﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetLOLF
        Implements IGestionVirFiche(Of LOLF_MISSION)

        Private WsListeMission As List(Of LOLF_MISSION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesMissions() As List(Of LOLF_MISSION)
            Get
                If WsListeMission Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of LOLF_MISSION) = Nothing
                LstRes = (From instance In WsListeMission Select instance _
                                               Order By instance.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeMissions() As Integer Implements IGestionVirFiche(Of LOLF_MISSION).NombredItems
            Get
                Return WsListeMission.Count
            End Get
        End Property

        Public Function Libelles_AE_UO(ByVal IdentificationAE As String, ByVal IdentificationUO As String) As ArrayList

            Dim RefMission As LOLF_MISSION
            Dim RefProgramme As LOLF_PROGRAMME
            Dim RefAction As LOLF_ACTION
            Dim LstAE As List(Of LOLF_AUTORISATION_EMPLOI)
            Dim RefBOP As LOLF_BOP
            Dim LstUO As List(Of LOLF_UO)
            Dim IndiceF As Integer
            Dim TableauIdent(0) As String
            Dim IdeMission As Integer
            Dim NumeroPgm As Integer
            Dim IdentAction As String = ""
            Dim IdeAE As Integer
            Dim IdeBOP As Integer
            Dim IdeUO As Integer

            TableauIdent = Strings.Split(IdentificationAE, "-", -1)
            If TableauIdent.Count >= 4 Then
                If IsNumeric(TableauIdent(0)) Then
                    IdeMission = CInt(TableauIdent(0))
                End If
                If IsNumeric(TableauIdent(1)) Then
                    NumeroPgm = CInt(TableauIdent(1))
                End If
                Select Case TableauIdent.Count
                    Case Is = 4
                        IdentAction = TableauIdent(2)
                        If IsNumeric(TableauIdent(3)) Then
                            IdeAE = CInt(TableauIdent(3))
                        End If
                    Case Else
                        IdentAction = TableauIdent(2) & "-" & TableauIdent(3)
                        If IsNumeric(TableauIdent(4)) Then
                            IdeAE = CInt(TableauIdent(4))
                        End If
                End Select
            End If
            TableauIdent = Strings.Split(IdentificationUO, "-", -1)
            Select Case TableauIdent.Count
                Case Is = 4
                    If IsNumeric(TableauIdent(2)) Then
                        IdeBOP = CInt(TableauIdent(2))
                    End If
                    If IsNumeric(TableauIdent(3)) Then
                        IdeUO = CInt(TableauIdent(3))
                    End If
            End Select

            RefMission = Fiche_Mission(IdeMission)
            If RefMission Is Nothing Then
                Return Nothing
            End If
            Dim TabLibelles As New ArrayList

            TabLibelles.Add(RefMission.Intitule)
            RefProgramme = RefMission.Fiche_Programme(NumeroPgm)
            If RefProgramme Is Nothing Then
                Return TabLibelles
            End If
            TabLibelles.Add(RefProgramme.Intitule)

            RefAction = RefProgramme.Fiche_Action(NumeroPgm, IdentAction)
            If RefAction Is Nothing Then
                Return TabLibelles
            End If
            TabLibelles.Add(RefAction.Intitule)

            LstAE = RefAction.ListedesAutorisations("Numero")
            If LstAE Is Nothing Then
                Return TabLibelles
            End If
            For IndiceF = 0 To LstAE.Count - 1
                If LstAE.Item(IndiceF).NumeroduProgramme = NumeroPgm _
                    And LstAE.Item(IndiceF).NumerodelAction = IdentAction _
                    And LstAE.Item(IndiceF).NumerodelAutorisation = IdeAE Then
                    TabLibelles.Add(LstAE.Item(IndiceF).CategorieFonctionnelle)
                    Exit For
                End If
            Next IndiceF

            RefBOP = RefProgramme.Fiche_BOP(NumeroPgm, IdeBOP)
            If RefBOP Is Nothing Then
                Return TabLibelles
            End If
            TabLibelles.Add(RefBOP.Intitule)

            LstUO = RefBOP.ListedesUOs("Numero")
            If LstUO Is Nothing Then
                Return TabLibelles
            End If
            For IndiceF = 0 To LstUO.Count - 1
                If LstUO.Item(IndiceF).NumeroduProgramme = NumeroPgm _
                    And LstUO.Item(IndiceF).NumeroduBOP = IdeBOP _
                    And LstUO.Item(IndiceF).NumerodelUO = IdeUO Then
                    TabLibelles.Add(LstUO.Item(IndiceF).Intitule)
                    Exit For
                End If
            Next IndiceF

            Return TabLibelles

        End Function

        Public Function Fiche_Mission(ByVal IdeDossier As Integer) As LOLF_MISSION Implements IGestionVirFiche(Of LOLF_MISSION).GetFiche
            Return WsListeMission.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Mission(ByVal Intitule As String) As LOLF_MISSION Implements IGestionVirFiche(Of LOLF_MISSION).GetFiche
            Return WsListeMission.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of LOLF_MISSION).Actualiser
            WsListeMission.Clear()
            WsListeMission = Nothing
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeMission.Clear()
            Dim FicheMission As LOLF_MISSION
            Dim FicheProgramme As LOLF_PROGRAMME
            Dim FicheAction As LOLF_ACTION
            Dim FicheBOP As LOLF_BOP

            WsListeMission.AddRange(CreationCollectionVIRFICHE(Of LOLF_MISSION).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeMission.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les programmes
            Dim lstprg As List(Of LOLF_PROGRAMME) = CreationCollectionVIRFICHE(Of LOLF_PROGRAMME).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstprg.ForEach(Sub(f)
                               FicheMission = Fiche_Mission(f.Ide_Dossier)

                               If (FicheMission Is Nothing) Then
                                   Return
                               End If

                               FicheMission.Ajouter_Programme(f)
                           End Sub)

            '** 3  Liste de toutes les actions et sous-actions
            Dim lstact As List(Of LOLF_ACTION) = CreationCollectionVIRFICHE(Of LOLF_ACTION).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstact.ForEach(Sub(f)
                               FicheMission = Fiche_Mission(f.Ide_Dossier)
                               If (FicheMission Is Nothing) Then
                                   Return
                               End If

                               FicheProgramme = FicheMission.Fiche_Programme(f.NumeroduProgramme)
                               If (FicheProgramme Is Nothing) Then
                                   Return
                               End If

                               FicheProgramme.Ajouter_Action(f)
                           End Sub)

            '** 4  Liste de toutes les Autorisations d'emploi
            Dim lstautor As List(Of LOLF_AUTORISATION_EMPLOI) = CreationCollectionVIRFICHE(Of LOLF_AUTORISATION_EMPLOI).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstautor.ForEach(Sub(f)
                                 FicheMission = Fiche_Mission(f.Ide_Dossier)
                                 If (FicheMission Is Nothing) Then
                                     Return
                                 End If

                                 FicheProgramme = FicheMission.Fiche_Programme(f.NumeroduProgramme)
                                 If (FicheProgramme Is Nothing) Then
                                     Return
                                 End If

                                 FicheAction = FicheProgramme.Fiche_Action(f.NumeroduProgramme, f.NumerodelAction)
                                 If (FicheAction Is Nothing) Then
                                     Return
                                 End If

                                 FicheAction.Ajouter_Autorisation(f)
                             End Sub)

            '** 5  Liste de tous les BOP
            Dim lstBOP As List(Of LOLF_BOP) = CreationCollectionVIRFICHE(Of LOLF_BOP).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstBOP.ForEach(Sub(f)
                               FicheMission = Fiche_Mission(f.Ide_Dossier)
                               If FicheMission Is Nothing Then
                                   Return
                               End If

                               FicheProgramme = FicheMission.Fiche_Programme(f.NumeroduProgramme)
                               If FicheProgramme Is Nothing Then
                                   Return
                               End If

                               FicheProgramme.Ajouter_BOP(f)
                           End Sub)

            '** 6  Liste de toutes les Unités Opérationnelles
            Dim lstUO As List(Of LOLF_UO) = CreationCollectionVIRFICHE(Of LOLF_UO).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstUO.ForEach(Sub(f)
                              FicheMission = Fiche_Mission(f.Ide_Dossier)
                              If FicheMission Is Nothing Then
                                  Return
                              End If

                              FicheProgramme = FicheMission.Fiche_Programme(f.NumeroduProgramme)
                              If FicheProgramme Is Nothing Then
                                  Return
                              End If

                              FicheBOP = FicheProgramme.Fiche_BOP(f.NumeroduProgramme, f.NumeroduBOP)
                              If FicheBOP Is Nothing Then
                                  Return
                              End If

                              FicheBOP.Ajouter_UO(f)

                          End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeMission = New List(Of LOLF_MISSION)
            ConstituerListe()
        End Sub

    End Class
End Namespace
