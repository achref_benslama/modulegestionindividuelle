﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ACT_FONCTIONS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsNatureFonction As String
        Private WsDureeMandat As Integer
        Private WsNombreMandat As Integer
        Private WsSiRenouvelable As String
        Private WsPresence As String
        Private WsChargeTravail As String
        Private WsRemuneration As String
        Private WsDescriptif As String
        Private WsMesures As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ACT_FONCTIONS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueCommission
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Nature() As String
            Get
                Return WsNatureFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureFonction = value
                    Case Else
                        WsNatureFonction = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNatureFonction
            End Set
        End Property

        Public Property Duree_Mandat() As Integer
            Get
                Return WsDureeMandat
            End Get
            Set(ByVal value As Integer)
                WsDureeMandat = value
            End Set
        End Property

        Public Property Nombre_Mandat() As Integer
            Get
                Return WsNombreMandat
            End Get
            Set(ByVal value As Integer)
                WsNombreMandat = value
            End Set
        End Property

        Public Property SiRenouvelable() As String
            Get
                Return WsSiRenouvelable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiRenouvelable = value
                    Case Else
                        WsSiRenouvelable = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Presence() As String
            Get
                Return WsPresence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPresence = value
                    Case Else
                        WsPresence = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property ChargeTravail_Estime() As String
            Get
                Return WsChargeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsChargeTravail = value
                    Case Else
                        WsChargeTravail = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Remuneration() As String
            Get
                Return WsRemuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsRemuneration = value
                    Case Else
                        WsRemuneration = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Mesures_Nominatives() As String
            Get
                Return WsMesures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMesures = value
                    Case Else
                        WsMesures = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Nature & VI.Tild)
                Chaine.Append(Duree_Mandat.ToString & VI.Tild)
                Chaine.Append(Nombre_Mandat.ToString & VI.Tild)
                Chaine.Append(SiRenouvelable & VI.Tild)
                Chaine.Append(Presence & VI.Tild)
                Chaine.Append(ChargeTravail_Estime & VI.Tild)
                Chaine.Append(Remuneration & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Mesures_Nominatives)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Rang = CInt(TableauData(1))
                Nature = TableauData(2)
                If TableauData(3) <> "" Then Duree_Mandat = CInt(TableauData(3))
                If TableauData(4) <> "" Then Nombre_Mandat = CInt(TableauData(4))
                SiRenouvelable = TableauData(5)
                Presence = TableauData(6)
                ChargeTravail_Estime = TableauData(7)
                Remuneration = TableauData(8)
                Descriptif = TableauData(9)
                Mesures_Nominatives = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub
    End Class
End Namespace


