﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EXT_PERSONNES
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNom As String
        Private WsPrenom As String
        Private WsDateNaissance As String
        Private WsAffectation As String
        Private WsEtablissement As String
        Private WsFonctionExercee As String
        Private WsResidenceAdministrative As String
        Private WsNumeroetRue As String
        Private WsComplementAdresse As String
        Private WsCodePostal As String
        Private WsVille As String
        Private WsCodeBanque As String
        Private WsCodeGuichet As String
        Private WsNumeroCompte As String
        Private WsCleRIB As String
        Private WsDomiciliationBancaire As String
        Private WsNoTelephone As String
        Private WsEmail As String
        Private WsCodeCompable As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EXT_PERSONNES"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePerExterne
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNom = value.ToUpper
                    Case Else
                        WsNom = Strings.Left(value.ToUpper, 35)
                End Select
                MyBase.Clef = WsNom
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Date_de_naissance() As String
            Get
                Return WsDateNaissance
            End Get
            Set(ByVal value As String)
                WsDateNaissance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Fonctionnel_Affectation() As String
            Get
                Return WsAffectation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsAffectation = value
                    Case Else
                        WsAffectation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Fonctionnel_Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Fonctionnel_FonctionExercee() As String
            Get
                Return WsFonctionExercee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonctionExercee = value
                    Case Else
                        WsFonctionExercee = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Fonctionnel_Residence() As String
            Get
                Return WsResidenceAdministrative
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsResidenceAdministrative = value
                    Case Else
                        WsResidenceAdministrative = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Domicile_Numero_NomdelaRue() As String
            Get
                Return WsNumeroetRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumeroetRue = value
                    Case Else
                        WsNumeroetRue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Domicile_Complement_Adresse() As String
            Get
                Return WsComplementAdresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsComplementAdresse = value
                    Case Else
                        WsComplementAdresse = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Domicile_CodePostal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Domicile_BureauDistributeur() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Banque_Code() As String
            Get
                If WsCodeBanque <> "" Then
                    Return Strings.Format(CInt(WsCodeBanque), "00000")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeBanque = value
                    Case Else
                        WsCodeBanque = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Banque_Code_Guichet() As String
            Get
                If WsCodeGuichet <> "" Then
                    Return Strings.Format(CInt(WsCodeGuichet), "00000")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeGuichet = value
                    Case Else
                        WsCodeGuichet = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Banque_NumeroduCompte() As String
            Get
                Return WsNumeroCompte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 11
                        WsNumeroCompte = value
                    Case Else
                        WsNumeroCompte = Strings.Left(value, 11)
                End Select
            End Set
        End Property

        Public Property Banque_CleRIB() As String
            Get
                If WsCleRIB <> "" Then
                    Return Strings.Format(CInt(Val(WsCleRIB)), "00")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCleRIB = value
                    Case Else
                        WsCleRIB = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Banque_Domiciliation() As String
            Get
                Return WsDomiciliationBancaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsDomiciliationBancaire = value
                    Case Else
                        WsDomiciliationBancaire = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsNoTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNoTelephone = value
                    Case Else
                        WsNoTelephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property AdresseEmail() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Code_Comptable() As String
            Get
                Return WsCodeCompable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeCompable = value
                    Case Else
                        WsCodeCompable = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Date_de_naissance & VI.Tild)
                Chaine.Append(Fonctionnel_Affectation & VI.Tild)
                Chaine.Append(Fonctionnel_Etablissement & VI.Tild)
                Chaine.Append(Fonctionnel_FonctionExercee & VI.Tild)
                Chaine.Append(Fonctionnel_Residence & VI.Tild)
                Chaine.Append(Domicile_Numero_NomdelaRue & VI.Tild)
                Chaine.Append(Domicile_Complement_Adresse & VI.Tild)
                Chaine.Append(Domicile_CodePostal & VI.Tild)
                Chaine.Append(Domicile_BureauDistributeur & VI.Tild)
                Chaine.Append(Banque_Code & VI.Tild)
                Chaine.Append(Banque_Code_Guichet & VI.Tild)
                Chaine.Append(Banque_NumeroduCompte & VI.Tild)
                Chaine.Append(Banque_CleRIB & VI.Tild)
                Chaine.Append(Banque_Domiciliation & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(AdresseEmail & VI.Tild)
                Chaine.Append(Code_Comptable)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                Prenom = TableauData(2)
                Date_de_naissance = TableauData(3)
                Fonctionnel_Affectation = TableauData(4)
                Fonctionnel_Etablissement = TableauData(5)
                Fonctionnel_FonctionExercee = TableauData(6)
                Fonctionnel_Residence = TableauData(7)
                Domicile_Numero_NomdelaRue = TableauData(8)
                Domicile_Complement_Adresse = TableauData(9)
                Domicile_CodePostal = TableauData(10)
                Domicile_BureauDistributeur = TableauData(11)
                Banque_Code = TableauData(12)
                Banque_Code_Guichet = TableauData(13)
                Banque_NumeroduCompte = TableauData(14)
                Banque_CleRIB = TableauData(15)
                Banque_Domiciliation = TableauData(16)
                Telephone = TableauData(17)
                AdresseEmail = TableauData(18)
                Code_Comptable = TableauData(19)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
