﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheWebFenetre
        Private WsIdentifiant As Integer
        Private WsSiReadOnly As Boolean = False

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property SiReadOnly As Boolean
            Get
                Return WsSiReadOnly
            End Get
            Set(value As Boolean)
                WsSiReadOnly = value
            End Set
        End Property
    End Class
End Namespace