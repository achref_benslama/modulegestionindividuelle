﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CET_HEBDO
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDateDebutSemaine As String
        Private WsDateFinSemaine As String
        Private WsNumSemaine As Integer
        Private WsHorairePrevu As String
        Private WsHoraireComptabilise As String
        Private WsCET_Total As String
        Private WsCET_MontantAcquis As Double = 0
        Private WsCET_MontantPaye As Double = 0
        Private WsCET_MontantNonPaye As Double = 0
        Private WsCET_HeuresNonPayees As String
        Private WsCET_TauxHoraire As Double = 0
        Private WsCET_HeuresAcquises As String
        Private WsCET_HeuresPrises As String
        Private WsCET_HeuresGain As String
        Private WsLundi_Matin As String
        Private WsLundi_ApresMidi As String
        Private WsMardi_Matin As String
        Private WsMardi_ApresMidi As String
        Private WsMercredi_Matin As String
        Private WsMercredi_ApresMidi As String
        Private WsJeudi_Matin As String
        Private WsJeudi_ApresMidi As String
        Private WsVendredi_Matin As String
        Private WsVendredi_ApresMidi As String
        Private WsSamedi_Matin As String
        Private WsSamedi_ApresMidi As String
        Private WsDimanche_Matin As String
        Private WsDimanche_ApresMidi As String
        Private WsHoraireConstate As String
        Private WsSignataire As String
        Private WsDateSignature As String
        '****** Traitement ************
        Private TsHeureDebut(6, 7) As String
        Private TsHeureFin(6, 7) As String
        Private TsActivite(6, 7) As String
        Private TsDuree(6, 7) As String
        Private TsSiReleveActivite As Boolean

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CET_HEBDO"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaHebdoCET
            End Get
        End Property

        Public Property Date_de_DebutSemaine() As String
            Get
                Return WsDateDebutSemaine
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDateDebutSemaine = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDateDebutSemaine = ""
                End Select
                MyBase.Date_de_Valeur = WsDateDebutSemaine
            End Set
        End Property

        Public Property Date_de_FinSemaine() As String
            Get
                Return WsDateFinSemaine
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDateFinSemaine = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDateFinSemaine = ""
                End Select
                MyBase.Date_de_Fin = WsDateFinSemaine
            End Set
        End Property

        Public Property Numero_de_Semaine() As Integer
            Get
                Return WsNumSemaine
            End Get
            Set(ByVal value As Integer)
                WsNumSemaine = value
                MyBase.Clef = WsNumSemaine
            End Set
        End Property

        Public Property HorairePrevisonnel() As String
            Get
                Return WsHorairePrevu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHorairePrevu = value
                    Case Else
                        WsHorairePrevu = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property HoraireComptabilise() As String
            Get
                Return WsHoraireComptabilise
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireComptabilise = value
                    Case Else
                        WsHoraireComptabilise = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property CET_HoraireTotal() As String
            Get
                Return WsCET_Total
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsCET_Total = value
                    Case Else
                        WsCET_Total = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property CET_MontantAcquis() As Double
            Get
                Return WsCET_MontantAcquis
            End Get
            Set(ByVal value As Double)
                WsCET_MontantAcquis = value
            End Set
        End Property

        Public Property CET_MontantPaye() As Double
            Get
                Return WsCET_MontantPaye
            End Get
            Set(ByVal value As Double)
                WsCET_MontantPaye = value
            End Set
        End Property

        Public Property CET_MontantNonPaye() As Double
            Get
                Return WsCET_MontantNonPaye
            End Get
            Set(ByVal value As Double)
                WsCET_MontantNonPaye = value
            End Set
        End Property

        Public Property CET_HeuresNonPayees() As String
            Get
                Return WsCET_HeuresNonPayees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsCET_HeuresNonPayees = value
                    Case Else
                        WsCET_HeuresNonPayees = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property CET_TauxHoraire() As Double
            Get
                Return WsCET_TauxHoraire
            End Get
            Set(ByVal value As Double)
                WsCET_TauxHoraire = value
            End Set
        End Property

        Public Property CET_HeuresAcquises() As String
            Get
                Return WsCET_HeuresAcquises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsCET_HeuresAcquises = value
                    Case Else
                        WsCET_HeuresAcquises = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property CET_HeuresPrises() As String
            Get
                Return WsCET_HeuresPrises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsCET_HeuresPrises = value
                    Case Else
                        WsCET_HeuresPrises = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property CET_HeuresGain() As String
            Get
                Return WsCET_HeuresGain
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsCET_HeuresGain = value
                    Case Else
                        WsCET_HeuresGain = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lundi_Matin() As String
            Get
                Return PlageVerifiee(0, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsLundi_Matin = value
                    Case Else
                        WsLundi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Lundi_ApresMidi() As String
            Get
                Return PlageVerifiee(0, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsLundi_ApresMidi = value
                    Case Else
                        WsLundi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mardi_ApresMidi() As String
            Get
                Return PlageVerifiee(1, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMardi_ApresMidi = value
                    Case Else
                        WsMardi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mardi_Matin() As String
            Get
                Return PlageVerifiee(1, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMardi_Matin = value
                    Case Else
                        WsMardi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mercredi_Matin() As String
            Get
                Return PlageVerifiee(2, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMercredi_Matin = value
                    Case Else
                        WsMercredi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mercredi_ApresMidi() As String
            Get
                Return PlageVerifiee(2, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMercredi_ApresMidi = value
                    Case Else
                        WsMercredi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Jeudi_Matin() As String
            Get
                Return PlageVerifiee(3, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsJeudi_Matin = value
                    Case Else
                        WsJeudi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Jeudi_ApresMidi() As String
            Get
                Return PlageVerifiee(3, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsJeudi_ApresMidi = value
                    Case Else
                        WsJeudi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Vendredi_Matin() As String
            Get
                Return PlageVerifiee(4, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsVendredi_Matin = value
                    Case Else
                        WsVendredi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Vendredi_ApresMidi() As String
            Get
                Return PlageVerifiee(4, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsVendredi_ApresMidi = value
                    Case Else
                        WsVendredi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Samedi_Matin() As String
            Get
                Return PlageVerifiee(5, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsSamedi_Matin = value
                    Case Else
                        WsSamedi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Samedi_ApresMidi() As String
            Get
                Return PlageVerifiee(5, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsSamedi_ApresMidi = value
                    Case Else
                        WsSamedi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Dimanche_Matin() As String
            Get
                Return PlageVerifiee(6, True)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDimanche_Matin = value
                    Case Else
                        WsDimanche_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Dimanche_ApresMidi() As String
            Get
                Return PlageVerifiee(6, False)
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDimanche_ApresMidi = value
                    Case Else
                        WsDimanche_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property HoraireConstate() As String
            Get
                Return WsHoraireConstate
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireConstate = value
                    Case Else
                        WsHoraireConstate = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Signataire() As String
            Get
                Return WsSignataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsSignataire = value
                    Case Else
                        WsSignataire = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Date_de_Signature() As String
            Get
                Return WsDateSignature
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDateSignature = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDateSignature = ""
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_de_DebutSemaine & VI.Tild)
                Chaine.Append(Date_de_FinSemaine & VI.Tild)
                Chaine.Append(Numero_de_Semaine.ToString & VI.Tild)
                Chaine.Append(HorairePrevisonnel & VI.Tild)
                Chaine.Append(HoraireComptabilise & VI.Tild)
                Chaine.Append(CET_HoraireTotal & VI.Tild)
                Chaine.Append(CET_MontantAcquis.ToString & VI.Tild)
                Chaine.Append(CET_MontantPaye.ToString & VI.Tild)
                Chaine.Append(CET_MontantNonPaye.ToString & VI.Tild)
                Chaine.Append(CET_HeuresNonPayees & VI.Tild)
                Chaine.Append(CET_TauxHoraire.ToString & VI.Tild)
                Chaine.Append(CET_HeuresAcquises & VI.Tild)
                Chaine.Append(CET_HeuresPrises & VI.Tild)
                Chaine.Append(CET_HeuresGain & VI.Tild)
                Chaine.Append(Lundi_Matin & VI.Tild)
                Chaine.Append(Lundi_ApresMidi & VI.Tild)
                Chaine.Append(Mardi_Matin & VI.Tild)
                Chaine.Append(Mardi_ApresMidi & VI.Tild)
                Chaine.Append(Mercredi_Matin & VI.Tild)
                Chaine.Append(Mercredi_ApresMidi & VI.Tild)
                Chaine.Append(Jeudi_Matin & VI.Tild)
                Chaine.Append(Jeudi_ApresMidi & VI.Tild)
                Chaine.Append(Vendredi_Matin & VI.Tild)
                Chaine.Append(Vendredi_ApresMidi & VI.Tild)
                Chaine.Append(Samedi_Matin & VI.Tild)
                Chaine.Append(Samedi_ApresMidi & VI.Tild)
                Chaine.Append(Dimanche_Matin & VI.Tild)
                Chaine.Append(Dimanche_ApresMidi & VI.Tild)
                Chaine.Append(HoraireConstate & VI.Tild)
                Chaine.Append(Signataire & VI.Tild)
                Chaine.Append(Date_de_Signature)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 32 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_de_DebutSemaine = TableauData(1)
                Date_de_FinSemaine = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                Numero_de_Semaine = CInt(TableauData(3))
                HorairePrevisonnel = TableauData(4)
                HoraireComptabilise = TableauData(5)
                CET_HoraireTotal = TableauData(6)

                If TableauData(7) <> "" Then CET_MontantAcquis = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then CET_MontantPaye = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then CET_MontantNonPaye = VirRhFonction.ConversionDouble(TableauData(9))
                CET_HeuresNonPayees = TableauData(10)
                If TableauData(11) <> "" Then CET_TauxHoraire = VirRhFonction.ConversionDouble(TableauData(11))
                CET_HeuresAcquises = TableauData(12)
                CET_HeuresPrises = TableauData(13)
                CET_HeuresGain = TableauData(14)
                Lundi_Matin = TableauData(15)
                Lundi_ApresMidi = TableauData(16)
                Mardi_Matin = TableauData(17)
                Mardi_ApresMidi = TableauData(18)
                Mercredi_Matin = TableauData(19)
                Mercredi_ApresMidi = TableauData(20)
                Jeudi_Matin = TableauData(21)
                Jeudi_ApresMidi = TableauData(22)
                Vendredi_Matin = TableauData(23)
                Vendredi_ApresMidi = TableauData(24)
                Samedi_Matin = TableauData(25)
                Samedi_ApresMidi = TableauData(26)
                Dimanche_Matin = TableauData(27)
                Dimanche_ApresMidi = TableauData(28)
                HoraireConstate = TableauData(29)
                Signataire = TableauData(30)
                Date_de_Signature = TableauData(31)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                Call InitialiserTraitement()
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Private Function CalculerSemaineConstatee() As String
            Dim TotalDuree As String = "0 h 00"
            Dim IndiceJ As Integer
            Dim IndiceP As Integer
            For IndiceJ = 0 To UBound(TsHeureDebut, 1)
                For IndiceP = 0 To UBound(TsHeureDebut, 2)
                    Select Case TsHeureDebut(IndiceJ, IndiceP)
                        Case Is <> ""
                            TsDuree(IndiceJ, IndiceP) = VirRhDate.DureeHeure(TsHeureDebut(IndiceJ, IndiceP), TsHeureFin(IndiceJ, IndiceP))
                            TotalDuree = VirRhDate.CalcHeure(TotalDuree, TsDuree(IndiceJ, IndiceP), True)
                        Case Else
                            TsDuree(IndiceJ, IndiceP) = ""
                    End Select
                Next IndiceP
            Next IndiceJ
            Return TotalDuree
        End Function

        Public ReadOnly Property DateJournee(ByVal NoJour As Integer) As String
            Get
                Dim DateW As String = ""
                Select Case NoJour
                    Case Is = 0
                        DateW = Date_de_DebutSemaine
                    Case Else
                        DateW = VirRhDate.CalcDateMoinsJour(Date_de_DebutSemaine, NoJour.ToString, "0")
                End Select
                Return DateW
            End Get
        End Property

        Public Property HeureDeDebutPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As String
            Get
                Select Case NoJour
                    Case 0 To UBound(TsHeureDebut, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureDebut, 2)
                                Return TsHeureDebut(NoJour, IndexPlage)
                        End Select
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case NoJour
                    Case 0 To UBound(TsHeureDebut, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureDebut, 2)
                                TsHeureDebut(NoJour, IndexPlage) = VirRhDate.VerifHeure(value, True)
                        End Select
                End Select
            End Set
        End Property

        Public Property HeureDeFinPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As String
            Get
                Select Case NoJour
                    Case 0 To UBound(TsHeureFin, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureFin, 2)
                                Return TsHeureFin(NoJour, IndexPlage)
                        End Select
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case NoJour
                    Case 0 To UBound(TsHeureFin, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureFin, 2)
                                TsHeureFin(NoJour, IndexPlage) = VirRhDate.VerifHeure(value, True)
                        End Select
                End Select
            End Set
        End Property

        Public Property DureedelaPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As String
            Get
                Select Case NoJour
                    Case 0 To UBound(TsDuree, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsDuree, 2)
                                Return TsDuree(NoJour, IndexPlage)
                        End Select
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case NoJour
                    Case 0 To UBound(TsDuree, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsDuree, 2)
                                TsDuree(NoJour, IndexPlage) = VirRhDate.VerifHeure(value, False)
                        End Select
                End Select
            End Set
        End Property

        Public Property ActivitedelaPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As String
            Get
                Select Case NoJour
                    Case 0 To UBound(TsActivite, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsActivite, 2)
                                Return TsActivite(NoJour, IndexPlage)
                        End Select
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case NoJour
                    Case 0 To UBound(TsActivite, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsActivite, 2)
                                TsActivite(NoJour, IndexPlage) = value
                        End Select
                End Select
            End Set
        End Property

        Public Property SiRelevedActivite() As Boolean
            Get
                Return TsSiReleveActivite
            End Get
            Set(ByVal value As Boolean)
                TsSiReleveActivite = value
            End Set
        End Property

        Public ReadOnly Property V_HeureDeDebutPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As Date
            Get
                Select Case NoJour
                    Case 0 To UBound(TsHeureDebut, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureDebut, 2)
                                Return VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(NoJour).ToShortDateString, TsHeureDebut(NoJour, IndexPlage))
                        End Select
                End Select
                Return VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(NoJour).ToShortDateString, "")
            End Get
        End Property

        Public ReadOnly Property V_HeureDeFinPlage(ByVal NoJour As Integer, ByVal IndexPlage As Integer) As Date
            Get
                Select Case NoJour
                    Case 0 To UBound(TsHeureFin, 1)
                        Select Case IndexPlage
                            Case 0 To UBound(TsHeureFin, 2)
                                Return VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(NoJour).ToShortDateString, TsHeureFin(NoJour, IndexPlage))
                        End Select
                End Select
                Return VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(NoJour).ToShortDateString, "")
            End Get
        End Property

        Private Function PlageVerifiee(ByVal NoJour As Integer, ByVal SiMatin As Boolean) As String
            Dim IOcc As Integer
            Dim Lg As Integer
            Dim ChaineMatin As New System.Text.StringBuilder
            Dim ChaineAM As New System.Text.StringBuilder
            Select Case SiRelevedActivite
                Case True
                    For IOcc = 0 To UBound(TsHeureDebut, 2)
                        Select Case TsActivite(NoJour, IOcc)
                            Case Is <> ""
                                Select Case TsDuree(NoJour, IOcc)
                                    Case Is <> ""
                                        Lg = 10 - TsDuree(NoJour, IOcc).Length
                                        Select Case IOcc
                                            Case 0 To 3
                                                ChaineMatin.Append(Strings.StrDup(Lg, "@") & TsDuree(NoJour, IOcc) & TsActivite(NoJour, IOcc) & VI.PointVirgule)
                                            Case Else
                                                ChaineAM.Append(Strings.StrDup(Lg, "@") & TsDuree(NoJour, IOcc) & TsActivite(NoJour, IOcc) & VI.PointVirgule)
                                        End Select
                                End Select
                        End Select
                    Next IOcc
                Case False
                    For IOcc = 0 To UBound(TsHeureDebut, 2)
                        Select Case TsHeureDebut(NoJour, IOcc)
                            Case Is <> ""
                                Select Case VirRhDate.ComparerHeures(TsHeureDebut(NoJour, IOcc), "12:00")
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                        Select Case VirRhDate.ComparerHeures(TsHeureFin(NoJour, IOcc), "15:00")
                                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                                ChaineMatin.Append(TsHeureDebut(NoJour, IOcc) & TsHeureFin(NoJour, IOcc) & TsActivite(NoJour, IOcc) & VI.PointVirgule)
                                            Case Else
                                                ChaineAM.Append(TsHeureDebut(NoJour, IOcc) & TsHeureFin(NoJour, IOcc) & TsActivite(NoJour, IOcc) & VI.PointVirgule)
                                        End Select
                                    Case Else
                                        ChaineAM.Append(TsHeureDebut(NoJour, IOcc) & TsHeureFin(NoJour, IOcc) & TsActivite(NoJour, IOcc) & VI.PointVirgule)
                                End Select
                        End Select
                    Next IOcc
            End Select
            Select Case SiMatin
                Case True
                    Return ChaineMatin.ToString
                Case Else
                    Return ChaineAM.ToString
            End Select
        End Function

        Private Sub InitialiserTraitement()
            Dim TableauData(0) As String
            Dim TableauOcc(0) As String
            Dim IndiceP As Integer
            Dim IOcc As Integer
            Dim ICar As Integer
            Dim IJour As Integer = 0
            Dim ChaineW As String

            TsHeureDebut.Initialize()
            TsHeureFin.Initialize()
            TsDuree.Initialize()
            TsActivite.Initialize()
            TableauData = Strings.Split(WsFicheLue.ToString, VI.Tild, -1)
            For IndiceP = 14 To 26 Step 2
                ChaineW = TableauData(IndiceP) & TableauData(IndiceP + 1)
                Select Case ChaineW
                    Case Is <> ""
                        TableauOcc = Strings.Split(ChaineW, VI.PointVirgule, -1)
                        For IOcc = 0 To TableauOcc.Count - 1
                            If TableauOcc(IOcc).Length >= 10 Then
                                Select Case Strings.Left(TableauOcc(IOcc), 1)
                                    Case Is = "@" 'Activité
                                        SiRelevedActivite = True
                                        TsActivite(IJour, IOcc) = Strings.Right(TableauOcc(IOcc), TableauOcc(IOcc).Length - 10)
                                        For ICar = 1 To 10
                                            If Strings.Mid(TableauOcc(IOcc), ICar, 1) <> "@" Then
                                                Exit For
                                            End If
                                        Next ICar
                                        TsDuree(IJour, IOcc) = Strings.Mid(TableauOcc(IOcc), ICar, TableauOcc(IOcc).Length - TsActivite(IJour, IOcc).Length - ICar + 1)
                                    Case Else
                                        SiRelevedActivite = False
                                        TsHeureDebut(IJour, IOcc) = Strings.Left(TableauOcc(IOcc), 5)
                                        TsHeureFin(IJour, IOcc) = Strings.Mid(TableauOcc(IOcc), 6, 5)
                                        TsDuree(IJour, IOcc) = VirRhDate.DureeHeure(TsHeureDebut(IJour, IOcc), TsHeureFin(IJour, IOcc))
                                        TsActivite(IJour, IOcc) = Strings.Right(TableauOcc(IOcc), TableauOcc(IOcc).Length - 10)
                                End Select
                            End If
                        Next IOcc
                End Select
                IJour += 1
            Next IndiceP
        End Sub

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<NumeroSemaine>" & Numero_de_Semaine & "</NumeroSemaine>" & vbCrLf)
                Chaine.Append("<DateDebutSemaine>" & Date_de_DebutSemaine & "</DateDebutSemaine>" & vbCrLf)
                Chaine.Append("<DateFinSemaine>" & Date_de_FinSemaine & "</DateFinSemaine>" & vbCrLf)
                Chaine.Append("<HoraireHebdoPrevisionnel>" & HorairePrevisonnel & "</HoraireHebdoPrevisionnel>" & vbCrLf)
                Chaine.Append("<HoraireHebdoConstate>" & HoraireConstate & "</HoraireHebdoConstate>" & vbCrLf)
                Chaine.Append("<LundiAM>" & Lundi_Matin & "</LundiAM>" & vbCrLf)
                Chaine.Append("<LundiPM>" & Lundi_ApresMidi & "</LundiPM>" & vbCrLf)
                Chaine.Append("<MardiAM>" & Mardi_Matin & "</MardiAM>" & vbCrLf)
                Chaine.Append("<MardiPM>" & Mardi_ApresMidi & "</MardiPM>" & vbCrLf)
                Chaine.Append("<MercrediAM>" & Mercredi_Matin & "</MercrediAM>" & vbCrLf)
                Chaine.Append("<MercrediPM>" & Mercredi_ApresMidi & "</MercrediPM>" & vbCrLf)
                Chaine.Append("<JeudiAM>" & Jeudi_Matin & "</JeudiAM>" & vbCrLf)
                Chaine.Append("<JeudiPM>" & Jeudi_ApresMidi & "</JeudiPM>" & vbCrLf)
                Chaine.Append("<VendrediAM>" & Vendredi_Matin & "</VendrediAM>" & vbCrLf)
                Chaine.Append("<VendrediPM>" & Vendredi_ApresMidi & "</VendrediPM>" & vbCrLf)
                Chaine.Append("<SamediAM>" & Samedi_Matin & "</SamediAM>" & vbCrLf)
                Chaine.Append("<SamediPM>" & Samedi_ApresMidi & "</SamediPM>" & vbCrLf)
                Chaine.Append("<DimancheAM>" & Dimanche_Matin & "</DimancheAM>" & vbCrLf)
                Chaine.Append("<DimanchePM>" & Dimanche_ApresMidi & "</DimanchePM>" & vbCrLf)
                Chaine.Append("<Signataire>" & Signataire & "</Signataire>" & vbCrLf)
                Chaine.Append("<DateSignature>" & Date_de_Signature & "</DateSignature>" & vbCrLf)

                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

